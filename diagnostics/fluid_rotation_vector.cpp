/** XSHELLS diagnostics:
*  mean rotation vector (based on angular momentum, See PhD thesis of J. Noir, page 60-61.)
*/

	if (evol_ubt & EVOL_U) {
		int ir0 = Ulm.ir_bci;	// entire fluid domain.
		int ir1 = Ulm.ir_bco;
		if (mp.var.find("L_rmin") != mp.var.end()) {	// check if L_rmin has been specified.
			double r0 = mp.var["L_rmin"];		// global integration boundary in radius
			ir0 = r_to_idx(r0);	// index of nearest actual shells
		}
		if (mp.var.find("L_rmax") != mp.var.end()) {	// check if L_rmax has been specified.
			double r1 = mp.var["L_rmax"];		//
			ir1 = r_to_idx(r1);	// index of nearest actual shells
		}
		double* diags = all_diags.append(3, "Wx, Wy, Wz\t ");		// append array for 3 diagnostics
		const double n = (pow(r[ir1],5)-pow(r[ir0],5))/5.;      // global normalization factor [r^5/5]
		if (Ulm.irs > ir0) ir0 = Ulm.irs;			// index of first local shell (mpi)
		if ((ir1 == 0) || (Ulm.ire < ir1)) ir1 = Ulm.ire;	// index of last local shell (mpi)

		double Wxyz[3];
		angular_momentum_local_sum(Ulm, ir0, ir1, Wxyz);
		diags[0] =  Wxyz[0] / (n*Y11_st);
		diags[1] = -Wxyz[1] / (n*Y11_st);
		diags[2] =  Wxyz[2] / (n*Y10_ct);
	}
