/** XSHELLS diagnostics:
*  dipolar magnetic field at the surface (3 components) + quadrupole data + fraction of dipole field
* f_dip     : rms of radial component of dipole field / rms of radial component of total field (all degrees l)  /!\ NON-STANDARD definition
* Bsurf10   : rms of the AXIAL dipole component at the fluid surface (l=1,m=0 at CMB)
* Bsurf1    : rms of the TOTAL dipole component at the fluid surface (l=1 at CMB)
* Bsurf1-12 : rms of degree 1 to 12 at the fluid surface (l=1 to l=12 at CMB)
*     /!\ Busrf1-12 was wrongly computed using all degrees before correction in july 2023 and Bsurf* was added.
* Bsurf*    : rms of total field at the fluid surface (all degrees l at CMB)
* 	Note that the above quantities (f_dip, Bsurf10, Bsurf1, Bsurf1-12 and Bsurf* are computed at the insulator boundary and then cast
*   back to fluid surface as if it were an insulator. This way it is the values that we could infer from surface measurements ignoring
*   a possible conducting layer
*/

	if (evol_ubt & EVOL_B) {
		double* diags = all_diags.append(10, "Bx, By, Bz, Bq0, Bqnz, f_dip, Bsurf10, Bsurf1, Bsurf1-12, Bsurf*\t ");		// append array for 10 diagnostics
		if (Blm.ire == Blm.ir_bco) {		// if this process has the last shell:
			const int ir = Blm.ir_bco;
			double rr_1 = 1.0;
			std::vector<double> r_pow_lp1(LMAX+1, 1.0);		// all values = 1.0
			if ((evol_ubt & EVOL_U) && Blm.ir_bco != Ulm.ir_bco) {		// magnetic field extends outside fluid domain
				rr_1 = r[ir]/r[Ulm.ir_bco];	// radius ratio of outer boundary to correct
				for (int l=0; l<=LMAX; l++) r_pow_lp1[l] = pow(rr_1, l+1);		// correction needed to compute at fluid surface.
			}
			if ((MMAX > 0) && (MRES == 1)) {
				double f = rr_1*rr_1/Y11_st;
				diags[0] = real(Blm.Pol[ir][LiM(shtns,1,1)]) * f;		// Equatorial dipole, x
				diags[1] = imag(Blm.Pol[ir][LiM(shtns,1,1)]) * f;		// Equatorial dipole, y
			}
			diags[2] = real(Blm.Pol[ir][LiM(shtns,1,0)])*rr_1*rr_1/Y10_ct;		// Axial dipole
			diags[3] = l2[2] * real(Blm.Pol[ir][LiM(shtns,2,0)])* rr_1*rr_1*rr_1;		// Axial quadrupole
			const double r_1 = 1.0/r[ir];
			double qnz = 0.0;
			const int m_max = (MMAX*MRES >= 2) ? 2 : MMAX*MRES;
			for (int im=1; im*MRES<=m_max; im++) {
				cplx Br = l2[2] * Blm.Pol[ir][LiM(shtns,2,im)];
				qnz += norm(Br);
			}
			diags[4] = sqrt(2*qnz) * rr_1*rr_1*rr_1;		// non-axial quadrupole (count twice for m>0)

			double Br2dip = 0.0;
			double Br2all = 0.0;
			double B2dip = 0.0;
			double B2dip0 = 0.0;
			double B2all = 0.0;
			double B2_12 = 0.0;
			double pre = 1.0;
			for (int im=0; im<=MMAX; im++) {
				for (int l=im*MRES; l<=LMAX; l++) {
					cplx P_r = Blm.Pol[ir][LiM(shtns,l,im)] * r_1 * r_pow_lp1[l];
					cplx Br = l2[l] * P_r;
					cplx S = P_r * (double)(-l);		// spheroidal at an insulating interface (does not work at a conducting one...)
					double Br2 = norm(Br);
					double B2 = Br2 + l2[l]*norm(S);		// total energy
					Br2all += pre*Br2;
					B2all += pre*B2;
					if (l==1) {
						Br2dip += pre*Br2;
						B2dip  += pre*B2;		// axial + equatorial dipole
						if (im==0)  B2dip0 += pre*B2;	// axial dipole
					}
					if (l<=12)	B2_12 += pre*B2;	// rms up to degree 12 (see Christensen & Aubert 2006, end of section 2)
				}
				pre = 2.0;		// count twice for m>0
			}
			diags[5] = sqrt( Br2dip / Br2all );		// f_dip based on Br only, recorded as f_dip. WARNING: this differs from definitions in other works.
			diags[6] = sqrt(B2dip0);
			diags[7] = sqrt(B2dip);
			diags[8] = sqrt(B2_12);
			diags[9] = sqrt(B2all);
		}
	}
