/** XSHELLS diagnostics:
*  record axial dipole component of magnetic field in the whole volume in a dedicated file (binary format).
*  In python, you can read this file with:
*       NR = np.fromfile("fieldU_0000.job",count=8,dtype=np.int32)[5] # get the number of radial shells NR from a field file.
*       r = np.fromfile("fieldU_0000.job",offset=1024,count=NR)   # the radii can also be obtained from a field file.
* 		data = np.fromfile("B10_profile.job").reshape((-1,NR+1))
*       t = data[:,0]    # the time of each profile
*       b10 = data[:,1:] # all the profiles of the axial dipole.
*/
	#ifdef XS_MPI
		static int* gather_n_ofs = 0;
	#endif
	if (evol_ubt & EVOL_B) {
		std::vector<double> g10(NR+1);
		for (int i=Blm.irs; i<=Blm.ire; i++) 	g10[1+i] = real(Blm.Pol[i][1]);		// g10(r)
		#ifdef XS_MPI
		if (gather_n_ofs == 0) {	// once for all, setup the parameters of MPI_Gatherv
			gather_n_ofs = (int*) malloc(2 * n_mpi * sizeof(int));
			for (int p=0; p<2*n_mpi; p++) gather_n_ofs[p] = 0;		// start with all zero
			for (int i=Blm.ir_bci; i<=Blm.ir_bco; i++) {
				int p = r_owner[i];
				if (gather_n_ofs[p] == 0) gather_n_ofs[p+n_mpi] = i;	// offset of first shell for this process
				gather_n_ofs[p]++;
			}
			if (gather_n_ofs[i_mpi] != Blm.ire-Blm.irs+1) runerr("MPI_Gatherv setup error");
		}
		// gather all values in i_mpi=0 process
		MPI_Gatherv((i_mpi==0) ? MPI_IN_PLACE : &g10[1+Blm.irs], Blm.ire-Blm.irs+1, MPI_DOUBLE,	
						&g10[1], gather_n_ofs, gather_n_ofs + n_mpi,	MPI_DOUBLE, 0, MPI_COMM_WORLD);
        #endif
        if (i_mpi == 0) {
			g10[0] = ftime;
			char fname[128];
			FILE* fp;
			sprintf(fname,"B10_profile.%s", jpar.job);		fp = fopen(fname,"ab");
			fwrite(&g10[0], sizeof(double), NR+1, fp);
			fclose(fp);
		}
	}
