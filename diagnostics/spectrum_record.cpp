/** XSHELLS diagnostics:
*  record the l and m energy spectrum of the different fields in a dedicated binary file.
*/

        std::string harmonics[] = {"l", "m"};
        std::string field_names[] = {"U", "B", "T", "C"};
        for (const std::string& field_name : field_names) {
            for (const std::string& harmonic : harmonics) {
                bool condition_harmonics_satisfied = false;
                int harmonic_max = 0;
                if (harmonic == "l") {
                    harmonic_max = LMAX; 
                } else if (harmonic == "m") {
                    harmonic_max = MMAX;
                }
                if (harmonic_max > 0) {
                    condition_harmonics_satisfied = true;
                }
                bool condition_field_satisfied = false;
                Spectral *Flm;
                if (field_name == "U" && (evol_ubt & EVOL_U)) {
                    condition_field_satisfied = true;
                    Flm = &Ulm;
                } else if (field_name == "B" && (evol_ubt & EVOL_B)) {                                                     
                    condition_field_satisfied = true;                               
                    Flm = &Blm;
                } else if (field_name == "T" && (evol_ubt & EVOL_T)) {                                              
                    condition_field_satisfied = true;                               
                    Flm = &Tlm;
                } else if (field_name == "C" && (evol_ubt & EVOL_C)) {                                                       
                    condition_field_satisfied = true;                               
                    Flm = &Clm;
                }                                                                   
                if (condition_harmonics_satisfied && condition_field_satisfied) {
                    char fname[128];
                    FILE* fp;
                    std::string harmonic_field = harmonic + "_" + field_name;
                    sprintf(fname, "energy_spectrum_%s.max%d.%s", harmonic_field.c_str(), harmonic_max, jpar.job);		
                    fp = fopen(fname,"ab");
                    double t = ftime; 
                    fwrite(&t, sizeof(double), 1, fp);
                    SpectralDiags sd(LMAX, MMAX);
                    Flm->energy(sd);
                    if (harmonic == "l") {
				        fwrite(sd.El, sizeof(double), harmonic_max+1, fp);
                    } else if (harmonic == "m") {
			            fwrite(sd.Em, sizeof(double), harmonic_max+1, fp);
                    }
                    fclose(fp);
		        }    
	        }
	    }
