/** XSHELLS diagnostics:
*  record m-spectrum
*/

	static std::vector<double> mspec(MMAX+1);	// backup spectrum
	if ((evol_ubt & EVOL_U) && (MMAX > 0)) {
		// generate labels
		static std::ostringstream labels;
		if (labels.str().length() == 0) {	// generate labels only once.
			for (int m=0; m<=MMAX; m++)  labels << "m=" << m << " ";
			labels << "\t";
		}
		// register diagnostics and labels
		double* diags = all_diags.append(MMAX+1, labels.str().c_str());		// append array for MMAX+1 diagnostics
		Ulm.spectrum(0, diags);					// fill m-spectrum to be summed accross shells and stored in energy file.
		if (own(NR/2)) {
			// process at mid-shell computes ans display (local) growth rate.
			double gmax = -1e192;
			int mgrow = -1;
			for (int m=0; m<=MMAX; m++) {
				double g = log(diags[m]/mspec[m])/(2.*dt_log);		// growth rate
				if (g > gmax) {
					gmax = g;		mgrow = m;
				}
				mspec[m] = diags[m];	// backup spectrum for next time step
			}
			printf(" maximum growth rate = %g  for m=%d\n", gmax, mgrow);
		}
	}
