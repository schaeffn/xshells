/** XSHELLS diagnostics:
*  compute and record convective power, also associated with composition if available.
*/

	if (((evol_ubt & (EVOL_T|EVOL_U)) == (EVOL_T|EVOL_U)) && (Grav0_r)) {
		const int nlm_align = ((NLM+3)/4)*4;
		cplx* Q = (cplx*) VMALLOC( ( (evol_ubt & EVOL_C) ? 3 : 2 ) *nlm_align*sizeof(cplx));
		cplx* T = Q + nlm_align;
		cplx* C = (evol_ubt & EVOL_C) ? Q + 2*nlm_align : 0;
		double Pconv = 0.0;
		double PconvC = 0.0;
		int i0 = Ulm.irs;
		if (r[i0] == 0.0) i0 += 1;	// skip r=0 that produces NaN
		for (int ir=i0; ir<=Ulm.ire; ir++) {
			for (int lm=0; lm<NLM; lm++) {
				Q[lm] = l2[lm]*Ulm.Pol[ir][lm] /r[ir];
				T[lm] = Tlm[ir][lm];
				if (C) C[lm] = Clm[ir][lm];
			}
			if (T0lm) {		// Add Background Temprature
				int nj = T0lm->nlm;
				unsigned* lma = T0lm->lm;
				cplx* TdT = T0lm->TdT + ir*nj*2;
				for (int j=0; j<nj; j++) {
					int lm = lma[j];
					T[lm] += TdT[j*2];
				}
			}
			if ((C) && (C0lm)) {		// Add Background Composition
				int nj = C0lm->nlm;
				unsigned* lma = C0lm->lm;
				cplx* CdC = C0lm->TdT + ir*nj*2;
				for (int j=0; j<nj; j++) {
					int lm = lma[j];
					C[lm] += CdC[j*2];
				}
			}
			double gr2dr = Grav0_r[ir]*r[ir] *r[ir]*r[ir]* Ulm.delta_r(ir); 
			Pconv += 2.0*coenergy(Q, T) * gr2dr;
			if (C) PconvC += 2.0*coenergy(Q, C) * gr2dr;
		}
		if (C) {
			double* diags = all_diags.append(2, "Pconv Pconv_C\t ");		// append array for 2 diagnostics
			diags[0] = Pconv;	diags[1] = PconvC;
		} else {
			double* diags = all_diags.append(1, "Pconv\t ");		// append array for 1 diagnostic
			diags[0] = Pconv;
		}
		VFREE(Q);
	}
