#!/usr/bin/env python

## Average along the z direction

import pyxshells
import numpy as np
import sys
import shtns
from timeit import default_timer as timer
import matplotlib.pyplot as plt

#flist = ["fieldU_%04d.bench" % i for i in range(1,10)]   # generate file list
flist = ["fieldU_0030.taw", ]   # explicit list
flist = ["fieldU_back.bench", ]   # explicit list

class Z_interpolator:
    def __init__(self, s,r, cos_theta):
        """ compute the intersections of the cylindrical grid (raddii s) with the spherical shells (radii r, cos_theta).
            s, r and cos_theta are 1-dimensional arrays defining the grids.
            cos_theta is the cosine of the colatitude and must go from north pole to south pole."""
        self.s = s
        self.r = r
        self.cos_theta = cos_theta

        nr, ns = len(r), len(s)
        z = np.zeros((ns, nr))
        dz = np.zeros((ns, nr))

        ir0 = np.zeros(ns, dtype='int')
        ofs = np.zeros(ns+1, dtype='int')      # array of offsets into the coefficient arrays
        it_all = np.zeros((ns+1)*(nr+1), dtype='int')
        alpha_all = np.zeros(2*(ns+1)*(nr+1))

        for i in range(ns):     # for each s
            si = s[i]
            
            ## generate vertical coordinate z and dz (integration weight):
            j0 = np.where(r >= si)[0][0]   # first shell with radius larger or equal the cylindric radius si
            ir0[i] = j0    # record the starting shell
            if j0 < nr-1:
                z[i, j0:nr] = np.sqrt( r[j0:nr]*r[j0:nr] - si*si )     # z of each shell at given cylindrical radius si
                norm = 0.5*0.5/(z[i, nr-1] - z[i, j0])   # normalization of integration (trapezoid rule)
                dz[i, j0] = (z[i, j0+1]- z[i, j0])*norm
                dz[i, j0+1:nr-1] = (z[i, j0+2:nr]-z[i, j0:nr-2])*norm
                dz[i, nr-1] = (z[i, nr-1] - z[i, nr-2])*norm
            else:
                dz[i, j0] = 0.5

            #latitudinal interpolation coefficients:
            it_list = np.zeros(nr-j0, dtype='int')
            alpha_list = np.zeros(2*(nr-j0))
            for j in range(j0,nr):   # sample a vertical line
                if si > 0:
                    cost = z[i, j] / r[j]
                    it = np.where( cos_theta < cost )[0][0]
                else:
                    cost, it = 1.0, 0
                
                it_list[j-j0] = it   # store it
                a0 = (cost - ct[it+1])/(ct[it]-ct[it+1])    # angular interpolation (1D)
                a1 = 1.0 - a0
                nit = sh.nlat-1 - it   # southern hemisphere index
                
                a0 *= dz[i, j]     # average north and south + integral weighting
                a1 *= dz[i, j]
                alpha_list[2*(j-j0)] = a0
                alpha_list[2*(j-j0)+1] = a1

            ofs0 = ofs[i]
            ofs[i+1] = ofs0 + nr-j0
            it_all[ofs0:ofs0+nr-j0] = it_list[:]
            alpha_all[2*ofs0:2*(ofs0+nr-j0)] = alpha_list[:]

        # truncate to actual length
        it_all = it_all[0:ofs[-1]]
        alpha_all = alpha_all[0:2*ofs[-1]]
        self.z = z
        self.dz = dz
        self.ir0 = ir0
        self.ofs = ofs
        self.i_theta = it_all
        self.alpha = alpha_all * 2  # the factor 2 is to average over north and south separately.

    def z_avg(self, v):
        """ average over z, separately for north and south hemisphere """
        ns, nr, nlat = len(self.s), len(self.r), len(self.cos_theta)
        nphi = v.shape[-1]  # last dimension
        v_north = np.zeros((ns,nphi))
        v_south = np.zeros((ns,nphi))
        print(nr,ns,nphi, v.shape)

        for i in range(ns):
            j0 = self.ir0[i]
            ofs0 = self.ofs[i]-j0
            for j in range(j0, nr):
                it = self.i_theta[ofs0+j]
                nit = nlat-1 - it
                a0, a1 = self.alpha[2*(ofs0+j)], self.alpha[2*(ofs0+j)+1]
                v_north[i,:] += v[j, it, :] * a0 + v[j, it+1,:] * a1
                v_south[i,:] += v[j, nit, :] * a0 + v[j, nit-1,:] * a1
        return v_north, v_south



### assumes all files have the same grid. So we use the first one to produce the z-averaged points
f = pyxshells.load_field(flist[0])

## construct the spherical grid
sh = f.sht
sh.set_grid(flags=shtns.sht_reg_poles)  # grid with poles
ct = sh.cos_theta       # cos(theta) at grid points
st = np.sqrt(1.-ct**2)  # sin(theta) at grid points
r = f.grid.r.copy()     # radii

## construct the cylindrical grid
s = r[f.irs : f.ire+1]   # use the same values for the cylindrical grid (or choose your favorite cylindrical grid here)
t_start = timer()
z_grid = Z_interpolator(s, r[f.irs : f.ire+1], ct)
t_stop = timer()
print("make_z_grids took %g seconds." % (t_stop-t_start))


## loop over the files:
for fname in flist:
    f=pyxshells.load_field(fname)
    f.sht.set_grid(flags=shtns.sht_reg_poles)  # grid with poles
    v = f.spat_full()    # convert to spatial data, shape of v is (NR, n_components, n_lat, n_phi)

        ## optionally compute the quantities you want

    t_start = timer()
    vp_north, vp_south = z_grid.z_avg(v[:,2,:,:])   # component 2 is v_phi
    t_stop = timer()
    print("z-averaging took %g seconds." % (t_stop-t_start))

        ## do something with the z-averaged quantities

plt.plot(s,vp_north[:,0])
plt.plot(s,vp_south[:,0])
plt.show()
