#!/usr/bin/python

## Computes and plot the specra of a field. ##

from pylab import *
from pyxshells import *
import sys

if len(sys.argv) != 2:
    print('  usage: %s <xshells-field-file>' % sys.argv[0])
    exit()

fname = sys.argv[1]

info, r = get_field_info(fname)

l = arange(0,info['lmax']+1)
m = arange(0,info['mmax']+1)*info['mres']
l2 = (l+1.0)*l
ir_list = [10,len(r)//2,len(r)-11]		# list of radius indices to plot

Flm = load_field(fname)

for ir in ir_list:
    el,em = Flm.r_spectrum(ir)
    figure(1)
    loglog(l, el, label='r=%g' % r[ir])
    figure(2)
    loglog(m+1, em, label='r=%g' % r[ir])

for fig, xtxt in zip((1,2), ('$\ell$','$m+1$')):
    figure(fig)
    xlabel(xtxt)
    grid(which='both')
    legend(loc='best')

show()
