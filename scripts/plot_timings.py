#!/usr/bin/python

from pylab import *
import sys

th_symb = ['o','^','v','d','s','<','>','*','x','+','.','p','1','2','3','4']
th_symb.reverse()
file_col = ['C0','C1','C2','C3','C4','C5','C6']

tmin_all,tmax_all = 1e100,0

if len(sys.argv) <= 3:		# only on file to plot
	fname = sys.argv[1]
	print(fname)
	y = loadtxt(fname)
	nmpi = y[:,0]
	nth = y[:,1]
	t = y[:,2]
	th_list = sort(unique(nth))
	tmin_all = amin(t)
	tmax_all = amax(t)
	for i in range(len(th_list)):
		sel = (nth==th_list[i])
		loglog(nmpi[sel]*nth[sel], t[sel], marker=th_symb[i], label="%d threads" % th_list[i])
	legend(loc='best')
	if len(sys.argv) == 3:
		title(sys.argv[2])

else:	# more than one file

	legend_elements = []
	symb = {}
	for fidx in range((len(sys.argv)-1)//2):
		fname = sys.argv[2*fidx+1]
		title = sys.argv[2*fidx+2]
		print(fname)
		y = loadtxt(fname)
		nmpi = y[:,0]
		nth = y[:,1]
		t = y[:,2]
		#if "amd" in fname: t /= 1.5
		#if "knl" in fname: t /= 1.4
		cores = nmpi*nth
		th_list = sort(unique(nth))
		#th_list = th_list[th_list <= 24]    # keep only up to 24 threads.
		core_count = sort(unique(cores))
		# generate thread -> symbol hash
		for i in range(len(th_list)):
			if th_list[i] not in symb.keys():
				print(th_list[i], th_symb)
				symb[th_list[i]] = th_symb.pop()
		# find best case for each core count
		for c in core_count:
			tmin = amin(t[cores==c])
			if tmin < tmin_all:  tmin_all = tmin
			if tmin > tmax_all:  tmax_all = tmin
			th = nth[t == tmin][0]
			if c == core_count[0]:
				loglog(core_count, tmin*c/core_count, 'k--', lw=0.5)
			if th in symb.keys():
				loglog(c, tmin, marker=symb[th], color=file_col[fidx])
		legend_elements.append(Line2D([0],[0], color=file_col[fidx], label=title))
	for k in symb.keys():
		legend_elements.append(Line2D([0],[0], color='k', lw=0, marker=symb[k], label='%g threads' % k))
	legend(handles=legend_elements,loc='best', ncol=2)

ylim(0.8*tmin_all,1.2*tmax_all)
grid()
xlabel('Core count')
ylabel('Wall time (s)')
show()

