#ifndef _GNUPLOT_PIPES_H_
#define _GNUPLOT_PIPES_H_

#include <iostream>
#include <string>
#include <fstream>
#include <cstdio>
#include <cstdlib>	// for getenv()
#include <unistd.h>	// for isatty()
#include <signal.h>	// for sigaction()

class Gnuplot
{
	///\brief pointer to the stream that can be used to write to the pipe
	FILE *gnucmd = 0;
	int output = 0;			///< 0: no output;  1: screen;  2: png file;  3: both;
	int last_output = 0;	///< 0: none;  1: screen;  2: file
	bool term = false;		///< indicate display is a terminal
	std::string plotcmd;
	std::string plot_to_screen;
	std::string plot_to_file;
	std::string xlabel_;
	std::string title_;

	Gnuplot(FILE* fp, const std::string &filename, int out);	// private constructor, so that we can ensure fp!=0 before constructing object

  public:
	~Gnuplot();
	friend Gnuplot* open_gnuplot(const std::string &filename, int out);		// only way to create object, ensuring gnucmd is valid

	/// send a command to gnuplot
	void cmd(const std::string &cmdstr) {
		if (gnucmd) {
			fputs( (cmdstr+"\n").c_str(), gnucmd );
			fflush(gnucmd);
		}
	}

	void set_plotcmd(const std::string &cmdstr) {
		plotcmd = cmdstr + "\n";
	}
	void set_title(const std::string &title) {
		title_ = "set title '" + title + "'\n";
		plot_to_file += title_;
		if (!term) plot_to_screen += title_;
	}
	void set_xlabel(const std::string &xlabel) {
		xlabel_ = "set xlabel '" + xlabel + "'\n";
		plot_to_file += xlabel_;
		if (!term) plot_to_screen += xlabel_;
	}
	void plot(); 	// output plot
	void save();	// write plot to file

	/// resets a gnuplot session (next plot will erase previous ones)
	void reset_plot();

	/// resets a gnuplot session and sets all variables to default
	void reset_all();
};

void Gnuplot::plot()
{
	if (!gnucmd) return;

	if (output & 1) {	// display
		if (last_output != 1) {
			fputs(plot_to_screen.c_str(), gnucmd);
			fputs(plotcmd.c_str(), gnucmd);
			last_output = 1;
		} else {
			fputs("replot\n", gnucmd);
		}
	}
	if (output & 2) {	// write to file
		fputs(plot_to_file.c_str(), gnucmd);
		fputs(plotcmd.c_str(), gnucmd);
		last_output = 2;
	}
	int err = fflush(gnucmd);
	if (err) {
		pclose(gnucmd);
		gnucmd = 0;		// disable future output in case of failure.
		puts("warning: gnuplot failure.\n");
	}
}

/// Save to png if it has not been saved yet.
void Gnuplot::save()
{
	if (gnucmd && (output & 2) == 0) {	// not already saved
		fputs(plot_to_file.c_str(), gnucmd);
		fputs(plotcmd.c_str(), gnucmd);
		fflush(gnucmd);
		last_output = 2;
	}
}

/// resets a gnuplot session and sets all varibles to default
void Gnuplot::reset_all()
{
	cmd("reset; clear");
}

/// Opens up a gnuplot session, ready to receive commands
Gnuplot::Gnuplot(FILE* fp, const std::string &filename, int out) : gnucmd(fp)
{
	// char * getenv ( const char * name );  get value of environment variable
	// Retrieves a C string containing the value of the environment variable 
	// whose name is specified as argument.  If the requested variable is not 
	// part of the environment list, the function returns a NULL pointer.

	if (out&1) {		// screen display requested
		char* disp = getenv("DISPLAY");
		if ((disp)&&(strlen(disp) > 0)) {	// we prabably have a correct display.
			output |= 1;	// enable screen display
			plot_to_screen = "set output; set terminal x11 noraise persist\n" + title_ + xlabel_;		// terminal set to X11
		} else if (isatty(fileno(stdout))) {
			term = true;
			output |= 1;	// enable screen display
			plot_to_screen = "set output; set term dumb size 120 25 ansi; set colorsequence classic; set tics nomirror scale 0.3; unset title; unset xlabel\n";
			output |= 2;	// as the screen display is "degraded", we also store to file.
		} else output |= 2;	// no display found => write to png instead.
	}
	if (out&2)	output |= 2;		// png output requested.

	// store output filename
	plot_to_file = "set terminal png; set output '" + filename + "'\n" + title_ + xlabel_;
}

Gnuplot::~Gnuplot()
{
	// A stream opened by popen() should be closed by pclose()
	if (gnucmd) {
		fputs("quit\n", gnucmd);
		pclose(gnucmd);
	}
}

/// factory to create a Gnuplot object, ensuring the pipe is open and valid!
Gnuplot* open_gnuplot(const std::string &filename, int out=1)
{
	if (out == -1) out = isatty(fileno(stdout));		// 1 for console output, 0 for redirection to file.
	if (out == 0) return 0;	// no output requested: nothing to do.

	FILE* gnucmd = popen("gnuplot","w");
	if (gnucmd)
	{
		// popen only fails if shell is not found. We must ensure xshells will not crash in case gnuplot is not found.
		signal(SIGPIPE, SIG_IGN);		// ignore "pipe error" signal, to avoid crash when gnuplot is not found.

		return new Gnuplot(gnucmd, filename, out);
	}

	fprintf(stderr, "Can't open connection to gnuplot");
	return 0;	// fail
}

#endif
