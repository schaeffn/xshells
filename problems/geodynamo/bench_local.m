% extract local measurement for geodynamo benchmark by interpolation.
%
% expect Vr, Vp, T, Bz equatorial fields + r,phi as coordinates.
% (loaded with load_disc.m)

% first, find r at mid-shell

r0 = (r(1)+r(end))/2;
ii = find(r>r0);
ir = ii(1)-1;		% betwen r(ir) and r(ir+1)

% radial interpolation:
alpha = (r0-r(ir))/(r(ir+1)-r(ir));	% relative distance from r(ir)

Vrr = Vr(:,ir)*(1-alpha) + Vr(:,ir+1)*alpha;
Vpr = Vp(:,ir)*(1-alpha) + Vp(:,ir+1)*alpha;
Bzr = Bz(:,ir)*(1-alpha) + Bz(:,ir+1)*alpha;
Tr = T(:,ir)*(1-alpha) + T(:,ir+1)*alpha;

% find where Vr is zero and dVr/dphi > 0
ii = find( (Vrr(2:end)>=0) & (Vrr(1:end-1)<0) );

% angular interpolation:
for k = 1:length(ii)
	iphi = ii(k)
	x = Vrr(iphi)/(Vrr(iphi)-Vrr(iphi+1))	% relative distance from iphi.
	U_phi(k)   = Vpr(iphi)*(1-x) + Vpr(iphi+1)*x;
	B_theta(k) = -(Bzr(iphi)*(1-x) + Bzr(iphi+1)*x)*sqrt(5e-3);
	Temp(k)    = Tr(iphi)*(1-x) + Tr(iphi+1)*x;
end

U_phi
B_theta
Temp

a=[mean(U_phi) mean(B_theta) mean(Temp)]

((a./[-7.625 -4.9289 0.37338])-1)*100
