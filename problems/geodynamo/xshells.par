##########################################################################
# XSHELLS : eXtendable Spherical Harmonic Earth-Like Liquid Simulator    #
#  > this is the input parameter file                                    #
# syntax : name = value                                                  #
##########################################################################

### parameters to reproduce the GEODYNAMO BENCHMARK (case 0 and 1)
### see "A numerical dynamo benchmark", Christensen et al.
### Phys. Earth Planet. Int. (2001), doi:10.1016/S0031-9201(01)00275-8
### comment out the line "b = ..." to run case 0.
job = bench

### USER DEFINED VARIABLES ###
E = 1e-3
Pm = 5
Ra = 100     # Ra for onset of convection is around 55.87 (m=4)

### PHYSICAL PARAMS ###
Omega0 = 1/E 	# Global rotation rate
nu = 1.0		# viscosity
kappa = 1.0		# thermal diffusivity
eta = 1/Pm		# magnetic diffusivity

#hyper_nu = 30
#hyper_kappa = 30
#hyper_diff_l0 = 0.8

### INITIAL FIELDS ###
## A file name containing a field may be given (load file), or 
## a predefined field name (run "./list_fields" for a complete list)
## or "random", "rands", "randa" and "0" for respectively :
## load file, xshells_init.cpp defined velocity and magnetic fields,
## random field, random symmetric, random anti-symmetric fields and zero field.
## Fields may be scaled by adding (e.g.) *1e-3 after the name.
## comment out the lines if the field does not exist to avoid time-stepping that field.

u = 0				# initial velocity field
b = bench2001*5/sqrt(Pm*E)	# initial magnetic field (scaled by sqrt(1/(Pm*E)))
tp = bench2001*0.1		# initial temperature field
tp0 = delta*-1			# imposed (base) temperature field
phi0 = radial*Ra/E		# radial gravity field (you should multiply it by Ra/E with geodynamo benchmark definition)

### BOUNDARY CONDITIONS AND RADIAL DOMAINS ###
#r = rayon.dat		# file containing the radial grid to use (either a field file, or an ascii file).
BC_U = 1,1			# inner,outer boundary conditions (0=zero velocity, 1=no-slip, 2=free-slip)
BC_T = 1,1			# 1=fixed temperature, 2=fixed flux.
R_U = 7/13 : 20/13	# Velocity field boundaries
R_T = 7/13 : 20/13	# Temperature field boundaries
R_B = 7/13 : 20/13	# Magnetic field boundaries
kill_sbr = 1		# (for free-slip only) 1: angular momentum kept at 0 (kill solid body rotation). 0: free solid body roration allowed. 2: conserve angular momentum to its inital value.

### NUMERICAL SCHEME ###
NR = 96		# total number of radial shells (overriden by r = ...)
N_BL = 10,10	# number of radial shells reserved for inner and outer boundaries.
dt_adjust = 1	# 0: fixed time-step (default), 1: variable time-step
dt = 0.0001		# time step
iter_max = 1000	# iteration number (total number of text and energy file ouputs)
sub_iter = 100	# sub-iterations (the time between outputs = 2*dt*sub_iter is fixed even with variable time-step)
iter_save = 10  # number of iterations between field writes (if movie > 0, see below).

### SHT ###
Lmax = 47	# max degree of spherical harmonics
Mmax = 11	# max fourier mode (phi)
Mres = 4	# phi-periodicity.
#Nlat = 32	# number of latitudinal points (theta). Optimal chosen if not specified.
#Nphi = 16	# number of longitudinal points (phi). Optimal chosen if not specified.

### OPTIONS ###
#interp = 1		# 1: allow interpolation of fields in case of grid mismatch. 0: fail if grids mismatch (default).
#restart = 1	# 1: try to restart from a previous run with same name. 0: no auto-restart (default).
#movie = 1		# 0=field output at the end only (default), 1=output every iter_save, 2=also writes time-averaged fields
#lmax_out = -1		# lmax for movie output (-1 = same as Lmax, which is also the default)
#mmax_out = -1		# mmax for movie output (-1 = same as Mmax, which is also the default)
#prec_out = 1		# 1: single precision movie output (default), 2: double precision movie outputs.
#zavg = 2			# 1=output z-averaged field, 2=also output rms z-averaged.
backup_time = 240		# ensures that full fields are saved to disk at least every backup_time minutes, for restart.
#nbackup = 0			# number of backups before terminating program (useful for time-limited jobs). 0 = no limit (default)

m_drift = 4 	# measure the drift rate of the m=4 mode (for geodynamo benchmark)

### ALGORITHM FINE TUNING ###

## C_vort and C_alfv control the time-step adjustment (active if dt_adjust=1),
## regarding vorticity and Alfven criteria.
## if dt_tol_lo < dt/dt_target < dt_tol_hi, no adjustement is done.
#C_u = 0.7
#C_vort = 0.2		# default: 0.2
#C_alfv = 1.0		# default: 1.0
#dt_tol_lo = 0.8		# default: 0.8
#dt_tol_hi = 1.1		# default: 1.1

## fine-tuning of PC2 time-scheme:
#pc2_ax = 0.4   # Most stable = 0.4. Most accurate = 0.5 (default). Should be >= 0.4
#pc2_ai = 0.9   # Most stable = 0.99. Most accurate = 0.5 (default). Should be >= 0.5

## sht_type : 0 = gauss-legendre (default), 1 = fastest with DCT enabled
##            2 = fastest on regular grid, 3 = full DCT, 4 = debug (quick_init), 6 = on-the-fly (good for parallel)
##   4 has the smallest init-time, useful for test/debug. Otherwise 0 or 6 should be used.
sht_type = 0

## sht_polar_opt_max = SHT polar optimization threshold : polar coefficients below that threshold are neglected (for high ms).
##    value under wich the polar values of the Legendre Polynomials Plm are neglected, leading to increased performance.
##    0 = no polar optimization;  1.e-14 = VERY safe (default);  1.e-10 = safe;  1.e-6 = aggresive.
sht_polar_opt_max = 1.0e-10

#TEST time=1; ref={'Eu':856.009,'w_drift':0.159369}; opt='-iter_max=50 -b= -Lmax=31 -Mmax=7'
#TEST time=0.2; ref={'Eu':135.372872,'Eb':10306.64813,'w_drift':-2.90756,'f_dip':0.9283446}; opt='-sht_type=4 -iter_max=10 -movie=2 -iter_save=5'; files={'fieldU.bench':135.372872, 'fieldUavg_0001.bench':119.1992, 'fieldBavg_0001.bench':13973.7, 'fieldUavg_0002.bench':74.4031, 'fieldBavg_0002.bench':11090.6}
