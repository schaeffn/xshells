#ifndef XS_H
	
	#define XS_H
#define XS_CUSTOM_DIAGNOSTICS
#define XS_ADJUST_DT
#else
#ifdef XS_CUSTOM_DIAGNOSTICS
void custom_diagnostic(diagnostics& all_diags)
{
    #include "diagnostics/dissipation.cpp"
    #include "diagnostics/fluid_rotation_vector.cpp"
}
#endif
#endif
