##########################################################################
# XSHELLS : eXtendable Spherical Harmonic Earth-Like Liquid Simulator    #
#  > this is the input parameter file                                    #
# syntax : name = value                                                  #
##########################################################################

### Solid-body rotation in a stress-free shell.
### Test to ensure the viscous dissipation is correctly reported nearly zero.
job = sf_sbr ### USER DEFINED VARIABLES ###
E = 1e-3 	# Ekman number

### PHYSICAL PARAMS ###
Omega0 = 1 	# Global rotation rate
nu = E		# viscosity

### INITIAL FIELDS ###
u = sb_rot * 1

### BOUNDARY CONDITIONS AND RADIAL DOMAINS ###
BC_U = 2,2		# inner,outer boundary conditions (0=zero velocity, 1=no-slip, 2=free-slip)
R_U = 0.35 : 1	# Velocity field boundaries
kill_sbr = 2		# (for free-slip only) 1: angular momentum kept at 0 (kill solid body rotation). 0: free solid body roration allowed. 2: conserve angular momentum to its inital value.

### NUMERICAL SCHEME ###
NR = 64 	# total number of radial shells (overriden by r = ...)
N_BL = 8,8	# number of radial shells reserved for inner and outer boundaries.
dt_adjust = 1	# 0: fixed time-step (default), 1: variable time-step
dt = 1e-4 	# time step
iter_max = 100  # iteration number (total number of text and energy file ouputs)
sub_iter = 10	  # sub-iterations (the time between outputs = 2*dt*sub_iter is fixed even with variable time-step)
iter_save = 100  # number of iterations between field writes (if movie > 0, see below).

### SHT ###
Lmax = 31	# max degree of spherical harmonics
Mmax = 31	# max fourier mode (phi)
Mres = 1	# phi-periodicity.
#Nlat = 32	# number of latitudinal points (theta). Optimal chosen if not specified.
#Nphi = 16	# number of longitudinal points (phi). Optimal chosen if not specified.

### OPTIONS ###
#interp = 1		# 1: allow interpolation of fields in case of grid mismatch. 0: fail if grids mismatch (default).
#restart = 1	# 1: try to restart from a previous run with same name. 0: no auto-restart (default).
#movie = 2		# 0=field output at the end only (default), 1=output every iter_save, 2=also writes time-averaged fields
#lmax_out = -1		# lmax for movie output (-1 = same as Lmax, which is also the default)
#mmax_out = -1		# mmax for movie output (-1 = same as Mmax, which is also the default)
#prec_out = 1		# 1: single precision movie output (default), 2: double precision movie outputs.
#zavg = 2			# 1=output z-averaged field, 2=also output rms z-averaged.
#backup_time = 240		# ensures that full fields are saved to disk at least every backup_time minutes, for restart.
#nbackup = 0			# number of backups before terminating program (useful for time-limited jobs). 0 = no limit (default)

### ALGORITHM FINE TUNING ###
stepper = SBDF3
## C_vort and C_alfv control the time-step adjustment (active if dt_adjust=1),
## regarding vorticity and Alfven criteria.
## if dt_tol_lo < dt/dt_target < dt_tol_hi, no adjustement is done.
#C_u = 0.7
#C_vort = 0.2		# default: 0.2
#C_alfv = 1.0		# default: 1.0
#dt_tol_lo = 0.8		# default: 0.8
#dt_tol_hi = 1.1		# default: 1.1

## sht_type : 0 = gauss-legendre (default), 1 = fastest with DCT enabled
##            2 = fastest on regular grid, 3 = full DCT, 4 = debug (quick_init), 6 = on-the-fly (good for parallel)
##   4 has the smallest init-time, useful for test/debug. Otherwise 0 or 6 should be used.
sht_type = 4

## sht_polar_opt_max = SHT polar optimization threshold : polar coefficients below that threshold are neglected (for high ms).
##    value under wich the polar values of the Legendre Polynomials Plm are neglected, leading to increased performance.
##    0 = no polar optimization;  1.e-14 = VERY safe (default);  1.e-10 = safe;  1.e-6 = aggresive.
sht_polar_opt_max = 1.0e-10

#TEST time=0.2; ref={'D_nu':0, 'Eu':0.833698, 'Wx':0, 'Wy':0, 'Wz':1.000407749}; tol=3e-6; opt=''
#TEST time=0.2; ref={'D_nu':0, 'Eu':0.833698, 'Wx':1.000407749, 'Wy':0, 'Wz':0}; tol=3e-6; opt='-u=spinover -sconv_lmin=1000'
