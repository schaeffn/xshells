##########################################################################
# XSHELLS : eXtendable Spherical Harmonic Earth-Like Liquid Simulator    #
#  > this is the input parameter file                                    #
# syntax : name = value                                                  #
##########################################################################

job = taw			# job name, used as a suffix for output files

Pm = 0.04
Va = 1e-3

### PHYSICAL PARAMETERS ###
Omega0 = 1.0 	# Global rotation rate
nu = 1e-8	# viscosity
eta = nu/Pm	# magnetic diffusivity in the core
etam = 3.125e-5	# magnetic diffusivity in the mantle

### INITIAL FIELDS ###
## A file name containing a field may be given (load file), or 
## a predefined field name (run "./list_fields" for a complete list)
## or "random", "rands", "randa" and "0" for respectively :
## load file, xshells_init.cpp defined velocity and magnetic fields,
## random field, random symmetric, random anti-symmetric fields and zero field.
## Fields may be scaled by adding (e.g.) *1e-3 after the name.
## comment out the lines if the field does not exist to avoid time-stepping that field.

u = geojet(0.0004)		# initial velocity field (same as -u command line option)
b = geojet(0.0004)		# initial magnetic field (same as -b command line option)
b0 = curfree(2)*Va/2

### BOUNDARY CONDITIONS AND RADIAL DOMAINS ###
#r = rayon.dat		# file containing the radial grid to use (either a field file, or an ascii file; same as -r command line option).
BC_U = 0,0			# inner,outer boundary conditions (0=zero velocity, 1=no-slip, 2=free-slip)
R_U = 0.35:1		# Velocity field boundaries
R_B = 0.35:1.025		# Magnetic field boundaries
#kill_sbr = 1		# for free-slip only. 1: angular momentum kept at 0 (kill solid body rotation). 0: free solid body roration allowed (don't conserve angular momentum). 2: conserve angular momentum to its inital value (default).

a_forcing = 0		# forcing amplitude (e.g. differential rotation of the inner core)
w_forcing = 0		# forcing pulsation (w = 2.pi.f) (0 = permanent forcing, <0 = sine, >0 = impulsive forcing ==> see xshells.hpp)

### NUMERICAL SCHEME ###
NR = 600		# total number of radial shells (overriden by r = ...)
N_BL = 100,150	# number of radial shells reserved for inner and outer boundaries.
dt_adjust = 1	# 0: fixed time-step (default), 1: variable time-step
dt = 0.0025		# time step
iter_max = 1200	  	# iteration number (total number of text and energy file ouputs)
sub_iter = 100	  	# sub-iterations (the time between outputs = 2*dt*sub_iter is fixed even with variable time-step)
iter_save = 10    	# number of iterations between field writes (if movie > 0, see below).

### SHT ###
Lmax = 250	# max degree of spherical harmonics
Mmax = 0	# max Fourier mode (phi)
Mres = 1	# phi-periodicity.
Nlat = 256	# number of latitudinal points (theta). Optimal chosen if not specified.
Nphi = 1	# number of longitudinal points (phi). Optimal chosen if not specified.

### OPTIONS ###
#interp = 1		# 1: allow interpolation of fields in case of grid mismatch. 0: fail if grids mismatch (default).
#restart = 1	# 1: try to restart from a previous run with same name. 0: no auto-restart (default).
movie = 1		# 0=field output at the end only (default), 1=output every iter_save, 2=also writes time-averaged fields
#lmax_out = -1		# lmax for movie output (-1 = same as Lmax, which is also the default)
#mmax_out = -1		# mmax for movie output (-1 = same as Mmax, which is also the default)
#lmax_out_surf = 84		# output surface (secular variation) data up to lmax_out_surf if lmax_out_surf > 0
backup_time = 240		# ensures that full fields are saved to disk at least every backup_time minutes, for restart.
#nbackup = 0			# number of backups before terminating program (useful for time-limited jobs). 0 = no limit (default)

### ALGORITHM FINE TUNING ###

## C_vort and C_alfv control the time-step adjustment (active if dt_adjust=1),
## regarding vorticity and Alfven criteria.
## if dt_tol_lo < dt/dt_target < dt_tol_hi, no adjustement is done.
#C_vort = 0.2		# default: 0.2
#C_alfv = 1.0		# default: 1.0
#dt_tol_lo = 0.8		# default: 0.8
#dt_tol_hi = 1.1		# default: 1.1

## sht_type : 0 = gauss-legendre (default), 1 = fastest with DCT enabled
##            2 = fastest on regular grid, 3 = full DCT, 4 = debug (quick_init), 6 = on-the-fly (good for parallel)
##   4 has the smallest init-time, useful for test/debug. Otherwise 0 or 6 should be used.
sht_type = 4

## sht_polar_opt_max = SHT polar optimization threshold : polar coefficients below that threshold are neglected (for high ms).
##    value under wich the polar values of the Legendre Polynomials Plm are neglected, leading to increased performance.
##    0 = no polar optimization;  1.e-14 = VERY safe (default);  1.e-10 = safe;  1.e-6 = aggresive.
sht_polar_opt_max = 1.0e-10
