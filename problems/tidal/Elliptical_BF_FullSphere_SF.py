#!/usr/bin/python3

#########################################################################
# Generates xshells.par and base flows for Tidally deformed flows in spheres
# see: Vidal, Cebron, Schaeffer, Hollerbach (2018)
# https://hal.archives-ouvertes.fr/hal-01644999
#########################################################################

#-----------------------------------------------------------------------
# Needed modules
#-----------------------------------------------------------------------
import numpy as np
import sympy as sp
from sympy.interactive import printing
printing.init_printing(use_latex='mathjax')

import pyxshells as px
import shtns
import os

print("""\n###############################################
\tELLIPTIC BASE STATE FOR XSHELLS
###############################################
""")

#-----------------------------------------------------------------------
# User-defined parameters
#-----------------------------------------------------------------------
eps_num = np.array([0.2])
N0_num = np.array([0.2])

rmn = 0.5
nbr = 448		# nb of radial points before the interpolation

#-----------------------------------------------------------------------
# Other xshells.par inputs
#-----------------------------------------------------------------------
Ek = 1e-4 
Pr = 1
Pm = 2
NR = 224
bct = 1			# 1: fixed temperature, 2: fixed flux
Lmax, Mmax = 127, 100
dt = 0.01
iter_max = 40000
sub_iter = 25
iter_save = 1000
nonlin = 'yes'
if nonlin == 'yes':
	flagnl = 2	# 1 for hydro, 2 for kinematic dynamo, 3 for self-consistent dynamo

#-----------------------------------------------------------------------
# Vector calculus
#-----------------------------------------------------------------------
# Cartesian and Spherical
x,y,z = sp.symbols('x,y,z', real=True)
r, theta, phi = sp.symbols('r, theta, phi',positive=True)

# Cylindrical
s = sp.symbols('s',positive=True)
z = sp.symbols('z',real=True)

def grad(f, system):
	grd = sp.zeros(3,1)
	if system == 'spherical':
		grd[0] = sp.diff(f,r)
		grd[1] = 1/r * sp.diff(f,theta)
		grd[2] = 1/(r*sp.sin(theta)) * sp.diff(f,phi)
	elif system == 'cartesian':
		grd[0] = sp.diff(f,x)
		grd[1] = sp.diff(f,y)
		grd[2] = sp.diff(f,z) 
	return grd

def curl(u, system):
	cu = sp.zeros(3,1)
	if system == 'spherical':
		cu[0] = 1/(r*sp.sin(theta)) * (sp.diff(sp.sin(theta)*u[2],theta) - sp.diff(u[1],phi))
		cu[1] = 1/(r*sp.sin(theta)) * sp.diff(u[0],phi) - 1/r * sp.diff(r*u[2],r)
		cu[2] = 1/r * sp.diff(r*u[1],r) - 1/r * sp.diff(u[0],theta)
	elif system == 'cartesian':
		cu[0] = sp.diff(u[2],y) - sp.diff(u[1],z)
		cu[1] = sp.diff(u[0],z) - sp.diff(u[2],x)
		cu[2] = sp.diff(u[1],x) - sp.diff(u[0],y)
	return cu

def taylor_expeven(Nini,Nend,var1,var2=0):
	F = 0
	coeff = []
	count = 0
	for n in range(Nini,Nend):
		if var2 != 0:
			coeff.append(sp.Function('a' + str(2*n))(var2))
		else:
			coeff.append(sp.symbols('a' + str(2*n), real=True))
		F = F + coeff[count] * var1**n
		count += 1
	return F,coeff

#-----------------------------------------------------------------------
# Base state
#-----------------------------------------------------------------------
print("\no Symbolic base state")
print("\t* Symbolic stream function")
print("\t\tx Init")

# Symbolic variables
beta = sp.Function('beta')
epsilon = sp.symbols('epsilon', positive=True)
ri = sp.symbols('r_i', positive=True)
rm = sp.symbols('r_m', positive=True)
xi = sp.symbols('xi', positive=True)

# Symbolic Stream function Psi
Psi_sph = -r**2/2 + s**2/2 * epsilon * beta(r**2) * sp.cos(2*phi)
#~ Psi_sph = -s**2/2 * (1 - epsilon * beta(r**2) * sp.cos(2*phi))

Psi_sph = Psi_sph.subs({s:r*sp.sin(theta)})
ez = sp.Matrix([sp.cos(theta), - sp.sin(theta), 0])
u_sph = curl(Psi_sph*ez, 'spherical')

# Boundary conditions
print("\t\tx Stress-free and regularity BC")
N = 6
ex,coeff = taylor_expeven(0,N,xi)
Eq1 = sp.Eq(ex.subs({xi:1}),0)
Eq2 = sp.Eq(sp.diff(ex,xi).subs({xi:1}),0)
Eq3 = sp.Eq(sp.diff(ex,xi,xi).subs({xi:1}),0)
Eq4 = sp.Eq(ex.subs({xi:0}),0)
Eq5 = sp.Eq(ex.subs({xi:rm**2}),1)
Eq6 = sp.Eq(ex.diff(xi).subs({xi:rm**2}),0)
sol = sp.solve([Eq1, Eq2, Eq3, Eq4, Eq5, Eq6], coeff)

# Boundary conditions
print("\t\tx Subs of beta(r)")
beta_final = ex.subs(sol).subs({xi:r**2})
Psi_sph_BC = Psi_sph.subs({beta(r**2):beta_final})

# Velocity field
print("\t* Symbolic velocity field")
ez = sp.Matrix([sp.cos(theta), - sp.sin(theta), 0])
U0 = curl(Psi_sph_BC*ez, 'spherical')

# Temperature and gravitationnal potential
print("\t* Symbolic temperature and grav. potential")
N02 = sp.symbols('N02', real=True)
Pot_sph = -Psi_sph_BC**1
Ts_sph = N02*Pot_sph**1
gradT0 = grad(Ts_sph,'spherical')
gsph = grad(-Pot_sph,'spherical')
gnorm = gsph.norm()

# Square of the Brunt-Väisälä frequency
print("\t* Symbolic Brunt-Väisälä frequency")
Psi = sp.Function('Psi')(r,theta,phi)
Pot = sp.Function('Phi')(Psi)
Ts = N02*Pot**1
dTs_dphi = Ts.diff(Pot)
N2_sph = gsph.norm()**2 * dTs_dphi.subs({Pot:Pot_sph})

#-----------------------------------------------------------------------
# Lambdify
#-----------------------------------------------------------------------
print("\no Lambdify")
Psi_lambda = sp.lambdify((r, theta, phi, rm, epsilon), Psi_sph_BC, "numpy")
Pot_lambda = sp.lambdify((r, theta, phi, rm, epsilon), Pot_sph, "numpy")
Ts_lambda = sp.lambdify((r, theta, phi, rm, epsilon, N02), Ts_sph, "numpy")
N0_lambda = sp.lambdify((r, theta, phi, rm, epsilon, N02), sp.sqrt(N2_sph), "numpy")
U0r_lambda = sp.lambdify((r, theta, phi, rm, epsilon), U0[0].simplify(), "numpy")
U0t_lambda = sp.lambdify((r, theta, phi, rm, epsilon), U0[1].simplify(), "numpy")
U0p_lambda = sp.lambdify((r, theta, phi, rm, epsilon), U0[2].simplify(), "numpy")

gradT0r_lambda = sp.lambdify((r, theta, phi, rm, epsilon, N02), gradT0[0].simplify(), "numpy")
gradT0t_lambda = sp.lambdify((r, theta, phi, rm, epsilon, N02), gradT0[1].simplify(), "numpy")
gradT0p_lambda = sp.lambdify((r, theta, phi, rm, epsilon, N02), gradT0[2].simplify(), "numpy")
g0r_lambda = sp.lambdify((r, theta, phi, rm, epsilon, N02), gsph[0].simplify(), "numpy")
g0t_lambda = sp.lambdify((r, theta, phi, rm, epsilon, N02), gsph[1].simplify(), "numpy")
g0p_lambda = sp.lambdify((r, theta, phi, rm, epsilon, N02), gsph[2].simplify(), "numpy")

#-----------------------------------------------------------------------
# Spherical grid
#-----------------------------------------------------------------------
print("\t* Numerical grid")
# SHTNS
sh = shtns.sht(Lmax,Mmax)	# sht
sh.set_grid()				# grid
l_2 = 1.0/((1+sh.l)*sh.l)
l_2[0] = 0					# to avoid NaN

# Grid
nbt = sh.nlat
nbp = sh.nphi
rnum = np.linspace(0, 1, nbr)
thetanum = np.arccos(sh.cos_theta)
phinum = np.arange(0,nbp)*2*np.pi/(nbp*sh.mres)
THETAbulk, Rbulk, PHIbulk = np.meshgrid(thetanum, rnum, phinum)
irs, ire = 0, len(rnum)-1	# define radial extent

#-----------------------------------------------------------------------
# Export to xshells
#-----------------------------------------------------------------------
print("\no Export to xshells")

# Filenames
filename_U0 = 'U0.out'
filename_T0 = 'T0.out'
filename_Phi0 = 'Phi0.out'
filename_N0 = 'N0.out'
xshells_out = "xshells.par"

# Loop
if not(os.path.isdir("Sphere")):
	os.mkdir("Sphere")

for strati in N0_num:
	for eps in eps_num:
		# Init
		print('\t* N0 = ' + str(strati) + ', eps = ' + str(eps))
		pathdirN0 = 'Sphere/N0=' + str(strati) + '/'
		pathdireps = pathdirN0 + 'eps=' + str(eps) + '/'
		if not(os.path.isdir(pathdirN0)):
			os.mkdir(pathdirN0)
			os.mkdir(pathdireps)
		else:
			if not(os.path.isdir(pathdireps)):
				os.mkdir(pathdireps)
			else:
				pass
		# Export to xshells
		grid = px.Grid(rnum)						# build the radial grid
		Psi_num = Psi_lambda(Rbulk, THETAbulk, PHIbulk, rmn, eps)
		Phi_num = Pot_lambda(Rbulk, THETAbulk, PHIbulk, rmn, eps)
		U0r_num = U0r_lambda(Rbulk, THETAbulk, PHIbulk, rmn, eps)
		U0t_num = U0t_lambda(Rbulk, THETAbulk, PHIbulk, rmn, eps)
		U0p_num = U0p_lambda(Rbulk, THETAbulk, PHIbulk, rmn, eps)
		if strati != 0:
			Ts_num = Ts_lambda(Rbulk, THETAbulk, PHIbulk, rmn, eps, np.real(strati**2))
			N0_num = N0_lambda(Rbulk, THETAbulk, PHIbulk, rmn, eps, np.real(strati**2))
			gradT0r_num = gradT0r_lambda(Rbulk, THETAbulk, PHIbulk, rmn, eps, np.real(strati**2))
			gradT0t_num = gradT0t_lambda(Rbulk, THETAbulk, PHIbulk, rmn, eps, np.real(strati**2))
			gradT0p_num = gradT0p_lambda(Rbulk, THETAbulk, PHIbulk, rmn, eps, np.real(strati**2))
			g0r_num = g0r_lambda(Rbulk, THETAbulk, PHIbulk, rmn, eps, np.real(strati**2))
			g0t_num = g0t_lambda(Rbulk, THETAbulk, PHIbulk, rmn, eps, np.real(strati**2))
			g0p_num = g0p_lambda(Rbulk, THETAbulk, PHIbulk, rmn, eps, np.real(strati**2))
			
		
		# Velocity field
		print('\t\tx Base flow U0')
		UB = px.PolTor(grid, sh)					# build a PolTor field for xshells from the grid and the sht
		UB.alloc(irs, ire)							# alloc memory for the field
		
		for ir in range(irs, ire+1):
			q, s, t = sh.analys(U0r_num[ir,:,:], U0t_num[ir,:,:], U0p_num[ir,:,:])	# to spectral space
			# we assume the field was divergence free, so that Pol = r*q/(l*(l+1))
			UB.set_pol(ir, rnum[ir]*q*l_2)
			UB.set_tor(ir, t)
	
		UB.set_BC(2,2)								# Stress-free
		UB.tofile(pathdireps + filename_U0)			# write to file
	
		if strati != 0:
			# Gravitational potential Phi
			print('\t\tx Potential Phi0')
			Phi0 = px.ScalarSH(grid, sh)			# build a PolTor field for xshells from the grid and the sht
			Phi0.alloc(irs, ire)					# alloc memory for the field
		
			for ir in range(irs, ire+1):
				hs = sh.analys(Phi_num[ir,:,:])		# to spectral space
				Phi0.set_sh(hs, ir)
		
			Phi0.set_BC(127,127)	# No BC
			Phi0.tofile(pathdireps + filename_Phi0)		# write to file

			# Tempreature field Ts
			print('\t\tx Temperature field T0')
			T0 = px.ScalarSH(grid, sh)			# build a PolTor field for xshells from the grid and the sht
			T0.alloc(irs, ire)					# alloc memory for the field
			
			for ir in range(irs, ire+1):
				hs = sh.analys(Ts_num[ir,:,:])	# to spectral space
				T0.set_sh(hs, ir)
			
			T0.set_BC(1,1)							# Fixed temperature
			T0.tofile(pathdireps + filename_T0)		# write to file
			
			# Brunt-Väisälä frequency N0
			print('\t\tx Brunt-Väisälä frequency N0')
			N0 = px.ScalarSH(grid, sh)			# build a PolTor field for xshells from the grid and the sht
			N0.alloc(irs, ire)					# alloc memory for the field
		
			for ir in range(irs, ire+1):
				hs = sh.analys(N0_num[ir,:,:])		# to spectral space
				N0.set_sh(hs, ir)
		
			N0.set_BC(127,127)	# No BC
			N0.tofile(pathdireps + filename_N0)		# write to file
			
		# Template xshells.par
		if strati != 0:
			xshells_in = "xshells.par.template"
		else:
			xshells_in = "xshells.par.template_N2=0"
		
		f = open(xshells_in,'r')
		filedata = f.read()
		f.close()
		if strati != 0:
			jobname = "sphere_eps" + str(eps) + "_N0" + str(strati) + "_Ek" + str(Ek) + "_Pr" + str(Pr) + "_L" + str(Lmax) + "_m" + str(Mmax)
		else:
			jobname = "sphere_eps" + str(eps) + "_N0" + str(strati) + "_Ek" + str(Ek) + "_L" + str(Lmax) + "_m" + str(Mmax)
		newdata = filedata.replace("$jobname$", jobname)
		newdata = newdata.replace("$Ek$", str(Ek))
		newdata = newdata.replace("$rin$", str(0))
		newdata = newdata.replace("$NR$", str(NR))
		newdata = newdata.replace("$Lmax$", str(Lmax))
		newdata = newdata.replace("$Mmax$", str(Mmax))
		newdata = newdata.replace("$dt$", str(dt))
		newdata = newdata.replace("$iter_max$", str(iter_max))
		newdata = newdata.replace("$sub_iter$", str(sub_iter))
		newdata = newdata.replace("$iter_save$", str(iter_save))
		if strati != 0:
			newdata = newdata.replace("#$Pr$", "Pr")
			newdata = newdata.replace("$Pr$", str(Pr))
			newdata = newdata.replace("$bct$", str(bct))
		if nonlin == 'yes':
			if flagnl == 1:
				newdata = newdata.replace("#$nonlin1$", "nonlin")
			elif flagnl in [2, 3]:
				newdata = newdata.replace("#$Pm$", "Pm")
				newdata = newdata.replace("$Pm$", str(Pm))
				newdata = newdata.replace("#$eta$", "eta")
				newdata = newdata.replace("#$bini$", "b")
				newdata = newdata.replace("#$R_B$", "R_B")
				if flagnl == 2:
					newdata = newdata.replace("#$nonlin2$", "nonlin")
				else:
					newdata = newdata.replace("#$nonlin3$", "nonlin")

		f = open(pathdireps + xshells_out,'w')
		f.write(newdata)
		f.close()
