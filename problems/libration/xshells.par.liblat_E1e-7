##########################################################################
# XSHELLS : eXtendable Spherical Harmonic Earth-Like Liquid Simulator    #
#  > this is the input parameter file                                    #
# syntax : name = value                                                  #
##########################################################################

### example file to reproduce the spherical simulations from
### "Mean zonal flows induced by weak mechanical forcings in rotating spheroids"
### J. Fluid Mech. (2021), doi:10.1017/jfm.2021.220
### https://arxiv.org/abs/2103.10260
job = liblat_E1e-7  		# job name, used as a suffix for output files.

### PHYSICAL PARAMS ###
nu = 1e-7	# viscosity
Omega0 = 1 	# Global rotation rate
domega_o = 1e-4	# Differential rotation amplitude of the outer boundary (libration forcing, see xshells.hpp)
w_o = 0.1       # frequency (pulsation) of the libration (see xshells.hpp)
m_o = 1		# 1 for latitudinal libration, 0 for longitudinal libration (default)
#eta = 10	# magnetic diffusivity

### INITIAL FIELDS ###
## A file name containing a field may be given (load file), or 
## a predefined field name (run "./list_fields" for a complete list)
## or "random", "rands", "randa" and "0" for respectively :
## load file, xshells_init.cpp defined velocity and magnetic fields,
## random field, random symmetric, random anti-symmetric fields and zero field.
## Fields may be scaled by adding (e.g.) *1e-3 after the name.
## comment out the lines if the field does not exist to avoid time-stepping that field.

u = 0		# initial velocity field : random so that instability can grow
#b = bigsister*0.175	# initial magnetic field (same as -b command line option).

### BOUNDARY CONDITIONS AND RADIAL DOMAINS ###
#r = rayon.dat		# file containing the radial grid to use (either a field file, or an ascii file; same as -r command line option).
BC_U = 2,1		# inner,outer boundary conditions (1=no-slip (default), 2=free-slip)
R_U = 0 : 1	# Velocity field boundaries
R_B = 0 : 1	# Magnetic field boundary (insulator outside)
#kill_sbr = 1		# (for free-slip only) 1: angular momentum kept at 0 (kill solid body rotation). 0: free solid body roration allowed. 2: conserve angular momentum to its inital value.

### NUMERICAL SCHEME ###
NR = 640	# total number of radial shells
N_BL = 30,170	# number of radial shells used to refine the grid near the inner and outer boundaries
dt_adjust = 0	# don't adjust time-step.
dt = 2*pi*0.005/1.0	# time step for Navier-Stokes equation
iter_max = 40000 	# iteration number (ouput #)
sub_iter = 200  	# sub-iterations (time steps between outputs)
iter_save = 2000  	# sub-iterations (time steps between outputs)

### SHT ###
Lmax = 255	# max degree of spherical harmonics
Mmax = 1	# max fourier mode (phi)
Mres = 1	# phi-periodicity.
#Nlat = 90	# number of latitudinal points (theta). Optimal chosen if not specified.
#Nphi = 16	# number of longitudinal points (phi). Optimal chosen if not specified.
rsat_ltr = 0.005

### OPTIONS ###
restart = 1
interp = 1
movie = 2		# 0=field output at the end only (default), 1=output every modulo, 2=also writes time-averaged fields
backup_time = 600	# ensures that full fields are saved to disk at least every backup_time minutes.
#lmax_out = 130		# lmax for movie output (-1 = same as Lmax, which is also the default)
#mmax_out = 20		# mmax for movie output (-1 = same as Lmax, which is also the default)

stepper = SBDF3

### ALGORITHM FINE TUNING ###

## sht_type : 0 = gauss-legendre, 1 = fastest (default) with DCT enabled
##            2 = fastest on regular grid, 3 = full DCT, 4 = debug, 6 = on-the-fly.
##   0 has the smallest init-time, usedful for test/debug.
sht_type = 6

## sht_polar_opt_max = SHT polar optimization threshold : polar coefficients below that threshold are neglected (for high ms).
##    value under wich the polar values of the Legendre Polynomials Plm are neglected, leading to increased performance (a few percent).
##    0 = no polar optimization;  1.e-14 = VERY safe;  1.e-10 = safe;  1.e-6 = aggresive.
#sht_polar_opt_max = 1.0e-10

