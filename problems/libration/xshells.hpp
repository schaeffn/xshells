/** \file xshells.hpp
Compile-time parmeters and customizable functions for XSHELLS.
*/

#ifndef XS_H
	/* DO NOT COMMENT */
	#define XS_H

///  EXECUTION CONTROL  ///
/// 1. stable and tested features ///

/* call custom_diagnostic() (defined below) from main loop to perform and store custom diagnostics */
#define XS_CUSTOM_DIAGNOSTICS

/* enable variable time-step adjusted automatically */
#define XS_ADJUST_DT

/* XS_LINEAR use LINEAR computation : no u.grad(u), no J0xB0  */
//#define XS_LINEAR

/* Impose arbitrary stationary flow at the boundaries (for Diapir/Monteux) */
//#define XS_SET_BC

/* use variable conductivity profile eta(r) [see definition of profile in calc_eta() below */
//#define XS_ETA_PROFILE

/* Variable L-truncation : l(r) = LMAX * sqrt(r/(rmax*VAR_LTR))  +1 */
#define VAR_LTR 0.5

///  EXECUTION CONTROL  ///
/// 2. unstable and beta features ///

/* Hyperdiffusion : enable enhanced diffusion constants (see xshells.par)*/
//#define XS_HYPER_DIFF

/* enable output magnetic field surface secular variation up to lmax=lmax_out_sv (set in xshells.par) */
//#define XS_WRITE_SV

/* compute electric potential and saves DTS specific measurements at end of run */
//#define XS_DTS_POTENTIAL

///  EXECUTION CONTROL  ///
/// 3. testing, unsupported alpha features (not working) ///

/* Poincare force for libration, controlled by a_forcing and w_forcing. 0 for longitudinal libration, 1 for latitudinal libration. */
//#define XS_LIBRATION 1


#ifdef XS_ETA_PROFILE
	// variable magnetic diffusivity is used :
	#define XS_ETA_VAR
#endif

#else

/* TIME-DEPENDANT BOUNDARY FORCING */
/// This function is called before every time-step, and allows you to modify
/// the Pol/Tor components of the velocity field.
/**  Useful global variables are :
 *  a_forcing : the amplitude set in the .par file
 *  w_forcing : the frequency (pulsation) set in the .par file
 *  \param[in] t is the current time.
 *  \param Bi is the inner SolidBody, whose angular velocity can be modified.
 *  \param Bo is the outer SolidBody, whose angular velocity can be modified.
 */
inline void calc_Uforcing(double t, double a_forcing, double w_forcing, struct SolidBody *Bi, struct SolidBody *Bo)
{
	#define U_FORCING "Arbitrary libration"
	#define FORCING_M 0

	double domega_i = mp.var["domega_i"];	// amplitude at inner boundary
	double domega_o = mp.var["domega_o"];	// amplitude at outer boundary
	double w_i = mp.var["w_i"];	// frequency at inner boundary (pulsation)
	double w_o = mp.var["w_o"];	// frequency at outer boundary (pulsation)
	int m_i = mp.var["m_i"];	// if m_i=0 longitudinal libration (axisymmetric) of inner boundary, otherwise latitudinal libration (m=1) around x-axis
	int m_o = mp.var["m_o"];	// if m_o=0 longitudinal libration (axisymmetric) of outer boundary, otherwise latitudinal libration (m=1) around x-axis

	domega_i *= sin(w_i*t);
	domega_o *= sin(w_o*t);

	if (m_i == 0) {
		Bi->Omega_z = domega_i;
	} else Bi->Omega_x = domega_i;

	if (m_o == 0) {
		Bo->Omega_z = domega_o;
	} else Bo->Omega_x = domega_o;

	if ((m_o | m_i) != 0) {
		if ((MMAX == 0) || (MRES != 1)) runerr("forcing requires mode m=1");
	}
}



#ifdef XS_CUSTOM_DIAGNOSTICS
/// compute custom diagnostics. They can be stored in the all_diags array , which is written to the energy.jobname file.
/// own(ir) is a macro that returns true if the shell is owned by the process (useful for MPI).
/// i_mpi is the rank of the mpi process (0 is the root).
/// This function is called outside an OpenMP parallel region, and it is permitted to use openmp parallel constructs inside.
/// After this function returns, the all_diags array is summed accross processes and written in the energy.jobname file.
void custom_diagnostic(diagnostics& all_diags)
{
	// Uncomment the lines below to use particular diagnostic.
	#include "diagnostics/split_energies.cpp"
	#include "diagnostics/m_spectrum.cpp"
	#include "diagnostics/dissipation.cpp"
	#include "diagnostics/torque.cpp"

	// record velocity at given probe positions
	if (evol_ubt & EVOL_U) {
		double*	diags = all_diags.append(9,
			"up1, up2, up3, up4, up5, up6, up7, up8, up9\t ");
		const double r_list[9] = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9};
		const int it0 = shtns->nlat_2-1;
		static int first = 1;
		if (i_mpi > 0) first = 0;   // only MPI rank 0 prints
		if (first) {	// print message at first iteration
			printf("recording probes at theta=%g degrees and s=", acos(shtns->ct[it0])*180./M_PI);
		}
		for (int k=0; k < sizeof(r_list)/sizeof(double); k++) {
			const int ir = r_to_idx(r_list[k] / shtns->st[it0]);
			if (first) printf("%g ", r[ir]*shtns->st[it0]);
			if (own(ir)) {
				cplx* vp = (cplx*) VMALLOC(sizeof(cplx) * shtns->nlat * 2);
				cplx* vt = vp + shtns->nlat;
				SHtor_to_spat_ml(shtns, 0, Ulm.Tor[ir], vt, vp, shtns->lmax);
				diags[k] = real(vp[it0]);
				VFREE(vp);
			}
		}
		if (first) {  printf("\n");  first=0; }		// print only once.
	}
}
#endif


#ifdef XS_ETA_PROFILE

/// define magnetic diffusivity eta as a function of r
/// discontinuous profiles supported.
void calc_eta(double eta0)
{
	for (int i=0; i<NR; i++) {
		etar[i] = eta0;			// eta0 by default.

//	/*	add // at the begining of this line to uncomment the following bloc.
	// DTS conductivity profile
		if (i > NM)		etar[i] = eta0 *9.0; 	// outer shell : inox (*9.0)
		if (i < NG)		etar[i] = eta0 /4.2;	// inner-core : copper (/4.2)
/*  ________
*/	

	}
}
#endif


/* DO NOT REMOVE */
#endif
