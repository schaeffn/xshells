##########################################################################
# XSHELLS : eXtendable Spherical Harmonic Earth-Like Liquid Simulator    #
#  > this is the input parameter file                                    #
# syntax : name = value                                                  #
##########################################################################

### parameters to reproduce the double-diffusive convection benchmark
### see Breuer et al. (2010, GJI, 183, 150-162)
### http://dx.doi.org/10.1111/j.1365-246X.2010.04722.x
job = breuer2010

### USER DEFINED VARIABLES ###
E = 1e-3
Pr = 0.3  # PrT
Sc = 3    # PrC
Ratot = 2e5
d = 0.8    # delta
Rat = d*Ratot
Rac = (1-d)*Ratot

### PHYSICAL PARAMS ###
Omega0 = 1/E 	# Global rotation rate
nu = 1.0		# viscosity
kappa = 1.0/Pr		# thermal diffusivity
kappa_c = 1.0/Sc	# chemical diffusivity

### INITIAL FIELDS ###
## A file name containing a field may be given (load file), or 
## a predefined field name (run "./list_fields" for a complete list)
## or "random", "rands", "randa" and "0" for respectively :
## load file, xshells_init.cpp defined velocity and magnetic fields,
## random field, random symmetric, random anti-symmetric fields and zero field.
## Fields may be scaled by adding (e.g.) *1e-3 after the name.
## comment out the lines if the field does not exist to avoid time-stepping that field.

u = 0			# initial velocity field
tp = random		# initial temperature field
c = random		# initial composition field
tp0 = delta*-Rat	# imposed (base) temperature field
c0 = delta*-Rac		# imposed (base) composition field
phi0 = radial		# radial gravity field

### BOUNDARY CONDITIONS AND RADIAL DOMAINS ###
#r = rayon.dat		# file containing the radial grid to use (either a field file, or an ascii file).
BC_U = 0,0		# inner,outer boundary conditions (0=zero velocity, 1=no-slip, 2=free-slip)
BC_T = 1,1		# 1=fixed temperature, 2=fixed flux.
BC_C = 1,1		# 1=fixed composition, 2=fixed flux.
R_U = 7/13 : 20/13	# Velocity field boundaries
R_T = 7/13 : 20/13	# Temperature field boundaries
R_C = 7/13 : 20/13	# Composition field boundaries
kill_sbr = 1		# (for free-slip only) 1: angular momentum kept at 0 (kill solid body rotation). 0: free solid body roration allowed. 2: conserve angular momentum to its inital value.

### NUMERICAL SCHEME ###
NR = 96		# total number of radial shells (overriden by r = ...)
N_BL = 10,10	# number of radial shells reserved for inner and outer boundaries.
dt_adjust = 1	# 0: fixed time-step (default), 1: variable time-step
dt = 0.0001		# time step
iter_max = 1000	# iteration number (total number of text and energy file ouputs)
sub_iter = 100	# sub-iterations (the time between outputs = 2*dt*sub_iter is fixed even with variable time-step)
iter_save = 10  # number of iterations between field writes (if movie > 0, see below).

### SHT ###
Lmax = 63	# max degree of spherical harmonics
Mmax = 15	# max fourier mode (phi)
Mres = 4	# phi-periodicity.
#Nlat = 32	# number of latitudinal points (theta). Optimal chosen if not specified.
#Nphi = 16	# number of longitudinal points (phi). Optimal chosen if not specified.

### OPTIONS ###
#interp = 1		# 1: allow interpolation of fields in case of grid mismatch. 0: fail if grids mismatch (default).
restart = 1		# 1: try to restart from a previous run with same name. 0: no auto-restart (default).
#movie = 1		# 0=field output at the end only (default), 1=output every iter_save, 2=also writes time-averaged fields
#lmax_out = -1		# lmax for movie output (-1 = same as Lmax, which is also the default)
#mmax_out = -1		# mmax for movie output (-1 = same as Mmax, which is also the default)
#prec_out = 1		# 1: single precision movie output (default), 2: double precision movie outputs.
#zavg = 2			# 1=output z-averaged field, 2=also output rms z-averaged.
backup_time = 240		# ensures that full fields are saved to disk at least every backup_time minutes, for restart.
#nbackup = 0			# number of backups before terminating program (useful for time-limited jobs). 0 = no limit (default)

### ALGORITHM FINE TUNING ###

## C_vort and C_alfv control the time-step adjustment (active if dt_adjust=1),
## regarding vorticity and Alfven criteria.
## if dt_tol_lo < dt/dt_target < dt_tol_hi, no adjustement is done.
#C_vort = 0.2		# default: 0.2
#C_alfv = 1.0		# default: 1.0
#dt_tol_lo = 0.8		# default: 0.8
#dt_tol_hi = 1.1		# default: 1.1

## sht_type : 0 = gauss-legendre (default), 1 = fastest with DCT enabled
##            2 = fastest on regular grid, 3 = full DCT, 4 = debug (quick_init), 6 = on-the-fly (good for parallel)
##   4 has the smallest init-time, useful for test/debug. Otherwise 0 or 6 should be used.
sht_type = 0

## sht_polar_opt_max = SHT polar optimization threshold : polar coefficients below that threshold are neglected (for high ms).
##    value under wich the polar values of the Legendre Polynomials Plm are neglected, leading to increased performance.
##    0 = no polar optimization;  1.e-14 = VERY safe (default);  1.e-10 = safe;  1.e-6 = aggresive.
sht_polar_opt_max = 1.0e-10

