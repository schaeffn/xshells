%% This matlab/octave script writes field files (in XSHELLS format) 
%% from PARODY data (already loaded in yVtr, yVpr, yBtr, yBpr, yTtr
%% it also takes care of the normalization conversion.


mca=1
%sht_norm = 1 + 256*4 + 256*8;   % four_pi + no_cs_phase + real_norm
sht_norm = 0;      % we renormalize to xshells' norm

if (time<=9.99999)
     sortie=num2str(time,'%7.5f');
 elseif(time<=99.9999)
     sortie=num2str(time,'%7.4f');
 elseif(time<=999.999)
     sortie=num2str(time,'%7.3f');
 elseif(time<=9999.99)
     sortie=num2str(time,'%7.2f');
 elseif(time<=99999.9)
     sortie=num2str(time,'%7.1f');
 end

for iout=1:3
    if (iout==1)
        fid = fopen(['fieldU_' sortie '.parody'],'w');
        ncomp = 2;
    end
    if (iout==2)
        fid = fopen(['fieldB_' sortie '.parody'],'w');
        ncomp = 2;
    end
    if (iout==3)
		fid = fopen(['fieldT_' sortie '.parody'],'w');
		ncomp = 1;
    end

%struct FieldInfo {
%	int version, lmax, mmax, mres, nlm, nr, irs, ire;	// 8 integers
%	int BCi, BCo, i3, i4, shtnorm, nc, iter, step;		// 8 integers
%	double d1, d2, d3, t, d5, d6, d7, d8;		// 8 doubles
%	char hgid[64];		// id string : 64 chars.
%	char text[832];		// 832 chars => total 1024 bytes.
%};

version=11;
fwrite(fid,version,'int');
fwrite(fid,lmax,'int');
fwrite(fid,round(mmax/mca),'int');
fwrite(fid, mca,'int');    % mres
fwrite(fid,lmmax,'int');
fwrite(fid,NR,'int');
fwrite(fid,0,'int');        % irs
fwrite(fid,NR-1,'int');     % ire

fwrite(fid,(iout-1)*3,'int');        % bci
fwrite(fid,(iout-1)*3,'int');        % bco
fwrite(fid,0,'int');        % dummy
fwrite(fid,0,'int');        % dummy
fwrite(fid,sht_norm,'int');        % sht_norm
fwrite(fid,ncomp,'int');        % number of components (2 for poltor, 1 for scalar)
fwrite(fid,0,'int');        % iter
fwrite(fid,0,'int');        % step

fwrite(fid,0.0,'double');   % dummy
fwrite(fid,0.0,'double');   % dummy
fwrite(fid,0.0,'double');   % dummy
fwrite(fid,time,'double');  % t
fwrite(fid,0.0,'double');   % dummy
fwrite(fid,0.0,'double');   % dummy
fwrite(fid,0.0,'double');   % dummy
fwrite(fid,0.0,'double');   % dummy

count=fwrite(fid,'converted from PARODY','char');
while (count < 896)
    count = count + fwrite(fid,0,'int8');   % dummy
end

fwrite(fid,r,'double');     % radial grid

pol=zeros(2*lmmax,NR);
tor=zeros(2*lmmax,NR);
lm=1;
for m=0:mmax
    renorm = sqrt(4*pi);
    if (m>0)
        renorm = sqrt(4*pi)/ sqrt(2) * (-1)^m;
    end
    if iout==1
      for l=m:lmax
        tor(2*lm-1,:) = real(yVtr(lm,:)) *renorm;
        tor(2*lm,:)   = imag(yVtr(lm,:)) *renorm;
        pol(2*lm-1,:) = real(yVpr(lm,:)) *renorm;
        pol(2*lm,:)   = imag(yVpr(lm,:)) *renorm;
        lm=lm+1;
      end
    end
    if iout==2
      for l=m:lmax
        tor(2*lm-1,:) = real(yBtr(lm,:)) *renorm;
        tor(2*lm,:)   = imag(yBtr(lm,:)) *renorm;
        pol(2*lm-1,:) = real(yBpr(lm,:)) *renorm;
        pol(2*lm,:)   = imag(yBpr(lm,:)) *renorm;
        lm=lm+1;
      end
    end
    if iout==3
      for l=m:lmax
        tor(2*lm-1,:) = real(yTtr(lm,:)) *renorm;
        tor(2*lm,:)   = imag(yTtr(lm,:)) *renorm;
        lm=lm+1;
      end    
    end
end

fwrite(fid,zeros(2*lmmax,1),'double');      % ghost shell = 0
if (iout < 3)
  fwrite(fid,zeros(2*lmmax,1),'double');
end
for ir=1:NR
    if (iout < 3)
     fwrite(fid,pol(1:2*lmmax,ir),'double');
    end
    fwrite(fid,tor(1:2*lmmax,ir),'double');
end
fwrite(fid,zeros(2*lmmax,1),'double');      % ghost shell = 0
if (iout < 3)
	fwrite(fid,zeros(2*lmmax,1),'double');
end

fclose(fid);
end
