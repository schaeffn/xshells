/*
 * Copyright (c) 2010-2019 Centre National de la Recherche Scientifique.
 * written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France).
 * 
 * nathanael.schaeffer@univ-grenoble-alpes.fr
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * 
 */

/// \file xshells_linop.cpp
/// Linear Operators: 3 banded matrices, 5-banded matrices (real), applied to complex vectors.

/* TODO: 
 * No (partial) pivoting is necessary for a strictly column diagonally dominant matrix when performing Gaussian elimination (LU factorization).
 *  	source: https://en.wikipedia.org/wiki/Diagonally_dominant_matrix#Applications_and_properties
 * for tridiagonal matrices, use	 https://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm
*/

/*	Prefetching does not seem to help
#define PREFETCH_W(mem) __builtin_prefetch(mem,1,3)
//#define PREFETCH_T1(mem) __builtin_prefetch(mem,0,2)
#define PREFETCH_BLOCK_W(mem0, mem1) for (const char* p = (const char*) mem0; p <= (const char*) mem1; p += 64) __builtin_prefetch(p,1,3);
*/

#define PREFETCH_W(mem)
#define PREFETCH_BLOCK_W(mem0, mem1)

//enum op_bc { none, dr_zero, d2r_zero, free_slip };

/// Flags for distributed MPI solver
enum op_solve_flags { SOLVE_OFF=0, SOLVE_UPDN=1, SOLVE_SYNC_BC_SND=2, SOLVE_SYNC_BC_RCV=4,
	SOLVE_SYNC_SND=8, SOLVE_SYNC_RCV=16, SOLVE_SYNC_RCV_1=32, SOLVE_DN_HALO=64 };

enum op_flags { LINOP_BCI_L0_ONLY=1, LINOP_BCO_L0_ONLY=2 };

class LinOpR {
	double *data;		///< store the matrix elements
  protected:
	unsigned nelem;

    void _alloc(int ir0, int ir1, int _nelem);
    void _free();

  public:
	int ir_bci, ir_bco;		///< global boundaries
	int irs, ire;			///< local boundaries (MPI)
    LinOpR() : data(0), nelem(0), ir_bci(0), ir_bco(-1), irs(0), ire(-1) {}
    ~LinOpR() { _free(); }
    inline double* Mr(int ir) const {
		//if ((ir < ir_bci) || (ir > ir_bco)) printf("out of bound access\n");
		return data + nelem*ir;		// data pointer already shifted.
	}
//    inline double* operator[](int ir) const { return Mr(ir); }
    void copy_r(int ir, const LinOpR& L);
    void scale(int ir, const double s);
    void zero(int ir);

	const unsigned short* mx_idx = 0;
	void set_custom_mapping(const unsigned short *lm_map) {	mx_idx = lm_map; }	///< use a different mapping of lm index to matrix index (l by default)
};

//  loop over all harmonics + lookup array for l : no assumption on l-storage is made + FASTER !! (cache miss seems better than branch prediction miss...)
#define LM_L_LOOP2( lms, lme, action ) { const unsigned short* lidx_ = mx_idx; long lm=(lms); do { long l=lidx_[lm]; action } while(++lm <= (lme)); }

class LinOp_solvable : public LinOpR {
  protected:
	int irs_solve, ire_solve;	// start and end for solve.
	short dir_solve = 1;		// initial direction of LU-solve, can be either +1 or -1. Default is +1.
	#ifdef XS_MPI
	unsigned short dist_flags;		// 0 = totally inactive process, skip all solve methods; 1 = involved in solve_up/dn methods; 2+ = involved in sync
	int ir_middle = -1;				// radius of middle shell at interface between processes, or -1 to disable x-solver.
	int share_dn, share_up;			// <0: no mpi com needed, >=0: MPI rank to send to
	#endif

  public:
	void init_solve(const Spectral& X, int bidir_split=0);		///< some initialization.
	int get_solve_direction() const {  return dir_solve;  }
};

template <class T>
class _LinOp3l : public LinOp_solvable {		// Base class using CRTP, for static polymorphism.
  protected:
	int lmax;

	void _precalc_solve(int i0, int i1, int l0, int l1, int dir) { static_cast<T*>(this)->_precalc_solve(i0,i1,l0,l1,dir); }
	void precalc_solve_common();

  public:
	int flags = 0;		///< some flags, should be propageted to clones

	inline void set_lo(int ir, double v) { static_cast<T*>(this)->set_lo(ir, v); }
	inline void set_up(int ir, double v) { static_cast<T*>(this)->set_up(ir, v); }
	inline void set_di(int ir, int l, double v)  { static_cast<T*>(this)->set_di(ir,l, v); }
	inline double coeff(int ir, int l, const int dir) {	return static_cast<T*>(this)->coeff(ir,l, dir); }
	int bandwidth() const { return 1; }

	void set_Laplace(int ir, const double s=1.0);
	void set_Laplace_bc(int ir, double a0, double a1, const double s=1.0);
	void set_Laplace(int ir, const double* d1r, const double* d2r, const double s=1.0);
#ifdef XS_MPI
	void solve_finish(Spectral& X, xs_array2d<cplx> x, int tag, MPI_Request **req, int lms_block, int nlm_block) const;
#endif
};

/// tri-diagonal operator, with only the diagonal dependend on l.
class LinOp3ld : public _LinOp3l<LinOp3ld> {
  #ifdef XS_MPI
	double* C = 0;	// coupling matrix for the X-solve distributed solve algorithm
  #endif

 public:
    void alloc(int ir0, int ir1, int _lmax);
	void apply(xs_array2d<cplx> x, xs_array2d<cplx> y, int lmstart, int lmend);

	inline double lo(int ir) const { return Mr(ir)[0]; }
	inline double up(int ir) const { return Mr(ir)[1]; }
	inline void set_lo(int ir, double v) { Mr(ir)[0] = v; }
	inline void set_up(int ir, double v) { Mr(ir)[1] = v; }
	inline void add_lo(int ir, double v) { Mr(ir)[0] += v; }
	inline void add_up(int ir, double v) { Mr(ir)[1] += v; }

	inline double di(int ir, int l) const { return Mr(ir)[2+l]; }
	inline double offdi(int ir, int dir) const { return Mr(ir)[(dir+1)>>1]; }
	inline void set_di(int ir, int l, double v) { Mr(ir)[2+l] = v; }
	inline void add_di(int ir, int l, double v) { Mr(ir)[2+l] += v; }

	inline double coeff(int ir, int l, const int dir) {
		if (dir==0) return di(ir,l);
		return offdi(ir,dir);
	}

	void _precalc_solve(int i0, int i1, int l0, int l1, int dir);	// internal function, not meant to be called by user
	void precalc_solve();
	void set_op(const double a, const double b, const LinOp3ld& L);		///< set operator to (a + b*L)
	void scale(int ir, const double* sl);		///< operation not supported, use LinOp3l instead !!
	void copy_index(int l_dst, int l_src);
	void solve(xs_array2d<cplx> x, int lms, int lme, int lm_shift=0) const;
#ifdef XS_MPI
	void solve_up(xs_array2d<cplx> x, int lms, int lme, int tag, MPI_Request **req, int lms_block, int nlm_block) const;
	void solve_dn(xs_array2d<cplx> x, int lms, int lme, int tag, MPI_Request **req, int lms_block, int nlm_block) const;
	void solve_dn_halo(xs_array2d<cplx> x, int lmstart, int lmend) const;
#endif
};

/// tri-diagonal operator
class LinOp3l : public _LinOp3l<LinOp3l> {
  protected:
	inline double* M(int ir, int l) const { return LinOpR::Mr(ir) + 3*l + 1; }
	inline double* operator()(int ir, int l) const { return M(ir,l); }

  public:
	void alloc(int ir0, int ir1, int _lmax);
	void apply(xs_array2d<cplx> x, xs_array2d<cplx> y, int lmstart, int lmend);

	inline double coeff(int ir, int l, const int dir) { return Mr(ir)[3*l+1+dir]; }

	inline void set_lo(int ir, int l, double v) { Mr(ir)[3*l] = v; }
	inline void set_di(int ir, int l, double v) { Mr(ir)[3*l+1] = v; }
	inline void set_up(int ir, int l, double v) { Mr(ir)[3*l+2] = v; }
	
	inline void add_lo(int ir, int l, double v) { Mr(ir)[3*l] += v; }
	inline void add_di(int ir, int l, double v) { Mr(ir)[3*l+1] += v; }
	inline void add_up(int ir, int l, double v) { Mr(ir)[3*l+2] += v; }

	inline void set_lo(int ir, double v) { for (int l=0; l<=lmax; l++) set_lo(ir, l, v); }	///< broadcast on all l
	inline void set_up(int ir, double v) { for (int l=0; l<=lmax; l++) set_up(ir, l, v); }	///< broadcast on all l
	inline void add_lo(int ir, double v) { for (int l=0; l<=lmax; l++) add_lo(ir, l, v); }	///< broadcast on all l
	inline void add_up(int ir, double v) { for (int l=0; l<=lmax; l++) add_up(ir, l, v); }	///< broadcast on all l

    void scale(int ir, const double* s);		///< scale a line by a value function of l (array from 0 to lmax)
    void copy_index(int l_dst, int l_src);
	void set_op(const double a, const double b, const LinOp3l& L);		///< set operator to (a + b*L)
	void set_op(const double a, const double b, const LinOp3ld& L);		///< set operator to (a + b*L)

	void _precalc_solve(int i0, int i1, int l0, int l1, int dir);	// internal function, not meant to be called by user
};

/// 5-diagonal operator
class LinOp5l : public LinOp_solvable {
  protected:
	int lmax;

//	void _set_BiLaplace(int ir, double* d1r, double* d2r, double* d3r, double* d4r, const double s1, const double s2);

  public:
	void alloc(int ir0, int ir1, int _lmax);
	void apply(xs_array2d<cplx> x, xs_array2d<cplx> y, int lmstart, int lmend);
	int bandwidth() const { return 2; }

	inline double coeff(int ir, int l, const int dir) { return Mr(ir)[5*l+2+dir]; }

	inline double* M(int ir, int l) const { return LinOpR::Mr(ir) + 5*l + 2; }
	inline double* operator()(int ir, int l) const { return M(ir,l); }

	void set_op(double a,  double b, const LinOp5l& L);							///< set operator to (a + b*L)
	void set_op(double a,  const LinOp3ld& La, double b, const LinOp5l& Lb);	///< set operator to (a*La + b*Lb)
	void set_op(double a,  const LinOp3l& La, double b, const LinOp5l& Lb);		///< set operator to (a*La + b*Lb)
	void set_op(double a,  const LinOp5l& La, double b, const LinOp5l& Lb);		///< set operator to (a*La + b*Lb)

	void set_BiLaplace4(int ir, const double* d1r, const double* d2r, const double* d3r, const double* d4r, const double s1 = 0.0, const double s2 = 1.0);
	void set_Laplace4(int ir, const double* d1r, const double* d2r, const double s = 1.0);
	void set_Laplace2(int ir, const double* Lr, const double s = 1.0);
	void set_BiLaplace2(int ir, const double* Lrl, const double* Lrd, const double* Lru, const double s = 1.0);

    void scale(int ir, const double* s);		///< scale a line by a value function of l (array from 0 to lmax)
};

/// Stores L and U factors for efficient memory access
class LUstorage {
  protected:
	double *L;		// lower diagonal
	double *U;		// upper diagonal
	int nelem_L;
	int nelem_U;
  #ifdef XS_MPI
	double* C = 0;	// coupling matrix for the X-solve distributed solve algorithm
  #endif

	void alloc(int irs, int ire, int lmax, int b, bool a2a=false) {
		nelem_L = (lmax+1)*(b+1);
		nelem_U = (lmax+1)*b;
		#ifdef XS_MPI
		if (!a2a) {
			if (irs < irs_shared) irs=irs_shared;
			if (ire > ire_shared) ire=ire_shared;
		}
		#endif
		L = 0;	U = 0;
		if (ire >= irs) {
			long n_loc = ire-irs+1;
			#ifdef XS_MPI
			n_loc += b;		// + a bit more to solve down on ghost shells.
			#endif
			L = (double*) malloc( sizeof(double) * n_loc*(nelem_L + nelem_U) );		// alloc storage for shared shells only
			U = L + (ire-irs+1)*nelem_L;
		}
	}
  public:
	~LUstorage() {  free(L);  }
};

class LU3l : public LinOp3l, public LUstorage {
  public:
	void alloc(int ir0, int ir1, int _lmax, bool a2a=false);

	void precalc_solve();						///< precompute solve
	void solve(xs_array2d<cplx> x, int lms, int lme, int lm_shift=0) const;
#ifdef XS_MPI
	void solve_up(xs_array2d<cplx> x, int lms, int lme, int tag, MPI_Request **req, int lms_block, int nlm_block) const;
	void solve_dn(xs_array2d<cplx> x, int lms, int lme, int tag, MPI_Request **req, int lms_block, int nlm_block) const;
	void solve_dn_halo(xs_array2d<cplx> x, int lmstart, int lmend) const;
#endif
};

class LU5l : public LinOp5l, public LUstorage {
	void _precalc_solve(int i0, int i1, int dir);						///< precompute solve
  public:
	void alloc(int ir0, int ir1, int _lmax, bool a2a=false);

	void precalc_solve();						///< precompute solve
	void solve(xs_array2d<cplx> x, int lms, int lme, int lm_shift=0) const;
#ifdef XS_MPI
	void solve_spike_coupling(double& xi, double& xprev, int ir_end, int l, int dir) const;
	void solve_up(xs_array2d<cplx> x, int lms, int lme, int tag, MPI_Request **req, int lms_block, int nlm_block) const;
	void solve_dn(xs_array2d<cplx> x, int lms, int lme, int tag, MPI_Request **req, int lms_block, int nlm_block) const;
	void solve_dn_halo(xs_array2d<cplx> x, int lmstart, int lmend) const;
	void solve_finish(Spectral& X, xs_array2d<cplx> x, int tag, MPI_Request **req, int lms_block, int nlm_block) const;
#endif
};

void LinOpR::_alloc(int ir0, int ir1, int _nelem) {
	if (data) runerr("[LinOpR] operator already allocated");
	nelem = _nelem;
	ir_bci = ir0;	// store global boundaries
	ir_bco = ir1;
	mpi_interval(ir0, ir1);		// reduce to local boundaries
	irs = ir0;
	ire = ir1;
	int nr = ir_bco - ir_bci + 1;		// allocation of full matrices on each MPI process
	data = (double *) malloc( nelem * nr * sizeof(double) );
	if (data==0) runerr("[LinOpR] allocation error");
	data -= ir_bci*nelem;		// shift pointer for faster access.
	mx_idx = li;
}

void LinOpR::_free() {
	free(data + ir_bci*nelem);
	data = 0;	nelem = 0;
}

/// copy one radius from operator L
void LinOpR::copy_r(int ir, const LinOpR& L) {
  #ifdef XS_DEBUG
	if UNLIKELY(L.nelem != nelem) runerr("[LinOpR::copy_r] operators mismatch");
  #endif
	double* Mi = Mr(ir);
	double* Li = L.Mr(ir);
	for (unsigned l=0; l<nelem; l++) 	Mi[l] = Li[l];
}


void LinOp3ld::alloc(int ir0, int ir1, int _lmax) {
	lmax = _lmax;
	_alloc(ir0, ir1, lmax+3);
}

void LinOp3l::alloc(int ir0, int ir1, int _lmax) {
	lmax = _lmax;
	_alloc(ir0, ir1, 3*(lmax+1));
}

void LinOp5l::alloc(int ir0, int ir1, int _lmax) {
	lmax = _lmax;
	_alloc(ir0, ir1, 5*(lmax+1));
}

void LU3l::alloc(int ir0, int ir1, int _lmax, bool a2a)
{
	LinOp3l::alloc(ir0, ir1, _lmax);
	LUstorage::alloc(ir0, ir1, lmax, bandwidth(), a2a);
}

void LU5l::alloc(int ir0, int ir1, int _lmax, bool a2a)
{
	LinOp5l::alloc(ir0, ir1, _lmax);
	LUstorage::alloc(ir0, ir1, lmax, bandwidth(), a2a);
}

/// scale a line (radius) of the matrix by s
void LinOpR::scale(int ir, const double s) {
	if (s != 1.0) {
		double* Mi = Mr(ir);
		for (unsigned l=0; l<nelem; l++)  Mi[l] *= s;
	}
}

void LinOpR::zero(int ir) {
	for (unsigned l=0; l<nelem; l++)  Mr(ir)[l] = 0.0;
}

void LinOp3l::scale(int ir, const double* s) {
	double* Md = Mr(ir);
	for (int l=0; l<=lmax; l++) {
		for (int k=0; k<3; k++)  Md[3*l+k] *= s[l];
	}
}

void LinOp3l::copy_index(int l_dst, int l_src) {
	for (int j=ir_bci; j<=ir_bco; j++) {
		double* Md = Mr(j);
		for (int k=0; k<3; k++)  Md[3*l_dst+k] = Md[3*l_src+k];
	}
}

void LinOp3ld::copy_index(int l_dst, int l_src) {
	for (int j=ir_bci; j<=ir_bco; j++) {
		double *Md = Mr(j) + 2;
		Md[l_dst] = Md[l_src];
	}
}


void LinOp3ld::scale(int ir, const double* s) {
	runerr("[LinOp3ld::scale] scaling as a function of l not supported, use LinOp3l instead.");
}

void LinOp5l::scale(int ir, const double* s) {
	double* Md = Mr(ir);
	for (int l=0; l<=lmax; l++) {
		for (int k=0; k<5; k++)  Md[5*l+k] *= s[l];
	}
}

/// compute y = (L+s)*x
void LinOp3ld::apply(xs_array2d<cplx> x, xs_array2d<cplx> y, int lmstart, int lmend)
{
	int i0 = irs;
	int i1 = ire;
	thread_interval_rad(i0, i1);
	const long dist_x = x.get_dist();
	v2d* vx = (v2d*) x[i0];
	for (int j=i0; j<=i1; j++) {
		double *Md = Mr(j) + 2;
		v2d* vy = (v2d*) y[j];
		s2d Ml = vdup(Md[-2]);		s2d Mu = vdup(Md[-1]);
		LM_L_LOOP2( lmstart, lmend,  vy[lm] = Ml * vx[lm-dist_x] + vdup(Md[l]) * vx[lm] + Mu * vx[lm+dist_x];  )
		vx += dist_x;
	}
}


/// Multiply complex vector x by a Tri-diagonal matrix (l-dependant)
/// y = (M+s).x    (y and x MUST be different)
/// x MUST have elements istart-1 and iend+1 defined ! (boundary conditions).
void LinOp3l::apply(xs_array2d<cplx> x, xs_array2d<cplx> y, int lmstart, int lmend)
{
	int i0 = irs;
	int i1 = ire;
	thread_interval_rad(i0, i1);
	const long dist_x = x.get_dist();
	v2d* vx = (v2d*) x[i0];
	for (int j=i0; j<=i1; j++) {
		v2d* vy = (v2d*) y[j];		double *Mj = Mr(j) + 1;
		LM_L_LOOP2( lmstart, lmend,  vy[lm] = vdup(Mj[3*l-1]) * vx[lm-dist_x] + vdup(Mj[3*l]) * vx[lm] + vdup(Mj[3*l+1]) * vx[lm+dist_x];  )
		vx += dist_x;
	}
}

/// Multiply complex vector x by a Penta-diagonal matrix (l-dependant)
/// y = (M+s).x    (y and x MUST be different)
/// x MUST have elements istart-2 and iend+2 defined ! (boundary conditions).
void LinOp5l::apply(xs_array2d<cplx> x, xs_array2d<cplx> y, int lmstart, int lmend)
{
	int i0 = irs;
	int i1 = ire;
	thread_interval_rad(i0, i1);
	const long dist_x = x.get_dist();	// distance between consecutive radial shells
	v2d* vx = (v2d*) x[i0];
	for (int j=i0; j<=i1; j++) {
		double *Mj = Mr(j) + 2;
		v2d* vy = (v2d*) y[j];
		LM_L_LOOP2( lmstart, lmend,  vy[lm] = vdup(Mj[5*l-2]) * vx[lm -2*dist_x] + vdup(Mj[5*l-1]) * vx[lm -dist_x] + vdup(Mj[5*l]) * vx[lm] + vdup(Mj[5*l+1]) * vx[lm +dist_x] + vdup(Mj[5*l+2]) * vx[lm +2*dist_x];  )
		vx += dist_x;
	}
}

void LinOp3ld::set_op(const double a, const double b, const LinOp3ld& L)
{
	for (int j=ir_bci; j<=ir_bco; j++) {
		double* Mj = Mr(j);
		double* Lj = L.Mr(j);
		Mj[0] = b*Lj[0];
		Mj[1] = b*Lj[1];
		for (int l=2; l<nelem; l++)
			Mj[l]   = a + b*Lj[l];
	}
}

void LinOp3l::set_op(double a, double b, const LinOp3l& L)
{
	for (int j=ir_bci; j<=ir_bco; j++) {
		double* Mj = Mr(j);
		double* Lj = L.Mr(j);
		for (int l=0; l<=lmax; l++) {
			Mj[3*l]   = b*Lj[3*l];
			Mj[3*l+1] = b*Lj[3*l+1] + a;
			Mj[3*l+2] = b*Lj[3*l+2];
		}
	}
}

void LinOp3l::set_op(double a, double b, const LinOp3ld& L)
{
	for (int j=ir_bci; j<=ir_bco; j++) {
		double* Mj = Mr(j);
		double lo = L.lo(j);
		double up = L.up(j);
		for (int l=0; l<=lmax; l++) {
			Mj[3*l]   = b*lo;
			Mj[3*l+1] = b*L.di(j,l) + a;
			Mj[3*l+2] = b*up;
		}
	}
}

void LinOp5l::set_op(double a, double b, const LinOp5l& L)
{
	for (int j=ir_bci; j<=ir_bco; j++) {
		double* Mj = Mr(j);
		double* Lj = L.Mr(j);
		for (int l=0; l<=lmax; l++) {
			Mj[5*l]   = b*Lj[5*l];
			Mj[5*l+1] = b*Lj[5*l+1];
			Mj[5*l+2] = b*Lj[5*l+2] + a;
			Mj[5*l+3] = b*Lj[5*l+3];
			Mj[5*l+4] = b*Lj[5*l+4];
		}
	}
}

void LinOp5l::set_op(double a, const LinOp3ld& La, double b, const LinOp5l& Lb)
{
	for (int j=ir_bci; j<=ir_bco; j++) {
		double* Mj = Mr(j);
		double* Aj = La.Mr(j);
		double* Bj = Lb.Mr(j);
		for (int l=0; l<=lmax; l++)	{
			Mj[5*l]   = b*Bj[5*l];
			Mj[5*l+1] = b*Bj[5*l+1] + a*Aj[0];
			Mj[5*l+2] = b*Bj[5*l+2] + a*Aj[2+l];
			Mj[5*l+3] = b*Bj[5*l+3] + a*Aj[1];
			Mj[5*l+4] = b*Bj[5*l+4];
		}
	}
}

void LinOp5l::set_op(double a, const LinOp3l& La, double b, const LinOp5l& Lb)
{
	for (int j=ir_bci; j<=ir_bco; j++) {
		double* Mj = Mr(j);
		double* Aj = La.Mr(j);
		double* Bj = Lb.Mr(j);
		for (int l=0; l<=lmax; l++)	{
			Mj[5*l]   = b*Bj[5*l];
			Mj[5*l+1] = b*Bj[5*l+1] + a*Aj[3*l];
			Mj[5*l+2] = b*Bj[5*l+2] + a*Aj[3*l+1];
			Mj[5*l+3] = b*Bj[5*l+3] + a*Aj[3*l+2];
			Mj[5*l+4] = b*Bj[5*l+4];
		}
	}
}

void LinOp5l::set_op(double a, const LinOp5l& La, double b, const LinOp5l& Lb)
{
	for (int j=ir_bci; j<=ir_bco; j++) {
		double* Mj = Mr(j);
		double* Aj = La.Mr(j);
		double* Bj = Lb.Mr(j);
		for (int l=0; l<nelem; l++)	Mj[l] = a*Aj[l] + b*Bj[l];
	}
}

template <class T>
void _LinOp3l<T>::precalc_solve_common() {
  #ifdef XS_MPI
	if (ir_middle > 0) {	// use X-solver
		if (flags & LINOP_BCI_L0_ONLY) {		// only l=0 exists at inner boundary; used for r==0, and freely-rotating inner-core
			_precalc_solve(ir_bci,   ir_middle-1, 0, 0,    1);		// l=0
			_precalc_solve(ir_bci+1, ir_middle-1, 1, lmax, 1);		// l>0
		} else _precalc_solve(ir_bci, ir_middle-1, 0, lmax, 1);	// split solver
		if (flags & LINOP_BCO_L0_ONLY) {		// only l=0 exists at outer boundary; used for freely-rotating mantle
			_precalc_solve(ir_middle, ir_bco,   0, 0,    -1);	// l=0
			_precalc_solve(ir_middle, ir_bco-1, 1, lmax, -1);	// l>0
		} else _precalc_solve(ir_middle, ir_bco, 0, lmax, -1);	// all l
	} else
  #endif
	{
		if (flags & (LINOP_BCI_L0_ONLY|LINOP_BCO_L0_ONLY)) {	// extension to boundaries only for l=0
			const int i0_l = (flags & LINOP_BCI_L0_ONLY) ? ir_bci+1 : ir_bci;
			const int i1_l = (flags & LINOP_BCO_L0_ONLY) ? ir_bco-1 : ir_bco;
			_precalc_solve(ir_bci, ir_bco, 0, 0,    dir_solve);	// l=0
			_precalc_solve(i0_l,   i1_l,   1, lmax, dir_solve);	// l>0
		} else _precalc_solve(ir_bci, ir_bco, 0, lmax, dir_solve);	// all l
	}
}

void LinOp3ld::precalc_solve() {
	precalc_solve_common();

	if (flags & LINOP_BCI_L0_ONLY) {
		for (int l=1; l<=lmax; l++)  set_di(ir_bci, l, 0.0);		// l>0, r=0  => trick to force to zero when solving
	}
	if (flags & LINOP_BCO_L0_ONLY) {
		for (int l=1; l<=lmax; l++)  set_di(ir_bco, l, 0.0);		// l>0  => trick to force to zero when solving
	}

	#ifdef XS_MPI
	if ((ire_solve == ir_middle-1) || (ire_solve == ir_middle)) {	// the coupling processes need to compute the coupling matrix
		if (C==0) C = (double*) malloc(sizeof(double)*(lmax+1)*3);
		double c0 = offdi(ir_middle-1,1);
		double c1 = offdi(ir_middle,-1);
		for (int l=0; l<=lmax; l++) {
			double C01 = c0 * di(ir_middle-1, l);	// C01
			double C10 = c1 * di(ir_middle,   l);	// C10
			// invert 2x2 matrix [[1, C01],[C10, 1]]
			double detC = 1.0 / (1.0 - C01*C10);	// inverse of determinant
			C[3*l+1-dir_solve]   = -C01 * detC;		// off-diagonal, line 0
			C[3*l+1] = detC;			// value on the diagonal (same for each line)
			C[3*l+1+dir_solve] = -C10 * detC;		// off-diagonal, line 1
		}
	}
	#endif
}

void LU3l::precalc_solve() {
	precalc_solve_common();

	if (flags & LINOP_BCI_L0_ONLY) {
		for (int l=1; l<=lmax; l++) {		// l>0, r=0  => trick to force to zero when solving
			set_lo(ir_bci, l, 0.0);
			set_di(ir_bci, l, 0.0);
			set_up(ir_bci, l, 0.0);
		}
	}
	if (flags & LINOP_BCO_L0_ONLY) {
		for (int l=1; l<=lmax; l++) {		// l>0, r=0  => trick to force to zero when solving
			set_lo(ir_bco, l, 0.0);
			set_di(ir_bco, l, 0.0);
			set_up(ir_bco, l, 0.0);
		}
	}

	#ifdef XS_MPI
	if ((ire_solve == ir_middle-1) || (ire_solve == ir_middle)) {	// the coupling processes need to compute the coupling matrix
		if (C==0) C = (double*) malloc(sizeof(double)*(lmax+1)*3);
		for (int l=0; l<=lmax; l++) {
			double C01 = M(ir_middle-1,l)[1];	// C01
			double C10 = M(ir_middle,l)[-1];	// C10
			// invert 2x2 matrix [[1, C01],[C10, 1]]
			double detC = 1.0 / (1.0 - C01*C10);	// inverse of determinant
			C[3*l+1-dir_solve]   = -C01 * detC;		// off-diagonal, line 0
			C[3*l+1] = detC;			// value on the diagonal (same for each line)
			C[3*l+1+dir_solve] = -C10 * detC;		// off-diagonal, line 1
		}
	}
	#endif

	#ifdef XS_MPI
	if (dist_flags & SOLVE_UPDN)
	#endif
	{
		// copy to L and U arrays to optimize cache usage during solve
		const int dir = dir_solve;
		const int n = abs(ire_solve - irs_solve);
		const int i0 = (irs_solve < ire_solve) ? irs_solve : ire_solve;
		for (int i=0; i<=n; i++) {
			int iL = i;
			int iU = n-i;
			if (dir < 0)  { iL=iU;  iU=i; }
			for (int l=0; l<=lmax; l++) {
				double* Mil = M(i+i0,l);
				L[iL*nelem_L + 2*l]   = Mil[0];
				L[iL*nelem_L + 2*l+1] = Mil[-dir];
				U[iU*nelem_U + l]     = Mil[dir];
			}
		}
		#ifdef XS_MPI
		int iU = n+1;	// add solve data for ghost shells, to avoid a communication step in ::solve_finish()
		int i = (dir > 0) ? -1 : iU;
		if ((i+i0 >= ir_bci) && (i+i0 <= ir_bco)) {
			for (int l=0; l<=lmax; l++) {
				double* Mil = M(i+i0,l);
				U[iU*nelem_U + l]     = Mil[dir];
			}
		}
		#endif
	}
}

void LinOp_solvable::init_solve(const Spectral& X, int bidir_split)			// some one-time initialization, done at allocation time.
{
	irs_solve = ir_bci;
	ire_solve = ir_bco;

	#ifdef XS_MPI
	// solves span the whole shared-memory space
	share_dn = -1;		// negative means no communication needed.
	share_up = -1;
	dist_flags = 0;
	#ifndef XS_MPI_ALL2ALL
	if (irs_solve < irs_shared) {
		irs_solve = irs_shared;
		share_dn = i_mpi_net - 1;
	}
	if (ire_solve > ire_shared) {
		ire_solve = ire_shared;
		share_up = i_mpi_net + 1;
	}
	#endif
	if (irs_solve <= ire_solve)	dist_flags |= SOLVE_UPDN;	// process is active in the solve

	dir_solve = 1;	// normal solve direction
	ir_middle = -1;	// X-solver deactivated by ir_middle = -1
	if ((bidir_split > 0) && (n_mpi_net > 1)) {		// enable X-solver
		ir_middle = bidir_split;
		if (irs_solve >= ir_middle) dir_solve = -1;		// reverse direction for upper half of processes.
		print_debug("[X-solver] process %d solves in direction %d (split at ir=%d)\n", i_mpi, dir_solve, ir_middle);
	} else if ((bidir_split != 0) && (n_mpi_shared > 1)) {	// enable bidirectionnal V-solver
		if (i_mpi_shared >= n_mpi_shared/2) dir_solve = -1;	// high ranks in shared-memory groups solve in opposite direction.
		print_debug("[V-solver] process %d solves in direction %d\n", i_mpi, dir_solve);
	}

	const bool solve_halo = true;			// whether we solve for halo instead of transmitting back through MPI.
	// setup the "solve_finish" phase:
	if ((X.ir_bco < irs_shared)||(X.ir_bci > ire_shared)) {
		dist_flags = SOLVE_OFF;		// The process is never involved with this field.
	} else if (dir_solve > 0) {
		if (ir_bci != X.ir_bci) {		// fix the first shell.
			if (irs_shared == X.ir_bci+1) dist_flags |= SOLVE_SYNC_BC_SND;		// send irs_shared == irs_solve
			if (ire_shared == X.ir_bci)   dist_flags |= SOLVE_SYNC_BC_RCV;		// recv ire_shared+1
		}
		if (solve_halo) {
			if  (ir_bco != X.ir_bco) {		// fix the last shell
				if (ire_shared == X.ir_bco-1) dist_flags |= SOLVE_SYNC_SND;
				if (irs_shared == X.ir_bco)   dist_flags |= SOLVE_SYNC_RCV;
			}
		} else {
			if (irs_shared > ir_bci)	dist_flags |= SOLVE_SYNC_RCV; 	// we are not the first block
			if ((ire_shared < X.ir_bco)&&(ire_shared >= ir_bci))  dist_flags |= SOLVE_SYNC_SND;		// we are not the last block
		}
	} else {	// dir < 0
		if (ir_bco != X.ir_bco) {
			if (ire_shared == X.ir_bco-1) dist_flags |= SOLVE_SYNC_BC_SND;		// send ire_shared == irs_solve
			if (irs_shared == X.ir_bco)   dist_flags |= SOLVE_SYNC_BC_RCV;		// recv irs_shared-1
		}
		if (solve_halo) {
			if (ir_bci != X.ir_bci) {
				if (irs_shared == X.ir_bci+1) dist_flags |= SOLVE_SYNC_SND;
				if (ire_shared == X.ir_bci)   dist_flags |= SOLVE_SYNC_RCV;
			}
		} else {
			if (ire_shared < ir_bco)   dist_flags |= SOLVE_SYNC_RCV;		// we are not the first block
			if ((irs_shared > X.ir_bci)&&(irs_shared <= ir_bco))   dist_flags |= SOLVE_SYNC_SND;		// we are not the last block.
		}
	}
	// for LU5l, sometimes some ordering is needed:
	if ((irs_solve == ire_solve) && (dist_flags & SOLVE_SYNC_SND))	dist_flags |= SOLVE_SYNC_RCV_1;

	if (dir_solve < 0) {
		int t=share_up;	 share_up=share_dn;  share_dn=t;	// adjust sending with direction of solve
	}

	if (solve_halo && (dist_flags & SOLVE_UPDN) && (share_dn >= 0))	dist_flags |= SOLVE_DN_HALO;
	#endif

	if (dir_solve < 0) {	// reverse solve direction.
		int t=irs_solve;	irs_solve=ire_solve; ire_solve=t;
	}
}


/// PARTIAL decomposition of tri-banded matrix M. All divisions happen here.
/// only the diagonal element is modified !!!
void LinOp3ld::_precalc_solve(int i0, int i1, const int l0, const int l1, const int dir)
{
	#ifdef XS_DEBUG
	static int ndd_old = 0;
	double lu_max = 0.0;
	int ndd = 0;			// non-diagonally dominant count
	int ir_max, l_max;
	#endif
	if (dir < 0) {	int t = i0;		i0 = i1;	i1 = t;	}	// swap i0 and i1
	for (int j=i0; j != i1+dir; j+=dir) {			//Decomposition.
		double dloc = fabs(di(j,l0));
		int lloc = l0;
		for (int l=l0; l<=l1; l++) {
			double p = di(j,l);
			double d = fabs(p);
			if (j != i0) {
				p -= offdi(j,-dir) * offdi(j-dir, dir) * di(j-dir, l);
			}
			if (d < dloc) {
				dloc = d;	lloc = l0;
			}
			set_di(j,l, 1.0/p);
		}
		#ifdef XS_DEBUG	
		double lu = 0.0;
		if (j != i0) lu += fabs(offdi(j, -dir));
		if (j != i1) lu += fabs(offdi(j, dir));
		if (dloc < lu) {		// not diagonally dominant -- see https://en.wikipedia.org/wiki/Diagonally_dominant_matrix
			lu = lu/dloc;
			ndd ++;
			//PRINTF0("lu/d [ir%d,l=%d] = %g\n",j,lloc, lu);
			if (lu > lu_max) {
				lu_max = lu;		l_max = lloc;		ir_max = j;
			}
		}
		#endif
	}
	#ifdef XS_DEBUG
	if (ndd > ndd_old) {
		PRINTF0(COLOR_WRN "[LinOp3ld::precalc_solve] Warning: %d non diagonal-dominant lines, max(lu/diag) = %g > 1 @[ir=%d, l=%d]" COLOR_END "\n", ndd, lu_max, ir_max, l_max);
		ndd_old = ndd;
	}
	#endif
}

/// Full decomposition of tri-banded matrix M. All divisions happen here.
void LinOp3l::_precalc_solve(int i0, int i1, const int l0, const int l1, const int dir)
{
	#ifdef XS_DEBUG
	static int ndd_old = 0;
	double lu_max = 0.0;
	int ndd = 0;			// non-diagonally dominant count
	int ir_max, l_max;
	#endif
	if (dir < 0) {	int t = i0;		i0 = i1;	i1 = t;	}	// swap i0 and i1
	for (int j=i0; j != i1+dir; j+=dir) {			//Decomposition.
		for (int l=l0; l<=l1; l++) {
			double p = M(j,l)[0];
			double lu = 0.0;
			#ifdef XS_DEBUG
			double d = fabs(p);
			#endif
			if (j != i0) {
				p -= M(j,l)[-dir] * M(j-dir,l)[dir] * M(j-dir,l)[0];
				lu += fabs(M(j,l)[-dir]);
			}
			M(j,l)[0] = 1.0/p;
			#ifdef XS_DEBUG
			if (j != i1) lu += fabs(M(j,l)[dir]);
			if (d < lu) {
				lu = lu/d;
				ndd ++;
				//PRINTF0("lu/d [ir%d,l=%d] = %g\n",j,l, lu);
				if (lu > lu_max) {
					lu_max = lu;		l_max = l;		ir_max = j;
				}
			}
			#endif
		}
	}
	#ifdef XS_DEBUG
	if (ndd > ndd_old) {
		PRINTF0(COLOR_WRN "[LU3l::precalc_solve] Warning: %d non diagonal-dominant lines, max(lu/diag) = %g > 1 @[ir=%d, l=%d]" COLOR_END "\n", ndd, lu_max, ir_max, l_max);
		ndd_old = ndd;
	}
	#endif

	for(int i=i0; i != i1+dir; i += dir) {
		for (int l=l0; l<=l1; l++) {
			double* Mil = M(i,l);
			double p_1 = Mil[0];
			Mil[-1] *= p_1;
			Mil[1]  *= p_1;
		}
	}
}

void LU5l::_precalc_solve(int i0, int i1, const int dir)
{
	#ifdef XS_DEBUG
	static int ndd_old = 0;
	double lu_max = 0.0;
	int ndd = 0;			// non-diagonally dominant count
	int ir_max, l_max;
	#endif

	if (dir < 0) {	int t = i0;		i0 = i1;	i1 = t;	}	// swap i0 and i1

	int i = i0+dir;
		for (int l=0; l<=lmax; l++) {
			double pp = M(i-dir,l)[0];
			double d = fabs(pp);
			pp = 1.0/pp;
			double lu = fabs(M(i-dir,l)[dir]) + fabs(M(i-dir,l)[2*dir]);
			M(i-dir,l)[0] = pp;
			#ifdef XS_DEBUG
			if (d < lu) {
				lu = lu/d;
				ndd ++;
				//PRINTF0("lu/d [ir%d,l=%d] = %g\n",i-dir,l, lu);
				if (lu > lu_max) {
					lu_max = lu;		l_max = l;		ir_max = i-dir;
				}
			}
			#endif
			double bet = M(i,l)[-dir] * pp;
			double p = M(i,l)[0];
			d = fabs(p);
			p -= bet * M(i-dir,l)[dir];
			lu = fabs(M(i,l)[-dir]) + fabs(M(i,l)[dir]) + fabs(M(i,l)[2*dir]);
			M(i,l)[dir] -= bet * M(i-dir,l)[2*dir];
			M(i,l)[0] = 1.0/p;
			#ifdef XS_DEBUG
			if (d < lu) {
				lu = lu/d;
				ndd ++;
				//PRINTF0("lu/d [ir%d,l=%d] = %g\n",i,l, lu);
				if (lu > lu_max) {
					lu_max = lu;		l_max = l;		ir_max = i;
				}
			}
			#endif
		}
	for(int i=i0+2*dir; i != i1+dir; i+=dir) {
		for (int l=0; l<=lmax; l++) {
			double alp = M(i,l)[-2*dir] * M(i-2*dir,l)[0];
			double t = M(i,l)[-dir] - alp * M(i-2*dir,l)[dir];
			double lu = fabs(M(i,l)[-2*dir]) + fabs(M(i,l)[-dir]);
			M(i,l)[-dir] = t;
			double bet = t * M(i-dir,l)[0];
			double p = M(i,l)[0];
			#ifdef XS_DEBUG
			double d = fabs(p);
			#endif
			p -= bet * M(i-dir,l)[dir] + alp * M(i-2*dir,l)[2*dir];
			lu += fabs(M(i,l)[dir]) + fabs(M(i,l)[2*dir]);
			M(i,l)[dir] -= bet * M(i-dir,l)[2*dir];
			M(i,l)[0] = 1.0/p;
			#ifdef XS_DEBUG
			if (d < lu) {
				lu = lu/d;
				ndd ++;
				//PRINTF0("lu/d [ir%d,l=%d] = %g\n",i,l, lu);
				if (lu > lu_max) {
					lu_max = lu;		l_max = l;		ir_max = i;
				}
			}
			#endif
		}
	}
	#ifdef XS_DEBUG
	if (ndd > ndd_old) {
		PRINTF0(COLOR_WRN "[LU5l::_precalc_solve] Warning: %d non diagonal-dominant lines, max(lu/diag) = %g > 1 @[ir=%d, l=%d]" COLOR_END "\n", ndd, lu_max, ir_max, l_max);
		ndd_old = ndd;
	}
	#endif

	for(int i=i0; i != i1+dir; i+=dir) {
		for (int l=0; l<=lmax; l++) {
			double* Mil = M(i,l);
			double p_1 = Mil[0];
			Mil[-2] *= p_1;
			Mil[-1] *= p_1;
			Mil[1]  *= p_1;
			Mil[2]  *= p_1;
		}
	}
}

#ifdef XS_MPI
// test OK
void matrix_inv_2x2(const double* A, double* A_1)
{
	double det_1 = 1.0/(A[0]*A[3] - A[1]*A[2]);
	double a00 = A[0];
	double a11 = A[3];
	A_1[0] = a11*det_1;
	A_1[1] = -A[1]*det_1;
	A_1[2] = -A[2]*det_1;
	A_1[3] = a00*det_1;
}

// test OK
void matrix_mul_2x2(const double* A, const double* B, double* C)
{
	double c00 = A[0]*B[0] + A[1]*B[2];
	double c01 = A[0]*B[1] + A[1]*B[3];
	double c10 = A[2]*B[0] + A[3]*B[2];
	double c11 = A[2]*B[1] + A[3]*B[3];
	C[0] = c00;		C[1] = c01;		C[2] = c10;		C[3] = c11;
}

void matrix_1_minus(double* A)
{
	A[0] = 1.0 - A[0];
	A[1] = - A[1];
	A[2] = - A[2];
	A[3] = 1.0 - A[3];
}

// offdi0 and offdi1 are the two off-diagonale blocks, as 2x2 matrices. The diagonal blocks are the Identiy matrix.
// test OK
void matrix_inv_4x4_id_block(const double* offdi0, const double* offdi1, double* A_1)
{
	double a[4];
	double d[4];
	matrix_mul_2x2(offdi0,offdi1, a);
	matrix_mul_2x2(offdi1,offdi0, d);
	matrix_1_minus(a);
	matrix_1_minus(d);
	matrix_inv_2x2(a,a);
	matrix_inv_2x2(d,d);
	A_1[0] = a[0];		A_1[1] = a[1];
	A_1[4] = a[2];		A_1[5] = a[3];
											A_1[10] = d[0];		A_1[11] = d[1];
											A_1[14] = d[2];		A_1[15] = d[3];
	matrix_mul_2x2(a,offdi0,a);		matrix_mul_2x2(d,offdi1,d);
											A_1[2] = -a[0];		A_1[3] = -a[1];
											A_1[6] = -a[2];		A_1[7] = -a[3];
	A_1[8] = -d[0];		A_1[9] = -d[1];
	A_1[12] = -d[2];	A_1[13] = -d[3];
}

// x[0] is the boundary value, x[-dir] is the previous value in solve up order.
void LU5l::solve_spike_coupling(double& xi, double& xprev, int ir_end, int l, int dir) const
{
	const int i=ir_end;
	// solve up starting from i-1, knowing that x[j] = 0 for j<i-1
		//x[-dir] = M(i-dir,l)[0] * x[-dir];	// this is already done by _precalc_solve()
		//x[0] = M(i,l)[0] * x[0] - M(i,l)[-dir]*x[-dir];	// x[0] already multiplied by M(i,l)[0]  ==>  replace this line by the next one
	//	xi -= M(i,l)[-dir] * xprev;			//x[0] -= M(i,l)[-dir]*x[-dir];
	// solve dn to i-1
		xprev -= M(i-dir,l)[dir] * xi;		//x[-dir] -= M(i-dir,l)[dir] * x[0];
}
#endif

void LU5l::precalc_solve()
{
  #ifdef XS_MPI
	if (ir_middle > 0) {		// use X-solver
		_precalc_solve(ir_bci, ir_middle-1, 1);
		_precalc_solve(ir_middle, ir_bco, -1);
	} else
  #endif
		_precalc_solve(ir_bci, ir_bco, dir_solve);

	#ifdef XS_MPI
	if ((ire_solve == ir_middle-1) || (ire_solve == ir_middle)) {	// the coupling processes need to compute the coupling matrix
		if (C==0) C = (double*) malloc(sizeof(double)*(lmax+1)*8);		// 4x4 matrix = 16 values for each l; but we keep only one half on each side of the interface
		for (int l=0; l<=lmax; l++) {
			double spike0[4];
			double spike1[4];
			double C_l[16];		// the matrix
			spike0[0] = M(ir_middle-2,l)[2];		//  M[-2,2]   0
			spike0[1] = 0.0;
			spike0[2] = M(ir_middle-1,l)[1];		//  M[-1,1]	  M[-1,2]
			spike0[3] = M(ir_middle-1,l)[2];
			solve_spike_coupling(spike0[2], spike0[0], ir_middle-1, l, 1);
			solve_spike_coupling(spike0[3], spike0[1], ir_middle-1, l, 1);

			spike1[0] = M(ir_middle, l)[-2];
			spike1[1] = M(ir_middle, l)[-1];
			spike1[2] = 0.0;
			spike1[3] = M(ir_middle+1,l)[-2];
			solve_spike_coupling(spike1[0], spike1[2], ir_middle, l, -1);
			solve_spike_coupling(spike1[1], spike1[3], ir_middle, l, -1);

			matrix_inv_4x4_id_block(spike0, spike1, C_l);		// this matrix has been checked to be OK

			// to 2nd column, we add 1st column times -M(ir_middle-2,l)[1]
			for (int k=0; k<4; k++) C_l[4*k + 1] -= M(ir_middle-2,l)[1] * C_l[4*k];
			// to 3rd column, we add 4th column time -M(ir_middle+1,l)[-1]
			for (int k=0; k<4; k++) C_l[4*k + 2] -= M(ir_middle+1,l)[-1] * C_l[4*k + 3];
			// ALL OK
			for (int k=0; k<8; k++)	C[8*l + (k^(2-2*dir_solve))] = C_l[k  +  4-4*dir_solve];	// copy the relevant part
		}
	}
	#endif

	#ifdef XS_MPI
	if (dist_flags & SOLVE_UPDN)
	#endif
	{
		const int dir = dir_solve;
		// copy to L and U arrays to optimize cache usage during solve
		const int n = abs(ire_solve - irs_solve);
		int i0 = (irs_solve < ire_solve) ? irs_solve : ire_solve;
		for (int i=0; i<=n; i++) {
			long iL = i;
			long iU = n-i;
			if (dir < 0) { iL=iU;	iU=i; }
			iL *= nelem_L;		iU *= nelem_U;
			for (int l=0; l<=lmax; l++) {
				double* Mil = M(i+i0,l);
				L[iL + 3*l]   = Mil[0];
				L[iL + 3*l+1] = Mil[-dir];
				L[iL + 3*l+2] = Mil[-2*dir];
				U[iU + 2*l]   = Mil[dir];
				U[iU + 2*l+1] = Mil[2*dir];
			}
		}
		#ifdef XS_MPI
		for (int iU = n+1; iU <= n+2; iU++) {	// add solve data for ghost shells, to avoid a communication step in ::solve_finish()
			int i = (dir > 0) ? n-iU : iU;
			if ((i+i0 >= ir_bci) && (i+i0 <= ir_bco)) {
				for (int l=0; l<=lmax; l++) {
					double* Mil = M(i+i0,l);
					U[iU*nelem_U + 2*l]   = Mil[dir];
					U[iU*nelem_U + 2*l+1] = Mil[2*dir];
				}
			}
		}
		#endif
	}
}

/// Solve M x = b, where b is initially stored in x.
/// x can have elements istart-1 and iend+1 set (if not => same as cTriSolve).
/// iend - istart >= 2  (ie at least 3 elements)
void LinOp3ld::solve(xs_array2d<cplx> x, int lmstart, int lmend, int lm_shift) const
{
	const int dir = dir_solve;
	const int ir0 = irs_solve;
	const int ir1 = ire_solve;
	const long vx_dist = x.get_dist() * dir;

	// TODO: blocking in lm for better cache reuse

	v2d* vx = (v2d*) x[ir0] - lm_shift;		// shift origin, allowing x to hold only part of the whole lm coefficients.
	for(int j = ir0 ; j != ir1+dir; j+=dir) {		// Forward substitution  + handle lower boundary : b(j) -> b(j) - Ml(j).x(j-1)
		double* Md = Mr(j) + 2;
	  #if VSIZE >= 4
		const unsigned short* li_ = mx_idx;
		v4d Ml = vall4(offdi(j,-dir));
		for (int lm=lmstart; lm<=lmend; lm+=2) {
			*(v4d*)(vx+lm)  = (vread4(vx+lm, 0) - Ml * vread4(vx + lm - vx_dist, 0)) * vdup_x2(Md + li_[lm],   Md + li_[lm+1]);
		}
	  #else
		s2d Ml = vdup(offdi(j,-dir));
		LM_L_LOOP2( lmstart, lmend,  vx[lm] = (vx[lm] - Ml*vx[lm-vx_dist]) * vdup(Md[l]); )		// FMA, MUL
	  #endif
		vx += vx_dist;
	}	// j = ir1+dir;
	for (int j = ir1; j != ir0-dir; j-=dir) {	// Back-substitution  + handle upper boundary : b(j) -> b(j) - Mu(j).x(j+1)
		vx -= vx_dist;
		double* Md = Mr(j) + 2;
	  #if VSIZE >= 4
		const unsigned short* li_ = mx_idx;
		v4d Mu = vall4(offdi(j,dir));
		for (int lm=lmstart; lm<=lmend; lm+=2) {
			*(v4d*)(vx+lm)  -= ( Mu * vread4(vx + lm + vx_dist, 0) ) * vdup_x2(Md + li_[lm],   Md + li_[lm+1]);
		}
	  #else
		s2d Mu = vdup(offdi(j,dir));
		LM_L_LOOP2( lmstart, lmend,  vx[lm] -= (Mu *vx[lm+vx_dist]) * vdup(Md[l]);  )		// MUL, FMA
	  #endif
	}
}


#ifdef XS_MPI
/// Solve M x = b, where b and x can be the same array.
/// x can have elements istart-1 and iend+1 set (if not => same as cTriSolve).
/// iend - istart >= 2  (ie at least 3 elements) 
void LinOp3ld::solve_up(xs_array2d<cplx> x, int lmstart, int lmend, int tag, MPI_Request **req, int lms_block, int nlm_block) const
{
	if ((dist_flags & SOLVE_UPDN) == 0) return;		// reject processes not involved.

	const int dir = dir_solve;
	int j = irs_solve;
	const int i1 = ire_solve;

	if (share_dn >= 0) {		// we are not the first block
		#pragma omp master
		{
			print_debug("solve_up: recv from %d (tag %d)\n",share_dn, tag);
			if (nlm_block <= 256) PREFETCH_BLOCK_W(x[j-dir]+lms_block, x[j-dir]+lms_block+nlm_block);		// prefetch buffer to receive MPI data
			MPI_Recv (x[j-dir]+lms_block, 2*nlm_block, MPI_DOUBLE, share_dn, tag, comm_net, MPI_STATUS_IGNORE);		// blocking receive
		}
		if (lmend-lmstart < 128) PREFETCH_BLOCK_W(x[j]+lmstart, x[j]+lmend);		// prefetch data for each thread
		#pragma omp barrier
	}
	for ( ; j != i1+dir; j += dir) {
		v2d* vx = (v2d*) x[j];		v2d* vxl = (v2d*) x[j-dir];
		const unsigned short * li_ = mx_idx;
		double* Md = Mr(j) + 2;
		#if VSIZE >= 4
		v4d Ml = vall4(offdi(j,-dir));
		for (int lm=lmstart; lm<=lmend; lm+=2) {
			*(v4d*)(vx+lm)  = (vread4(vx+lm, 0) - Ml * vread4(vxl + lm, 0)) * vdup_x2(Md + li_[lm],   Md + li_[lm+1]);
		}
		#else
		s2d Ml = vdup(offdi(j,-dir));	
		for (int lm=lmstart; lm<=lmend; lm++) {
			vx[lm] = (vx[lm] - Ml*vxl[lm]) * vdup(Md[li_[lm]]);		// FMA, MUL
		}
		#endif
	}	// j = i1+dir;
	if (share_up >= 0) {		// we are not the last block
		if (C==0) {
		#pragma omp barrier
		#pragma omp master
		{
			print_debug("solve_up: send to %d (tag %d)\n",share_up, tag);
			MPI_Isend(x[i1]+lms_block, 2*nlm_block, MPI_DOUBLE, share_up, tag, comm_net, *req);
			*req += 1;
		}
		}
	} else {	// we are the last block, start the solve-down phase: allows to reuse data likely still in cache.
		const int i2 = irs_solve;
		// handle upper boundary : x(j) -> x(j) - Mu(j).x(j+1)
		for(int j=i1 ; j != i2-dir; j -= dir) {		// Back-substitution.
			v2d* vx = (v2d*) x[j];		v2d* vxu = (v2d*) x[j+dir];
			s2d Mu = vdup(offdi(j, dir));	double* Md = Mr(j) + 2;
			LM_L_LOOP2( lmstart, lmend,  vx[lm] -= ( Mu *vxu[lm] ) * vdup(Md[l]);  )	// MUL, FMA
		}
		if (share_dn >= 0) {		// we are not the first block
			#pragma omp barrier
			#pragma omp master
			{
				print_debug("solve_up: send to %d (tag %d)\n",share_dn, tag+2);
				MPI_Isend(x[i2]+lms_block, 2*nlm_block, MPI_DOUBLE, share_dn, tag, comm_net, *req);
				*req += 1;
			}
		}
	}
}

void LinOp3ld::solve_dn(xs_array2d<cplx> x, int lmstart, int lmend, int tag, MPI_Request **req, int lms_block, int nlm_block) const
{
	if ((dist_flags & SOLVE_UPDN) == 0) return;		// reject processes not involved.

	const int dir = dir_solve;
	int j = ire_solve;
	const int i1 = irs_solve;

	if (share_up >= 0) {		// we are not the last block.
		#pragma omp master
		{
			if (C==0) {
				print_debug("solve_dn: recv from %d (tag %d)\n",share_up, tag);
				MPI_Recv (x[j+dir]+lms_block, 2*nlm_block, MPI_DOUBLE, share_up, tag, comm_net, MPI_STATUS_IGNORE);		// blocking receive
			} else {	// X-solver, at interface
				print_debug("solve_dn: send-recv to/from %d (tag %d)\n",share_up, tag);
				MPI_Sendrecv (x[ire_solve]    +lms_block, 2*nlm_block, MPI_DOUBLE, share_up, tag,		// blocking send-receive to reuse both send and receive buffer
					      x[ire_solve+dir]+lms_block, 2*nlm_block, MPI_DOUBLE, share_up, tag, comm_net, MPI_STATUS_IGNORE);
			}
		}
		#pragma omp barrier
		if (C) {		// apply 2x2 coupling matrix before solving down:
			const unsigned short * li_ = mx_idx;
			xs_array2d<v2d> vx = x;
			for (int lm=lmstart; lm<=lmend; lm++) {
				int la = 3*li_[lm];
				v2d x0 = vdup(C[la+1]) * vx[ire_solve][lm]      +  vdup(C[la])   * vx[ire_solve+dir][lm];
				v2d x1 = vdup(C[la+1]) * vx[ire_solve+dir][lm]  +  vdup(C[la+2]) * vx[ire_solve][lm];
				vx[ire_solve][lm] =     x0;
				vx[ire_solve+dir][lm] = x1;		// also update ghost
			}
			j -= dir;
		}
	}
	else return;	// the down-solve has already started.
	// handle upper boundary : x(j) -> x(j) - Mu(j).x(j+1)
	for( ; j != i1-dir; j -= dir) {		// Back-substitution.
		v2d* vx = (v2d*) x[j];		v2d* vxu = (v2d*) x[j+dir];
		const unsigned short * li_ = mx_idx;
		double* Md = Mr(j) + 2;
		#if VSIZE >= 4
		v4d Mu = vall4(offdi(j, dir));
		for (int lm=lmstart; lm<=lmend; lm+=2) {
			*(v4d*)(vx+lm)  -= (Mu * vread4(vxu + lm, 0)) * vdup_x2(Md + li_[lm],   Md + li_[lm+1]);
		}
		#else
		s2d Mu = vdup(offdi(j, dir));
		for (int lm=lmstart; lm<=lmend; lm++) {
			vx[lm]  -= (Mu * vxu[lm]) * vdup(Md[li_[lm]]);		// MUL, FMA
		}
		#endif
	}	// j = i1-dir
	if (share_dn >= 0) {		// we are not the first block
		#pragma omp barrier
		#pragma omp master
		{
			print_debug("solve_dn: send to %d (tag %d)\n",share_dn, tag);
			MPI_Isend(x[i1]+lms_block, 2*nlm_block, MPI_DOUBLE, share_dn, tag, comm_net, *req);
			*req += 1;
		}
	}
}

void LinOp3ld::solve_dn_halo(xs_array2d<cplx> x, int lmstart, int lmend) const
{
	//if ((dist_flags & SOLVE_UPDN) && (share_dn >= 0)) {
	if (dist_flags & SOLVE_DN_HALO) {		// update the halo by solving instead of MPI communication
		const int dir = dir_solve;
		const int j = irs_solve - dir;
			v2d* vx = (v2d*) x[j];		v2d* vxu = (v2d*) x[j+dir];
			const unsigned short * li_ = mx_idx;
			double* Md = Mr(j) + 2;
			s2d Mu = vdup(offdi(j, dir));
			for (int lm=lmstart; lm<=lmend; lm++) {
				vx[lm]  -= (Mu * vxu[lm]) * vdup(Md[li_[lm]]);		// MUL, FMA
			}
	}
}

template <class T>
void _LinOp3l<T>::solve_finish(Spectral& X, xs_array2d<cplx> x, int tag, MPI_Request **req, int lms_block, int nlm_block) const
{
	if (dist_flags == SOLVE_OFF) return;		// reject processes not involved.

  #pragma omp master
  {
	const int dir = dir_solve;
	if (dist_flags & SOLVE_SYNC_RCV) {
		print_debug("solve_finish: recv from %d (tag %d)\n",i_mpi-n_mpi_shared*dir, tag);
		MPI_Irecv(x[irs_solve-dir]+lms_block, 2*nlm_block, MPI_DOUBLE, i_mpi_net-dir, tag, comm_net, *req);             // sync halo
		*req += 1;
	} else if (dist_flags & SOLVE_SYNC_BC_SND) {
		print_debug("solve_finish: send to %d (tag %d)\n",i_mpi-n_mpi_shared*dir, tag);
		MPI_Isend(x[irs_solve]+lms_block, 2*nlm_block, MPI_DOUBLE, i_mpi_net-dir, tag, comm_net, *req);		// for boundary to be up to date !
		*req += 1;
	}
	if (dist_flags & SOLVE_SYNC_SND) {
		print_debug("solve_finish: send to %d (tag %d)\n",i_mpi+n_mpi_shared, tag);
		MPI_Isend(x[ire_solve]+lms_block, 2*nlm_block, MPI_DOUBLE, i_mpi_net+dir, tag, comm_net, *req);         // sync halo
		*req += 1;
	} else if (dist_flags & SOLVE_SYNC_BC_RCV) {
		const int i0 = (dir > 0) ? ire_shared : irs_shared;
		print_debug("solve_finish: recv from %d (tag %d)\n",i_mpi+n_mpi_shared*dir, tag);
		MPI_Irecv(x[i0+dir]+lms_block, 2*nlm_block, MPI_DOUBLE, i_mpi_net+dir, tag, comm_net, *req);		// for boundary to be up to date !
		*req += 1;
	}
  }
}
#endif

/// Solve M x = b, where b is initially stored in x.
/// x MUST have elements istart-1 and iend+1 defined ! (boundary conditions).
void LU3l::solve(xs_array2d<cplx> x, int lmstart, int lmend, int lm_shift) const
{
	const int n = ir_bco-ir_bci+1;
	const int ir0 = (dir_solve > 0) ? ir_bci : ir_bco;
	const long vx_dist = x.get_dist() * dir_solve;
	if UNLIKELY(n<=0) return;		// helps the compiler for the next two loops, where it can now assume n>0.

	v2d* vx = (v2d*) x[ir0] - lm_shift;		// shift origin, allowing x to hold only part of the whole lm coefficients.
// forward
	const double* Lj = L;
	for(int j=n; j>0; j--) {
		LM_L_LOOP2( lmstart, lmend,  vx[lm] = vdup(Lj[2*l]) * vx[lm] - vdup(Lj[2*l+1]) * vx[lm-vx_dist];  )
		Lj += nelem_L;
		vx += vx_dist;
	}
// backward
	const double* Uj = U;
	for(int j=n; j>0; j--) {
		vx -= vx_dist;
		LM_L_LOOP2( lmstart, lmend,  vx[lm] -= vdup(Uj[l]) * vx[lm+vx_dist];  )
		Uj += nelem_U;
	}
}

/// Solve M x = b, where b is initially stored in x.
/// x MUST have elements istart-2 and iend+2 defined ! (boundary conditions).
void LU5l::solve(xs_array2d<cplx> x, int lmstart, int lmend, int lm_shift) const
{
	const int n = ir_bco-ir_bci+1;
	const int ir0 = (dir_solve > 0) ? ir_bci : ir_bco;
	const long vx_dist = x.get_dist() * dir_solve;
	if UNLIKELY(n<=0) return;		// helps the compiler for the next two loops, where it can now assume n>0.

	v2d* vx = (v2d*) x[ir0] - lm_shift;		// shift origin, allowing x to hold only part of the whole lm coefficients.
// forward
	const double* Lj = L;
	for(int j=n; j>0; j--) {
		LM_L_LOOP2( lmstart, lmend,  vx[lm] = vdup(Lj[3*l]) * vx[lm] - vdup(Lj[3*l+1]) * vx[lm-vx_dist] - vdup(Lj[3*l+2]) * vx[lm-2*vx_dist];  )
		Lj += nelem_L;
		vx += vx_dist;
	}
// backward
	const double* Uj = U;
	for(int j=n; j>0; j--) {
		vx -= vx_dist;
		LM_L_LOOP2( lmstart, lmend,  vx[lm] = vx[lm] - vdup(Uj[2*l]) * vx[lm+vx_dist] - vdup(Uj[2*l+1]) * vx[lm+2*vx_dist];  )		// don't use -= to allow for 2 FMA
		Uj += nelem_U;
	}
}

#ifdef XS_MPI
/// x MUST have elements istart-1 and iend+1 defined ! (boundary conditions).
void LU3l::solve_up(xs_array2d<cplx> x, int lmstart, int lmend, int tag, MPI_Request **req, int lms_block, int nlm_block) const
{
	if ((dist_flags & SOLVE_UPDN) == 0) return;		// reject processes not involved.

	const int dir = dir_solve;
	xs_array2d<v2d> vx = x;
	if (share_dn >= 0) {		// we are not the first block
		#pragma omp master
		{
			int j=irs_solve;
			print_debug("LU3l::solve_up: recv from %d (tag %d)\n",share_dn, tag);
			if (nlm_block <= 256) PREFETCH_BLOCK_W(x[j-dir]+lms_block, x[j-dir]+lms_block+nlm_block);		// prefetch buffer to receive MPI data
			MPI_Recv(x[j-dir]+lms_block, 2*nlm_block, MPI_DOUBLE, share_dn, tag, comm_net, MPI_STATUS_IGNORE);	// blocking receive
		}
		if (lmend-lmstart < 128) PREFETCH_BLOCK_W(x[irs_solve]+lmstart, x[irs_solve]+lmend);		// prefetch data for each thread
		#pragma omp barrier
	}
// forward
	const unsigned short* li_ = mx_idx;
	const double* Lj = L;
	for(int j=irs_solve; j != ire_solve+dir; j+=dir) {
	  #if VSIZE >= 4
		for (int lm=lmstart; lm<=lmend; lm+=2) {
			long la = li_[lm];		long lb = li_[lm+1];
			*(v4d*)(vx[j]+lm) = vread4(vx[j] + lm, 0)     * vdup_x2(Lj +2*la,   Lj +2*lb)
							  - vread4(vx[j-dir] + lm, 0) * vdup_x2(Lj +2*la+1, Lj +2*lb+1);
		}
	  #else
		for (int lm=lmstart; lm<=lmend; lm++) {
			long la = li_[lm];
			vx[j][lm]   = vdup(Lj[2*la]) * vx[j][lm]   - vdup(Lj[2*la+1]) * vx[j-dir][lm];
		}
	  #endif
		Lj += nelem_L;
	}
	if ((share_up >= 0) && (C==0)) {
		#pragma omp barrier
		#pragma omp master
		{
			int j=ire_solve;
			print_debug("LU3l::solve_up: send to %d (tag %d)\n",share_up, tag);
			MPI_Isend(x[j]+lms_block,   2*nlm_block, MPI_DOUBLE, share_up, tag, comm_net, *req);	// non-blocking send
			*req += 1;
		}
	}
}

/// x MUST have elements istart-2 and iend+2 defined ! (boundary conditions).
void LU5l::solve_up(xs_array2d<cplx> x, int lmstart, int lmend, int tag, MPI_Request **req, int lms_block, int nlm_block) const
{
	if ((dist_flags & SOLVE_UPDN) == 0) return;		// reject processes not involved.

	const int dir = dir_solve;
	xs_array2d<v2d> vx = x;
	if (share_dn >= 0) {		// we are not the first block
		#pragma omp master
		{
			const int j=irs_solve;
			print_debug("LU5l::solve_up: recv from %d (tag %d,%d)\n",share_dn, tag, tag+1);
			MPI_Request rcv[2];
			MPI_Irecv(x[j-2*dir]+lms_block, 2*nlm_block, MPI_DOUBLE, share_dn, tag,   comm_net, rcv);		// non-blocking receive
			MPI_Irecv(x[j-dir]  +lms_block, 2*nlm_block, MPI_DOUBLE, share_dn, tag+1, comm_net, rcv+1);		// non-blocking receive
			if (nlm_block <= 256) {
				PREFETCH_BLOCK_W(x[j-2*dir]+lms_block, x[j-2*dir]+lms_block+nlm_block);		// prefetch buffer to receive MPI data
				PREFETCH_BLOCK_W(x[j-dir]  +lms_block, x[j-dir]  +lms_block+nlm_block);		// prefetch buffer to receive MPI data
			}
			MPI_Waitall(2, rcv, MPI_STATUSES_IGNORE);		// wait to receive the 2 shells
		}
		if (lmend-lmstart < 128) PREFETCH_BLOCK_W(x[irs_solve]+lmstart, x[irs_solve]+lmend);		// prefetch data for each thread
		#pragma omp barrier
	}
// forward
	const unsigned short* li_ = mx_idx;
	const double* Lj = L;
	for(int j=irs_solve; j != ire_solve+dir; j+=dir) {
		#if VSIZE >= 4
		for (int lm=lmstart; lm<=lmend; lm+=2) {
			int la = li_[lm];		int lb = li_[lm+1];
			*(v4d*)(vx[j]+lm)     = vread4(vx[j] + lm, 0) * vdup_x2(Lj+3*la,Lj+3*lb)
								  - vread4(vx[j-dir] + lm, 0) * vdup_x2(Lj +3*la+1, Lj+3*lb+1)
								  - vread4(vx[j-2*dir] + lm, 0) * vdup_x2(Lj +3*la+2, Lj+3*lb+2);
		}
		#else
		for (int lm=lmstart; lm<=lmend; lm++) {
			int la = li_[lm];
			vx[j][lm]   = vdup(Lj[3*la]) * vx[j][lm]   - vdup(Lj[3*la+1]) * vx[j-dir][lm]   - vdup(Lj[3*la+2]) * vx[j-2*dir][lm];
		}
		#endif
		Lj += nelem_L;
	}
	if ((share_up >= 0) && (C==0)) {
		#pragma omp barrier
		#pragma omp master
		{
			int j=ire_solve;
			print_debug("LU5l::solve_up: send to %d (tag %d,%d)\n",share_up, tag,tag+1);
			MPI_Isend(x[j-dir]+lms_block, 2*nlm_block, MPI_DOUBLE, share_up, tag,   comm_net, *req);	// non-blocking send
			MPI_Isend(x[j]+lms_block,     2*nlm_block, MPI_DOUBLE, share_up, tag+1, comm_net, *req +1);	// non-blocking send
			*req += 2;
		}
	}
}

/// x MUST have elements istart-1 and iend+1 defined ! (boundary conditions).
void LU3l::solve_dn(xs_array2d<cplx> x, int lmstart, int lmend, int tag, MPI_Request **req, int lms_block, int nlm_block) const
{
	if ((dist_flags & SOLVE_UPDN) == 0) return;		// reject processes not involved.

	const int dir = dir_solve;
	xs_array2d<v2d> vx = x;
	const unsigned short* li_ = mx_idx;
	const double* Uj = U;
	int j = ire_solve;
	if (share_up >= 0) {		// we are not the last block.
		#pragma omp master
		{
			if (C==0) {
				print_debug("LU3l::solve_dn: recv from %d (tag %d)\n",share_up, tag);
				MPI_Recv (x[ire_solve+dir]+lms_block, 2*nlm_block, MPI_DOUBLE, share_up, tag, comm_net, MPI_STATUS_IGNORE);		// blocking receive
			} else {	// X-solver, at interface
				print_debug("LU3l::solve_dn split: sendrecv from %d (tag %d)\n",share_up, tag);
				MPI_Sendrecv (x[ire_solve]     +lms_block, 2*nlm_block, MPI_DOUBLE, share_up, tag,		// blocking send-receive to reuse both send AND receive buffer
					      x[ire_solve+dir]+lms_block, 2*nlm_block, MPI_DOUBLE, share_up, tag, comm_net, MPI_STATUS_IGNORE);
			}
		}
		#pragma omp barrier
		if (C) {		// apply 2x2 coupling matrix before solving down:
			for (int lm=lmstart; lm<=lmend; lm++) {
				long la = 3*li_[lm];
				v2d x0 = vdup(C[la+1]) * vx[ire_solve][lm]      +  vdup(C[la])   * vx[ire_solve+dir][lm];
				v2d x1 = vdup(C[la+1]) * vx[ire_solve+dir][lm]  +  vdup(C[la+2]) * vx[ire_solve][lm];
				vx[ire_solve][lm] =     x0;
				vx[ire_solve+dir][lm] = x1;		// also update ghost
			}
			j -= dir;
			Uj += nelem_U;
		}
	}
// backward
	for(; j != irs_solve -dir; j-=dir) {
	  #if VSIZE >= 4
		for (int lm=lmstart; lm<=lmend; lm+=2) {
			long la = li_[lm];	long lb = li_[lm+1];
			*(v4d*)(vx[j]+lm) -= vdup_x2(Uj+la,Uj+lb) * vread4(vx[j+dir] + lm, 0);
		}
	  #else
		for (int lm=lmstart; lm<=lmend; lm++) {
			vx[j][lm]   -= vdup(Uj[li_[lm]]) * vx[j+dir][lm];
		}
	  #endif
		Uj += nelem_U;
	}
	#pragma omp barrier
	#pragma omp master
	{
		if (share_dn >= 0) {
			print_debug("LU3l::solve_dn: send to %d (tag %d)\n",share_dn, tag);
			MPI_Isend(x[irs_solve]+lms_block,   2*nlm_block, MPI_DOUBLE, share_dn, tag,   comm_net, *req);		// for solve to go on
			*req += 1;
		}
	}
}

void LU3l::solve_dn_halo(xs_array2d<cplx> x, int lmstart, int lmend) const
{
	//if ((dist_flags & SOLVE_UPDN) && (share_dn >= 0)) {		// update the halo by solving instead of MPI communication
	if (dist_flags & SOLVE_DN_HALO) {		// update the halo by solving instead of MPI communication
		xs_array2d<v2d> vx = x;
		const int dir = dir_solve;
		const double* Uj = U + nelem_U*(abs(ire_solve-irs_solve)+1);
		const unsigned short* li_ = mx_idx;
		int j = irs_solve - dir;
		for (int lm=lmstart; lm<=lmend; lm++) {
			int la = li_[lm];
			vx[j][lm]   -= vdup(Uj[la]) * vx[j+dir][lm];
		}
	}
}

/// x MUST have elements istart-2 and iend+2 defined ! (boundary conditions).
void LU5l::solve_dn(xs_array2d<cplx> x, int lmstart, int lmend, int tag, MPI_Request **req, int lms_block, int nlm_block) const
{
	if ((dist_flags & SOLVE_UPDN) == 0) return;		// reject processes not involved.

	const int dir = dir_solve;
	xs_array2d<v2d> vx = x;
	int j = ire_solve;
	const unsigned short* li_ = mx_idx;
	const double* Uj = U;
	if (share_up >= 0) {		// we are not the last block.
		#pragma omp master
		{
			print_debug("LU5l::solve_dn: recv from %d (tag %d,%d)\n",share_up, tag,tag+1);
			MPI_Request sr[4];
			MPI_Irecv(x[j+2*dir]+lms_block, 2*nlm_block, MPI_DOUBLE, share_up, tag,   comm_net, sr);	// non-blocking receive
			MPI_Irecv(x[j+dir]  +lms_block, 2*nlm_block, MPI_DOUBLE, share_up, tag+1, comm_net, sr+1);	// non-blocking receive
			int nreq = 2;
			if (C) {	// X-solver, exchange data and wait for completion to reuse.
				MPI_Isend(x[j-dir]+lms_block, 2*nlm_block, MPI_DOUBLE, share_up, tag,   comm_net, sr+2);
				MPI_Isend(x[j]+lms_block,     2*nlm_block, MPI_DOUBLE, share_up, tag+1, comm_net, sr+3);
				nreq = 4;
			}
			MPI_Waitall(nreq, sr, MPI_STATUSES_IGNORE);		// wait for send and receives to complete
		}
		#pragma omp barrier

		if (C) {		// apply coupling matrix before solving down:
			const int j0 = (dir > 0) ? ire_solve-1 : ire_solve-2;
			for (int lm=lmstart; lm<=lmend; lm++) {
				long la = 8 * (long) li_[lm];
				v2d xx[2];
				for (int i=0; i<2; i++)  xx[i] = vdup(C[la + i*4]) * vx[j0][lm];
				for (int k=1; k<4; k++) for (int i=0; i<2; i++) {
					xx[i] += vdup(C[la + i*4+k]) * vx[j0+k][lm];
				}
				vx[j-dir][lm] = xx[0];
				vx[j][lm] = xx[1];
			}
			j -= 2*dir;
			Uj += 2*nelem_U;
		}
	}
// backward
	for(; j != irs_solve -dir; j-=dir) {
		#if VSIZE >= 4
		for (int lm=lmstart; lm<=lmend; lm+=2) {
			int la = li_[lm];		int lb = li_[lm+1];
			*(v4d*)(vx[j]+lm) -= vread4(vx[j+dir]   + lm, 0) * vdup_x2(Uj+2*la,   Uj+2*lb)
							   + vread4(vx[j+2*dir] + lm, 0) * vdup_x2(Uj+2*la+1, Uj+2*lb+1);
		}
		#else
		for (int lm=lmstart; lm<=lmend; lm++) {
			int l = li_[lm];
			vx[j][lm] -= vdup(Uj[2*l]) * vx[j+dir][lm] + vdup(Uj[2*l+1]) * vx[j+2*dir][lm];
		}
		#endif
		Uj += nelem_U;
	}

	#pragma omp barrier
	#pragma omp master
	{
		if (share_dn >= 0) {
			print_debug("LU5l::solve_dn: send to %d (tag %d,%d)\n",share_dn, tag,tag+1);
			MPI_Isend(x[irs_solve+dir]+lms_block, 2*nlm_block, MPI_DOUBLE, share_dn, tag,   comm_net, *req+1);
			MPI_Isend(x[irs_solve]    +lms_block, 2*nlm_block, MPI_DOUBLE, share_dn, tag+1, comm_net, *req);
			*req += 2;
		}
	}
}

void LU5l::solve_dn_halo(xs_array2d<cplx> x, int lmstart, int lmend) const
{
	//if ((dist_flags & SOLVE_UPDN) && (share_dn >= 0)) {		// update the halo by solving instead of MPI communication
	if (dist_flags & SOLVE_DN_HALO) {		// update the halo by solving instead of MPI communication
		xs_array2d<v2d> vx = x;
		const int dir = dir_solve;
		const int j0 = irs_solve - dir;
		const int j1 = irs_solve - 3*dir;
		const unsigned short* li_ = mx_idx;
		const double* Uj = U + nelem_U*(abs(ire_solve-irs_solve)+1);
		for(int j=j0; j != j1; j-=dir) {
			if (j<ir_bci || j>ir_bco) break;	// DO NOT OVERWRITE BOUNDARY CONDITIONS!
			for (int lm=lmstart; lm<=lmend; lm++) {
				int l = li_[lm];
				vx[j][lm] -= vdup(Uj[2*l]) * vx[j+dir][lm] + vdup(Uj[2*l+1]) * vx[j+2*dir][lm];
			}
			Uj += nelem_U;
		}
	}
}

/// synchronizing halo after solve (to allow 1 shell per mpi process)
void LU5l::solve_finish(Spectral& X, xs_array2d<cplx> x, int tag, MPI_Request **req, int lms_block, int nlm_block) const
{
	if (dist_flags == SOLVE_OFF) return;		// reject processes not involved.

	#pragma omp master
	{
		const int dir = dir_solve;
		if ((C) && (share_up >= 0)) {		// update halo at interface
			print_debug("LU5l::solve_finish: xchg with %d (tag %d,%d)\n",share_up, tag,tag+1);
			const int dir = dir_solve;
			MPI_Irecv(x[ire_solve+2*dir]+lms_block, 2*nlm_block, MPI_DOUBLE, share_up, tag,   comm_net, *req);		// non-blocking receive
			MPI_Irecv(x[ire_solve+dir]  +lms_block, 2*nlm_block, MPI_DOUBLE, share_up, tag+1, comm_net, *req+1);	// non-blocking receive
			MPI_Isend(x[ire_solve-dir]+lms_block, 2*nlm_block, MPI_DOUBLE, share_up, tag,   comm_net, *req+2);
			MPI_Isend(x[ire_solve]+lms_block,     2*nlm_block, MPI_DOUBLE, share_up, tag+1, comm_net, *req+3);
			*req += 4;
		}
		if (dist_flags & SOLVE_SYNC_SND) {
			print_debug("LU5l::solve_finish: send to %d (tag %d)\n",i_mpi+n_mpi_shared*dir, tag);
			MPI_Isend(x[ire_solve]+lms_block, 2*nlm_block, MPI_DOUBLE, i_mpi_net+dir, tag, comm_net, *req); // update halo
			*req += 1;
		} else if (dist_flags & SOLVE_SYNC_BC_RCV) {
			const int i0 = (dir > 0) ? ire_shared : irs_shared;
			print_debug("LU5l::solve_finish: recv from %d (tag %d,%d)\n",i_mpi+n_mpi_shared*dir, tag,tag+1);
			MPI_Irecv(x[i0+dir]+lms_block,   2*nlm_block, MPI_DOUBLE, i_mpi_net+dir, tag,   comm_net, *req);	// for boundary to be up to date !
			MPI_Irecv(x[i0+2*dir]+lms_block, 2*nlm_block, MPI_DOUBLE, i_mpi_net+dir, tag+1, comm_net, *req+1);	// for boundary to be up to date !
			*req += 2;
		}
		if (dist_flags & SOLVE_SYNC_RCV) {
			print_debug("LU5l::solve_finish: recv from %d (tag %d)\n",i_mpi-n_mpi_shared*dir, tag+1);
			MPI_Irecv(x[irs_solve-2*dir]+lms_block, 2*nlm_block, MPI_DOUBLE, i_mpi_net-dir, tag+1, comm_net, *req);         // update halo
			*req += 1;
			if ((dist_flags & SOLVE_SYNC_RCV_1) == 0) {
				print_debug("LU5l::solve_finish: recv from %d (tag %d)\n",i_mpi-n_mpi_shared*dir, tag);
				MPI_Irecv(x[irs_solve-dir]+lms_block, 2*nlm_block, MPI_DOUBLE, i_mpi_net-dir, tag, comm_net, *req);             // update halo
				*req += 1;
			} else {
				print_debug("LU5l::solve_finish: blocking recv from %d (tag %d)\n",i_mpi-n_mpi_shared*dir, tag);
				MPI_Recv (x[irs_solve-dir]+lms_block, 2*nlm_block, MPI_DOUBLE, i_mpi_net-dir, tag, comm_net, MPI_STATUS_IGNORE);                // blocking update halo
			}
		} else if (dist_flags & SOLVE_SYNC_BC_SND) {
			print_debug("LU5l::solve_finish: send to %d (tag %d,%d)\n",i_mpi-n_mpi_shared*dir, tag,tag+1);
			MPI_Isend(x[irs_solve]+lms_block,     2*nlm_block, MPI_DOUBLE, i_mpi_net-dir, tag,   comm_net, *req);	// for boundary to be up to date !
			MPI_Isend(x[irs_solve+dir]+lms_block, 2*nlm_block, MPI_DOUBLE, i_mpi_net-dir, tag+1, comm_net, *req+1);	// for boundary to be up to date !
			*req += 2;
		}
		if (dist_flags & SOLVE_SYNC_SND) {
			print_debug("LU5l::solve_finish: send to %d (tag %d)\n",i_mpi+n_mpi_shared*dir, tag+1);
			MPI_Isend(x[ire_solve-dir]+lms_block, 2*nlm_block, MPI_DOUBLE, i_mpi_net+dir, tag+1, comm_net, *req);   // update halo
			*req += 1;
		}
	}
}
#endif

template <class T>
void _LinOp3l<T>::set_Laplace(int ir, const double* d1r, const double* d2r, const double s)
{
	double r_1 = 1.0/r[ir];

	set_lo(ir, s*(d2r[0] + 2.*r_1*d1r[0]) );
	set_up(ir, s*(d2r[2] + 2.*r_1*d1r[2]) );
	double d = s*(d2r[1] + 2.*r_1*d1r[1]);
	double sr_2 = s*r_1*r_1;
	for (int l=0; l<=lmax; l++)
		set_di(ir,l,  d - sr_2*(l*(l+1)) );
}

template <class T>
void _LinOp3l<T>::set_Laplace(int ir, const double s)
{
	double gr[3], d2r[3];

	fd_deriv_o2(r,ir, gr, d2r);	
	set_Laplace(ir, gr, d2r, s);
}

/// Laplace operator considering the boundary condition a0*f + a1*f' = ghost
template <class T>
void _LinOp3l<T>::set_Laplace_bc(int ir, double a0, double a1, const double s)
{
	double gr[3], d2r[3];
	int ii;

	if ((ir!=ir_bci)&&(ir!=ir_bco)) {
		runerr("[set_Laplace] cannot set boundary condition outside boundaries");
	}
	if (r[ir] == 0.0) {			// special boundary condition for r=0 (scalar)
		double dx_2 = 1.0/(r[ir+1]*r[ir+1]);
		set_lo(ir,    0.0 );
		set_up(ir,    6.*s * dx_2 );
		set_di(ir,0, -6.*s * dx_2 );		// only for l=0
		for (int l=3; l<nelem; l++)  Mr(ir)[l] = 0.0;		// l>0 (for LinOp3l and LinOp3)
		flags |= LINOP_BCI_L0_ONLY;
		return;
	}

	if (ir==ir_bci)	 ii = +1;
	if (ir==ir_bco)  ii = -1;
	fd_deriv_o2_bc(r[ir+ii]-r[ir], a0, a1, 0.0, gr, d2r);
	set_Laplace(ir, gr, d2r, s);
}

void LinOp5l::set_BiLaplace4(int ir, const double* d1r, const double* d2r, const double* d3r, const double* d4r,
	const double s1, const double s2)
{
	double LLr[5], LLr_l2[5];
	double r_1 = 1.0/r[ir];

	for (int k=0; k<5; k++) {
		LLr[k] = s2*(d4r[k] + 4.0*r_1*d3r[k])		// bilaplace radial
		       + s1*(d2r[k] + 2.0*r_1*d1r[k]);		// laplace radial
		LLr_l2[k] = s2*(2.0*r_1*r_1*d2r[k]);			// bilaplace multiplied by l(l+1)
	}
	double s1r_2 = s1*(r_1*r_1);
	double s2r_4 = s2*(r_1*r_1)*(r_1*r_1);
	for (int l=0; l<=lmax; l++) {
		double l2 = l*(l+1);
		for (int k=0; k<5; k++) {
			M(ir,l)[k-2] = LLr[k] - l2*LLr_l2[k];
		}
		M(ir,l)[0] += l2*(s2r_4*(l2-2.) - s1r_2);
	}
}

void LinOp5l::set_Laplace4(int ir, const double* d1r, const double* d2r, const double s)
{
	double Lr[5];
	double r_1 = 1.0/r[ir];

	for (int k=0; k<5; k++) {
		Lr[k] = s*(d2r[k] + 2.0*r_1*d1r[k]);		// laplace radial
	}
	double sr_2 = s*(r_1*r_1);
	for (int l=0; l<=lmax; l++) {
		double l2 = l*(l+1);
		for (int k=0; k<5; k++) {
			M(ir,l)[k-2] = Lr[k];
		}
		M(ir,l)[0] -= l2*sr_2;
	}
}

void LinOp5l::set_Laplace2(int ir, const double* Lr, const double s)
{
	double r_2 = 1.0/(r[ir]*r[ir]);
	for (int l=0; l<=lmax; l++) {
		double l2 = (l*(l+1));
		M(ir,l)[-2] = 0.0;
		M(ir,l)[-1] = s*Lr[0];
		M(ir,l)[0]  = s*(Lr[1] - l2*r_2);
		M(ir,l)[1]  = s*Lr[2];
		M(ir,l)[2]  = 0.0;
	}
}

void LinOp5l::set_BiLaplace2(int ir, const double* Lrl, const double* Lrd, const double* Lru, const double s)
{
	if (r[ir-1] == 0.0  &&  Lrl[3] != 0.0) runerr("problem in Laplace operator at r=0");		// Laplace(r=0) should be treated separately.
	if (fabs(Lrd[3] * (r[ir]*r[ir]) - 1.0) >= 1e-15) runerr("problem in Laplace operator");
	for (int l=0; l<=lmax; l++) {
		double l2 = l*(l+1);
		double l2_r2 = l2 * Lrd[3];
		M(ir,l)[-2] = Lrd[0]*Lrl[0];
		M(ir,l)[-1] = Lrd[0]*(Lrl[1] - l2*Lrl[3]) + (Lrd[1] - l2_r2)*Lrd[0];
		M(ir,l)[0]  = Lrd[0]*Lrl[2]               + (Lrd[1] - l2_r2)*(Lrd[1] - l2_r2) + Lrd[2]*Lru[0];
		M(ir,l)[1]  =                               (Lrd[1] - l2_r2)*Lrd[2]           + Lrd[2]*(Lru[1] - l2*Lru[3]);
		M(ir,l)[2]  =                                                                   Lrd[2]*Lru[2];
	}
	LinOpR::scale(ir, s);
}

#undef LM_L_LOOP2
