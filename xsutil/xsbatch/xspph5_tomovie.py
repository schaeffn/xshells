import sys
import shutil
from subprocess import call
from os import path
from itertools import cycle, chain

import h5py
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import seaborn as sns

from xsbatch.xspp_read import plot_merid, plot_surf, plot_disc
from xsbatch.utilfuncs import Toolbar, Dumpdir
from xsenergy.read_dataset import read_table

timeseries_gs = GridSpec(1, 1,
                         left=0.0, right=0.3,
                         bottom=0.0, top=0.3)
sns.color_palette('colorblind')
cmaps = {'u': sns.diverging_palette(220, 20, sep=20, as_cmap=True),
         'b': sns.diverging_palette(280, 145, sep=20, as_cmap=True)}
def engplot(fname, time):
    ax = plt.subplot(timeseries_gs)
    dat = read_table(fname, heirarchy=True)
    dat.kinetic.nonzonal.plot(ax=ax)

def discframe(h5, prefix, sub, levels, **kwargs):
    c1 = h5.get(getcoord(pref, 's')).value
    c2 = h5.get(getcoord(pref, 'phi')).value
    pltdat = h5.get(d)[i]
    plot_disc(c1, c2, pltdat, levels=levels, sub=sub,
              cmap=cmaps[pref[0]], cbar=kwargs.get('cbar', True))
    sub.plot(c1[-1] * np.cos(-h5.get('phase')[i]) * np.r_[0.95, 1.0],
             c1[-1] * np.sin(-h5.get('phase')[i]) * np.r_[0.95, 1.0],
             color='black')
    sub.plot(c1[0] * np.cos(h5.get('phase')[i] - h5.get('time')[i]) * np.r_[0, 0.9],
             c1[0] * np.sin(h5.get('phase')[i] - h5.get('time')[i]) * np.r_[0, 0.9],
             ls='--', color='black')
    return sub

def shellframe(h5, prefix, sub, levels, **kwargs):
    c1 = h5.get(getcoord(pref, 't')).value
    c2 = h5.get(getcoord(pref, 'p')).value
    pltdat = h5.get(d)[i]
    plot_surf(c1, c2, pltdat, levels=levels, sub=sub,
              cmap=cmaps[pref[0]], cbar=kwargs.get('cbar', True))
    return sub

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
        description='''reads an hdf5 file output by xspp_toh5 and outputs plots thereof''',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('fname', type=str,
                        help='file to process')
    parser.add_argument('dlist', type=str, nargs='+',
                        help='list of attributes to plot')
    parser.add_argument('-so', '--subplotopts', type=str, nargs='+', default=['nrows=1', 'ncols=1'],
                        help='options for subplots in the form OPT=VALUE')
    parser.add_argument('-o', '--output', type=str, default='movie.png',
                        help='name of the output file')
    parser.add_argument('--labels', type=str, nargs='+', default=None,
                        help='label for each subplot')
    parser.add_argument('--t_range', type=int, nargs='+', default=None,
                        help='range of time indices to render')
    parser.add_argument('--rlim', type=np.double, nargs=2, default=None,
                        help='range of radii to plot')
    parser.add_argument('--azim_fluc', action='store_true',
                        help='set to subtract azimuthal average')
    parser.add_argument('--time_fluc', action='store_true',
                        help='set to subtract the time average')
    parser.add_argument('--rotation', type=np.double, default=0,
                        help='set to rotate the disc and surf images with the specified rate')
    parser.add_argument('--engfile', type=str, default=None,
                        help='energy file to plot from')
    parser.add_argument('--colorbar_off', action='store_false',
                        help='set to inhibit colorbar')
    parser.add_argument('--markrad', type=np.double, nargs='+', default=[],
                        help='set to draw a circle at a specified radius')
    def boolcheck(a):
        if a == 'True':
            return True
        if a == 'False':
            return False
        raise ValueError
    def conv_kwargs(kwdict):
        for k, v in kwdict.items():
            v = v.split(',')
            for dtype in (int, float, boolcheck):
                try:
                    v = [dtype(a) for a in v]
                    if len(v) == 1: v = v[0]
                    kwdict[k] = v
                    break
                except:
                    ValueError
                    pass
        return kwdict

    clin = parser.parse_args()
    subplotkwargs = conv_kwargs({k: v for k, v in [opt.split('=') for opt in clin.subplotopts]})
    if clin.labels is None:
        labels = cycle(' ')
    else:
        labels = clin.labels

    # get the number of timesteps
    if clin.t_range == None:
        with h5py.File(clin.fname, 'r') as h5:
            t_range = (h5.get('time').size, )
    else:
        t_range = clin.t_range

    # list of output file names
    fouts= ['fig_{:04d}.png'.format(i) for i, j in enumerate(range(*t_range))]

    def getcoord(prefix, basename):
        return ''.join((prefix[:2], basename))

    with h5py.File(clin.fname, 'r') as h5, \
      Dumpdir() as dumpdir, \
      Toolbar(sum((1 for i in range(*t_range))), full_width=True) as toolbar:
        dlevels = {d: None for d in clin.dlist}

        fig = plt.figure(figsize=(subplotkwargs.get('ncols', 1)*4, subplotkwargs.get('nrows', 1)*4))
        gs = GridSpec(subplotkwargs.get('nrows', 1), subplotkwargs.get('ncols', 1))
        phase = h5.get('phase')
        if phase is None:
            phase = np.zeros_like(h5.get('time').value)
        for i, fout in zip(range(*t_range), fouts):
            subplots = [fig.add_subplot(g) for g in gs]
                
            for d, sub, lab in zip(clin.dlist, subplots, labels):
                pref = d.split('.')[0]
                if dlevels[d] is None:
                    dset = h5.get(d).value
                    if clin.azim_fluc:
                        dset = dset - dset[:, :, :-1].mean(axis=-1, keepdims=True)
                    if clin.time_fluc:
                        dset = dset - dset.mean(axis=0, keepdims=True)
                    dmax = np.abs(np.r_[dset.max(), dset.min()]).max()
                    if dset.min() < 0:
                        dmin = -dmax
                    else:
                        dmin = 0
                    dlevels[d] = np.linspace(dmin, dmax, 20)
                
                if ('_disc' in pref) or ('zavg' in pref):
                    sub = discframe(h5, pref, sub, levels=dlevels[d], cbar=clin.colorbar_off)
                    for rad in clin.markrad:
                        ph = np.linspace(0, np.pi*2, 100)
                        sub.plot(rad * np.cos(ph), rad * np.sin(ph), ls='--', color='black')
                    # c1 = h5.get(getcoord(pref, 's')).value
                    # c2 = h5.get(getcoord(pref, 'phi')).value + h5.get('time')[i] * clin.rotation
                    # dset = h5.get(d)
                    # pltdat = dset[i]
                    # if clin.azim_fluc:
                    #     pltdat = dset[i] - dset[i, :, :-1].mean(axis=-1, keepdims=True)
                    # elif clin.time_fluc:
                    #     pltdat = dset[i] - dset.value.mean(axis=0)
                    # else:
                    #     pltdat = dset[i]
                    # get the number of steps to roll the axis
                    # nstep = c2 // 360
                    # nstep = np.searchsorted(nstep, nstep.max())
                    # c2 = np.roll(c2, nstep)
                    # c2 -= 360 * (c2 // 360)
                    # pltdat = np.roll(pltdat, nstep, axis=-1)
                    # plot_disc(c1, c2, pltdat, levels=dlevels[d], sub=sub,
                    #           cmap=cmaps[pref[0]], cbar=clin.colorbar_off)
                    # sub.plot(c1[-1] * np.sin(phase[i]) * np.r_[0.95, 1.0],
                    #          c1[-1] * np.cos(phase[i]) * np.r_[0.95, 1.0],
                    #          color='black')
                    # sub.plot(c1[0] * np.sin(phase[i] - h5.get('time')[i]) * np.r_[0, 0.9],
                    #          c1[0] * np.cos(phase[i] - h5.get('time')[i]) * np.r_[0, 0.9],
                    #          ls='--', color='black')
                    for rad in clin.markrad:
                        sub.plot(rad * np.cos(np.linspace(0, np.pi * 2, 100)),
                                 rad * np.sin(np.linspace(0, np.pi * 2, 100)),
                                 ls='--', color='black')[0].set_alpha(0.3)
                        
                if '_shell' in pref:
                    c1 = h5.get(getcoord(pref, 't')).value.reshape(-1, 1)
                    c2 = h5.get(getcoord(pref, 'p')).value + h5.get('time')[i] * clin.rotation
                    plt.sca(sub)
                    dset = h5.get(d)
                    if clin.azim_fluc:
                        pltdat = dset[i] - dset[i, :, :-1].mean(axis=-1, keepdims=True)
                    elif clin.time_fluc:
                        pltdat = dset[i] - dset.value.mean(axis=0)
                    else:
                        pltdat = dset[i]
                    
                    # get the number of steps to roll the axis
                    nstep = c2 // 360
                    nstep = np.searchsorted(nstep, nstep.max())
                    c2 = np.roll(c2, nstep)
                    c2 -= 360 * (c2 // 360)
                    pltdat = np.roll(pltdat, nstep, axis=-1)
                    plot_surf(c1, c2, pltdat, levels=dlevels[d],
                              cmap=cmaps[pref[0]], cbar=clin.colorbar_off)
                if '_V' in pref:
                    c1 = h5.get(getcoord(pref, 'r')).value.reshape(-1, 1)
                    c2 = h5.get(getcoord(pref, 'cost')).value
                    plt.sca(sub)
                    dset = h5.get(d).value
                    if dmax[d] == 0:
                        dmax[d] = np.abs(np.r_[dset.max(), dset.min()]).max()
                        if dset.min() < 0:
                            dmin[d] = -dmax[d]
                        else:
                            dmin[d] = 0
                    plot_merid(c1, c2, dset[i], levels=dlevels[d],
                               cmap=cmaps[pref[0]], cbar=clin.colorbar_off)
                sub.annotate(lab, (0.95, 0.95), xycoords='axes fraction',
                             ha='right', va='top', size='large')
            for sub in subplots:
                sub.annotate('{:04d}'.format(i), (0.05, 0.95), xycoords='axes fraction',
                             ha='left', va='top', size='large')
            # subplots[0].annotate('{:04d}'.format(i), (0.05, 0.95), xycoords='axes fraction',
            #                  ha='left', va='top', size='large')
            if clin.engfile is not None:
                engplot(clin.engfile, time[i])
            fig.savefig(fout, transparent=True)
            fig.clear()
            toolbar.update()
        # call apngasm to generate the final movie
        call(['apngasm', clin.output] + fouts)
        # move it to the cwd
        shutil.os.rename(clin.output, path.join(dumpdir.cwd, clin.output))
