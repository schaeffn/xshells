#!/bin/python3
"""xspp_read.py

routines for reading in outputs of xspp. This includes some wrappers on
xsplot routines to inhibit some of the prints to stdout they include,
and to handle different encodings of the output data.
"""

from io import StringIO
from functools import wraps
from subprocess import call, check_output, CalledProcessError
from itertools import chain, filterfalse
import re
from subprocess import Popen, PIPE
from collections import deque
import shutil

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker
from matplotlib.colors import SymLogNorm, LogNorm

import xsplot
from xsbatch.utilfuncs import chompstdout, isoload, boolcheck, conv_kwargs, OpenFilename

FMT=ticker.ScalarFormatter(useMathText=True)
FMT.set_powerlimits((-3,3))

def get_levels(*args, **kwargs):
    # if levels are specified manually, return them directly
    try:
        if len(args[1]) > 2:
            return args[1], 'neither'
    except TypeError:
        pass
    return xsplot.get_levels(*args, **kwargs)

@chompstdout
@isoload
def load_merid(*args, maskrho=None, **kwargs):
    """r, cost, V = load_merid(fname, ang=0, maskrho=None)."""
    r, ct, a = xsplot.load_merid(*args, **kwargs)
    if maskrho is not None:
        rho = r.reshape(-1, 1) * np.sqrt(1.0 - ct**2)
        a = np.ma.masked_where(rho < maskrho, a, copy=False)
    return r, ct, a

@chompstdout
@isoload
def load_surf(*args, **kwargs):
    """theta, phi, ur, ut, up = load_surf(file)"""
    return xsplot.load_surf(*args, **kwargs)

@chompstdout
@isoload
def load_disc(*args, **kwargs):
    """r, phi, vr, vp, vz = plot_disc(file, mres=1)"""
    return xsplot.load_disc(*args, **kwargs)

def plot_merid(r, ct, field, stream=0, superrotation=False,
               levels=20, czoom=1, **kwargs):
    sub = kwargs.pop('sub', plt.gca())
    st = np.sqrt(1.0 - ct**2)
    x = r.reshape(-1, 1) * st
    y = r.reshape(-1, 1) * ct
    # do the colorzooming à la xsplot.plot_merid
    m = np.max(np.abs(field))
    if m > 0:
        levels, ext = get_levels(m, levels, czoom)
    cnt = sub.contourf(x, y, field, levels=levels, **kwargs)
    plt.axis('equal')
    plt.axis('off')
    if kwargs.get('cbar', True):
        plt.colorbar(cnt, orientation='vertical', format=FMT, ax=sub)
    streammax = np.abs(stream).max()
    if streammax > 0:
        streamlevels = np.linspace(-1.0, 1.0, 12) * streammax
        plt.contour(x, y, stream,
                    streamlevels,
                    colors='black')
        if superrotation:
            plt.contour(x, y, stream, np.r_[1.0, ],
                        linestyles = '--',
                        colors='black')
            
    return sub

def plot_surf(theta, phi, field,
              levels=20, czoom=1, **kwargs):
    """Aitoff projection"""
    sub = kwargs.pop('sub', plt.gca())
    t = (90.0 - theta) * np.pi/180.
    ip = np.mgrid[0:phi.size]
    ip[-1] = 0  # loop
    p = (phi[ip] - 180.0) * np.pi /360 # longitude
    p[-1] = np.pi / 2
    # do the colorzooming à la xsplot.plot_surf
    m = np.max(np.abs(field))
    if m > 0:
        levels, ext = get_levels(m, levels, czoom)

    # aitoff projection
    t = t.reshape(-1, 1)
    aitlong = np.arccos(np.cos(t) * np.cos(p))
    aitlong = aitlong / np.sin(aitlong)
    x = 2.0 * aitlong * np.cos(t) * np.sin(p)
    y = aitlong * np.sin(t)
    cnt = sub.contourf(x, y, field[:, ip], **kwargs)
    if kwargs.get('markphi', False):
        mp = kwargs['markphi'] - np.pi / 2
        mt = np.pi/3
        mal = np.arccos(np.cos(mt) * np.cos(mp))
        mal = mal / np.sin(mal)
        x = 2.0 * mal * np.cos(mt) * np.sin(mp)
        y = mal * np.sin(mt)
        plt.plot(x, y, color='black', marker='s')
        
    sub.set_xlim(-3.2, 3.2)
    sub.set_ylim(-1.6, 1.6)
    plt.axis('equal')
    plt.axis('off')
    if kwargs.get('cbar', True):
        plt.colorbar(cnt, format=FMT, ax=sub)
    return sub

# @chompstdout
# def plot_disc(*args, **kwargs):
#     xsplot.plot_disc(*args, **kwargs)
def plot_disc(r, phi, field,
              levels=20, czoom=1, **kwargs):
    sub = kwargs.pop('sub', plt.gca())
    x = r.reshape(-1, 1) * np.cos(phi)
    y = r.reshape(-1, 1) * np.sin(phi)
    # do the colorzooming à la xsplot.plot_merid
    m = np.max(np.abs(field))
    if m == 0:
        return sub
    levels, ext = get_levels(m, levels, czoom)
    cnt = sub.contourf(x, y, field, levels=levels, **kwargs)
    sub.set_aspect('equal')
    sub.set_axis_off()
    if kwargs.get('cbar', True):
        plt.colorbar(cnt, format=FMT, ax=sub)
    return sub

def plot_spec(spec, **kwargs):
    ax = kwargs.pop('sub', plt.gca())
    specgram = kwargs.pop('specgram', False)
    yscale = kwargs.pop('yscale', 'log')
    if specgram:
        r = spec.index
        m = spec.columns
        if (m.min() == 0) and (yscale != 'linear') :
            m += 1
        maxeng = kwargs.pop('maxeng', spec.values.max())
        levels = maxeng * np.logspace(specgram, 0, 8)
        R = np.r_[r, 2*r[-1]-r[-2]]
        M = np.r_[m, 2*m[-1]-m[-2]]
        norm = LogNorm(vmin=levels.min(), vmax=spec.values.max(), clip=True)
        cnt = plt.pcolormesh(R, M, spec.T.values, **kwargs)
        ax.set_xlim(R.min(), R.max())
        ax.set_ylim(M.min(), M.max())
        ax.set_yscale(yscale)
        if not ax.yaxis_inverted():
            ax.invert_yaxis()
        cbar = plt.colorbar(cnt, ticks=levels, **kwargs)
        # for i, el in enumerate(levels):
        #     cbar.ax.annotate('{:7.1e}'.format(el), (1, float(i) / 7),
        #                      va='center', ha='left')
        ax.set_xlabel('r')
        ax.set_ylabel(spec.columns.name)
        return ax
    spec.plot(ax=ax)
    ax.set_yscale(yscale)
    return ax

RESOLUTION_REGEX = re.compile(r'(?P<spec>LMAX|MMAX|MRES)=(?P<val>\d*)')

def check_file(fname):
    """
    Confirms that the first line of the file indicates that it was
    output by xspp.

    """
    with OpenFilename(fname, 'r') as fobj:
        # check that it's an xspp file
        xshead = '% [XSHELLS]'
        line = '%'
        while line[0] == '%':
            line = fobj.readline()
            if line.startswith(xshead):
                return line[len(xshead):]
    raise ValueError('file must be output by xspp')

def get_kind(fname):
    '''check what kind of output the file is'''
    with OpenFilename(fname, 'r') as fobj:
        # check the first line for a %plot_*.py entry
        bookmark = fobj.tell()
        line = fobj.readline()
        quicktype = re.match(r'%plot_(\w*)\.py', line)
        fobj.seek(bookmark)
        if quicktype:
            return quicktype.group(1)
        # otherwise
        line = check_file(fobj)
        if line.find('Meridian slice') != -1:
            return 'merid'
        if line.find('spectrum') != -1:
            return 'spec'
        if line.find('coefficients') != -1:
            return 'SH'
        if line.find('disc') != -1:
            return 'disc'
        if line.find('Surface') != -1:
            return 'surf'
        if line.find('line') != -1:
            return 'line'

def load_fullspectrum(fname, dropzero=True, dropghost=True):
    with OpenFilename(fname, 'r') as fobj:
        line = check_file(fobj)
        # make sure this is a full spectrum
        assert 'l-contiguous storing' in line
        # get the resolution
        res = {m.group('spec'): int(m.group('val')) for m in  RESOLUTION_REGEX.finditer(line)}
        # make the column labels
        specind = [[l, m]
                   for m in range(0, res['MMAX']*res['MRES']+1, res['MRES'])
                   for l in range(m, res['LMAX']+1)]
        # transpose
        specind = [list(c) for c in zip(*specind)]
        colindex = pd.MultiIndex.from_arrays(specind, names=['L', 'M'])
        # get the r index from the comments
        with Popen(['grep', '-Po', r'%\s*ir=[^,]*,\s*r=\K[.\d]*', fobj.name], stdout=PIPE) as proc:
            index = pd.Series(np.loadtxt(proc.stdout))
        index.name = 'r'
        # read the data into a numpy array, then recast it as complex data of the same precision
        dat = np.loadtxt(fobj, comments='%')
        cdat = dat.view(dtype=np.complex128)
        assert cdat.size == dat.size / 2
        dat = pd.DataFrame(cdat, index=index, columns=colindex)
        # dat = pd.read_table(fobj, comment='%', sep='\s*', engine='python',
        #                     header=None, names=colindex).fillna(0)
        # dat.index = index
        # dat.columns.names = ('l', 'm', 'phase')
    if dropzero:
        dat = dat.reindex(columns=dat.columns[dat.sum(axis=0) != 0.0])
    if dropghost: # ghost points have r values of zero
        dat = dat.ix[dat.index[dat.index != 0]]
    return dat

def load_spec(fname, dropzero=True):
    '''read an output of xspp spec into a pandas dataframe'''
    with OpenFilename(fname, 'r') as fobj:
        line = check_file(fobj)
        # further check that this is a spectrum output
        line = line.split(':')
        assert len(line) > 1
        # get the resolution
        res = {m.group('spec'): int(m.group('val')) for m in  RESOLUTION_REGEX.finditer(line[1])}
        # construct the column label
        label = ''.join([r'$', line[0].strip()[0], '_',
                         {True: '{m}', False: '{\ell}'}['LMAX' in res.keys()],
                         r'$'])
        # get the number of columns in the first row
        for line in fobj:
            if line[0] == '%':
                continue
            ncol = len(line.strip().split())
            break
        names = ['r',] + [i for i in range(ncol)]
    # read in the data from the text file
    # set nans to zero energy
    dat = pd.read_table(fname, comment='%', sep=' ',
                        header=None, names=names,
                        index_col='r').fillna(0)
    # set the label based on the text in the resolution line
    dat.columns.name = label
    if dropzero:
        dat = dat.reindex(columns=dat.columns[dat.sum(axis=0) != 0.0])
    return dat

def load_line(fname):
    'read the output of xspp line into a pandas dataframe.'''
    with OpenFilename(fname, 'r') as fobj:
        line = check_file(fname)
        # read the second line, containing the header information
        line = fobj.readline() # need a second read, don't know why
        line = fobj.readline()
        assert line[0] == '%' # must be a comment
        # split the header by tabs
        line = line.strip('%').strip()
        names = re.split(r'\s*', line)
        groups = deque(re.split(r'\s*\t\s*', line))
        columns = []
        # line coordinate
        index = groups.popleft().strip('%').strip()
        # cartesian and spherical coordinates
        columns += [['cart', 'coord', g] for g in groups.popleft().split()]
        columns += [['sph', 'coord', g] for g in groups.popleft().split()]
        # scalar value
        if len(groups) == 1:
            columns += [['scalar', '', ''],]
        else:
            columns += [['cart', 'velocity', g] for g in groups.popleft().split()]
            columns += [['sph', 'velocity', g] for g in groups.popleft().split()]
        dat = pd.read_table(fobj, names=names, index_col=index, sep=' ')
    dat.columns = pd.MultiIndex.from_arrays(list(zip(*columns)))
    return dat

def xspp_run(fname, xsppopts, verbose=False, prefix=None, oneshell=False):
    '''Runs xspp

    Calls xspp and returns a dict of output files separated into kinds.
    '''
    try:
        xsppout = check_output(['xspp', fname] + xsppopts).decode('utf-8')
    except CalledProcessError as err:
        print(''.join(err.output.decode('utf-8')))
        raise err
    if verbose:
        print(xsppout)
    xsoutlist = [m.group(0) for m in re.finditer(r'\bo_\w*(\.m0|\.\d+)?', xsppout)]
    # if prefix is set, rename the output files
    if prefix is not None:
        preflist = [f.replace('o_', prefix, 1) for f in xsoutlist]
        for src, dst in zip(xsoutlist, preflist):
            shutil.os.rename(src, dst)
        xsoutlist = preflist
    # shells are suffixed by the shell number, which impedes the operations of xsppbatch
    if oneshell:
        shells = [(i, f) for i,f in enumerate(xsoutlist) if '_shell' in f]
        for i, shell in shells:
            dst = '.'.join(shell.split('.')[:-1]) # drop the numerical suffix
            shutil.os.rename(shell, dst)
            xsoutlist[i] = dst
        
    # use a set comprehension to prevent double counts
    xsppkinds = {k: list({f for f in xsoutlist if get_kind(f) == k})
                 for k in ('SH', 'spec', 'merid', 'disc', 'line', 'surf')}
    return xsppkinds

if __name__ == '__main__':
    import argparse
    from matplotlib import pyplot as plt
    from matplotlib.lines import Line2D
    from os import path
    import seaborn as sns
    cmap = sns.diverging_palette(220, 20, sep=20, as_cmap=True)
    # routines for dealing with the plot interactively
    class LineList:
        def __init__(self):
            self.list = []

        def onpick1(self, event):
            if isinstance(event.artist, Line2D):
                self.list.append(event.artist.get_label())

        def onrelease(self, event):
            self.list.sort()
            while self.list:
                print(self.list.pop())

    parser = argparse.ArgumentParser(
        description='plots output of xspp',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('flist', nargs='+', type=str,
                        help='file to read, must be output by xspp')
    parser.add_argument('plotopts', type=str, nargs='*',
                        help='options for plotting in the form OPT=VALUE')
    parser.add_argument('--label', type=str, default='xspp_read',
                        help='label for figure window')
    parser.add_argument('--reverse', action='store_true',
                        help='set to reverser order of flist')
    parser.add_argument('-dc', '--disc_component', type=str, default='p',
                        help='set to plot a specific component of a disc file')
    parser.add_argument('-so', '--subplotopts', type=str, nargs='+', default=['nrows=1', 'ncols=1'],
                        help='options for subplots in the form OPT=VALUE')
    clin = parser.parse_args()
    keyvals = (opt.split('=') for opt in clin.plotopts)
    kwargs = {k: v for k, v in keyvals}
    subplotkwargs = {k: v for k, v in [opt.split('=') for opt in clin.subplotopts]}
    conv_kwargs(kwargs)
    conv_kwargs(subplotkwargs)
    if 'figsize' not in subplotkwargs.keys():
        subplotkwargs['figsize'] = (4*subplotkwargs.get('ncols', 1), 4*subplotkwargs.get('nrows', 1))

    if clin.reverse:
        clin.flist = [f for f in clin.flist[::-1]]
    # get the unique directories
    dlist = {path.split(f)[0] for f in clin.flist}
    
    # set up the figures
    figsubs = {d: plt.subplots(num=d, squeeze=False, **subplotkwargs)
               for d in dlist}
    # get an iterator
    subs = {d: (f for f in chain(*figsubs[d][1])) for d in dlist}
    
    for fname in clin.flist:
        dname, base = path.split(fname)
        with OpenFilename(fname) as fobj:
            try:
                kind = get_kind(fname)
                print(fname, kind)
            except ValueError:
                print(fname)
                continue
            if kind == 'merid':
                plt.sca(subs[dname].__next__())
                plot_merid(*load_merid(fobj), cmap=cmap)
                plt.gca().annotate(base, (0.95, 0.1),
                                   xycoords='axes fraction',
                                   ha='right')
            if kind == 'line':
                dat = read_line(fname)
                ax = dat['sph']['velocity'].plot(ax=subs[dname].__next__(),
                                                 legend=True, picker=5, **kwargs)
                ax.set_title(base)
            if kind == 'spec':
                dat = load_spec(fname)
                # drop columns with zero values
                dat = dat.reindex(columns=dat.columns[dat.sum(axis=0) != 0.0])
                # kind of kludgy, but gets the spectral maxima out at least
                title = check_file(fname).split('.')[0].split(',')[0]
                ax = dat.plot(ax=subs[dname].__next__(), legend=False,
                              title=title, picker=5, **kwargs)
                ax.set_yscale('log')
                # plot the last mode extra thick
                dat[dat.columns[-1]].plot(ax=ax, legend=False, lw=5, **kwargs)
            if kind == 'disc':
                r, phi, vs, vp, vz = load_disc(fname)
                v = {'s': vs, 'p': vp, 'z': vz}[clin.disc_component]
                plt.sca(subs[dname].__next__())
                plot_disc(r, phi, v)
                plt.gca().annotate(base, (0.95, 0.1),
                                   xycoords='axes fraction',
                                   ha='right')
            if kind == 'SH':
                dat = read_fullspectrum(fname)
                ax = dat.plot(ax=subs[dname].__next__(), legend=False,
                              title=base, picker=5, **kwargs)
                ax.set_yscale('log')
        linelistobj = LineList()
    for sub in subs.values():
        for s in sub:
            s.set_axis_off()
    for fig, subs in figsubs.values():
        fig.canvas.mpl_connect('pick_event', linelistobj.onpick1)
        fig.canvas.mpl_connect('button_release_event', linelistobj.onrelease)

    plt.show()
