#!/usr/bin/python3
from xspar.xspar import parse_xspar

if __name__=='__main__':
    import argparse
    import pandas as pd
    from pandas import DataFrame

    outputformats = [to for to in dir(DataFrame()) if to.startswith('to_')] 
    pd.disp_width=(200)
    
    parser = argparse.ArgumentParser(
        description='Summarize parameter files')
    parser.add_argument('flist', nargs='+', type=str,
                        help='list of xshells.par files to parse')
    parser.add_argument('-p', '--params', nargs='+', type=str,
                        help='list of parameters to print')
    parser.add_argument('--sort_by', type=str, default=None,
                        help='parameter to sort by')
    for outputformat in outputformats:
        parser.add_argument('--'+outputformat, type=str, default=None,
                            help='set to output dataframe to {} format with specified name.'.format(outputformat[3:]))
    opts = vars(parser.parse_args())

    flist = opts.pop('flist')
    params = opts.pop('params')
    sort_by = opts.pop('sort_by')

    dat = DataFrame([parse_xspar(f, *params) for f in flist], index=flist, columns=params)
    
    if sort_by:
        dat.sort_index(by=sort_by, inplace=True)
    for outputformat in outputformats:
        if opts[outputformat]:
            getattr(dat, outputformat)(opts[outputformat])
    print(dat)
