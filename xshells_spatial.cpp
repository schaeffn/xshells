/*
 * Copyright (c) 2010-2020 Centre National de la Recherche Scientifique.
 * written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France).
 * 
 * nathanael.schaeffer@univ-grenoble-alpes.fr
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * 
 */

///\file xshells_spatial.cpp
/// 3D Fields definition and operations, in physical space + Non-Linear term computations.

/// Common spatial field info.
class Spatial {
  public:
	int irs, ire;			///< radial boundaries.
	int inz0, inz1;			///< radial boundaries of non-zero shells.
	unsigned ncomp, nshell, nelem;

  protected:
	xs_array2d<double> data;
//	unsigned ncomp, nshell, nelem;
	int ir_bci, ir_bco;			///< absolute (global) boundaries.

  public:
	Spatial();
	Spatial(int ncomp, int nelem, int istart, int iend);
	~Spatial();
	void alloc(int ncomp, int nelem, int istart, int iend);
	void free_field();
	void zero_out();
	double* get_data(int ic, int ir) const {
		return data[ic*nshell + ir-irs];		// no ghost shell for spatial fields.
	}

	double energy(int irstart=-10, int irend=(1<<27)) const;		///< computes energy.
	double absmax(int irstart=-10, int irend=(1<<27)) const;		///< compute the maximum of the absolute value of field.

	void write_hdf5(const char *fn, int irs, int ire, int cartesian=0, int irstep=1) const;		///< write hdf5+xdmf data on spherical grid (readable by octave and paraview)
	void write_shell(const char *fn, int ir) const;
};

/// a simple 3D, 1 component, scalar field definition.
class ScalField : public Spatial {
  public:
	xs_array2d<double> sca;

	ScalField();
	ScalField(int istart, int iend);		///< does not allocate field by default.
	void alloc(int istart, int iend, int nspat=0);

	void from_SH(class ScalarSH& SH);
	void square();

	double* operator[](int ir) const {
		return sca[ir];
	}
};

/// a simple 3D, 3 components, vector field definition.
class VectField : public Spatial {
  public:
	xs_array2d<double> vr, vt, vp;

	VectField();
	VectField(int istart, int iend);		///< does not allocate field by default.
	void alloc(int istart, int iend, int nspat=0);

	void from_PolTor(class PolTor* PT);
	void from_gradSH(class ScalarSH* SH);

	void square();
	void NL_vect(const VectField *A, const VectField *B);
	void NL_vect(const VectField *A, const VectField *B, int istart, int iend);
	void NL_vect_add(const VectField *A, const VectField *B);
	void NL_vect_add(const VectField *A, const VectField *B, int istart, int iend);
	void NL_Fluid(const VectField *V, const VectField *W);
	void NL_Fluid_Coriolis(const VectField *V, double Om0);
	void NL_Ind_Solid(SolidBody *sb, const VectField *B);
	void NL_Magnetic(const VectField *V, SolidBody *Vin, SolidBody *Vout, const VectField *B, const double alpha=0.0);

	void NL_dot(const VectField *A, const VectField *B);	/// compute the dot product (stored in vr component)
	void NL_dot_add(const VectField *A, const VectField *B);
	void NL_mul(const ScalField *F, const VectField *G);
	void NL_mul_add(const ScalField *F, const VectField *G);
};

Spatial::Spatial() {
	data.clear();
	ncomp = 0;		nshell = 0;		nelem = 0;
	irs = 10;		ire = 0;		inz0 = 10;		inz1 = 0;
	ir_bci = irs;	ir_bco = ire;
}

Spatial::Spatial(int nc, int ne, int istart, int iend) {
	data.clear();
	alloc(nc, ne, istart, iend);
}

void Spatial::free_field() {
	if (data) {
		VFREE(data[0]);
		data.clear();
	}
}

Spatial::~Spatial() {
	free_field();
}

/// istart,iend : global boundaries.
void Spatial::alloc(int nc, int ne, int istart, int iend)
{
	if (data) runerr("[Spatial::alloc]  already allocated.");
	ir_bci = istart;	ir_bco = iend;		// global boundaries.
	#ifdef XS_MPI
		if (istart < irs_mpi)	istart = irs_mpi;
		if (iend > ire_mpi)		iend = ire_mpi;
	#endif
	irs = istart;	ire = iend;		// local boundaries.
	if (iend < istart) return;		// nothing to alloc for me.
	const int nr = iend-istart+1;		// no ghost shells.	
	ne += (ne & 1);			// correction to have an even number of elements.
	ncomp = nc;		nelem = ne;		nshell = nr;
	inz0 = istart+1;		inz1 = istart;		// marked all zero.

	size_t sze = sizeof(double) * nc * ne * nr;
	double* mem = (double*) VMALLOC(sze);
	memset(mem, 0, sze);			// allows using absmax with padding
	data.init_from_buffer( mem, ne );
}

ScalField::ScalField() {
	sca.clear();		ncomp = 1;
}

ScalField::ScalField(int istart, int iend) {
	alloc(istart, iend);
}

void ScalField::alloc(int istart, int iend, int nspat)
{
	if (nspat==0) {
		nspat = 2*(NPHI/2+1)*NLAT;
		if (NPHI == 1) nspat = NLAT;		// no fft, so we don't need any additional storage.
		if (NLAT & 1) runerr("[ScalField::alloc] NLAT should be even");
	}
	Spatial::alloc(1, nspat, istart, iend);
	sca.init_from_buffer(data[0], nelem, irs);
}

void ScalField::from_SH(ScalarSH& SH)
{
	SH.to_spat(this);
}


void VectField::alloc(int istart, int iend, int nspat)
{
	if (nspat==0) {
		nspat = shtns->nspat;
	}
#ifdef _OPENMP
	if (nspat < 2*NLM) nspat = 2*NLM;	// the OpenMP version requires more storage here.
#endif
	if (nspat & 1) runerr("[VectField::alloc] nspat should be even");
	Spatial::alloc(3, nspat, istart, iend);
	vr.init_from_buffer(data[0], nelem, irs);
	vt.init_from_buffer(vr[nshell], nelem);
	vp.init_from_buffer(vr[2*nshell], nelem);
}

void Spatial::zero_out()
{
	if (data) {
		#pragma omp master
		{
			inz0 = irs + 1;		inz1 = irs;		// mark all zero.
		}
		int i0 = 0;		int i1 = nshell-1;
		thread_interval_rad(i0, i1);
		for (int ir=i0; ir<=i1; ir++) {
			v2d* x = (v2d*) data[ir];
			for (long k=nelem*ncomp; k>0; k-=2) {		// assumes interleaved components.
				*x++ = vdup(0.0);
			}
		}
		#pragma omp barrier
	}
}

/************************
 *** NON-LINEAR TERMS ***
 ************************/


void ScalField::square()
{
	int ir, lm;

	#pragma omp for schedule(static) private(ir,lm) nowait
	for (ir=irs; ir<=ire; ir++) {
		s2d* s = (s2d*) sca[ir];
		lm = 0;		do {
			s[lm] *= s[lm];
		} while(++lm < NS2D);
	}
	#pragma omp barrier
}

void VectField::square()
{
	int ir, lm;

	#pragma omp for schedule(static) private(ir,lm) nowait
	for (ir=irs; ir<=ire; ir++) {
		s2d* Vr = (s2d*) vr[ir];		s2d* Vt = (s2d*) vt[ir];		s2d* Vp = (s2d*) vp[ir];
		lm = 0;		do {
			Vr[lm] = Vr[lm]*Vr[lm] + Vt[lm]*Vt[lm] + Vp[lm]*Vp[lm];	// A.B
		} while(++lm < NS2D);
	}
	#pragma omp barrier
}



void VectField::NL_dot(const VectField *A, const VectField *B)
{
	int ir, lm;
	int istart, iend;

	if ((A == NULL) || (B == NULL)) {
		zero_out();
		return;
	}

	istart = irs;		iend = ire;
	// find common shells
	if (istart < A->inz0) istart = A->inz0;			if (iend > A->inz1) iend = A->inz1;
	if (istart < B->inz0) istart = B->inz0;			if (iend > B->inz1) iend = B->inz1;
	#pragma omp master
	{
		inz0 = istart;		inz1 = iend;		// save current non-zero boundaries.
	}

	#pragma omp for schedule(static) private(ir,lm) nowait
	for (ir=istart; ir<=iend; ir++) {
		s2d* Vr = (s2d*) A->vr[ir];		s2d* Vt = (s2d*) A->vt[ir];		s2d* Vp = (s2d*) A->vp[ir];
		s2d* Wr = (s2d*) B->vr[ir];		s2d* Wt = (s2d*) B->vt[ir];		s2d* Wp = (s2d*) B->vp[ir];
		s2d* dot = (s2d*) vr[ir];
		lm = 0;		do {
			dot[lm] = Vr[lm]*Wr[lm] + Vt[lm]*Wt[lm] + Vp[lm]*Wp[lm];	// A.B
		} while(++lm < NS2D);
	}
	#pragma omp for schedule(static) private(ir,lm) nowait
	for (ir=irs; ir<istart; ir++) {
		s2d* dot = (s2d*) vr[ir];
		lm = 0;		do {
			dot[lm] = vdup(0.0);
		} while(++lm < NS2D);
	}
	#pragma omp for schedule(static) private(ir,lm) nowait
	for (ir=iend+1; ir<=ire; ir++) {
		s2d* dot = (s2d*) vr[ir];
		lm = 0;		do {
			dot[lm] = vdup(0.0);
		} while(++lm < NS2D);
	}
	#pragma omp barrier
}

void VectField::NL_dot_add(const VectField *A, const VectField *B)
{
	int ir, lm;
	int istart, iend;

	if ((A == NULL) || (B == NULL)) return;

	istart = irs;		iend = ire;
	// find common shells
	if (istart < A->inz0) istart = A->inz0;			if (iend > A->inz1) iend = A->inz1;
	if (istart < B->inz0) istart = B->inz0;			if (iend > B->inz1) iend = B->inz1;
	#pragma omp master
	{
		inz0 = istart;		inz1 = iend;		// save current non-zero boundaries.
	}

	#pragma omp for schedule(static) private(ir,lm) nowait
	for (ir=istart; ir<=iend; ir++) {
		s2d* Vr = (s2d*) A->vr[ir];		s2d* Vt = (s2d*) A->vt[ir];		s2d* Vp = (s2d*) A->vp[ir];
		s2d* Wr = (s2d*) B->vr[ir];		s2d* Wt = (s2d*) B->vt[ir];		s2d* Wp = (s2d*) B->vp[ir];
		s2d* dot = (s2d*) vr[ir];
		lm = 0;		do {
			dot[lm] += Vr[lm]*Wr[lm] + Vt[lm]*Wt[lm] + Vp[lm]*Wp[lm];	// A.B
		} while(++lm < NS2D);
	}
	#pragma omp barrier
}

VectField::VectField() {
	vr.clear();		vt.clear();		vp.clear();		ncomp = 3;
}

VectField::VectField(int istart, int iend) {
	alloc(istart, iend);
}

void VectField::from_PolTor(PolTor* PT)
{
	PT->to_spat(this, PT->irs, PT->ire);
}

void VectField::from_gradSH(ScalarSH* SH)
{
	SH->to_grad_spat(this, irs, ire);
}

void VectField::NL_vect(const VectField *A, const VectField *B)
{
	NL_vect(A, B, irs, ire);
}

/// Compute A x B and store it to VectField.
void VectField::NL_vect(const VectField *A, const VectField *B, int istart, int iend)
{
	int ir, lm;

#ifdef XS_DEBUG
	if ((istart < irs)||(iend > ire)) runerr("[NL_vect] radial boundary error");
#endif

	if ((A == NULL) || (B == NULL)) {
		zero_out();
		return;
	}
	// find common shells
	if (istart < A->irs) istart = A->irs;			if (iend > A->ire) iend = A->ire;
	if (istart < B->irs) istart = B->irs;			if (iend > B->ire) iend = B->ire;

	mpi_interval(istart, iend);
	#pragma omp master
	{
		inz0 = istart;		inz1 = iend;		// save current non-zero boundaries.
	}

	#pragma omp for schedule(static) private(ir,lm) nowait
	for (ir=istart; ir<=iend; ir++) {
		s2d* Vr = (s2d*) A->vr[ir];		s2d* Vt = (s2d*) A->vt[ir];		s2d* Vp = (s2d*) A->vp[ir];
		s2d* Wr = (s2d*) B->vr[ir];		s2d* Wt = (s2d*) B->vt[ir];		s2d* Wp = (s2d*) B->vp[ir];
		s2d* Xr = (s2d*) vr[ir];		s2d* Xt = (s2d*) vt[ir];		s2d* Xp = (s2d*) vp[ir];
		lm = 0;		do {
			rnd vvt = vread(Vt,lm);		rnd vwt = vread(Wt,lm);
			rnd vvp = vread(Vp,lm);		rnd vwp = vread(Wp,lm);
			rnd vr = vvt*vwp - vvp*vwt;	// AxB
			rnd vvr = vread(Vr,lm);		rnd vwr = vread(Wr,lm);
			rnd vt = vvp*vwr - vvr*vwp;
			rnd vp = vvr*vwt - vvt*vwr;
			vstore(Xr,lm, vr);	vstore(Xt,lm, vt);	vstore(Xp,lm, vp);
		} while(++lm < NV2D);
	}
	#pragma omp for schedule(static) private(ir,lm) nowait
	for (ir=irs; ir<istart; ir++) {
		s2d* Xr = (s2d*) vr[ir];		s2d* Xt = (s2d*) vt[ir];		s2d* Xp = (s2d*) vp[ir];
		lm = 0;		do {
			Xr[lm] = vdup(0.0);	Xt[lm] = vdup(0.0);	Xp[lm] = vdup(0.0);
		} while(++lm < NS2D);
	}
	#pragma omp for schedule(static) private(ir,lm) nowait
	for (ir=iend+1; ir<=ire; ir++) {
		s2d* Xr = (s2d*) vr[ir];		s2d* Xt = (s2d*) vt[ir];		s2d* Xp = (s2d*) vp[ir];
		lm = 0;		do {
			Xr[lm] = vdup(0.0);	Xt[lm] = vdup(0.0);	Xp[lm] = vdup(0.0);
		} while(++lm < NS2D);
	}
	#pragma omp barrier
}

void VectField::NL_vect_add(const VectField *A, const VectField *B)
{
	NL_vect_add(A, B, irs, ire);
}

/// Compute A x B and adds it to field.
void VectField::NL_vect_add(const VectField *A, const VectField *B, int istart, int iend)
{
	int ir, lm;

	if ((A == NULL) || (B == NULL)) return;

#ifdef XS_DEBUG
	if ((istart < irs)||(iend > ire)) runerr("[NL_vect_add] radial boundary error");
#endif

	// find common shells
	if (istart < A->irs) istart = A->irs;			if (iend > A->ire) iend = A->ire;
	if (istart < B->irs) istart = B->irs;			if (iend > B->ire) iend = B->ire;

	mpi_interval(istart, iend);
	#pragma omp master
	{
		if (istart < inz0) inz0 = istart;		// update current non-zero boundaries.
		if (iend > inz1) inz1 = ire;
	}

	#pragma omp for schedule(static) private(ir,lm) nowait
	for (ir=istart; ir<=iend; ir++) {
		s2d* Vr = (s2d*) A->vr[ir];		s2d* Vt = (s2d*) A->vt[ir];		s2d* Vp = (s2d*) A->vp[ir];
		s2d* Wr = (s2d*) B->vr[ir];		s2d* Wt = (s2d*) B->vt[ir];		s2d* Wp = (s2d*) B->vp[ir];
		s2d* Xr = (s2d*) vr[ir];		s2d* Xt = (s2d*) vt[ir];		s2d* Xp = (s2d*) vp[ir];
		#ifdef XS_JxB_M0
		for (int it=0; it<VNLAT; it++) {	// assumes data is stored with consecutive theta, grouped by phi.
			rnd ar, at, ap;
			ar = vall(0.0);	at = vall(0.0);	ap = vall(0.0);
			for (int ip=0; ip<NPHI; ip++) {
				lm = it + ip*VNLAT;
				ar += vread(Vt,lm)*vread(Wp,lm) - vread(Vp,lm)*vread(Wt,lm);	// AxB summed over phi.
				at += vread(Vp,lm)*vread(Wr,lm) - vread(Vr,lm)*vread(Wp,lm);
				ap += vread(Vr,lm)*vread(Wt,lm) - vread(Vt,lm)*vread(Wr,lm);
			}
			ar *= vall(1.0/NPHI);	at *= vall(1.0/NPHI);	ap *= vall(1.0/NPHI);		// average over phi
			for (int ip=0; ip<NPHI; ip++) {
				lm = it + ip*VNLAT;
				vmemadd(Xr,lm, ar);		vmemadd(Xt,lm, at);		vmemadd(Xp,lm, ap);		// add and store
			}
		}
		#else
		lm = 0;		do {
			rnd vvt = vread(Vt,lm);		rnd vwt = vread(Wt,lm);
			rnd vvp = vread(Vp,lm);		rnd vwp = vread(Wp,lm);
			rnd vr = vvt*vwp - vvp*vwt;	// AxB
			rnd vvr = vread(Vr,lm);		rnd vwr = vread(Wr,lm);
			rnd vt = vvp*vwr - vvr*vwp;
			rnd vp = vvr*vwt - vvt*vwr;
			vmemadd(Xr,lm, vr);		vmemadd(Xt,lm, vt);		vmemadd(Xp,lm, vp);
		} while(++lm < NV2D);
		#endif
	}
	#pragma omp barrier
}

/// Compute F.G and adds it to field.
void VectField::NL_mul(const ScalField *F, const VectField *G)
{
	int ir, lm;
	int istart, iend;

	if ((F == NULL) || (G == NULL)) {
		zero_out();
		return;
	}

	istart = irs;		iend = ire;
	// find common shells
	if (istart < F->irs) istart = F->irs;			if (iend > F->ire) iend = F->ire;
	if (istart < G->irs) istart = G->irs;			if (iend > G->ire) iend = G->ire;
	#pragma omp master
	{
		inz0 = istart;		inz1 = iend;		// save current non-zero boundaries.
	}

	#pragma omp for schedule(static) private(ir,lm) nowait
	for (ir=istart; ir<=iend; ir++) {
		s2d* FF = (s2d*) (*F)[ir];
		s2d* Gr = (s2d*) G->vr[ir];		s2d* Gt = (s2d*) G->vt[ir];		s2d* Gp = (s2d*) G->vp[ir];
		s2d* Xr = (s2d*) vr[ir];		s2d* Xt = (s2d*) vt[ir];		s2d* Xp = (s2d*) vp[ir];
		lm = 0;		do {
			rnd ff = vread(FF, lm);
			rnd vr = ff*vread(Gr,lm);		rnd vt = ff*vread(Gt,lm);		rnd vp = ff*vread(Gp,lm);
			vstore(Xr,lm,  vr);	vstore(Xt,lm, vt);		vstore(Xp,lm, vp);
		} while(++lm < NV2D);
	}
	#pragma omp barrier
}

/// Compute F.G and adds it to field.
void VectField::NL_mul_add(const ScalField *F, const VectField *G)
{
	int ir, lm;
	int istart, iend;

	if ((F == NULL) || (G == NULL)) return;

	istart = irs;		iend = ire;
	// find common shells
	if (istart < F->irs) istart = F->irs;			if (iend > F->ire) iend = F->ire;
	if (istart < G->irs) istart = G->irs;			if (iend > G->ire) iend = G->ire;
	#pragma omp master
	{
		if (istart < inz0) inz0 = istart;		// update current non-zero boundaries.
		if (iend > inz1) inz1 = ire;
	}

	#pragma omp for schedule(static) private(ir,lm) nowait
	for (ir=istart; ir<=iend; ir++) {
		s2d* FF = (s2d*) (*F)[ir];
		s2d* Gr = (s2d*) G->vr[ir];		s2d* Gt = (s2d*) G->vt[ir];		s2d* Gp = (s2d*) G->vp[ir];
		s2d* Xr = (s2d*) vr[ir];		s2d* Xt = (s2d*) vt[ir];		s2d* Xp = (s2d*) vp[ir];
		lm = 0;		do {
			rnd ff = vread(FF, lm);
			rnd vr = ff*vread(Gr,lm);		rnd vt = ff*vread(Gt,lm);		rnd vp = ff*vread(Gp,lm);
			vmemadd(Xr,lm,  vr);	vmemadd(Xt,lm, vt);		vmemadd(Xp,lm, vp);
		} while(++lm < NV2D);
	}
	#pragma omp barrier
}

/// compute V x W and store it to NL
/// V must be the dynamic velocity field, because "Mask" magic is performed here.
void VectField::NL_Fluid(const VectField *V, const VectField *W)
{
	int ir,lm;

#ifdef XS_DEBUG
	if ((W->irs != irs)||(V->irs != irs)) runerr("[NL_Fluid] irs mismatch");
	if ((W->ire != ire)||(V->ire != ire)) runerr("[NL_Fluid] ire mismatch");
#endif
	#pragma omp master
	{
		inz0 = irs;		inz1 = ire;
	}

	#pragma omp for schedule(static) private(ir,lm)
	for (ir=irs; ir<=ire; ir++) {
		s2d* Vr = (s2d*) V->vr[ir];		s2d* Vt = (s2d*) V->vt[ir];		s2d* Vp = (s2d*) V->vp[ir];
		s2d* Wr = (s2d*) W->vr[ir];		s2d* Wt = (s2d*) W->vt[ir];		s2d* Wp = (s2d*) W->vp[ir];
		s2d* Xr = (s2d*) vr[ir];		s2d* Xt = (s2d*) vt[ir];		s2d* Xp = (s2d*) vp[ir];
  		{ lm=0;	do {		// 3*3 = 9 disjoint arrays.
			rnd vvt = vread(Vt,lm);		rnd vwt = vread(Wt,lm);
			rnd vvp = vread(Vp,lm);		rnd vwp = vread(Wp,lm);
			rnd vr = vvt*vwp - vvp*vwt;	// AxB
			rnd vvr = vread(Vr,lm);		rnd vwr = vread(Wr,lm);
			rnd vt = vvp*vwr - vvr*vwp;
			rnd vp = vvr*vwt - vvt*vwr;
			vstore(Xr,lm, vr);	vstore(Xt,lm, vt);	vstore(Xp,lm, vp);
		} while (++lm < NV2D); }
	}
}

/// compute V x (2*Om0.ez) and store it to NL
void VectField::NL_Fluid_Coriolis(const VectField *V, double Om0)
{
	rnd Wz0;
	int ir,lm,it;

#ifdef XS_DEBUG
	if (V->irs != irs) runerr("[NL_Fluid_Coriolis] irs mismatch");
	if (V->ire != ire) runerr("[NL_Fluid_Coriolis] ire mismatch");
#endif

	if (Om0 == 0.0) {
		zero_out();
		return;
	}

	Wz0 = vall(Om0 + Om0);	// vorticity.
	#pragma omp master
	{
		inz0 = irs;		inz1 = ire;
	}
	#pragma omp for schedule(static) private(ir,lm,it)
	for (ir=irs; ir<=ire; ir++) {
		s2d* Vr = (s2d*) V->vr[ir];		s2d* Vt = (s2d*) V->vt[ir];		s2d* Vp = (s2d*) V->vp[ir];
		s2d* Xr = (s2d*) vr[ir];		s2d* Xt = (s2d*) vt[ir];		s2d* Xp = (s2d*) vp[ir];
		{ lm=0;	it=0;	do {
			rnd wr = Wz0 * vread(ct,it);	rnd wt = Wz0 * vread(st,it);	// project wz to wr and -wt.
				it++;	if (it>=VNLAT) it=0;
			rnd vp = vread(Vp, lm);
			rnd nlp = - (vread(Vr,lm)*wt + vread(Vt,lm)*wr);
			vstore(Xr,lm, vp*wt);		vstore(Xt,lm, vp*wr);		vstore(Xp,lm, nlp);
		} while(++lm < NV2D); }
	}
}

void VectField::NL_Ind_Solid(struct SolidBody *sb, const VectField *B)
{
	if (sb == NULL) return;		// not defined.
	int ir0 = sb->irs;
	int ir1 = sb->ire;
	if (ir0 >= ir1) return;		// no solid body (boundary only).
	if ( (sb->Omega_x == 0.0) && (sb->Omega_y == 0.0) ) {
		#ifndef XS_DTS_POTENTIAL
		//return;	// Vp only induction does not require spatial computations (TODO: DOES NOT WORK YET !)
		#endif
	} else {
		// full solid induction support.
		if ((MRES != 1)||(MMAX<1)) runerr("[NL_Ind_Solid] equatorial rotation not supported without m=1");
	}

	if (ir0 < B->irs) ir0 = B->irs;		// reduce to magnetic field domain.
	if (ir1 > B->ire) ir1 = B->ire;
	if (ir1 < ir0) return;		// I have nothing to do here.
#ifdef XS_DEBUG
	if ((ir0 < irs)||(ir1 > ire)) runerr("[NL_Ind_Solid] radial boundary error");
#endif
	#pragma omp master
	{
		if (ir0 < inz0) inz0 = ir0;	// record induction zone.
		if (ir1 > inz1) inz1 = ir1;
	}

	sb->calc_spatial();
	#pragma omp for schedule(static) nowait
	for (int ir=ir0; ir<=ir1; ir++) {
		s2d* Br = (s2d*) B->vr[ir];		s2d* Bt = (s2d*) B->vt[ir];		s2d* Bp = (s2d*) B->vp[ir];
		s2d* VxBr = (s2d*) vr[ir];		s2d* VxBt = (s2d*) vt[ir];		s2d* VxBp = (s2d*) vp[ir];
		s2d* vpt = (s2d*) sb->vp_r;		s2d* vtt = (s2d*) sb->vt_r;
		rnd rr = vall(r[ir]);
		int lm=0; do {
			rnd xt = vread(vpt,lm) * rr;
			rnd xp = vread(vtt,lm) * rr;
			rnd xr = xp*vread(Bp,lm) - xt*vread(Bt,lm);
			rnd br = vread(Br,lm);
			xt *= br;		xp *= -br;
			vstore(VxBt,lm, xt);	vstore(VxBp,lm, xp);
			vstore(VxBr,lm, xr);		// do not write to VxBr before Br is no more required.
		} while(++lm < NV2D);
	}
	#pragma omp barrier
}

/// compute curl(VxB + alpha.B).
/// \param[in] V, B      are spatial vector fields (unchanged)
/// \param[in] Vin, Vout are the differential rotation of the solid inner and outer shells.
/// \param[out] VxB      is the temporary VxB resulting vector field, which is destroyed. (Note: VxB can be either of V or B or a third vector)
void VectField::NL_Magnetic(const VectField *V, struct SolidBody *Vin, struct SolidBody *Vout, const VectField *B, const double alpha)
{
	int ir0 = V->irs;
	int ir1 = V->ire;
	if (ir0 < B->irs) ir0 = B->irs;		// find common shells
	if (ir1 > B->ire) ir1 = B->ire;
#ifdef XS_DEBUG
	if ((ir0 < irs)||(ir1 > ire)) runerr("[NL_Magnetic] radial boundary error");
	if ((ir0 <= Vin->ire)||(ir1 >= Vout->irs)) runerr("[NL_Magnetic] fluid-solid overlap");
#endif
	#pragma omp master
	{
		inz0 = ir0;		// record induction zone.
		inz1 = ir1;
	}
	if (alpha == 0.0) {
		#pragma omp for schedule(static) nowait
		for (int ir=ir0; ir<=ir1; ir++) {
			s2d* Vr = (s2d*) V->vr[ir];		s2d* Vt = (s2d*) V->vt[ir];		s2d* Vp = (s2d*) V->vp[ir];
			s2d* Br = (s2d*) B->vr[ir];		s2d* Bt = (s2d*) B->vt[ir];		s2d* Bp = (s2d*) B->vp[ir];
			s2d* VxBr = (s2d*) vr[ir];		s2d* VxBt = (s2d*) vt[ir];		s2d* VxBp = (s2d*) vp[ir];
			int lm=0;	do {
				rnd vvt = vread(Vt,lm);		rnd vbt = vread(Bt,lm);
				rnd vvp = vread(Vp,lm);		rnd vbp = vread(Bp,lm);
				rnd vr = vvt*vbp - vvp*vbt;	// AxB
				rnd vvr = vread(Vr,lm);		rnd vbr = vread(Br,lm);
				rnd vt = vvp*vbr - vvr*vbp;
				rnd vp = vvr*vbt - vvt*vbr;
				vstore(VxBr,lm, vr);	vstore(VxBt,lm, vt);	vstore(VxBp,lm, vp);
			} while (++lm < NV2D);
		}
	} else {
		#pragma omp for schedule(static) nowait
		for (int ir=ir0; ir<=ir1; ir++) {
			double r3 = r[ir]*r[ir]*r[ir];
			rnd alpha_r = vall(alpha*r3*r3);
			s2d* Vr = (s2d*) V->vr[ir];		s2d* Vt = (s2d*) V->vt[ir];		s2d* Vp = (s2d*) V->vp[ir];
			s2d* Br = (s2d*) B->vr[ir];		s2d* Bt = (s2d*) B->vt[ir];		s2d* Bp = (s2d*) B->vp[ir];
			s2d* VxBr = (s2d*) vr[ir];		s2d* VxBt = (s2d*) vt[ir];		s2d* VxBp = (s2d*) vp[ir];
			int lm=0;	do {
				rnd vvt = vread(Vt,lm);		rnd vbt = vread(Bt,lm);
				rnd vvp = vread(Vp,lm);		rnd vbp = vread(Bp,lm);
				rnd vvr = vread(Vr,lm);		rnd vbr = vread(Br,lm);
				rnd vr = vvt*vbp - vvp*vbt + alpha_r*vbr;	// AxB + alpha*r^6*B
				rnd vt = vvp*vbr - vvr*vbp + alpha_r*vbt;
				rnd vp = vvr*vbt - vvt*vbr + alpha_r*vbp;
				vstore(VxBr,lm, vr);	vstore(VxBt,lm, vt);	vstore(VxBp,lm, vp);
			} while (++lm < NV2D);
		}
	}
	NL_Ind_Solid(Vin, B);
	NL_Ind_Solid(Vout, B);

	// wait for everyone
	#pragma omp barrier
}




/****************************
 *** DIAGNOSTIC FUNCTIONS ***
 ****************************/

void fill_stdt(double* stdt)
{
	int it;
	int nth_2 = (NLAT+1)/2;
// compute dtheta, assume symmetry.
	for (it=0; it <= nth_2; it++)
		stdt[it+1] = acos(ct[it]);		// theta
	it = 0;
		stdt[it] = st[it] * 0.5*(stdt[it+2] + stdt[it+1]);
	for (it=1; it<nth_2; it++)
		stdt[it] = st[it] * 0.5*(stdt[it+2] - stdt[it]);
	for (it=nth_2; it<NLAT; it++)
		stdt[it] = stdt[NLAT-1-it];
}

/// MPI: collective call, only rank 0 returns the correct value.
double Spatial::energy(int irstart, int irend) const
{
	if (irstart >= irend) return 0.0;
	if (irstart <= irs) irstart = irs;
	if (irend >= ire) irend = ire;

	double E = 0.0;
	double* stdt = new double[NLAT];		// array of size NLAT, required for integration.
	fill_stdt(stdt);	// for theta-integration.

	#pragma omp parallel for schedule(static) reduction(+ : E)
	for (int ir=irstart; ir<=irend; ir++) {
		double er = 0.0;
		for (int ic=0; ic<ncomp; ic++) {
			double *d = get_data(ic, ir);
			for (int ip=0; ip<NPHI; ip++) {
				for (int it=0; it<NLAT; it++) {
					int lm = ip*NLAT+it;
					er += d[lm]*d[lm] * stdt[it];
				}
			}
		}
		int ir_p = ir+1;
		int ir_m = ir-1;
		if (ir == irs)      ir_m = ir;
		else if (ir == ire) ir_p = ir;
		double dr = 0.5*(r[ir_p]-r[ir_m]);
		E += er * r[ir]*r[ir] * dr;
	}
	#ifdef XS_MPI
		double E2;
		MPI_Reduce(&E, &E2, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
		E = E2;			// only rank 0 has the correct energy here.
	#endif

	delete[] stdt;
	return E * (M_PI/NPHI);	// energy
}

/// compute Linf norm (maximum value) of a spatial field.
/// MPI: collective call, only rank 0 returns the correct value.
double Spatial::absmax(int irstart, int irend) const
{
	double maxmax = 0.0;
	if (irstart < irs)	irstart = irs;
	if (irend > ire)	irend = ire;

  #pragma omp parallel shared(maxmax)
  {
	double max,v2;
	int ic,ir,lm;

	max = 0.0;
	#pragma omp for schedule(static) nowait
	for (ir=irstart; ir<=irend; ir++) {
		for (lm=0; lm<shtns->nspat; lm++) {
			double *d = get_data(0, ir);
			v2 = d[lm]*d[lm];
			for (ic=1; ic<ncomp; ic++) {
				double *d = get_data(ic, ir);
				v2 += d[lm]*d[lm];
			}
			if (v2 > max) max=v2;
		}
	}
	#pragma omp critical
	{
		if (max > maxmax) maxmax = max;
	}
  }
	#ifdef XS_MPI
		double max2;
		MPI_Reduce(&maxmax, &max2, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
		maxmax = max2;			// only rank 0 has the correct max here.
	#endif
	return sqrt(maxmax);
}


/********************************************
 *** SPECTRAL/SPATIAL TRANSFORM FUNCTIONS ***
 ********************************************/

/// compute full spatial field from ScalarSH representation
void ScalarSH::to_grad_spat(class VectField *V) const
{
	to_grad_spat(V, irs, ire);
}

/// compute partial spatial field from ScalarSH representation
void ScalarSH::to_grad_spat(class VectField *V, int istart, int iend) const
{
	cplx *Q;	// dT/dr
	cplx *S;	// T/r

#ifdef XS_DEBUG
	if ((istart < irs)||(iend > ire)) runerr("[to_grad_spat] S radial domain mismatch !");
	if ((istart < V->irs)||(iend > V->ire)) runerr("[to_grad_spat] V radial domain mismatch !");
#endif

	mpi_interval(istart, iend);
	#pragma omp master
	{
		V->inz0 = istart;		V->inz1 = iend;		// save non-zero radial boundaries.
	}

	Q = (cplx*) VMALLOC(2*NLM*sizeof(cplx));
	S = Q + NLM;
	#pragma omp for schedule(FOR_SCHED_SH) nowait
	for (int ir = istart; ir <= iend; ir++) {
		Gradr(ir, Q, S);
		SH_SPAT(Q, V->vr[ir]);
		SHS_SPAT(S, V->vt[ir], V->vp[ir]);
	}
	VFREE(Q);

	// here we wait for everybody.
	#pragma omp barrier
}


/// compute spatial field from Poloidal/Toroidal representation.
// \todo : should B always be computed between 0 and NR ? even in Linear computations ?
// \todo : b only needed in fluid domain if J0 != 0 + where U0 !=0 (solid ? fluid ?)
// \todo : test for SolidBody rotation, and compute B there only if !=0 !!!
void ScalarSH::cte_to_grad_spat(StatSpecScal *T0, class VectField *V, int istart, int iend) const
{
	if ( (T0 == NULL) || (T0->nlm <= 0) ) {		// no background field.
  		to_grad_spat(V, istart, iend);
  		return;
	}

	unsigned *lma;
	v2d *TdT0;
	int ir, lm, nj;
	v2d *Q;	// dT/dr
	v2d *S;	// T/r

#ifdef XS_DEBUG
	if ((istart < irs)||(iend > ire)) runerr("[ScalarSH::cte_to_spat] PT radial domain mismatch !");
	if ((istart < V->irs)||(iend > V->ire)) runerr("[ScalarSH::cte_to_spat] V radial domain mismatch !");
#endif

	mpi_interval(istart, iend);
	#pragma omp master
	{
		V->inz0 = istart;		V->inz1 = iend;		// save non-zero radial boundaries.
	}
	nj = T0->nlm;		lma = T0->lm;		TdT0 = (v2d*) T0->TdT;

	Q = (v2d*) VMALLOC(2*NLM*sizeof(v2d));
	S = Q + NLM;
	#pragma omp for schedule(FOR_SCHED_SH) nowait
	for (ir=istart; ir<=iend; ir++) {
		s2d ri_1 = vdup(r_1[ir]);
		Gradr(ir, (cplx*) Q, (cplx*) S);
		v2d* TdT = TdT0 + ir*nj*2;	//QST[ir*nlm*2 + 2*j]
		int j=0;	do {		// Add Background Spectral
			lm = lma[j];
			S[lm] += TdT[j*2]*ri_1;		Q[lm] += TdT[j*2 +1];
		} while(++j < nj);
		SH_SPAT(Q, V->vr[ir]);
		SHS_SPAT(S, V->vt[ir], V->vp[ir]);
	}
	VFREE(Q);
	// here we wait for everybody.	
	#pragma omp barrier
}

/// compute full spatial field from Poloidal/Toroidal representation
void PolTor::to_spat(class VectField *V) const
{
	to_spat(V, irs, ire);
}

/// compute partial spatial field from Poloidal/Toroidal representation
void PolTor::to_spat(class VectField *V, int istart, int iend) const
{
	int ir;
	v2d* Q;	// l(l+1) * Pol/r
	v2d* S;	// dP/dr + Pol/r = 1/r.d(rP)/dr = Wr(Pol)

#ifdef XS_DEBUG
	if ((istart < irs)||(iend > ire)) runerr("[PolTor::to_spat] PT radial domain mismatch !");
	if ((istart < irs)||(iend > ire)) runerr("[PolTor::to_spat] V radial domain mismatch !");
#endif

	mpi_interval(istart, iend);
	#pragma omp master
	{
		V->inz0 = istart;		V->inz1 = iend;		// save non-zero radial boundaries.
	}

	Q = (v2d*) VMALLOC(2*NLM*sizeof(v2d));
	S = Q + NLM;
	#pragma omp for schedule(FOR_SCHED_SH) nowait
	for (ir = istart; ir <= iend; ir++) {
		RadSph(ir, (cplx *) Q, (cplx *) S );
		SHV3_SPAT(Q, S, Tor[ir], V->vr[ir], V->vt[ir], V->vp[ir]);
	}
	VFREE(Q);
	// here we wait for everybody.	
	#pragma omp barrier
}


void PolTor::cte_to_spat(struct StatSpecVect *V0, class VectField *V) const
{
	cte_to_spat(V0, V, irs, ire);
}

/// compute spatial field from Poloidal/Toroidal representation.
// \todo : should B always be computed between 0 and NR ? even in Linear computations ?
// \todo : b only needed in fluid domain if J0 != 0 + where U0 !=0 (solid ? fluid ?)
// \todo : test for SolidBody rotation, and compute B there only if !=0 !!!
void PolTor::cte_to_spat(struct StatSpecVect *V0, class VectField *V, int istart, int iend) const
{
	if ( (V0 == NULL) || (V0->nlm <= 0) ) {		// no background field.
  		to_spat(V, istart, iend);
  		return;
	}

	xs_array2d<double> Vr, Vt, Vp;
	v2d *QST0;
	unsigned *lma;
	int ir,lm, nj;
	v2d *Q, *S, *T;

#ifdef XS_DEBUG
	if ((istart < irs)||(iend > ire)) runerr("[PolTor::cte_to_spat] PT radial domain mismatch !");
	if ((istart < V->irs)||(iend > V->ire)) runerr("[PolTor::cte_to_spat] V radial domain mismatch !");
#endif

	mpi_interval(istart, iend);
	#pragma omp master
	{
		V->inz0 = istart;		V->inz1 = iend;		// save non-zero radial boundaries.
	}
	Vr = V->vr;			Vt = V->vt;			Vp = V->vp;
	nj = V0->nlm;		lma = V0->lm;		QST0 = (v2d*) V0->QST;

	Q = (v2d*) VMALLOC(3*NLM*sizeof(v2d));
	S = Q + NLM;		T = Q + 2*NLM;
	#pragma omp for schedule(FOR_SCHED_SH) nowait
	for (ir=istart; ir<=iend; ir++) {
		RadSph(ir, (cplx *) Q, (cplx *) S);
		v2d* Tr = (v2d*) Tor[ir];
		LM_LOOP(  T[lm] = Tr[lm];  )
		v2d* QST = QST0 + ir*nj*3;	//QST[ir*nlm*3 + 3*j]
		int j=0;	do {		// Add Background Spectral
			lm = lma[j];
			Q[lm] += QST[j*3];	S[lm] += QST[j*3+1];	T[lm] += QST[j*3+2];
		} while(++j < nj);
		SHV3_SPAT(Q, S, T, Vr[ir], Vt[ir], Vp[ir]);
	}
	VFREE(Q);
	// here we wait for everybody.	
	#pragma omp barrier
}


/// Compute W = curl(V) + Wz0.ez + W0 from PolTor components.
/// \param[in] Wz0 = background value along ez (2*Omega0)
/// \param[in] W0 = background vector field.
/// \param[out] W = r,theta,phi components of curl(V) + Wz0.ez + W0
void PolTor::cte_to_curl_spat(struct StatSpecVect *W0, class VectField *W, int istart, int iend, double Wz0) const
{
/// Pol = Tor;  Tor = - Lap.Pol
	s2d Wz;
	v2d *QST0;
	unsigned *lma;
	xs_array2d<double> Jr, Jt, Jp;
	int ir,lm, nj;
	v2d *Q, *S, *T;

#ifdef XS_DEBUG
	if ((istart < irs)||(iend > ire)) runerr("[PolTor::cte_to_curl_spat] PT radial domain mismatch !");
	if ((istart < W->irs)||(iend > W->ire)) runerr("[PolTor::cte_to_curl_spat] V radial domain mismatch !");
#endif

	mpi_interval(istart, iend);
	#pragma omp master
	{
		W->inz0 = istart;		W->inz1 = iend;		// save non-zero radial boundaries.
	}

	Wz = vdup(Wz0 * Y10_ct);			// multiply by representation of cos(theta) in spherical harmonics (l=1,m=0)
	Jr = W->vr;		Jt = W->vt;		Jp = W->vp;
	nj = 0;		// no additional constant field
	if ((W0 != NULL) && (W0->nlm > 0)) {
		nj = W0->nlm;		lma = W0->lm;		QST0 = (v2d*) W0->QST;
		Wz = vdup(0.0);		// W0 already contains Wz0
	}

	Q = (v2d*) VMALLOC(3*NLM*sizeof(v2d));
	S = Q + NLM;		T = Q + 2*NLM;
	#pragma omp for schedule(FOR_SCHED_SH) nowait
	for (ir=istart; ir <= iend; ir++) {
		curl_QST(ir, (cplx *)Q, (cplx *)S, (cplx *)T);
	// Pour Coriolis : ez ^ u
	//	ez = cos(theta).er - sin(theta).etheta
	//	cos theta = Y(m=0,l=1) * 2*sqrt(pi/3)		>> can be added to Qlm. (=> Vr)
	//	-sin theta = dY(m=0,l=1)/dt * 2*sqrt(pi/3)	>> can be added to Slm  (=> Vt)
		// add Background mode (l=1, m=0) [ie Coriolis force (2*Omega0) or Magnetic field (B0z)]
		lm = LiM(shtns,1,0);	Q[lm] += Wz;	S[lm] += Wz;
		if (nj > 0) {
			v2d* QST = QST0 + ir*nj*3;	//QST[ir*nlm*3 + 3*j]
			int j=0;	do {		// Add Background Spectral
				lm = lma[j];
				Q[lm] += QST[j*3];	S[lm] += QST[j*3+1];	T[lm] += QST[j*3+2];
			} while(++j < nj);
		}
		SHV3_SPAT(Q, S, T, Jr[ir], Jt[ir], Jp[ir]);
	}
	VFREE(Q);

	// here we wait for everybody.	
	#pragma omp barrier
}

/// Compute W = curl(V) + Wz0.ez  from PolTor components.
/// \param[in] Wz0 = background value along ez
/// \param[out] W = r,theta,phi components of curl(V) + Wz0.ez
void PolTor::to_curl_spat(class VectField *W, int istart, int iend, double Wz0) const
{
	cte_to_curl_spat(NULL, W, istart, iend, Wz0);
}



/// spatial to curl Pol/Tor
///	Pol <- Tor
///	Tor <- Q/r - 1/r.d(rS)/dr = (Q-S)/r - dS/dr
/// parallel-able version, with VectField V destroyed (used as temporary storage)
/// MPI: involves communications.
void PolTor::curl_from_spat(class VectField *V)
{
	xs_array2d<cplx> Plm, Tlm;
	int ir;
	int istart = irs;
	int iend = ire;
#ifdef XS_DEBUG
	if ((istart < V->irs)||(iend > V->ire)) runerr("[PolTor::curl_from_spat] radial domain mismatch !");
#endif
	Plm = Pol;		Tlm = Tor;

	if (istart < V->inz0) {
		#pragma omp for schedule(static) nowait
		for (ir=istart; ir< V->inz0; ir++) {
			v2d* Tr = (v2d*) Tlm[ir];		v2d* Pr = (v2d*) Plm[ir];
			LM_LOOP(  Pr[lm] = vdup(0.0);	Tr[lm] = vdup(0.0);  )
		}
		istart = V->inz0;
	}
	if (iend > V->inz1) {
		#pragma omp for schedule(static) nowait
		for (ir=V->inz1 +1; ir<=iend; ir++) {
			v2d* Tr = (v2d*) Tlm[ir];		v2d* Pr = (v2d*) Plm[ir];
			LM_LOOP(  Pr[lm] = vdup(0.0);	Tr[lm] = vdup(0.0);  )
		}
		iend = V->inz1;
	}

#ifndef _OPENMP

	v2d *Sl, *Sd, *Su;
	v2d *S = (v2d*) VMALLOC( 3*NLM* sizeof(cplx) );
	#ifdef XS_DEBUG
		if (S == NULL) runerr("[PolTor::curl_from_spat] VMALLOC error");
	#endif
	Sl = S;   Sd = S + NLM;   Su = S + 2*NLM;	// init pointers to buffer.

	ir = istart;	{
		SPAT_SHV(V->vt[ir], V->vp[ir], Sd, Plm[ir]);
		SPAT_SHV3(V->vr[ir], V->vt[ir+1], V->vp[ir+1], Tlm[ir], Su, Plm[ir+1]);
		s2d dx = vdup( 1.0/(r[ir+1]-r[ir]) );
		v2d* Tr = (v2d*) Tlm[ir];
		s2d r1 = vdup(r_1[ir]);
		LM_LOOP(  Tr[lm] = r1*(Tr[lm] - Sd[lm]) - dx*(Su[lm] - Sd[lm]);  )
	}
	for (ir=istart+1; ir < iend; ir++) {
		v2d* St = Sl;	Sl = Sd;	Sd = Su;	Su = St;		// rotate buffers.
		SPAT_SHV3(V->vr[ir], V->vt[ir+1], V->vp[ir+1], Tlm[ir], Su, Plm[ir+1]);
		OpCurlLapl& W = CURL_LAPL(ir);
		s2d r1 = vdup(W.r_1);
		v2d* Tr = (v2d*) Tlm[ir];
		s2d Wl = vdup(W.Wl);	s2d Wd = vdup(W.Wd);	s2d Wu = vdup(W.Wu);
		LM_LOOP(  Tr[lm] = r1*Tr[lm] - Wl * Sl[lm] - Wd * Sd[lm] - Wu * Su[lm];  )
	}
	ir = iend;	{
		v2d* St = Sl;	Sl = Sd;	Sd = Su;	Su = St;		// rotate buffers.
		SPAT_SH(V->vr[ir], Tlm[ir]);
		s2d dx = vdup( 1.0/(r[ir]-r[ir-1]) );
		v2d* Tr = (v2d*) Tlm[ir];
		s2d r1 = vdup(r_1[ir]);
		LM_LOOP(  Tr[lm] = r1*(Tr[lm] - Sd[lm]) - dx*(Sd[lm] - Sl[lm]);  )
	}
	VFREE(S);

#else

	xs_array2d<cplx> Slm;
	// BAD IDEA: use Vr as temp array (overwritten). requires shtns->nlat >= 2*NLM
	Slm.init_from_buffer((cplx*) (V->vr[0]), V->vr.get_dist() / 2);

	#pragma omp for schedule(FOR_SCHED_SH) nowait
	for (ir=istart; ir <= iend; ir++) {
		SPAT_SH(V->vr[ir], Tlm[ir]);	// compute Tlm from Vr first. Vr not required any more.
		SPAT_SHV(V->vt[ir], V->vp[ir], Slm[ir], Plm[ir]);	// overwrite Vr
	}
	#pragma omp barrier

	#ifdef XS_MPI
	  #pragma omp master
	  {
		if (share_dn)
			MPI_Isend(Slm[irs], 2*NLM, MPI_DOUBLE, i_mpi-1, irs, MPI_COMM_WORLD, 0);
		if (share_up) {
			MPI_Recv(Tlm[ire+1], 2*NLM, MPI_DOUBLE, i_mpi+1, ire+1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);		// blocking receive
			MPI_Isend(Slm[ire], 2*NLM, MPI_DOUBLE, i_mpi+1, ire, MPI_COMM_WORLD, 0);
		}
		if (share_dn)
			MPI_Recv(Tlm[irs-1], 2*NLM, MPI_DOUBLE, i_mpi-1, irs-1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);		// blocking receive
	  }
	  #pragma omp barrier
	#endif

	#pragma omp for schedule(FOR_SCHED_SH) nowait
	for (ir=istart; ir <= iend; ir++) {
		OpCurlLapl& W = CURL_LAPL(ir);
		v2d* Tr = (v2d*) Tlm[ir];		v2d* Sd = (v2d *) Slm[ir];
		if ((ir>ir_bci)&&(ir<ir_bco)) {
			v2d* Sl = (v2d *) Slm[ir-1];		v2d* Su = (v2d *) Slm[ir+1];
			#ifdef XS_MPI
				if (ir==irs) Sl = (v2d*) Tlm[ir-1];
				if (ir==ire) Su = (v2d*) Tlm[ir+1];
			#endif
			s2d Wl = vdup(W.Wl);	s2d Wd = vdup(W.Wd);	s2d Wu = vdup(W.Wu);
			s2d r1 = vdup(W.r_1);
			LM_LOOP(  Tr[lm] = r1*Tr[lm] - (Wl * Sl[lm] + Wd * Sd[lm] + Wu * Su[lm]);  )
		} else {		// order 1 approx at boundary.
			// TODO: must match solid-body induction in case of B, zero in case of no-slip (don't care).
			// Best is probably not to compute on the boundary, besides for free-slip.
			int ii = (ir==ir_bci) ? ir+1 : ir-1;
			s2d dx = vdup(1.0/(r[ii]-r[ir]));
			s2d r1 = vdup(r_1[ir]);
			v2d* Si = (v2d *) Slm[ii];
			LM_LOOP(  Tr[lm] = r1*(Tr[lm] - Sd[lm]) - dx*(Si[lm] - Sd[lm]);  )
		}
	}
#endif
	#pragma omp barrier
}



/// compute full spatial field from ScalarSH representation
void ScalarSH::to_spat(ScalField *S) const
{
	to_spat(S, irs, ire);
}

/// compute partial spatial field from ScalarSH representation
void ScalarSH::to_spat(ScalField *S, int istart, int iend) const
{
	int ir;

	mpi_interval(istart, iend);
#ifdef XS_DEBUG
	if ((istart < irs)||(iend > ire)) runerr("[ScalarSH::to_spat] spectral radial domain mismatch !");
	if ((istart < S->irs)||(iend > S->ire)) runerr("[ScalarSH::to_spat] spatial radial domain mismatch !");
#endif

	#pragma omp master
	{
		S->inz0 = istart;		S->inz1 = iend;		// save non-zero radial boundaries.
	}
	#pragma omp for schedule(FOR_SCHED_SH) nowait
	for (ir = istart; ir <= iend; ir++) {
		SH_SPAT( Sca[ir], (*S)[ir] );
	}

	// here we wait for everybody.
	#pragma omp barrier
}

void ScalarSH::cte_to_spat(StatSpecScal *S0, ScalField *S) const
{
	cte_to_spat(S0, S, irs, ire);
}

void ScalarSH::cte_to_spat(StatSpecScal *S0, ScalField *S, int istart, int iend) const
{
	if ( (S0 == NULL) || (S0->nlm <= 0) ) {		// no background field.
  		to_spat(S, istart, iend);
  		return;
	}

	unsigned *lma;
	v2d *TdT0;
	int ir, lm, nj;
	v2d *Q;		// T

	mpi_interval(istart, iend);
#ifdef XS_DEBUG
	if ((istart < irs)||(iend > ire)) runerr("[ScalarSH::cte_to_spat] spectral radial domain mismatch !");
	if ((istart < S->irs)||(iend > S->ire)) runerr("[ScalarSH::cte_to_spat] spatial radial domain mismatch !");
#endif

	#pragma omp master
	{
		S->inz0 = istart;		S->inz1 = iend;		// save non-zero radial boundaries.
	}
	nj = S0->nlm;		lma = S0->lm;		TdT0 = (v2d*) S0->TdT;

	Q = (v2d*) VMALLOC(NLM*sizeof(v2d));
	#pragma omp for schedule(FOR_SCHED_SH) nowait
	for (ir = istart; ir <= iend; ir++) {
		v2d* Sr = (v2d*) Sca[ir];
		v2d* TdT = TdT0 + ir*nj*2;	//TdT[ir*nlm*2 + 2*j]
		LM_LOOP( Q[lm] = Sr[lm]; )
		int j=0;	do {		// Add Background Spectral
			lm = lma[j];
			Q[lm] += TdT[j*2];
		} while(++j < nj);
		SH_SPAT( Q, (*S)[ir] );
	}
	VFREE(Q);

	// here we wait for everybody.
	#pragma omp barrier
}

/// transform scalar data from (the first component of) a spatial field.
void ScalarSH::from_spat(Spatial *V)
{
	int ir, istart, iend;

	istart = V->inz0;		iend = V->inz1;
#ifdef XS_DEBUG
	if ((irs > istart)||(ire < iend)) runerr("[from_spat] spatial radial domain mismatch !");
#endif

	#pragma omp for schedule(FOR_SCHED_SH) nowait
	for (ir = istart; ir <= iend; ir++) {
		double *datar = V->get_data(0, ir);
		SPAT_SH( datar, Sca[ir] );
	}

	// here we wait for everybody.
	#pragma omp barrier	
}

void ScalarSH::from_grad_spat(VectField& V)
{
	cplx *Tlm;
	int ir, istart, iend;

	istart = V.irs;		iend = V.ire;
#ifdef XS_DEBUG
	if ((irs > istart)||(ire < iend)) runerr("[from_grad_spat] spatial radial domain mismatch !");
#endif

	Tlm = (cplx*) VMALLOC(NLM*sizeof(cplx));
	#pragma omp for schedule(FOR_SCHED_SH) nowait
	for (ir = istart; ir <= iend; ir++) {
		SPAT_SHV(V.vt[ir], V.vp[ir], Sca[ir], Tlm);	// Tlm is discarded here (hopefully small)
		LM_LOOP2( 1, NLM-1, Sca[ir][lm] *= r[ir]; )
		spat_to_SH_l( shtns, V.vr[ir], Tlm, 0 );		// we only need l=0 here.
		Sca[ir][0] = Tlm[0];	// this must be integrated later.
	}
	VFREE(Tlm);
	#pragma omp barrier

	// radial integration of l=0, m=0 component.
	#pragma omp single
	{
		ir = V.irs;
		double sum = 0;						// start with 0.
		double p0 = Sca[ir][0].real();		// keep value for next step.
		Sca[ir][0] = sum;
		for (ir++; ir<=V.ire; ir++) {
			double p1 = Sca[ir][0].real();
			sum += (p0 + p1)*0.5*(r[ir]-r[ir-1]);
			Sca[ir][0] = sum;
			p0 = p1;
		}
	}
}

