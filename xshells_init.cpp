/*
 * Copyright (c) 2010-2020 Centre National de la Recherche Scientifique.
 * written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France).
 * 
 * nathanael.schaeffer@univ-grenoble-alpes.fr
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * 
 */

/// \file xshells_init.cpp initialization and parameter reading functions.

#include <vector>
#include "mparser.hpp"
mparser mp;		// math parser.

/// Class to record diagnostics.
class diagnostics : public std::vector<double> {
	int ndiags = 0;		// record number of diagnostics
  public:
	std::string header;
	void reset() {
		resize(0);
	}
	/// adds n elements to the diagnostic vector and returns a pointer to the first added element.
	/// the caller can then simply fill the reserved elements with values.
	double* append(const int n, const char *s = 0) {
		if (n<=0) return 0;
		int ofs = size();
		resize(ofs+n);			// this may trigger reallocation; also sets the new values to zero.
		if (ofs+n > ndiags) {	// append header info for the new group
			if (s) {
				header += std::string(s);
			} else {
				for (int k=0; k<n; k++) header += "? ";
				header += "\t ";
			}
			ndiags += n;
		}
		return &((*this)[ofs]);
	}
	int get_index(const std::string &key) const {
		int n = header.find(key);
		if (n == std::string::npos) return -1;	// key not found.
		int w = 0;
		int i = 3;		// skip the "% t" chars of the header
		while (i<n) {
			int j = header.find_first_of(", \t", i);
			if (j>i) {	// new word found
				w+=1;
				//printf("%c : %d,%d words=%d\n",header[i], i, j, w);
			}
			i=j+1;
		}
		if (w>4) w-=1;		// correct for "dt" added in between
		//printf("diag %s is at position %d, values=%g\n", key.c_str(), w, at(w));
		return w;
	}
	double get(const std::string &key) const {
		int i = get_index(key);
		if (i>=0) return at(i);
		return NAN;		// key not found.
	}
};

/// read and parse a single argument (error if more than one argument is given)
static int read_1arg(const char* args, double* val) {
	if (args[0] == 0) return 0;			// no argument
	int res = mp.parse(args, val);
	if (res) runerr("[read_1arg] parse error");
	return 1;
}

/// read and parse one real argument. Update args to the next argument.
static int read_next_arg(const char* &args, double* val) {
	if (args==0) return 0;		// nothing to do
	const char* end = strchr(args, ',');		// find first delimiter
	char dump[240];
	if (end) {		// split needed
		strncpy(dump, args, end-args);	dump[end-args] = 0;
		args = dump;
		end ++;		// point past delimeter
	}
	int n = read_1arg(args, val);
	args = end;
	return n;
}

/// read and parse one integer argument. Update args to the next argument.
static int read_next_arg(const char* &args, int* val) {
	double tmp = *val;
	int n = read_next_arg(args, &tmp);
	*val = tmp;		// convert to integer
	if (fabs(tmp - *val) > 1e-10*tmp) PRINTF0(COLOR_WRN "** WARNING **: real argument rounded to integer!" COLOR_END "\n");		// warn if value is not an int!
	return n;
}

/// read and parse all arguments in string. Rerurns them into a vector.
void read_all_args(const char* args, std::vector<double>& vals)
{
	vals.clear();
	double x = 0;
	while ( read_next_arg(args, &x) )	vals.push_back(x);
}


/* INITIAL OR IMPOSED FIELDS */
#if defined(XS_LEGACY) || defined(XS_LINEAR)
#define CURRENT_FREE Blm.zero_curl = 1;		// mark field as current free
#else
#define CURRENT_FREE 0;
#endif

#define BEGIN_FIELD(field_name, desc) \
	if (strcmp(name,field_name) == 0) { \
		if (i_mpi==0) printf("   + predefined field '%s' : %s\n", name, desc);
#define END_FIELD return(1); }

/// This function is called to set initial magnetic field value. (see xshells.par)
int init_B0(PolTor &Blm, char *name, double b0, const char *args)
{
	cplx z;		// temp variable.
	double rr, v;
	int i,l,m;
	const int irs = Blm.ir_bci;
	const int ire = Blm.ir_bco;

BEGIN_FIELD( "dipoleBC", "dipolar boundary conditions (imposed)")
	CURRENT_FREE	// mark magnetic field as current-free
	char d = args[0];
	i = (d=='i') ? irs-1 : ire+1;	// inner-boundary selected by 'i' argument, outer boundary by default
	l=1;
		Blm.set_Prlm(i, l, 0, b0 * Y10_ct );
END_FIELD

BEGIN_FIELD( "uniform", "uniform field (imposed)" )
	CURRENT_FREE	// mark magnetic field as current-free
	cplx z0,z1;
	double angle = 0.0;
	if ((strlen(args) > 0) && mp.parse(args, &angle)) {	// not an angle, try a symbolic axis (x, y or z):
		char d = 'z';
		sscanf(args,"%c",&d);
		if (d=='x')      {  z1 = Y11_st;			z0 = 0.0;  }
		else if (d=='y') {  z1 = cplx(0,Y11_st);	z0 = 0.0;  }
		else if (d=='z') {  z0 = Y10_ct;			z1 = 0.0;  }
		else runerr("bad argument");
		PRINTF0("   + along %c axis\n", d);
	} else {
		z0 = Y10_ct * cos(angle * M_PI/180.);
		z1 = Y11_st * sin(angle * M_PI/180.);
		PRINTF0("   + tilted by %g degrees\n", angle);
	}
	if ( (z1 != 0.0) && ((MRES!=1) || (MMAX==0)) ) runerr("m=1 does not exist!");
	l=1;
	for (i=irs; i <= ire+1; i++) {		// impose at outer boundary.
		rr = Blm.radius(i);
		Blm.set_Prlm(i, l, 0,  b0*z0* rr/2. );		// m=0
		if (z1 != 0.0) Blm.set_Prlm(i, l, 1,  b0*z1* rr/2. );		// m=1
	}
END_FIELD

BEGIN_FIELD( "dipole", "current-free internal dipole (imposed)" )
	CURRENT_FREE	// mark magnetic field as current-free
	l=1;
	if (r[irs] == 0) runerr("internal dipole singular at r=0");
	char d = args[0];
	if (strlen(args) > 1) runerr("bad argument");	// only 1 char accepted.
	switch(d) {
		case 'x' : m=1;		z = Y11_st;
			break;
		case 'y' : m=1;		z = cplx(0, Y11_st);
			break;
		case 0 :	// default
		case 'z' : m=0;		z = Y10_ct;
			break;
		default : runerr("bad argument");
	}
	PRINTF0("   + along %c axis\n", d);
	for (i=irs-1; i <= ire; i++) {
		rr = Blm.radius(i);
		Blm.set_Prlm(i, l, m,  b0* z/(2.*rr*rr) );
	}
END_FIELD

BEGIN_FIELD( "bigsister", "bigsister current loop (imposed)" )
	CURRENT_FREE	// mark magnetic field as current-free
	double rloop = 1.89/1.46 * r[NM];
	int ltr = 11;
	m=0;
	for (i=irs; i <= ire+1; i++) {		// include boundary forcing.
		double rr = (Blm.radius(i)/rloop);
		double f = b0*sqrt(4./(3.*M_PI));		// normalization: |B(r=0)| = b0
		for (l=1; l <= ltr; l+=2) {
			Blm.set_Prlm(i, l, m, pow(rr, l) * f * sqrt(4*M_PI/(2*l+1)) );
			f *= -l/(l+3.);
		}
	}
END_FIELD

BEGIN_FIELD( "bench2001", "magnetic field used in Christensen et al 2001 for case 1" )
	for (i=Ulm.ir_bci; i <= Ulm.ir_bco; i++) {		// only in fluid shell
		rr = Blm.radius(i);
		double ri = r[Ulm.ir_bci];		// boundaries of fluid shell
		double ro = r[Ulm.ir_bco];
		double rs4 = ri*ri/rr;		rs4 *= rs4;
		l=1;	m=0;
			z = (4*ro*rr - 3*rr*rr - rs4)/8 * Y10_ct;
			Blm.set_Prlm(i, l, m,  b0* z );
		l=2;	m=0;
			z = sin(M_PI*(rr - ri)/(ro-ri)) * 4/3*sqrt(M_PI/5);		// zero at ri and ro
			if ((i == irs) || (i == ire))  z = 0.0;		// toroidal MUST be zero at the boundaries of the conducting domain.
			Blm.set_Trlm(i, l, m, b0 * z );
	}
	for (i=Ulm.ir_bco+1; i <= ire; i++) {		// solid region: poloidal dipole field (current-free)
		rr = Blm.radius(i);
		double ri = r[Ulm.ir_bci];		// boundaries of fluid shell
		double ro = r[Ulm.ir_bco];
		double rs4 = ri*ri/ro;		rs4 *= rs4;
		l=1;	m=0;
			z = (4*ro*ro - 3*ro*ro - rs4)/8 * Y10_ct * pow(rr/ro, -(l+1));
			Blm.set_Prlm(i, l, m,  b0* z );
	}
END_FIELD

BEGIN_FIELD( "bench2001_2", "magnetic field used in Christensen et al 2001 for case 2" )
	for (i=irs; i <= Ulm.ir_bco; i++) {		// in solid core, up to fluid shell.
		rr = Blm.radius(i);
		double ro = r[Ulm.ir_bco];		// outer fluid radius
		l=1;	m=0;
			z = rr*(4*ro - 3*rr)/(6 + 2*ro) * Y10_ct;
			Blm.set_Prlm(i, l, m,  b0* z );
		l=2;	m=0;
			z = sin(M_PI*rr/ro) * 4/3*sqrt(M_PI/5);		// zero at ro
			if ((i == irs) || (i == ire))  z = 0.0;		// toroidal MUST be zero at the boundaries of the conducting domain.
			Blm.set_Trlm(i, l, m, b0 * z );
	}
	for (i=Ulm.ir_bco+1; i <= ire; i++) {		// solid region: poloidal dipole field (current-free)
		rr = Blm.radius(i);
		double ro = r[Ulm.ir_bco];		// outer fluid radius
		l=1;	m=0;
			z = ro*ro/(6 + 2*ro) * Y10_ct * pow(rr/ro, -(l+1));
			Blm.set_Prlm(i, l, m,  b0* z );
	}
END_FIELD


BEGIN_FIELD( "bench_fs", "initial magnetic field for the full-sphere benchmark" )
	b0 *= sqrt(4.*M_PI);
	for (i=irs; i<=ire; i++) {
		rr = Blm.radius(i);		double r2 = rr*rr;		double r4 = r2*r2;		double r6 = r4*r2;
		l=1;	m=1;
			z = b0 *rr*(3 -12*r2 +18*r4 -9*r6)/4 * cplx(1,1);
			if ((i>irs)&&(i<ire))
				Blm.set_Trlm(i, l, m, z * sqrt(2./3) );
		l=2;	m=0;
			z = b0 *r2*(6 -21*r2 +27*r4 -12*r6)/4;
			if ((i>irs)&&(i<ire))
				Blm.set_Trlm(i, l, m, z * sqrt(1./5) );
	}
END_FIELD

BEGIN_FIELD( "curfree", "current free multipole (imposed)" )
	CURRENT_FREE	// mark magnetic field as current-free
	l=1;	m=0;		// default to axial dipole
	double x = 1.0;		// default to pure external source
	read_next_arg(args, &l);
	read_next_arg(args, &m);
	read_next_arg(args, &x);
	if (r[irs] == 0.0)	x = 1.0;		// no internal source possible (divergence).
	v = 1.0-x;
	PRINTF0("   + l = %d, m = %d, %3.2lf external, %3.2lf internal\n", l,m, x,v);
	for (i=irs; i <= ire; i++) {
		rr = Blm.radius(i);
		double z = 0.0;
		if (x != 0.0)	z += x*pow(rr, l);		// source externe
		if (v != 0.0)	z += v*pow(rr, -(l+1));		// source interne
		Blm.set_Prlm(i, l, m,  b0*z );
	}
	if (v != 0.0) {		// internal source imposed
		i = irs-1;
		rr = Blm.radius(i);
		double z = v*pow(rr, -(l+1));
		Blm.set_Prlm(i, l, m,  b0*z );
	}
	if (x != 0.0) {		// external source imposed
		i = ire+1;
		rr = Blm.radius(i);
		double z = x*pow(rr, l);
		Blm.set_Prlm(i, l, m,  b0*z );
	}
END_FIELD

BEGIN_FIELD( "toraxi", "axisymmetric toroidal field" )
	l=1; m=0;
	for (i=irs+1; i < ire; i++) {
		rr = Blm.radius(i);
		z = Y10_ct*(rr-r[irs])*(r[ire]-rr);          // Toroidal vanish at both ends.
		Blm.set_Trlm(i, l, m,  b0* z );
	}
END_FIELD

BEGIN_FIELD( "dj08", "Jault 2008 (not current-free) normalized version" )
	for (i=irs; i <= ire; i++) {
		rr = Blm.radius(i);
	  if (rr != 0.0) {
		l=1; m=0;
			v = pi * rr;
			z = ((sin(v)/v-cos(v))/v - 0.3*(sin(2*v)/(2*v)-cos(2*v))/(2*v)) *sqrt(4.*pi/(2*l+1));	// j1(pi*rr) - 0.3*j1(2*pi*rr)
			Blm.set_Prlm(i, l, m,  b0* z );
		l=3; m=0;
			v = 5.7635 * rr;
			z = -0.2*((sin(v)*(3./(v*v)-1.)-3.*cos(v)/v)/v) *sqrt(4.*pi/(2*l+1));	// -0.2*j3(k*rr)
			Blm.set_Prlm(i, l, m,  b0* z );
	  }
	}
END_FIELD

BEGIN_FIELD( "dj3D", "3D based on Jault 2008 (dipole symmetry, not current-free, normalized version, no toroidal)" )
	double b1 = 0.0;
	read_1arg(args, &b1);
	PRINTF0("   + b1 = %g\n", b1);
	for (i=irs; i <= ire; i++) {
		rr = Blm.radius(i);
	  if (rr != 0.0) {
		l=1; m=0;
			v = pi * rr;
			z = ((sin(v)/v-cos(v))/v - 0.3*(sin(2*v)/(2*v)-cos(2*v))/(2*v));	// j1(pi*rr) - 0.3*j1(2*pi*rr)
			Blm.set_Prlm(i, l, m,  b0* z *sqrt(4.*pi/(2*l+1)) );
//          if ((i>irs)&&(i<ire))    Blm.set_Trlm(i, l, m,  10.0 * b0*(rr-1.)*(rr-0.35)* z *sqrt(4.*pi/(2*l+1)) );
		l=MRES*2+1; m=MRES;
			if (MMAX>0) Blm.set_Prlm(i, l, m,  10.*rr*rr/(l*l) * b0* b1*z *sqrt(4.*pi/(2*l+1)) );
		l=3; m=0;
			v = 5.7635 * rr;
			z = -0.2*((sin(v)*(3./(v*v)-1.)-3.*cos(v)/v)/v);	// -0.2*j3(k*rr)
			Blm.set_Prlm(i, l, m,  b0* z *sqrt(4.*pi/(2*l+1)) );
		l=MRES*2+4; m=MRES;
			if (MMAX>0) Blm.set_Prlm(i, l, m,  10.*rr*rr/(l*l) * b0* b1*z *sqrt(4.*pi/(2*l+1)) );
	  }
	}
END_FIELD

BEGIN_FIELD( "quad3D", "3D quadrupole symmetry (not current-free, normalized version)" )
	double b1 = 0.0;
	read_1arg(args, &b1);
	PRINTF0("   + b1 = %g\n", b1);
	for (i=irs; i <= ire; i++) {
		rr = Blm.radius(i);
      if (rr != 0.0) {
        l=2; m=0;
                v = pi * rr;
                z = ((sin(v)/v-cos(v))/v - 0.3*(sin(2*v)/(2*v)-cos(2*v))/(2*v));        // j1(pi*rr) - 0.3*j1(2*pi*rr)
                Blm.set_Prlm(i, l, m,  b0* z *sqrt(4.*pi/(2*l+1)) );
        l=MRES+2; m=MRES;
                if (MMAX>0) Blm.set_Prlm(i, l, m,  10.*rr*rr/(l*l) * b0* b1*z *sqrt(4.*pi/(2*l+1)) );
        l=4; m=0;
                v = 5.7635 * rr;
                z = -0.2*((sin(v)*(3./(v*v)-1.)-3.*cos(v)/v)/v);        // -0.2*j3(k*rr)
                Blm.set_Prlm(i, l, m,  b0* z *sqrt(4.*pi/(2*l+1)) );
        l=MRES+5; m=MRES;
                if (MMAX>0) Blm.set_Prlm(i, l, m,  10.*rr*rr/(l*l) * b0* b1*z *sqrt(4.*pi/(2*l+1)) );
	  }
	}
END_FIELD

BEGIN_FIELD( "bna3D", "3D for bna runs (not current-free, normalized version)" )
	double b1 = 0.0;
	read_1arg(args, &b1);
	PRINTF0("   + b1 = %g\n", b1);
	for (i=irs; i <= ire; i++) {
		rr = Blm.radius(i);
	  if (rr != 0.0) {
		l=1; m=0;
			v = pi * rr;
			z = ((sin(v)/v-cos(v))/v - 0.3*(sin(2*v)/(2*v)-cos(2*v))/(2*v)) *sqrt(4.*pi/(2*l+1));   // j1(pi*r) - 0.3*j1(2*pi*r)
			Blm.set_Prlm( i, l, m, b0* z );
		l=MRES+1; m=MRES;
       		Blm.set_Prlm( i, l, m, b0* b1*z);
		l=3; m=0;
			v = 5.7635 * rr;
			z = -0.2*((sin(v)*(3./(v*v)-1.)-3.*cos(v)/v)/v) *sqrt(4.*pi/(2*l+1));   // -0.2*j3(k*r)
			Blm.set_Prlm( i, l, m, b0* z );
		l=MRES+4; m=MRES;
			Blm.set_Prlm( i, l, m , b0* b1*z );
		l=1; m=0;
			z = Y10_ct*rr*(1.-rr);
		//	if ((i>irs)&&(i<ire)) Blm.set_Trlm(i, l, m,  b0* z );		// add a toroidal field
	  }
	}
END_FIELD

BEGIN_FIELD( "potential", "potential field imposed at boundaries (imposed)" )
	CURRENT_FREE	// mark magnetic field as current-free
	int iref, i0, i1;
	char arg2[4];
	char fname[48];
	std::vector<cplx> sh(NLM);

	iref = sscanf(args, "%47[^,],%3s",fname, arg2);
	if ((iref == 2)&&(arg2[0] == 'o')) {
		iref=ire;	i0=irs;		i1=ire+1;	// outer field includes outer boundary.
	} else {
		iref=irs;	i0=irs-1;	i1=ire;		// inner field includes inner boundary.
	}
	load_sh_text(fname, &sh.front(), NULL);	// read data from file (as poloidal scalar at a fixed r)

	for (i=i0; i <= i1; i++) {
		rr = Blm.radius(i);
		for (l=0; l<=LMAX; l++) {
			if (iref == irs) {
				z = pow(rr/r[iref], -l-1);	// internal field
			} else {
				z = pow(rr/r[iref], l);	// external field;
			}
			for (m=0; m<=MMAX*MRES; m+=MRES) {
				Blm.set_Prlm(i, l, m, b0*z* sh[ LM(shtns,l, m) ]);
			}
		}
	}
END_FIELD

BEGIN_FIELD( "magBC", "externally imposed magnetic boundary conditions (imposed)")
	l=1;	m=0;		// default l,m
	read_next_arg(args, &l);
	read_next_arg(args, &m);
	PRINTF0("   + l=%d, m=%d\n",l,m);
	i = ire+1;		// the boundary condition is stored in a ghost shell.
		Blm.set_Prlm(i, l, m, b0 * Y10_ct );
END_FIELD

BEGIN_FIELD( "geojet", "magnetic field counterpart of geojet" )
	double sig2, s, s0, d2;

	s0 = 0.5*(r[NM] + r[NG]);
	sig2 = 0.0025;		// default spatial size
	read_1arg(args, &sig2);
	PRINTF0("   + sig2 = %g\n",sig2);

	Shell2d Uph(shtns), Uth(shtns);
	for (int ir=Ulm.irs; ir<=Ulm.ire; ir++)		// in the fluid volume.
	{
		for (int im=0; im<NPHI; im++) {
			for (int it=0; it<NLAT; it++) {
				s = r[ir]*st[it];
				d2 = (s-s0)*(s-s0);
				Uph(it,im) = b0 * s * exp(- d2 / sig2);
			}
		}
		Uth.zero();
		spat_to_SHsphtor( shtns, Uth, Uph, Blm.Pol[ir], Blm.Tor[ir] );	// go to spectral space.
		for (int im=0; im<NLM; im++) Blm.Pol[ir][im] = 0.0;		// keep toroidal only
	}
END_FIELD

BEGIN_FIELD( "geojet_m1", "magnetic field counterpart of geojet for m=1 uniform field" )
	double sig2, s, s0, d2;

	s0 = 0.5*(r[NM] + r[NG]);
	sig2 = 0.0025;          // default spatial size
	read_1arg(args, &sig2);
	PRINTF0("   + sig2 = %g\n",sig2);

	Shell2d Uph(shtns), Uth(shtns);
	for (int ir=Ulm.irs+100; ir<=Ulm.ire-100; ir++)         // in the fluid volume.
	{
		for (int im=0; im<NPHI; im++) {
			double bb = b0 * cos(phi_rad(im));
			for (int it=0; it<NLAT; it++) {
				s = r[ir]*st[it];
				d2 = (s-s0)*(s-s0);
				Uph(it,im) = bb * s * exp(- d2 / sig2);
			}
		}
		Uth.zero();
		spat_to_SHsphtor( shtns, Uth, Uph, Blm.Pol[ir], Blm.Tor[ir] );  // go to spectral space.
		for (int im=0; im<NLM; im++) Blm.Pol[ir][im] = 0.0;             // keep toroidal only
	}
END_FIELD


BEGIN_FIELD( "core", "Core-like magnetic field" )
	double b1 = 0.1;			// relative amplitude of surface poloidal field
	Shell2d p(shtns);

	read_1arg(args, &b1);
	PRINTF0("   + b1 = %g\n", b1);
	b1 *= -0.53;		// scale so that b1 is a relative amplitude compared to sqrt(bs2)

	for (int ir=irs; ir<=ire; ir++) {
		double rr = Blm.radius(ir);
		if (ir < Ulm.ir_bco) {
			double fr = pow(r[Ulm.ir_bco]-rr,2) * b0 * 4.0;
			for (int it=0; it<NLAT; it++) {
				double v=0;
				for (int l=2; l<=8; l+=2) v += pow(rr*st[it],l);
				for (int ip=0; ip<NPHI; ip++)  p(it,ip) = v * fr;
			}
		} else p.zero();
		SPAT_SH(p, Blm.Pol[ir]);
		Blm.Pol[ir][LiM(shtns,2,0)] += b0*b1 * pow(rr, 2);		// add external quadrupole for coupling.
	}
	Blm.Pol[ire+1][LiM(shtns,2,0)] += b0*b1 * pow(Blm.radius(ire), 2);		// add external quadrupole for coupling (imposed).
END_FIELD

BEGIN_FIELD( "core2", "Core-like magnetic field (with m=1 dipole)" )
	double b1 = 0.1;			// relative amplitude of surface poloidal field
	Shell2d p(shtns);

	read_1arg(args, &b1);
	PRINTF0("   + b1 = %g\n", b1);
	b1 *= -0.53;		// scale so that b1 is a relative amplitude compared to sqrt(bs2)

	for (int ir=irs; ir<=ire; ir++) {
		double rr = Blm.radius(ir);
		if (ir < Ulm.ir_bco) {
			double fr = pow(r[Ulm.ir_bco]-rr,2) * b0 * 4.0;
			for (int it=0; it<NLAT; it++) {
				double v=0;
				for (int l=2; l<=8; l+=2) v += pow(rr*st[it],l);
				for (int ip=0; ip<NPHI; ip++)  p(it,ip) = v * fr;
			}
		} else p.zero();
		SPAT_SH(p, Blm.Pol[ir]);
		Blm.Pol[ir][LiM(shtns,MRES,1)] += b0*b1 * Y11_st * rr/2.;		// add axial external dipole for coupling.
	}
	Blm.Pol[ire+1][LiM(shtns,2,0)] += b0*b1 * Y11_st * Blm.radius(ire)/2.;		// add axial external dipole for coupling (imposed).
END_FIELD

BEGIN_FIELD( "m0m1", "special taw field" )
	CURRENT_FREE	// mark magnetic field as current-free
	double ratio = 0.0;	// ratio of non-axi / axi
	read_1arg(args, &ratio);
	for (i=irs; i <= ire; i++) {
		rr = Blm.radius(i);
		Blm.Pol[i][LiM(shtns,2,0)] = b0* Y10_ct * rr*rr;		// external quadrupole
		if (ratio != 0.0)
			Blm.Pol[i][LiM(shtns,MRES,1)] = b0*ratio * Y11_st * rr/2.;		// external axial dipole
	}
	i = ire+1;
		rr = Blm.radius(i);
		Blm.Pol[i][LiM(shtns,2,0)] = b0* Y10_ct * rr*rr;		// external quadrupole
		if (ratio != 0.0)
			Blm.Pol[i][LiM(shtns,MRES,1)] = b0*ratio * Y11_st * rr/2.;		// external axial dipole
END_FIELD

	return(0);		// return failure code.
}

/// This function is called to set initial velocity field value (see xshells.par)
int init_U0(PolTor &Ulm, char *name, double V0, const char *args)
{
	cplx z;
	double v;
	int ir,l,m;
	int irs = Ulm.irs;
	int ire = Ulm.ire;

BEGIN_FIELD( "spinover", "Equatorial solid body rotation (l=1, m=1)" )
	for (ir=irs; ir<=ire; ir++) {
		l=1;	m=1;
			z = V0 * r[ir];
			Ulm.set_Trlm(ir, l, m, z * Y11_st);
	}
END_FIELD

BEGIN_FIELD( "sb_rot", "Axial solid body rotation (l=1, m=0)" )
	for (ir=irs; ir<=ire; ir++) {
		l=1;	m=0;
			z = V0 * r[ir];
			Ulm.set_Trlm(ir, l, m, z * Y10_ct);
	}
END_FIELD

BEGIN_FIELD( "strain", "Strain flow l=2,m=2" )
	const double z0 = V0 * sqrt(2.*M_PI/15.) * 1./3.;
	for (ir=irs; ir<=ire; ir++) {
		l=2;	m=2;
			z = z0 * r[ir]*r[ir];
			Ulm.set_Prlm(ir, l, m, z);
	}
END_FIELD

BEGIN_FIELD( "precession_align", "Basic inviscid flow in a precessing spheroid, aligned with the fluid rotation: T(l=1,m=0) + P(l=2,m=1,2)" )
	double Po = 0.0;	// Poincare
	double flat = 0.0;	// flattening (or oblateness): sphere by default
	double normalize = 1.0;	// normalize (0=don't normalize)
	read_next_arg(args, &Po);
	read_next_arg(args, &flat);
	read_next_arg(args, &normalize);		// optional argument to normalize fluid vorticity to 1
	PRINTF0("   + Po = %g, 1-c/a = %g\n",Po,flat);
	if ((Po==0) || (flat==0)) runerr("Po and 1-c/a must be non-zero\n");
	const double eta = flat * (2.-flat)/((1.-flat)*(1.-flat));
	double V0_so  = Po*(2./eta + 1.);		// amplitude of spin-over mode
	double V0_pol = Po / 3 * sqrt(2*M_PI/15);		// amplitude of poloidal mode (l=2,m=1) [!! for orthonormal spherical harmonics !!]

	// normalize so that spin is unity:
	if (normalize != 0) V0 /= sqrt(1. + V0_so*V0_so);
	V0_so *= V0;	V0_pol *= V0;

	// rotate to align rotation axis with z-axis, using degree 2 harmonic representation (NLM=6)
	cplx tor[6] = {};		cplx pol[6] = {};		// all zeros
	tor[1] = V0 * Y10_ct;			//l=1,m=0
	tor[3] = V0_so * Y11_st;		//l=1,m=1
	pol[4] = V0_pol * cplx(0.,-1.);	//l=2,m=1

	shtns_rot rot2 = shtns_rotation_create(2,2, sht_orthonormal);
	double beta = -atan2(V0_so, V0);		// rotation angle to align the spin axis with z-axis
	shtns_rotation_set_angles_ZYZ(rot2, 0,beta,0);
	shtns_rotation_apply_real(rot2, tor, tor);
	shtns_rotation_apply_real(rot2, pol, pol);
	if (norm(tor[3]) > 1e-30*norm(tor[1])) runerr("T11 should be zero after rotation");

	printf("tor[1,0]=%g\n",tor[1].real());
	for (ir=irs; ir<=ire; ir++) {
		l=1;	m=0;
			z = tor[1].real() * r[ir];
			Ulm.set_Trlm(ir, l, m, z);			// axial solid-body rotation, non-axial is zero
		l=2;
		for (int m=0; m<=l; m++) {
			int idx = (m*(5-m))/2 + l;
			if (norm(pol[idx]) > V0_pol*1e-15) {	// only set non-zero coeffs
				if (ir==irs) printf("m=%d, pol=%g,%g\n",m,pol[idx].real(), pol[idx].imag());
				z = pol[idx] * r[ir]*r[ir];
				Ulm.set_Prlm(ir, l, m, z);			// // poloidal flow induced by non-sphericity (flattening)
			}
		}
	}

	shtns_rotation_destroy(rot2);
END_FIELD

BEGIN_FIELD( "bubble", "Free-fall bubble velocity field (Poloidal l=1, m=0)" )
	for (ir=irs; ir<=ire; ir++) {
		l=1;	m=0;
			z = V0 * 0.5*(r[ire]*r[ire] - r[ir]*r[ir])*r[ir];
			Ulm.set_Prlm(ir, l, m, z * Y10_ct);
			if (ir == ire)	// set analytic non-zero derivative at boundary.
				Ulm.set_Prlm(ir+1, l, m, -V0*r[ir]*r[ir] *Y10_ct );
	}
END_FIELD

BEGIN_FIELD( "dud02", "m=0 l=2 simple roll flow (eq 24 from Dudley & James 1989)" )
	// see https://doi.org/10.1098/rspa.1989.0112  eq.24 and figure 8a.
	// leads to growing magnetic field (kinematic dynamo) for R >= 54, which corresponds here to amplitude 85.
	double eps = 0.14;		// default epsilon parameter, see eq 24 from Dudley & James (1989).
	read_1arg(args, &eps);		// read spatial size from args.
	PRINTF0("   + epsilon = %g\n",eps);
	for (ir=irs; ir<=ire; ir++) {
		l=2; m=0;
			z = r[ir]*sin(pi*r[ir]);
			Ulm.set_Trlm(ir, l, m,  V0* z );
			Ulm.set_Prlm(ir, l, m,  V0* z * eps );
			if (ir == ire)	// set analytic non-zero derivative at outer boundary.
				Ulm.set_Prlm(ir+1, l, m, V0*eps*(sin(pi*r[ir]) +r[ir]*pi*cos(pi*r[ir])) );
	}
END_FIELD

BEGIN_FIELD( "gubbins02", "m=0 l=2 Gubbins flow (from Dudley & James 1989)" )
	for (ir=irs; ir<=ire; ir++) {		// see https://doi.org/10.1098/rspa.1989.0112
		l=2; m=0;
			z = -r[ir]*sin(2*pi*r[ir]) * tanh(2*pi*(1-r[ir]));
			Ulm.set_Trlm(ir, l, m,  V0* z );
			Ulm.set_Prlm(ir, l, m,  V0* z * 0.1 );
	}
END_FIELD

BEGIN_FIELD( "steen_krause", "m=0 l=1 Steenbeck & Krause flow (from Jault 1995)" )
	for (ir=irs; ir<=ire; ir++) {		// see https://doi.org/10.1080/03091929508228993
		l=1; m=0;
			z = r[ir] * (r[ir]-1);
			Ulm.set_Trlm(ir, l, m,  V0* z );
	}
END_FIELD

BEGIN_FIELD( "pekeris22", "m=2 l=2 j2 pekeris flow (eq 20-21 Dudley & James 1989)" )
	for (ir=irs; ir<=ire; ir++) {
		if (r[ir] != 0.0) {
		  l=2; m=0;
			v =  5.7634591968447*r[ir];
			z = 5.7634591968447 * ((3.0/(v*v*v) -1.0/v)*sin(v) - 3./(v*v) * cos(v));
			Ulm.set_Prlm(ir, l, m,  V0 * z );
			Ulm.set_Trlm(ir, l, m,  V0 * 5.7634591968447 * z );
		}
	}
END_FIELD

BEGIN_FIELD( "blob", "localized blob" )
	double rr,ph, cp, sp, x,y,z, x0,y0,z0, d2,sig2;
	xs_array2d<cplx> X;		// pol or tor
	int im,it;

	if (strncmp(args, "tor", 3) == 0) {
		X = Ulm.Tor;		// toroial component
		PRINTF0("   + toroidal blob\n");
	} else {
		X = Ulm.Pol;		// poloidal component
		PRINTF0("   + poloidal blob\n");
	}
	sig2 = 0.002;		// default spatial size
	const char* nxt = strchr(args,',');		// find separator
	if (nxt) read_1arg(nxt+1, &sig2);		// read spatial size from args.
	PRINTF0("   + sig2 = %g\n",sig2);

	Shell2d S(shtns);
	for (ir=irs; ir<=ire; ir++) {
		x0 = 0.5*(r[NM] + r[NG]);	y0 = 0.0;	z0 = 0.0;
		rr = r[ir];
		for (im=0; im<NPHI; im++) {
			ph = 2.*(im-NPHI/2)*pi/(NPHI*MRES);	// phi centered around y0 = 0.0 so that mres periodicity is ok
			cp = rr*cos(ph);	sp = rr*sin(ph);
			for (it=0; it<NLAT; it++) {
				z = rr*ct[it];		x= cp*st[it];	y=sp*st[it];
				d2 = (x-x0)*(x-x0) + (y-y0)*(y-y0) + (z-z0)*(z-z0);
				S(it,im) = V0 * exp(- d2 / sig2);
			}
		}
		spat_to_SH( shtns, S, X[ir] );	// go to spectral space.
	}
END_FIELD

BEGIN_FIELD( "vblob", "localized vector blob" )
	double rr,ph, cp, sp, x,y,z, x0,y0,z0, d2,sig2;
	Shell2d S(shtns), Z1(shtns), Z2(shtns);		// 3 shells
	double *vr,*vt,*vp;
	int im,it;

	if (strncmp(args, "r", 1) == 0) {
		vr = S;		// radial component
		PRINTF0("   + radial force blob\n");
		vt = Z1;	vp = Z2;	// these components are zero
	} else if (strncmp(args, "t", 1) == 0) {
		vt = S;		// theta component
		PRINTF0("   + theta force blob\n");
		vr = Z1;	vp = Z2;	// these components are zero
	} else {
		vp = S;		// phi component
		PRINTF0("   + azimutal force blob\n");
		vr = Z1;	vt = Z2;	// these components are zero
	}
	sig2 = 0.01;		// default spatial size
	const char* nxt = strchr(args,',');		// find separator
	if (nxt) read_1arg(nxt+1, &sig2);		// read spatial size from args.
	PRINTF0("   + sig2 = %g\n",sig2);

	ScalarSH* Sph = new ScalarSH;
	Sph->alloc(Ulm.ir_bci, Ulm.ir_bco);				// alloc temporary field

	for (ir=irs; ir<=ire; ir++) {
		x0 = 0.5*(r[NM] + r[NG]);	y0 = 0.0;	z0 = 0.0;
		rr = r[ir];
		for (im=0; im<NPHI; im++) {
			ph = 2.*(im-NPHI/2)*pi/(NPHI*MRES);	// phi centered around y0 = 0.0 so that mres periodicity is ok
			cp = rr*cos(ph);	sp = rr*sin(ph);
			for (it=0; it<NLAT; it++) {
				z = rr*ct[it];		x= cp*st[it];	y=sp*st[it];
				d2 = (x-x0)*(x-x0) + (y-y0)*(y-y0) + (z-z0)*(z-z0);
				S(it, im) = V0 * exp(- d2 / sig2);
//				d2 = (x-x0)*(x-x0) + (y-y0)*(y-y0) + (z+z0)*(z+z0);
//				S(it, im) -= V0 * exp(- d2 / sig2);
			}
		}
		Z1.zero();		Z2.zero();		// the 2 other components are zero.
		SPAT_SHV3(vr,vt,vp, Ulm.Tor[ir], Sph->get_data(0,ir), Ulm.Pol[ir]);
	}
	Ulm.curl_from_TQS(Sph, Ulm.ir_bci, Ulm.ir_bco, ::U);			// take the curl, includes MPI sync with tag ::U
	delete Sph;
END_FIELD

BEGIN_FIELD( "geojet", "geostrophic axisymmetric azimutal jet" )
	double sig2, s, s0, d2;
	int im,it;

	s0 = 0.5*(r[NM] + r[NG]);
	sig2 = 0.0025;		// default spatial size
	read_1arg(args, &sig2);
	PRINTF0("   + sig2 = %g\n",sig2);

	Shell2d Uph(shtns), Uth(shtns);
	for (ir=irs; ir<=ire; ir++)		// in the fluid volume.
	{
		for (im=0; im<NPHI; im++) {
			for (it=0; it<NLAT; it++) {
				s = r[ir]*st[it];
				d2 = (s-s0)*(s-s0);
				Uph(it,im) = V0 * s * exp(- d2 / sig2);
			}
		}
		Uth.zero();
		spat_to_SHsphtor( shtns, Uth, Uph, Ulm.Pol[ir], Ulm.Tor[ir] );	// go to spectral space.
		for (im=0; im<NLM; im++) Ulm.Pol[ir][im] = 0.0;		// keep toroidal only
	}
END_FIELD

BEGIN_FIELD( "jupiter", "Alternating Jets (Jupiter-like)" )
	double njets = 6.0;		// default number of jets.
	int im,it;

	read_1arg(args, &njets);
	PRINTF0("   + number of jets = %g\n",njets);

	Shell2d Uph(shtns), Uth(shtns);
	for (ir=irs; ir<=ire; ir++)		// in the fluid volume.
	{
		for (im=0; im<NPHI; im++) {
			for (it=0; it<NLAT; it++) {
				Uph(it,im) = V0 * r[ir]*st[it] * cos(njets * M_PI*(r[ir]*st[it]-1.));
			}
		}
		Uth.zero();
		spat_to_SHsphtor( shtns, Uth, Uph, Ulm.Pol[ir], Ulm.Tor[ir] );	// go to spectral space.
		for (im=0; im<NLM; im++) Ulm.Pol[ir][im] = 0.0;
	}
END_FIELD

BEGIN_FIELD( "moss08", "axisymmetric roll flow (Moss 2008)" )
	const double n1 = V0 * (4.0/5) * sqrt(4*M_PI/(2*1+1));
	const double n3 = V0 * (-2.0/15) * sqrt(4*M_PI/(2*3+1));
	for (ir=irs; ir<=ire; ir++)		// in the fluid volume.
	{	// flow contributed by C. Hardy, from Moss (2008) with m=4, n=0 [Moss's notation].
		double r2 = (r[ire]-r[ir]) * (r[ir]-r[irs]);
		r2 *= r2;
		if (r[irs] != 0.0) {
			r2 /= r[ir];		// avoid singularity at r=0 (for which r1=r)
		} else r2 *= r[ir];
		l=1; m=0;
				Ulm.set_Prlm(ir, l, m,  r2 * n1); 
		l=3; m=0;
				Ulm.set_Prlm(ir, l, m,  r2 * n3);
	}
END_FIELD

BEGIN_FIELD( "bench_fs", "initial velocity field for the full-sphere benchmark" )
	V0 *= sqrt(4.*M_PI);
	for (ir=irs; ir<=ire; ir++) {
		double rr = r[ir];		double r2 = rr*rr;		double r4 = r2*r2;		double r6 = r4*r2;
		l=2;	m=1;
			z = V0 *r2*cplx( (30 +1250./3*r2 -130*r4 -90*r6), 
					(105 -245*r2 +155*r4 -145./7*r6) );
				Ulm.set_Trlm(ir, l, m, z * sqrt(2./5) );
		l=1;	m=0;
			z = V0 *rr*(-54625./198 +350*r2 +625./2*r4 -325*r6);
				Ulm.set_Trlm(ir, l, m, z / sqrt(3) );
		l=3;	m=0;
			z = V0 *rr*r2*(45 -575*r2 +835*r4 -350*r6);
				Ulm.set_Trlm(ir, l, m, z / sqrt(7) );
	}
END_FIELD

BEGIN_FIELD( "elliptic", "elliptic base flow" )
	for (ir=irs; ir<=ire; ir++) {
		double r2 = r[ir]*r[ir];
		l=2;	m=2;
			z = V0/3 * r2;	// * (1. - r2/(r[ire]*r[ire]));		// must vanish at r=0 and r=ro.
			Ulm.set_Prlm(ir, l, m, z * 4.*sqrt(2.*M_PI/15.) );
	}
END_FIELD

	return(0);		// return failure code.
}

int init_T0(ScalarSH &Tlm, char *name, double T0, char *args)
{
	cplx z;
	int ir,l,m;
	int irs = Tlm.ir_bci;
	int ire = Tlm.ir_bco;

BEGIN_FIELD( "internal", "homogeneous internal heating profile (l=0)" )
	for (ir=irs; ir<=ire; ir++) {
		l=0;	m=0;
			z = (T0/6) * (r[ire]*r[ire] -  r[ir]*r[ir]);
			Tlm.set_rlm(ir, l, m, z * Y00_1);
	}
END_FIELD

BEGIN_FIELD( "flux", "fixed temperature flux (l=0)" )
	if (r[irs] == 0) runerr("fixed flux not supported for r=0.");
	for (ir=irs; ir<=ire; ir++) {
		l=0;	m=0;
			z = T0*r[ire]*(r[ire]/r[ir] - 1.0);
			Tlm.set_rlm(ir, l, m, z * Y00_1);
	}
END_FIELD

BEGIN_FIELD( "delta", "fixed temperature difference (l=0)" )
	if (r[irs] == 0) runerr("not supported for r=0.");
	for (ir=irs; ir<=ire; ir++) {
		l=0;	m=0;
			z = (-T0*r[irs]/(r[ire]-r[irs])) * (r[ire]/r[ir] -1.0);
			Tlm.set_rlm(ir, l, m, z * Y00_1);
	}
	//Tlm.bci = BC_FIXED_TEMPERATURE;		Tlm.bco = BC_FIXED_TEMPERATURE;
END_FIELD

BEGIN_FIELD( "linear", "linear temperature field (l=0), constant N" )
	if (r[irs] == 0) runerr("not supported for r=0.");
	for (ir=irs; ir<=ire; ir++) {
		l=0;	m=0;
			z = T0*(r[ir]-r[irs])/(r[ire]-r[irs]);
			Tlm.set_rlm(ir, l, m, z * Y00_1);
	}
	//Tlm.bci = BC_FIXED_TEMPERATURE;		Tlm.bco = BC_FIXED_TEMPERATURE;
END_FIELD

BEGIN_FIELD( "bench2001", "initial temperature and boundary conditions for the geodynamo benchmark (l=4,m=4)" )
	for (ir=irs; ir<=ire; ir++) {
		l=4;	m=4;
			double x2 = 2*r[ir]-r[irs]-r[ire];
			x2 *= x2;
			z = T0 * (1. - 3.*x2 + 3.*x2*x2 - x2*x2*x2);	// the factor 210/sqrt(17920*pi) is included in the spherical harmonic definition.
			if ((ir>irs)&&(ir<ire))
				Tlm.set_rlm(ir, l, m, z);
	}
END_FIELD

BEGIN_FIELD( "bench_fs", "initial temperature perturbation for the full-sphere benchmark (l=3,m=3)" )
	for (ir=irs; ir<=ire; ir++) {
		l=3;	m=3;
			z = T0 * r[ir]*r[ir]*r[ir]*(1.-r[ir]*r[ir]);
			if ((ir>irs)&&(ir<ire))
				Tlm.set_rlm(ir, l, m, z);
	}
END_FIELD

BEGIN_FIELD( "thermochem", "thermochemical codensity profile (l=0) see Aubert et al. GJI 2009" )
	// To reproduce Olson+ 2009 [ https://doi.org/10.1016/j.pepi.2008.11.010 ], use this in xshells.par:
	//    R_U = 7/13 : 20/13
	//    tp0 = thermochem(1) * 1.1617357
	double fch = 0.75;				// default chemical power fraction.
	read_1arg(args, &fch);
	PRINTF0("   + fch = %g\n",fch);
	double ri3 = r[irs]*r[irs]*r[irs];
	double ro3 = r[ire]*r[ire]*r[ire];
	double xi = 0.5*(2*fch-1)/(ro3-ri3);
	double xt = (ro3*fch - ri3*(1-fch))/(ro3-ri3);

	for (ir=irs; ir<=ire; ir++) {
		l=0;	m=0;
			z = xi*r[ir]*r[ir]  + xt/r[ir];
			Tlm.set_rlm(ir, l, m, T0 * z * Y00_1);
	}
END_FIELD

BEGIN_FIELD( "heteroflux", "heterogeneous heat flux imposed at boundaries (imposed)" )
	int iref, ig;
	char arg2[4];
	char fname[48];
	std::vector<cplx> sh(NLM);

	iref = sscanf(args, "%47[^,],%3s",fname, arg2);
	load_sh_text(fname, &sh.front(), NULL);	// read data from file (as scalar at a fixed r)
	if ((iref == 2)&&(arg2[0] == 'i')) {
		iref = irs;		// ghost shell with imposed derivative
		ig = irs-1;
	} else {
		iref = ire;		// ghost shell with imposed derivative
		ig = ire+1;
	}

	if (own(iref) || own(ig)) {
		for (int lm=0; lm<NLM; lm++) {
			if (sh[lm] != 0.0) printf("lm=%d, sh=%g,%g\n", lm, real(sh[lm]), imag(sh[lm]));
			Tlm[ig][lm] = T0 * sh[lm];		// store the imposed flux
		}
	}
END_FIELD

BEGIN_FIELD( "strati", "stratified layer at the top of the core" )
		double delta = 0.05;			// layer size.
		read_1arg(args, &delta);		// read size from args.
		PRINTF0("   + delta = %g\n",delta);
		double r0 = r[ire]*(1.0-delta);
		for (ir=irs; ir<=ire; ir++) {
			l=0;    m=0;
			double z = 0.0;
			if (r[ir] > r0)   z = r[ir]-r0;
			Tlm.set_rlm(ir, l, m, z * Y00_1);
		}
END_FIELD

	return(0);		// return failure code.
}

int init_Phi0(ScalarSH &Tlm, char *name, double g0, char *args)
{
	cplx z;
	int ir,l,m;
	int irs = 0;
	int ire = NR-1;

BEGIN_FIELD( "radial", "radial gravity increasing linearly with r (l=0)" )
	g0 *= Y00_1 / (2*r[Ulm.ir_bco]);	// input value is gravity at the outer FLUID shell.
	for (ir=irs; ir<=ire; ir++) {
		l=0;	m=0;
			z = g0 * r[ir]*r[ir];
			Tlm.set_rlm(ir, l, m, z);
	}
END_FIELD

BEGIN_FIELD( "constant_radial", "constant radial gravity (l=0)" )
	g0 *= Y00_1;	// input value is the constant gravity.
	for (ir=irs; ir<=ire; ir++) {
		l=0;	m=0;
			z = g0 * r[ir];
			Tlm.set_rlm(ir, l, m, z);
	}
END_FIELD

BEGIN_FIELD( "central", "radial gravity decreasing with r as 1/r^2 (l=0)" )
	const double ro = r[Ulm.ir_bco];
	g0 *= Y00_1 * ro*ro;		// input value is gravity at the outer FLUID shell.
	for (ir=irs; ir<=ire; ir++) {
		l=0;	m=0;
			z = - g0 / r[ir];
			Tlm.set_rlm(ir, l, m, z);
	}
END_FIELD

BEGIN_FIELD( "vertical", "uniform gravity along z (l=1)" )
	g0 *= -Y10_ct;		// input value is g, downward (-e_z)
	for (ir=irs; ir<=ire; ir++) {
		l=1;	m=0;
			z = g0 * r[ir];
			Tlm.set_rlm(ir, l, m, z);
	}
END_FIELD

BEGIN_FIELD( "centrifuge", "centrifugal force (l=0+2)" )
	g0 *= g0 * 0.5*(4.*sqrt(M_PI)/3);		// orthonormal SH, input value is Omega.
	for (ir=irs; ir<=ire; ir++) {
		z = g0 * (r[ir]*r[ir]);
		l=0;	m=0;
			Tlm.set_rlm(ir, l, m, z);
		l=2;	m=0;
			Tlm.set_rlm(ir, l, m, -1.*z/sqrt(5.));
	}
END_FIELD

	return(0);		// return failure code.
}

#undef BEGIN_FIELD
#undef END_FIELD


/// include a printable version of xshells.hpp
static const unsigned char xshells_h[] = {
	#include "xshells_h.hex"
	0	// string must end with 0
};

/// returns the size of a file, in bytes.
long fsize(FILE *fp) {
	long prev = ftell(fp);
    fseek(fp, 0L, SEEK_END);
    long sz = ftell(fp);
    fseek(fp, prev, SEEK_SET); //go back to where we were
    return sz;
}

/// returns the size of a string that can hold the arguments, in bytes
long arg_size(int argc, char* argv[])
{
	long s = 0;
	for (int i=0; i<argc; i++)	s += strlen(argv[i]) + 1;
	return s;
}

/// Return pointer to first non-whitespace char in given string.
///  from inih https://github.com/benhoyt/inih
static char* lskip(const char* s)
{
    while (*s && isspace((unsigned char)(*s)))
        s++;
    return (char*)s;
}

/// Find where the matching closing parenthesis is. c must point to the open parenthesis.
/// Returns 0 if no matching parenthesis.
static char* match_parenthesis(char* c)
{
	int npar = 0;
	if (*c == '(') npar++;
	while (npar > 0) {
		c++;	// next char
		if (*c == '(') npar++;
		if (*c == ')') npar--;
		if (*c == 0) break;
	}
	if (npar > 0) return 0;
	return c;
}

/// returns amplitude, fills name and args.
static double split_name_args_ampl(const char *cmd, char *name, char *args)
{
	double ampl = 1.0;		// default amplitude.
	char dump[240];

	cmd = lskip(cmd);		// skip leading spaces
	// first get name:
	sscanf(cmd, "%[^ (\t*]", name);
	cmd += strlen(name);	// skip name

	strncpy(dump, cmd, 239);	dump[239]=0;
	args[0] = 0;				// no arguments (default)
	char* x = lskip(dump);
	if (*x == '(') {	// open parenthesis found
		char* c = match_parenthesis(x);		// find matching closing parenthesis
		if (c==0) runerr("no closing parenthesis.");
		*c = 0;		// end of arguments (zero-terminated string)
		strcpy(args, x+1);
		x = lskip(c+1);		// is there something after arguments?
	}
	if (*x == '*') {
		read_1arg(x+1, &ampl);	// read given amplitude, starting after "*"
	} else if (*x) {
		runerr("malformed line in xshells.par\n");
	}
	return ampl;
}


void init_Frame(RefFrame &frame, char *cmd, double Omega0)
{
	char name[127], args[127];

	double amplitude = split_name_args_ampl(cmd, name, args);
	if (strcmp(name,"precessing") == 0) {
		double angle = M_PI_2;
		read_1arg(args, &angle);
		frame.set_precession(Omega0, amplitude, angle*M_PI/180.);
	} else
	if (strcmp(name,"librating") == 0) {
		double freq = 0.0;
		char axis = 'z';
		const char* nxt = args;
		read_next_arg(nxt, &freq);
		if (nxt) axis = *nxt;
		frame.set_libration(Omega0, amplitude, freq, axis);
	} else
	if (strcmp(name,"nutating") == 0) {
		double freq = 0.0;
		read_1arg(args, &freq);
		frame.set_nutation(Omega0, amplitude, freq);
	} else
#ifdef XS_STRAIN
	if (strcmp(name,"strain") == 0) {
		double freq = -1.0;			// default freqency -1 (for precession in spheroid)
		read_1arg(args, &freq);
		frame.set_rotation(Omega0);
		frame.set_strain(amplitude, freq);
	} else
#endif
		runerr("unknown frame");
}

/// contains Job parameters read from .par file by \ref read_Par
struct JobPar {
	int iter_max, modulo, iter_save, nbackup;
	int uBCin, uBCout;		// boundary conditions for u
	int bBCin, bBCout;		// boundary conditions for b
	int tBCin, tBCout;		// BC for t
	int cBCin, cBCout;		// BC for c
	double dt;				// time-step
	double Omega0, Omega0_angle;
	double Inertia_i, Inertia_o;
	double nu, eta, kappa, kappa_c;		// viscosity, magnetic diffusivity, thermal diffusivity, compositional diffusivity
	double Rbs, Rbe;		// radial limits for b
	double Rus, Rue;		// radial limits for u
	double Rts, Rte;		// radial limits for t
	double Rcs, Rce;		// radial limits for c
	double polar_opt_max;
	double dt_tol_lo, dt_tol_hi;	// for variable time-step.
	double rsat_ltr;		// for variable l-truncation (if enabled).
	double pc2_ai, pc2_ax;	// adjusting the PC2 time-scheme.
	double sconv_stop;		// maximum tolerable Sconv before aborting.
	double a_forcing, w_forcing;
	double hyper_diff_l0;		// controls the start of hyper-diffusion in l-spectrum.
	double tsave_limit;
	int lmax_out, mmax_out;
	int lmax_out_sv;
	int prec_out;		// if 1 => single precision (default) otherwise double-precision.
	int sht_type, allow_restart;
	int no_j0xb0, cur_free;
	int Nin, Nout;			// points in boundary layer (velocity).
	int dt_adjust;
	int kill_sbr;
	int no_ugradu, no_jxb;
	int plot, parity;
	int make_movie;
	int make_time_derivative;
	int shared_mem;
	int debug_no_write, allow_interp;	// controls behaviour of xshells_io
	// filenames or init commands for different fields :
	char* job;
	char* ffile[MAXFIELD];
	char* f0file[MAXFIELD];
	char *rfile, *phi0file, *framecmd;
	char* nonlin;
	char* stepper_name;
};

struct vartabd {	const char* name;		double *d; };		// double variables
struct vartabi {	const char* name;		int *i;	};			// integer variables
struct vartabs {	const char* name;		char **s;	const char *fmt; };		// string variables

JobPar jpar;		///< job parameter structure

// .par file to variable mapping.
static vartabd vard[] = {
	{ "nu", &jpar.nu },  { "eta", &jpar.eta },  { "kappa", &jpar.kappa },  { "kappa_c", &jpar.kappa_c },
	{ "Omega0", &jpar.Omega0 },  { "Omega0_angle", &jpar.Omega0_angle },
	{ "Inertia_i", &jpar.Inertia_i },  { "Inertia_o", &jpar.Inertia_o },
	{ "hyper_diff_l0", &jpar.hyper_diff_l0 },	// this hyper-diff parameter is always kept to allow for error detection (e.g. hyper_diff not compiled but asked for in .par)
	{ "Rmin", &jpar.Rbs }, { "Ric", &jpar.Rus }, { "Rcmb", &jpar.Rue }, { "Rmax", &jpar.Rbe },
	{ "DeltaOmega", &jpar.a_forcing }, { "a_forcing", &jpar.a_forcing }, { "w_forcing", &jpar.w_forcing },
	{ "dtU", &jpar.dt }, { "dtB", &jpar.dt }, { "dt", &jpar.dt },		// aliases for time-step dt.
	{ "sht_polar_opt_max", &jpar.polar_opt_max }, { "pc2_ai", &jpar.pc2_ai },  { "pc2_ax", &jpar.pc2_ax }, 
	{ "dt_tol_lo", &jpar.dt_tol_lo }, { "dt_tol_hi", &jpar.dt_tol_hi },
	{ "backup_time", &jpar.tsave_limit },
	{ "rsat_ltr", &jpar.rsat_ltr },
	{ "sconv_stop", &jpar.sconv_stop },
	{ 0, 0 }
};
static vartabi vari[] = {
	{ "NR", &NR },	{ "Nlat", &NLAT },	{ "Lmax", &LMAX },
	{ "Mmax", &MMAX }, { "Mres", &MRES }, {"Nphi", &NPHI },
	{ "dt_adjust", &jpar.dt_adjust },
	{ "iter_max", &jpar.iter_max }, { "modulo", &jpar.modulo }, { "sub_iter", &jpar.modulo }, { "iter_save", &jpar.iter_save },
	{ "movie", &jpar.make_movie }, {"time_derivative", &jpar.make_time_derivative} , { "no_j0xb0", &jpar.no_j0xb0 }, { "current_free", &jpar.cur_free }, { "kill_sbr", &jpar.kill_sbr },
	{ "lmax_out", &jpar.lmax_out }, { "mmax_out", &jpar.mmax_out }, { "prec_out", &jpar.prec_out },
	{ "sht_type", &jpar.sht_type }, { "interp", &jpar.allow_interp }, { "restart", &jpar.allow_restart },
	{ "lmax_out_sv", &jpar.lmax_out_sv }, { "nbackup", &jpar.nbackup }, { "plot", &jpar.plot },
	{ "sconv_lmin", &sconv_lmin }, { "sconv_mmin", &sconv_mmin },
	{ "no_jxb", &jpar.no_jxb }, { "no_ugradu", &jpar.no_ugradu },
	{ "parity", &jpar.parity },
	{ "no_write", &jpar.debug_no_write },
	{ "shared_mem", &jpar.shared_mem },
	{ 0, 0 }
};
static struct vartabs vars[] = {
	{ "job", &jpar.job, "%239s" }, { "r", &jpar.rfile, "%239s" },
	{ "u", &jpar.ffile[U], "%239[^#\n\r]" }, { "u0", &jpar.f0file[U], "%239[^#\n\r]" },
	{ "b" , &jpar.ffile[B], "%239[^#\n\r]" }, { "b0" , &jpar.f0file[B], "%239[^#\n\r]" },
	{ "tp", &jpar.ffile[T], "%239[^#\n\r]" }, { "tp0", &jpar.f0file[T], "%239[^#\n\r]" },
	{ "c", &jpar.ffile[C], "%239[^#\n\r]" }, { "c0", &jpar.f0file[C], "%239[^#\n\r]" },
	{ "phi0" , &jpar.phi0file, "%239[^#\n\r]" }, { "frame", &jpar.framecmd, "%239[^#\n\r]" },
	{ "nonlin", &jpar.nonlin, "%239[^#\n\r]" }, { "stepper", &jpar.stepper_name, "%239[^#\n\r]" },
	{ 0, 0, 0 }
};

int parse_param(char* start)
{
	double tmp, tmp2;
	int parse_error, k, err, erri, tmpi, match;
	char *val;
	char str2[240];
	char name[60];
	char dump[240];

	parse_error = 1;
	strcpy(str2, start);		// copy to str2 where we can modify it.
	val = strchr(str2, '#');	if (val) *val = 0;			// remove comment.
	val = strchr(str2, '=');
	if (val) {
		*val++ = 0;		// split at '='.
		val = lskip(val);		// remove leading spaces.
		parse_error = 0;
		match = sscanf(str2,"%59s", name);		// get name of token (max 59 chars)
		err = mp.parse(start, &tmp);
		tmpi = tmp;		erri = err + (tmpi != tmp);

		if (err == 0) {		// assign double precision variables.
			k=-1;	while ((match) && (vard[++k].name != NULL))
				if (strcmp(name,vard[k].name) == 0) {	*vard[k].d = tmp;	match=0;  }
			if ((match)&&(strcmp(name,"t_forcing") == 0))	{
				jpar.w_forcing = (tmp != 0.0) ? 2.*M_PI/tmp : 0.0;
				k=0;	match=0;
			}
		}
		if (erri == 0) {
			k=-1;	while ((match) && (vari[++k].name != NULL))		// assign integer variables.
				if (strcmp(name,vari[k].name) == 0) {	*vari[k].i = tmpi;	match=0;  }
		}

		k=-1;	while ((match) && (vars[++k].name != NULL))		// assign string variables.
			if (strcmp(name,vars[k].name) == 0) {
				if (val[0] != 0) {
					*vars[k].s = (char*) malloc(240);	sscanf(val, vars[k].fmt, *vars[k].s);
				} else *vars[k].s = 0;		// unset variable, useful to disable field from command line.
				match=0;
			}

		if (match) {		// Boundary Conditions
			int err = 2 - sscanf(val, "%d%*[ ,:\t]%d", &tmpi, &k);
			//PRINTF0("%d :: %d x %d\n",err, tmpi, k);
			if (err != 0) parse_error = 1;
			match = 0;
			if (strcmp(name,"BC_U") == 0) {
				if (err == 0) {		jpar.uBCin = tmpi;		jpar.uBCout = k; 	}
			}
			else if (strcmp(name,"BC_B") == 0) {
				if (err == 0) {		jpar.bBCin = tmpi;		jpar.bBCout = k; 	}
			}
			else if (strcmp(name,"BC_T") == 0) {
				if (err == 0) {		jpar.tBCin = tmpi;		jpar.tBCout = k; 	}
			}
			else if (strcmp(name,"BC_C") == 0) {
				if (err == 0) {		jpar.cBCin = tmpi;		jpar.cBCout = k; 	}
			}
			else match = 1;
		}
		if (match) {
			parse_error = 0;
			int err = 2 - sscanf(val, "%119[^ ,:\t]%*[ ,:\t]%119[^ ,:\t]", dump, dump+120);
			//PRINTF0("%d :: %s x %s\n",err, dump, dump+120);
			if (err == 0) {
				err += mp.parse(dump, &tmp);
				err += mp.parse(dump+120, &tmp2);
			}
			if (err) parse_error = 1;
			match = 0;
			if (strcmp(name,"R_U") == 0) {
				if (err == 0) {		jpar.Rus = tmp;	jpar.Rue = tmp2; 	}
			}
			else if (strcmp(name,"R_B") == 0) {
				if (err == 0) {		jpar.Rbs = tmp;	jpar.Rbe = tmp2; 	}
			}
			else if (strcmp(name,"R_T") == 0) {
				if (err == 0) {		jpar.Rts = tmp;	jpar.Rte = tmp2; 	}
			}
			else if (strcmp(name,"R_C") == 0) {
				if (err == 0) {		jpar.Rcs = tmp;	jpar.Rce = tmp2; 	}
			}
			else if (strcmp(name,"N_BL") == 0) {
				if (err == 0) {		jpar.Nin = tmp;			jpar.Nout = tmp2; 	}
			}
			else match = 1;
		}
		if (match) {	// no match found
			parse_error = err;		// we may have defined a user variable in the parser
		}
	}
	return parse_error;
}

/// read parameters from xshells.par file (see \ref par_file).
/// Parameters are returned in a \ref JobPar structure.
/// returns a pointer to a string containing the parameter list in compact format.
char* read_Par(int argc, char *argv[], int backup=1)
{
	int i, k, argi;
	char* par_string;
	FILE *fp, *fpw;
	static char fname[120] = "xshells.par";
	char str[480];

	argi=1;
	if ((argi<argc) && (argv[argi][0] != '-')) {		// par file given as argument ?
		strncpy(fname, argv[argi], 119);		fname[119]=0;
		argi++;	// go to next argument.
	}
	
	PRINTF0("[read_Par] reading parameters from '%s'...\n",fname);
	fp = fopen(fname,"r");
	if (fp == NULL) runerr("[read_Par] file not found !");
	par_string = (char*) malloc( fsize(fp) + arg_size(argc, argv) + strlen(_HGID_) + 3 );
	if (i_mpi==0) fpw = fopen("tmp.par","w");	// keep the parameters for that job.

	// DEFAULT VALUES
	memset(&jpar, 0, sizeof(jpar));	// set evrything to zero by default.
	jpar.Omega0 = 1.0;		// global rotation is on by default.
	jpar.kill_sbr = 2;		// keep solid-body-rotation constant.
	jpar.eta = 0;	jpar.nu = 0;		jpar.kappa = 0;		jpar.kappa_c = 0;		// if eta, nu or kappa is not set, copy the other value. (Pm=1)
	jpar.dt_adjust = 0;		// no time-step adjustment by default.
	jpar.nbackup = 0;		// no backup limit by default.
	jpar.tsave_limit = 240;	//	240 minutes (=4 hours) by default between backups.
	#ifndef XS_MPI
	jpar.plot = -1;			// controls gnuplot: -1 indicates default behaviour (0 is disabled, 1 is screen output, 2 is png, 3 is both screen and png)
	#endif
	#ifdef VAR_LTR
	jpar.rsat_ltr = VAR_LTR;	// default value for variable ltr.
	#else
	jpar.rsat_ltr = 0.0;	// default value for variable ltr.
	#endif
	MRES = 1;
	InnerCore.zero_out();	Mantle.zero_out();
	k=-1;	while (vars[++k].name != NULL) *vars[k].s = NULL;	// mark all files and string variables as unused.

	jpar.Rbs = 1.e290; 	jpar.Rbe = 0.;		// default radii
	jpar.Rus = 1.e290; 	jpar.Rue = 0.;
	jpar.Rts = 1.e290; 	jpar.Rte = 0.;
	jpar.uBCin = BC_NO_SLIP;	jpar.uBCout = BC_NO_SLIP;
	jpar.bBCin = BC_MAGNETIC;	jpar.bBCout = BC_MAGNETIC;
	jpar.polar_opt_max = 1e-14;		// default SHT optimization (very safe).
	jpar.pc2_ax = 0.5;		jpar.pc2_ai = 0.5;		// defaults values for PC2: order 2 in space.
	jpar.lmax_out = -1;		jpar.mmax_out = -1;	// auto = defaults to max values.
	jpar.prec_out = 2;			// double precision movie output by default.
 	jpar.sht_type = sht_gauss;	// default SHT type.
 	jpar.iter_max = 0;             jpar.modulo = 1;               jpar.iter_save = 1;    // default iteration numbers
 	jpar.allow_restart = 0;
 	jpar.Nin = 0;	jpar.Nout = 0;
 	#ifdef NO_J0xB0
		jpar.no_j0xb0 = 1;		// legacy define, can be overriden by xshells.par
	#endif
	#ifdef XS_CUR_FREE
		jpar.cur_free = 1;		// legacy define, can be overriden by xshells.par
	#endif
	jpar.no_ugradu = 0;			jpar.parity = 0;		// compute u.grad(u) by default; and both parities.
	jpar.no_jxb = 0;			// compute j x b  by default.
	jpar.sconv_stop = 0.3;		// maximum tolerable Sconv before aborting.
	// READ PARAMETER FILE
	i=0;	// line counter.
	char* s = par_string;
	while ( fgets(str, 479, fp) )
	{
		if (i_mpi==0) fputs(str, fpw);		// save file...
		i++;
		char* start = lskip(str);	// skip whitespaces at beginning of str
		if ((*start != 0) && (*start != '#')) {		// ignore commented lines (#) and empty lines.
			int parse_error = parse_param(start);
			if (parse_error) {
				PRINTF0(" parse error (line %d of %s) : '%s'\n",i, fname, start);
				runerr("[Read_Par] malformed line in xshells.par\n");
			}

			// store parameter in compact form in par_string
			char* c = start;
			while (*c && (*c != '#')) {		// stop at comment or end-of-line
				if (!isspace(*c)) *s++ = *c;
				c++;
			}
			*s++ = ' ';		// end with a space
		}
	}

	// PARSE COMMAND LINE OPTIONS
	if ((argi<argc) && (i_mpi==0)) {
		fprintf(fpw, "\n#COMMAND LINE PARAMETERS:\n");
	}
	while (argi<argc) {
		int parse_error = 1;
		if (argv[argi][0] == '-') {		// we have an option
			parse_error = parse_param(argv[argi]+1);	// parse after the '-'
		}
		if (parse_error) {
			char dump[240];
			snprintf(dump, sizeof(dump), "command line option not understood (or missing argument) : '%s'", argv[argi]);
			runerr(dump);
		} else if (i_mpi==0) {		// save option at end of par file...
			fprintf(fpw, "%s\n", argv[argi]+1);
			s += sprintf(s, "%s ", argv[argi]);
		}
		argi++;
	}
	
	// SOME BASIC COMPUTATIONS
	if (jpar.lmax_out<0 || jpar.lmax_out > LMAX) jpar.lmax_out = LMAX;
	if (jpar.mmax_out<0 || jpar.mmax_out > MMAX) jpar.mmax_out = MMAX;
	if (jpar.mmax_out*MRES > jpar.lmax_out)  jpar.mmax_out = jpar.lmax_out/MRES;

	if (jpar.job == NULL) runerr("job name was not defined.");
	fclose(fp);
	if (i_mpi==0) {
		fflush(stdout);		// when writing to a file.
		fclose(fpw);
		if ((backup) && (jpar.debug_no_write<=1)) {
			snprintf(str, sizeof(str), "xshells.par.%s",jpar.job);	rename("tmp.par", str);		// rename file.
			// saves xshells.hpp used at compile time.
			snprintf(str, sizeof(str), "xshells.hpp.%s",jpar.job);	fpw = fopen(str,"w");
			fwrite(xshells_h, 1, strlen((char*)xshells_h), fpw);		fclose(fpw);
		}
	}
	#ifdef XS_MPI
		MPI_Barrier(MPI_COMM_WORLD);
	#endif
	sprintf(s, "#%s %s", argv[0], _HGID_);
	return par_string;
}


/// load or initialize field.
/// Warning, this resets boundary conditions !
double load_init(const char* fname, Spectral *Vlm)
{
	double ampl;
	double ftime = 0.0;
	PolTor* PT = 0;
	int res;
	FieldInfo jinfo;
	char cmd[200];
	char args[120], name[60];

	if (Vlm == 0) runerr("[load_init] try to load unallocated field");

	Vlm->zero_out();		// start with zero everywhere.
	if (fname == NULL) {
		PRINTF0("   + zero\n");
		return(0.0);
	}
	if (Vlm->get_ncomp() == 2)	PT = static_cast<PolTor*>(Vlm);		// we have a PolTor field.

	const char* s0 = fname;
	const char* s = 0;
	int pos_init = 0;	// counter for initialization position
	do {
		s = strchr(s0, '|');		// find special separator character
		if (s) {
			strncpy(cmd, s0, s-s0);		cmd[s-s0] = 0;	// copy and mark end of string
			ampl = split_name_args_ampl(cmd, name, args);
		} else ampl = split_name_args_ampl(s0, name, args);
	#ifdef XS_DEBUG
		PRINTF0("   + name = '%s', ampl = '%g', args = '%s'\n",name, ampl, args);
	#endif

		if (pos_init == 0  &&  strcmp(name, "0") == 0) {
			PRINTF0("   + zero\n");
			res = 1;
		} else {
			if (PT) {	// vector field
				res = Vlm->bco;		// use this to select imposed velocity or magnetic fields.
				Vlm->bci = BC_NONE;		Vlm->bco = BC_NONE;		/// set BC to none
				if (res == BC_MAGNETIC) {
					res = init_B0(*PT, name, ampl, args);
					if (res) {
						for (int lm=0; lm<NLM; lm++) {		// multiply potential imposed at boundaries by 2l+1 (as required by BC).
							if (own(Vlm->ir_bci)) PT->Pol[Vlm->ir_bci-1][lm] *= 2.*el[lm]+1.;
							if (own(Vlm->ir_bco)) PT->Pol[Vlm->ir_bco+1][lm] *= 2.*el[lm]+1.;
						}
					}
				} else {
					res = init_U0(* PT, name, ampl, args);
				}
			} else {	// scalar field
				if (fname == jpar.phi0file) {
					res = init_Phi0(* static_cast<ScalarSH*>(Vlm), name, ampl, args);		// gravity field.
				} else
					res = init_T0(* static_cast<ScalarSH*>(Vlm), name, ampl, args);			// temperature field.
			}
		}

		if (pos_init == 0  &&  res == 0) {		// no field match, try file name and random, but ONLY for the first init (pos_init==0) :
		  int rand_sym = -1;
		  int rand_mmax = MMAX;
		  int rand_poltor = 2;		// 2 = both poloidal and toroidal. 1 = poloidal only
		  double lscale = 10. * Vlm->delta_r((Vlm->ir_bci+Vlm->ir_bco)/2);		// correlation length scale for random fields.
		  if (strcmp(name, "random") == 0) {		// random
			PRINTF0("   + Random field (correlation length=%f)\n", lscale);
			rand_sym = 0;
		  } else if (strcmp(name, "rands") == 0) {		// random, symmetric only
			PRINTF0("   + Random symmetric field (correlation length=%f)\n", lscale);
			rand_sym = 2;
		  } else if (strcmp(name, "randa") == 0) {		// random, anti-symmetric only
			PRINTF0("   + Random anti-symmetric field (correlation length=%f)\n", lscale);
			rand_sym = 1;
		  } else if (PT  &&  strcmp(name, "random_polaxi") == 0) {		// random, poloidal, axisymmetric
			PRINTF0("   + Random poloidal axi-symmetric field (correlation length=%f)\n", lscale);
			rand_sym = 0;
			rand_mmax = 0;
			rand_poltor = 1;
		  }
		  if (rand_sym >= 0) {
			Vlm->add_random(ampl, 0, rand_mmax, rand_sym);
			Vlm->filter_scale(lscale);
			if (rand_poltor == 1) Vlm->zero_out_comp(1);	// remove toroidal component.
		  } else {
			PRINTF0("   + loading from file \"%s\" ...\n",name);
			if ( load_FieldInfo(name, &jinfo) ) {
				Vlm->load(name, &jinfo);
				if (i_mpi==0) print_FieldInfo(&jinfo);		// load from file.
				ftime = jinfo.t;
			} else {
		#ifndef XS_MPI
				if (PT) load_PolTor_text(name, PT);
				else
		#endif
				runerr("file not found.");
			}
			if (ampl != 1.0) Vlm->scale(ampl);
		  }
		} else if (res==0) runerr("[load_init] sequence initialization error: only predefined fields are allowed after first position");
		if (ampl != 1.0) PRINTF0("   + amplitude scaled by %g\n",ampl);
		s0 = s+1;
		pos_init++;
	} while (s);	// repeat to initialize in sequence

	#ifdef XS_MPI
		Vlm->sync_mpi();		// exchange ghost shells.
	#endif
	return(ftime);
}
