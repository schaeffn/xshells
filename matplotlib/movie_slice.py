#!/usr/bin/python
from pylab import *     # matplotlib
from subprocess import *
import sys              # acces a la ligne de commande.
import glob
import time
import xsplot
import argparse

parser = argparse.ArgumentParser(
    description='''Makes a movie of the specified axisymmetric field components for a
    given run of xshells.

    ''',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('base', type=str,
                    help='file base to read')
parser.add_argument('job', type=str,
                    help='name of the xshells job')
parser.add_argument('-i', '--index', type=int, default=1,
                    help='first file to place in movie')
clin = parser.parse_args()
ir0=1
rg=0.35
#ang=0 # vitesse vphi
ang=1 # vitesse angulaire

i=clin.index;   # snapshot numbering

base=clin.base
job=clin.job
files=sorted(glob.glob( base + '_*.' + job))

for f in files[i-1:] :
    print f
    try:
        xsppout = check_output(["./xspp", f, "axi"])
    except CalledProcessError:
        print('error from xspp again, abort.')
        exit()

    figure(figsize=(4, 6))
    subplots_adjust(left=0.01, bottom=0.01, right=0.78, top=0.99, wspace=0.5, hspace=0.2)
    r,ct,V = xsplot.load_merid('o_Vp.m0', ang)
    m = xsplot.plot_merid(r,ct,V,title=str(i))

    savefig( "slide_%04d.png" % i )
    print('>>> slide', i, "saved.\n")
    i+=1
    close()

#retcode = os.system( 'mencoder "mf://slide_*.png" -mf fps=10 -o movie.avi -ovc lavc -lavcopts vcodec=mjpeg' )
