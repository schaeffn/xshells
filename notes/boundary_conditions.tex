\documentclass[12pt,a4paper]{article}



\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{a4wide}
%\usepackage{graphicx}
%\usepackage{epstopdf}

\usepackage[colorlinks=true, linkcolor=blue, citecolor=blue, urlcolor=blue]{hyperref}	% pdf active links.

%\renewcommand{\v}[1]{\vec{#1}}			% vectors with an array
\renewcommand{\v}[1]{\mathbf{#1}}		% vectors in bold font
\newcommand{\vnabla}[0]{\v{\nabla}}
\newcommand{\f}[2]{\frac{#1}{#2}}
\newcommand{\eps}[0]{\epsilon}

\author{Nathanaël Schaeffer}
\title{Boundary conditions in spherical geometry}
\date{\today}


\begin{document}

\maketitle

\section{Framework}

To solve numerically the induction equation and the Navier-Stokes equation, 
spherical harmonic expansion is used together with finite differences in the radial direction.
Spherical harmonics are very convenient for expressing the Laplace operator :
\begin{equation}
\Delta f  =  \f{1}{r} \partial^2_{rr} (rf) - \f{1}{r^2}\ell(\ell+1)f = \partial^2_{rr} f + \f{2}{r}\partial_r f - \f{1}{r^2}\ell(\ell+1)f 
\end{equation}
where $\ell$ is the harmonic degree.


Poloidal/Toroidal expansion is used, and any vector field $\v{v}$ can be written as :

\begin{equation}
\v{v} = \left( \begin{array}{rcl}
0 & + & \frac{1}{r} \ell(\ell+1) P_v \\
\frac{1}{\sin\theta} \partial_\phi T_v & + & \partial_\theta S_v \\
- \partial_\theta T_v & + & \frac{1}{\sin\theta} \partial_\phi S_v
\end{array} \right)
\label{eq:vect_def}
\end{equation}

Where $T_v$ is the toroidal scalar.
When the vector is divergent free $\vnabla . \v{v} = 0$, the Spheroidal component $S_v$ is related to the poloidal $P_v$ :
\begin{equation}
S = \frac{1}{r} \partial_r(rP_v) = \partial_r P_v + \frac{P_v}{r}
\end{equation}
In this document we will consider only these type of solenoidal vectors.


\section{Central consistency condition}
\label{sec:centre}

At the center of the sphere, special conditions apply, which are independent of the physical nature of the field.

\paragraph{For a scalar field} $S$ to be consistent, only one value is allowed at the center, which implies $S=0$ for $\ell>0$.

\paragraph{For a vector field} $\v{v}$ to be consistent, only one vector is allowed at the center.
This can be ensured by choosing a Cartesian projection $v_x, v_y, v_z$ and transforming it to spherical components.
This gives $P_v = 0$ and $T_v = 0$ for every $\ell$, but $S_v = 0$ only for $\ell \neq 1$.
More precisely, we have:
\begin{equation}
\v{v}|_{r=0} = \left( \begin{array}{c}
S_v \\
 \partial_\theta S_v \\
 \frac{1}{\sin\theta} \partial_\phi S_v
\end{array} \right)
\end{equation}
with
\begin{equation}
S_v(r=0) = \left\{ \begin{array}{ll}
 2\partial_r P_v|_{r=0} & \textrm{for } \ell=1 \\
 0 &  \textrm{for } \ell \neq 1
\end{array} \right.
\end{equation}


\section{Magnetic field}

\subsection{Electric insulator}

\textbf{In the insulator}, we have no electrical currents $\v{j} = \vnabla \times \v{b} = 0$ which means $\Delta P_b=0$ and $T_b=0$.
Hence one has, for a given spherical harmonic degree $\ell$ :
\begin{equation}
\f{1}{r} \partial^2_{rr} (rP) - \f{\ell(\ell+1)}{r^2} P = 0
\end{equation}
which has solutions of the form
\begin{equation}
P = C_o \, r^\ell + C_i \, r^{-(\ell+1)}
\label{eq:P_r}
\end{equation}
Here $C_i=0$ (resp. $C_o=0$) if there are no internal (resp. external) sources of magnetic field.


\subsubsection{Without other sources of magnetic field}

\textbf{At the boundary}, the normal current $\v{j} . \v{n}$ is continuous and so is $T_b$.
In addition, the magnetic field $\v{b}$ is continuous too, implying the continuity of $P_b$ and $\partial_r P_b$.
Hence, at the boundary, depending on whether the field is computed outside or inside the boundary, one has
\begin{eqnarray}
T = 0 \\
\partial_r P = \ell P/r	& & \textrm{for inner boundary} \\
\partial_r P = -(\ell+1) P/r	& & \textrm{for outer boundary}
\end{eqnarray}
Of course this is only valid if there are no external sources of magnetic fields.

\emph{In particular one should pay attention not to apply these boundary conditions on imposed fields when calculating currents...}

\subsubsection{With applied magnetic field}

With a magnetic field coming from a source different from our system, both $C_i$ and $C_o$ are non-zero.
However, at the boundary with an insulator, one still has the continuity of the magnetic field's poloidal scalar $P$ and its derivative $\partial_r P$.

The externally imposed magnetic field is known at the boundary as $P_{out} = C_o \, r^\ell$.
Hence the derivative of eq. \ref{eq:P_r} reads :
$$
\partial_r P = \ell/r \, P_{out} -(\ell+1)/r \, (P - P_{out})
$$

The internally imposed magnetic field is known at the boundary as $P_{in} = C_i \, r^{-(\ell+1)}$.
Hence the derivative of eq. \ref{eq:P_r} reads :
$$
\partial_r P = \ell/r \, (P - P_{in}) -(\ell+1)/r \, P_{in}
$$

And finally the general boundary conditions for the poloidal and toroidal scalar of the magnetic field at an insulating boundary reads :


\begin{eqnarray}
T = 0 \\
\partial_r P = \ell P/r	      & - (2\ell+1)/r \, P_{in}  & \textrm{for inner boundary} \\
\partial_r P = -(\ell+1) P/r  & + (2\ell+1)/r \, P_{out} & \textrm{for outer boundary}
\end{eqnarray}


\subsubsection{finite-difference formulation}
\label{sec:fdb}

At the boundary, $\Delta P$ is approximated with only two points thanks to the previous boundary conditions.
Accurate to second order in $\eps$, the Taylor expansion gives :
$$P(r+\eps) = P(r) + \eps \partial_r P + \f{\eps^2}{2} \partial^2_{rr} P$$
and using the BC :
$$P(r+\eps) = P(r) + \eps \left( \alpha \f{P(r)}{r} + \beta \f{P_0}{r} \right) + \f{\eps^2}{2} \partial^2_{rr} P$$
with $\alpha = \ell$, $\beta = -(2\ell+1)$ for the inner boundary and $\alpha = -(\ell+1)$, $\beta = 2\ell+1$ for the outer boundary, and $P_0$ the poloidal scalar at the boundary of the imposed field.
Finally
\begin{equation}
\partial^2_{rr} P = -\left( \f{2}{\eps^2} + \f{2\alpha}{r\eps} \right) P(r) \: + \: \f{2}{\eps^2} P(r+\eps) \: - \: \f{2\beta}{r\eps} P_0
\end{equation}
From which we write the discrete Laplace operator :
\begin{eqnarray}
\Delta P &=& \f{2\alpha}{r^2}P - \left( \f{2}{\eps^2} + \f{2\alpha}{r\eps} \right) P - \f{\ell(\ell+1)}{r^2} P \\
         &+& \f{2}{\eps^2} P(r+\eps) \\
         &+& 2\beta P_0 \left( \f{1}{r^2} - \f{1}{r\eps} \right)
\end{eqnarray}

In xshells, we add an additional \emph{ghost} shell which contains $(2\ell+1)P_0$, so that an arbitrary potential field can be imposed from outside the conductive domain.


\section{Temperature field}

\subsection{Imposed temperature}

The temperature $T$ at the boundary is imposed. For example the crystallization of the inner-core happens at fixed temperature (at a given pressure).
\begin{equation}
T = T_\textrm{boundary}
\end{equation}
There is no need to compute the Laplacian here, as the temperature is imposed. This boundary condition is straightforward to impose.


\subsection{Imposed thermal flux}

The flux at the boundary is imposed, that is $\partial_r T = ( \partial_r T )_\textrm{boundary}$.
For example, the heat flux extracted from the core by the mantle is supposed to be imposed by the mantle.

\subsubsection{finite-difference formulation}

From the taylor expansion, one gets
\begin{equation}
\partial^2_{rr} T = \f{2}{\eps^2} T(r+\eps) \: - \: \f{2}{\eps^2} T(r) \: - \: \f{2}{\eps} (\partial_r T)_0
\end{equation}
which leads to the Laplacian of $T$ at the boundary, with $(\partial_r T)_0$ imposed :
\begin{eqnarray}
\Delta T(r_0) &=& \f{2}{\eps^2} \, T(r_0+\eps) \\
		 &-& \left( \f{2}{\eps^2} + \f{\ell(\ell+1)}{r^2} \right) \, T(r_0) \\
		 &+& \left( \f{2}{r} - \f{2}{\eps} \right) \, (\partial_r T)_0
\end{eqnarray}

An additional \emph{ghost} shell which contains $(\partial_r T)_0$ is added, so that an arbitrary flux can be imposed at the boundary.

\subsection{Stationary solution without motion for $\ell=0$}

The temperature equation reads (constant thermal diffusivity):
\begin{equation}
\partial_t T = \Delta T + Q
\end{equation}
where Q is a constant accounting for internal heating.
The stationary solution for $\ell=0$ is then:
\begin{equation}
T_0(r) = -\f{Qr^2}{6} + \f{A}{r} + B
\end{equation}
where $A$ and $B$ are constants defined by the boundary conditions.
In particular, if the domain extends to $r=0$ (full sphere), $A=0$ and the profile is fixed besides a temperature shift ($B$).
If the domain excludes $r=0$, one must provide two boundary conditions, either the flux or the temperature at each boundary.

\subsection{Central condition}

For a full sphere, the temperature is free at $r=0$, but only for $\ell=0$ (see \ref{sec:centre}).
For $\ell>1$, there is no problem, as the temperature field at the centre is fixed to $0$.

For $\ell=0$ we have $\partial_r T =0$ because $\v{\nabla} T$ must be defined at $r=0$ (implying $\partial_r T=0$ for $\ell \neq 1$).
To summarize, we have:
\begin{align}
T=0 & \quad \textrm{for } \ell>0 \\
\partial_r T = 0 & \quad \textrm{for } \ell \neq 1 \\
\Delta T = 0 & \quad \textrm{for } \ell > 0
\end{align}

\subsubsection{finite-difference formulation for $l=0$}

Accurate to second order in $\eps$, the Taylor expansion gives :
$$T(\eps) = T(0) + \eps \partial_r T|_0 + \f{\eps^2}{2} \partial^2_{rr} T_0$$
and using the BC $\partial_r T=0$:
$$T(\eps) = T(0) + \f{\eps^2}{2} \partial^2_{rr} T_0$$
Finally
\begin{equation}
\partial^2_{rr} T = \f{2}{\eps^2} \left( T(\eps) - T(0) \right)
\end{equation}
This implies $T \sim C r^2 + B$, so that we can compute
\begin{equation}
\f{1}{r}\partial_{r} T = 2 C = \partial_{rr} T
\end{equation}
from which we can estimate the Laplacian:
\begin{eqnarray}
\Delta T &=& -\f{6}{\eps^2} \, T(0) \\
		&+& \f{6}{\eps^2} \, T(\eps)
\end{eqnarray}


\section{Velocity field}

\subsection{No-slip velocity}

This is the most realistic boundary condition for a viscous fluid : the fluid follows the boundary.
\begin{equation}
\v{u} = \v{u}_\textrm{boundary}
\end{equation}
%For a solid boundary, the angular motion is completely described by the toroidal part.
which translates into
\begin{eqnarray}
T &=& T_\textrm{boundary} \\
P &=& P_\textrm{boundary} \\
S &=& S_\textrm{boundary} \qquad \Rightarrow \: \partial_r P = (\partial_r P)_\textrm{boundary} \\
\end{eqnarray}
%By assuming $u_r=0$ at the boundary, one can often find this last boundary condition reduced to $P=0$ and $\partial_r P=0$, but I'm not quite sure it leads to the same behavior when inverting matrices involving $\Delta\Delta P$ (numerical tests support this).
%However, this will change the matrix to be inverted, and leads to a different behavior.
%\emph{Knowledge of how the solid boundary may deform seems required to impose proper boundary conditions.}

For an arbitrary boundary velocity, we thus need to prescribe $T$, $P$ and $\partial_r P$ at the boundary.
This allows also to compute the flow in a spherical sinking bubble.

\subsubsection{finite-difference formulation}

%Same as for the electric insulator BC (see sec. \ref{sec:fdb}), but with $\alpha = -1$ :
%\begin{eqnarray}
%\Delta P &=& -\f{2}{r^2}P - \left( \f{2}{\eps^2} - \f{2}{r\eps} \right) P - \f{\ell(\ell+1)}{r^2} P \\
%         &+& \f{2}{\eps^2} P(r+\eps)
%\end{eqnarray}

From the taylor expansion, one gets
\begin{equation}
\partial^2_{rr} P = -\f{2}{\eps^2} P_0 \: + \: \f{2}{\eps^2} P(r+\eps) \: - \: \f{2}{\eps} (\partial_r P)_0
\end{equation}
which leads to the Laplacian of $P$ at the boundary, with $P_0$ and $(\partial_r P)_0$ imposed :
\begin{eqnarray}
\Delta P &=& \f{2}{\eps^2} \, P(r+\eps) \\
		 &-& \left( \f{2}{\eps^2} + \f{\ell(\ell+1)}{r^2} \right) \, P_0 \\
		 &+& \left( \f{2}{r} - \f{2}{\eps} \right) \, (\partial_r P)_0
\end{eqnarray}

In xshells, we add an additional \emph{ghost} shell which contains $(\partial_r P)_0$, so that the boundary can have an arbitrary velocity.

\subsection{Stress-free velocity}

In order to neglect the thin viscous boundary layers that may form in the no-slip case, we can alternatively choose to let the fluid freely slip over the boundaries, that is to impose that the tangential constraint vanishes :
\begin{eqnarray}
r \partial_r \f{u_\theta}{r} + \f{1}{r} \partial_\theta u_r  = 0  		\label{eq:sfbc1} \\
r \partial_r \f{u_\phi}{r} + \f{1}{r\sin\theta} \partial_\phi u_r = 0	\label{eq:sfbc2}
\end{eqnarray}

\subsubsection{Assuming $u_r=0$}

Combining equations \ref{eq:sfbc1} and \ref{eq:sfbc2}, while assuming $u_r=0$ at the boundary, one finds :
$$ P = 0, \quad \partial_r \f{T}{r} = 0, \quad \partial_r \f{S}{r} = 0 $$
which can be rewritten as :
\begin{eqnarray}
P &=& 0 \\
\partial_r T &=& T/r \\
\partial^2_{rr} P &=&  0
\end{eqnarray}

\subsubsection{Arbitrary imposed $u_r$}

Combining equations \ref{eq:sfbc1} and \ref{eq:sfbc2}, without assuming $u_r=0$, one finds :

\begin{eqnarray}
\partial_r \f{T}{r} &=& 0	\qquad  \textrm{for any $u_r$} \\
\partial_r \f{S}{r} &=& - \f{u_r}{r^2}
\end{eqnarray}
which can be rewritten as :
\begin{eqnarray}
\partial_r T &=& T/r \\
\partial^2_{rr} P &=&  \f{2 - \ell(\ell+1)}{r^2} P
\end{eqnarray}
One need to add a condition which constrains $u_r$ so that $P=P_0$ is known at the boundary.

\emph{We can notice here, that the result obtained by assuming $u_r=0$ leads to a different matrix than the case of arbitrary $u_r$, possibly leading to a different numerical behaviour.}

\subsubsection{finite-difference formulation}

$\Delta T$ is handled like the electric insulator BC (sec. \ref{sec:fdb}), but with $\alpha = 1$ (and $\beta=0$) :
\begin{eqnarray}
\Delta T &=& \f{2}{r^2}T - \left( \f{2}{\eps^2} + \f{2}{r\eps} \right) T - \f{\ell(\ell+1)}{r^2} T \\
         &+& \f{2}{\eps^2} T(r+\eps)
\end{eqnarray}

At the boundary, $\Delta P$ is approximated with only two points thanks to the previous boundary conditions.
Accurate to second order in $\eps$, the Taylor expansion gives :
$$P(r+\eps) = P_0 + \eps \partial_r P + \f{\eps^2}{2} \partial^2_{rr} P$$
and using the BC :
$$P(r+\eps) = P_0 + \eps \partial_r P + \f{\eps^2}{2} \f{2 - \ell(\ell+1)}{r^2} P_0$$
and finally
\begin{equation}
(\partial_r P)_0 = \left( -\f{1}{\eps} + \eps \f{\ell(\ell+1) - 2}{2r^2} \right) P_0 \: + \: \f{1}{\eps} P(r+\eps)
\end{equation}
From which we write the discrete Laplace operator :
\begin{eqnarray}
\Delta P &=& \left( \eps \f{\ell(\ell+1)-2}{r^3} + \f{2 - 2\ell(\ell+1)}{r^2} - \f{2}{r\eps} \right) \, P_0 \\
         &+& \f{2}{r\eps} \, P(r+\eps)
\end{eqnarray}

\subsection{Central condition}

Section \ref{sec:centre} gives geometrical constraints on the Poloidal/Toroidal decomposition at $r=0$.
For $\ell \neq 1$, we have enough constraints on the field: $T=0$, $P=0$, $\partial_r P = 0$.

For $\ell = 1$, the same regularity conditions for the vorticity field adds $\Delta P = 0$ (toroidal scalar of vorticity) at $r=0$.
Expanding $P(r \to 0) = ar + br^2 + cr^3$ gives for the laplace operator:

$$\left. \Delta P\right|_{r \to 0}^{\ell=1} = 4b = 0$$

which translates into $\partial^2_{rr} P = 0$ for $\ell = 1$.

To summarize, we have:
\begin{align}
T=0, \quad  P=0, \quad \partial_r P = 0 & \quad \textrm{for } \ell > 1 \\
T=0, \quad  P=0, \quad \partial^2_{rr} P = 0 & \quad \textrm{for } \ell = 1
\end{align}


\subsection{Slightly elliptic boundary}

Here, we examine the possibility to implement boundary conditions that would model a small elliptic deformation of our spherical boundary.

The virtual boundary is defined by :
$$ r(\theta) = 1 + \eps^0 Y_2^0(\theta) + \eps^2 Y_2^2(\theta,\phi) $$
where $\eps^0$ and $\eps^2$ are small corrections to the spherical geometry, corresponding to a pole flattening ($m=0$) and a tidal deformation ($m=2$).

In the free-slip formulation, $u.n=0$ leads to

$$ u_r = \eps^0 u_\theta \partial_\theta Y_2^0(\theta) + \eps^2 ( u_\theta \partial_\theta Y_2^2(\theta,\phi) + u_\phi \partial_\phi Y_2^0(\theta, \phi) ) $$


\subsubsection{Perturbation approach}

the elliptic boundary induces a perturbation in $u_r$ at the boundary :
$$ u_r^1 = \eps f(T^0, P^0) $$
leading to an imposed $P^1$ :
$$ P^1 = \frac{r}{\ell(\ell+1)} u_r^1 $$
Since the boundary condition on $T$ does not depend on $u_r$, the toroidal part is not modified :
$$ T^1 = 0 $$

\paragraph{time-lag approach:} should be good for stationary problems :

\begin{enumerate}
\item Set $ u_r(t+dt) = \eps f( T(t), P(t))$
\item then solve for $P(t+dt)$ and $T(t+dt)$.
\end{enumerate}

\paragraph{iterative approach:} solve iteratively
\begin{enumerate}
\item use previous values as first guess : $u_r^0(t) = u_r(t-dt)$
\item solve for $T^0$ and $P^0$
\item correct $u_r^{k+1} = \eps f(T^0, P^k) $
\item solve for $P^{k+1}$
\item hope $u_r^{k}$ converges quickly !
\end{enumerate}


\newpage
\section{Integrals, spherical harmonics and scalar products}

Integrals over the sphere of a quadratic term $ab$ is in fact a scalar product.
Hence, such integrals can be conveniently expressed in spherical harmonic space, because scalar products in real space correspond to scalar product in harmonic space:
$$ \int_S a(\theta,\phi)\,b(\theta,\phi)\,dS  = \left<a,b\right> = \sum_{\ell,m}  a_\ell^m \, \overline{b_\ell^m}$$
Note that for real $a$ and $b$, the hermitian symmetry $a_\ell^{-m} = \overline{a_\ell^m}$ allows to write:
$$  \sum_{\ell,m}  a_\ell^m \, \overline{b_\ell^m} = \sum_\ell a_\ell^0 b_\ell^0 + 2 \sum_{\ell,m>0} \Re (a_\ell^m) \Re(b_\ell^m) + \Im(a_\ell^m)\Im(b_\ell^m)$$


\subsection{Axial magnetic torque on a spherical surface}

The magnetic torque $\mathcal{T}_z$ reads:
$$\mathcal{T}_z = \int_{S} B_r B_\phi \sin\theta \, dS $$
From the definition of a vector (eq. \ref{eq:vect_def}), we see that the scalar
$$ B_\phi \sin\theta = - \sin\theta \, \partial_\theta T + \partial_\phi S $$
can be expressed readily in spherical harmonic space as $MT + imS$, where $M$ is a tridiagonal matrix (with zero on the diagonal) in spectral space that transforms a scalar expansion $T_\ell^m$
into the expansion of $\sin\theta \, \partial_\theta T$.
This allows us to write $\mathcal{T}_z$ as a scalar product in spectral space:
$$\mathcal{T}_z = \left<B_r,MT+imS\right>$$
This formulation avoids costly spherical harmonic transforms to compute the electromagnetic torque on a spherical surface. One just need to compute $Q$ and $MT+imS$ without leaving spectral space.

\subsection{Work of viscous forces}

The total work of visous forces $F_\nu$ can be integrated by parts
$$ F_\nu = \nu \int_V u.\nabla^2 u \, dV  = \nu \int_S (u_\theta \omega_\phi - u_\phi \omega_\theta)\, dS - \int_V \omega^2 \, dV $$
where $\omega = \nabla \times u$ is the vorticity.

Besides the total enstrophy that is routinely computed in spectral space, the surface term on the right-hand-side also resembles a scalar product:
$$I = \int_S (u_\theta \omega_\phi - u_\phi \omega_\theta)\, dS = \int_S (u_\theta v_\theta + u_\phi v_\phi)\, dS = \left< u, v\right>$$
with $v_\theta = \omega_\phi$ and $v_\phi = -\omega_\theta$.
This corresponds to defining vector $v$ from $S_v=-T_\omega$ and $T_v = S_\omega$.
Then, the surface integral can be identified with
$$I = \left< (S_u,T_u),(-T_\omega,S_\omega) \right>$$
And since the scalar product for vectors is defined in spectral space as:
$$\left<(S,T),(W,X)\right> = \sum_{\ell,m} \ell(\ell+1) (S_\ell^m \overline{W_\ell^m} + T_\ell^m \overline{X_\ell^m})$$
this can be computed efficiently without transforming to spatial space.

\subsection{Viscous dissipation}

The viscous dissipation $D_\nu$ is the contraction of the symmetric part of the velocity gradient tensor $\mathcal{S}$:
$$ D_\nu = \int_V \mathcal{S}:\mathcal{S} \,dV$$
If we want to evaluate the total dissipation, one should be able to use the same kind of tricks to avoid transforming 6 quantities to spectral space.


\end{document}

