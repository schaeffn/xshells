\documentclass[12pt,a4paper]{article}

\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{a4wide}
%\usepackage{graphicx}
%\usepackage{epstopdf}

\usepackage[colorlinks=true, linkcolor=blue, citecolor=blue, urlcolor=blue]{hyperref}	% pdf active links.

%\renewcommand{\v}[1]{\vec{#1}}			% vectors with an array
\renewcommand{\v}[1]{\mathbf{#1}}		% vectors in bold font
\newcommand{\vnabla}[0]{\v{\nabla}}
\newcommand{\f}[2]{\frac{#1}{#2}}
\newcommand{\eps}[0]{\epsilon}

\author{Nathanaël Schaeffer}
\title{Implementation of variable conductivity}


\begin{document}

\maketitle

\section{Framework}

To solve numerically the induction equation and the Navier-Stokes equation, 
spherical harmonic expansion is used together with finite differences in the radial direction.
Spherical harmonics are very convenient for expressing the Laplace operator :
\begin{equation}
\Delta f  =  \f{1}{r} \partial_{rr} (rf) - \f{1}{r^2}\ell(\ell+1)f = \partial_{rr} f + \f{2}{r}\partial_r f - \f{1}{r^2}\ell(\ell+1)f 
\end{equation}
where $\ell$ is the harmonic degree.


Poloidal/Toroidal expansion is used, and any vector field $\v{v}$ can be written as :

\begin{equation}
\v{v} = \left( \begin{array}{rcl}
0 & + & \frac{1}{r} \ell(\ell+1) P_v \\
\frac{1}{\sin\theta} \partial_\phi T_v & + & \partial_\theta S_v \\
- \partial_\theta T_v & + & \frac{1}{\sin\theta} \partial_\phi S_v
\end{array} \right)
\end{equation}

Where $T_v$ is the toroidal scalar.
When the vector is divergent free $\vnabla . \v{v} = 0$, the Spheroidal component $S_v$ is related to the poloidal $P_v$ :
\begin{equation}
S = \frac{1}{r} \partial_r(rP_v) = \partial_r P_v + \frac{P_v}{r}
\end{equation}
In this document we will consider only these type of solenoidal vectors.


\section{Induction equation}

The induction equation reads :

\begin{equation}
\partial_t \v{b} = \vnabla \times ( \v{u} \times \v{b} - \eta \vnabla \times \v{b} )
\end{equation}

\section{Conductivity depends only on $r$}

\subsection{Continous variation}

When $\eta \equiv \eta(r)$ we can expand the induction equation:
\begin{equation}
\partial_t \v{b} = \vnabla \times ( \v{u} \times \v{b} ) + \eta(r) \Delta \v{b} +( \vnabla \times \v{b}) \times \vnabla \eta(r)
\end{equation}

\subsubsection{Poloidal part}

We introduce the electrical current $\v{j} = \vnabla \times \v{b}$. The term involving $\vnabla \eta$ has no radial component
\begin{equation}
\v{r}.[ \v{j} \times \vnabla \eta(r) ] = 0
\end{equation}
implying \textbf{the poloidal part of the induction equation is unaltered}
\begin{equation}
\partial_t P_b = \eta(r) \Delta P_b + \mathrm{NL}_P
\label{eq:dT_eta}
\end{equation}


\subsubsection{Toroidal part}

The toroidal part is obtained by taking the radial component of the curl of the induction equation:
\begin{equation}
\v{r}.\partial_t \v{j} = \v{r}.\vnabla\times\vnabla\times( \v{u} \times \v{b} ) + \v{r}.\vnabla \times (\eta(r) \Delta \v{b}) +\v{r}.\vnabla \times (\v{j} \times \vnabla \eta(r))
\end{equation}

The second term on the right hand side can be developed:
\begin{eqnarray}
\v{r}.\vnabla \times (\eta(r) \Delta \v{b}) &=& \v{r}.( \vnabla\eta \times \Delta\v{b} + \eta(r) \Delta\v{j} ) \\
											&=& \eta(r) \: \v{r}.\Delta\v{j} \\
											&=& \eta(r) \: \ell(\ell+1) \Delta T_b
\end{eqnarray}
because, again, the term involving $\vnabla \eta$ has no radial component.

The last term expands to:
\begin{equation}
\v{r}.\vnabla \times [ \v{j} \times \vnabla \eta(r) ]  =  r \partial_r \eta \: \left( - \f{1}{r\sin\theta} \partial_\theta (\sin\theta j_\theta) - \f{1}{r\sin\theta}\partial_\phi j_\phi \right)
\end{equation}
using the definition of the vector components
\begin{eqnarray}
j_\theta & = & \f{1}{\sin\theta} \partial_\phi T_j  + \partial_\theta S_j \\
j_\phi & = & \f{1}{\sin\theta} \partial_\phi S_j - \partial_\theta T_j
\end{eqnarray}
we find after some simplification:
\begin{equation}
\v{r}.\vnabla \times [ ( \vnabla \times \v{b}) \times \vnabla \eta(r) ] = \partial_r \eta \: \ell(\ell+1) S_j
\end{equation}

Since $S_j = \f{1}{r}\partial_r(rT_b)$, we finally obtain the \textbf{toroidal induction equation}:
\begin{equation}
\partial_t T_b = \eta(r) \Delta T_b + (\partial_r \eta) \: \f{1}{r}\partial_r(rT_b) + \mathrm{NL}_T
\label{eq:dT_eta}
\end{equation}
where $\mathrm{NL}_T$ is the toroidal contribution of the non-linear terms.
This is readily included into the tri-banded matrix and can be solved at no additional computational cost.

Interestingly, equation \ref{eq:dT_eta} can be rewritten as
\begin{equation}
\partial_t T_b = \f{1}{r} \f{\partial}{\partial r} \left( \eta \f{\partial \, rT_b}{\partial r} \right) - \f{\ell(\ell+1)}{r^2} T_b + \mathrm{NL}_T
\label{eq:dT_eta_alt}
\end{equation}


\subsection{Discontinuous conductivities}

For discontinuous conductivities (ie a solid boundary with a different conductivity), the first jump conditions is $\v{B}$ continuous across the interface.
Hence $P_b$, $\partial_r P_b$ and $T_b$ are continuous.
The second jump condition, is $(j \times \v{n})/\sigma$ continuous across the interface, from which we have the continuity of $\eta \partial_r (rT_b)$ and $\eta \Delta P_b$ (if $\mu$ is the same everywhere).

Since $\eta$ is constant everywhere but at the discontinuity, the governing equation is
\begin{equation}
\partial_t \v{b} = \vnabla \times ( \v{u} \times \v{b} ) + \eta \Delta \v{b}
\end{equation}


In order to derive the finite difference approximation, we write the Taylor expansions left and right of the discontinuity located at $r_0$:
\begin{equation}
f_\pm = f_{0\pm} \pm dr (\partial_r f)_{0\pm}  +  \f{dr^2}{2} (\partial_{rr} f)_{0\pm} \label{eq:f_pm}
\end{equation}
where $f_\pm = f(r_0 \pm dr)$, and $f_{0\pm}$ is the limit $f(r_0 \pm \varepsilon)$ when $\varepsilon \to 0^+$.
This is the start for a finite difference formulation, with equidistant points around the discontinuity.

We can isolate $\partial_{rr} f$
\begin{equation}
2\f{f_\pm - f_{0\pm}}{dr^2} \mp \f{2}{dr} (\partial_r f)_{0\pm} = (\partial_{rr} f)_{0\pm}
\end{equation}
and add the angular part to have an evaluation of $\Delta (f/r)$ at each side of the discontinuity:
\begin{equation}
2\f{f_\pm - f_{0\pm}}{dr^2} \mp \f{2}{dr} (\partial_r f)_{0\pm} - \f{\ell(\ell+1)}{r_0^2} f_{0\pm}
	= (r\Delta (f/r))_{0\pm}
	\label{eq:fd_jump}
\end{equation}

\subsubsection{Poloidal part}

Here, we take $f = rP$ in the previous finite difference formulation.
In this case, $f_0$ and $(\partial_r f)_0$ are uniquely defined (by continuity).
Multiplying equation \ref{eq:fd_jump} by $\eta_+ \eta_-$ gives:
\begin{equation}
2\eta_+ \eta_- \left( \f{ f_\pm - f_0 }{dr^2} - \f{\ell(\ell+1)}{2r_0^2} f_0 \right)= \pm \eta_+ \eta_- \f{2}{dr} (\partial_r f)_0 + \eta_+ \eta_- (r\Delta P)_{0\pm}
\end{equation}
By taking the sum of these equations, we eliminate $(\partial_r f)_0$:
\begin{equation}
2\eta_+ \eta_- \left( \f{ f_+ + f_- - 2f_0 }{dr^2} - \f{\ell(\ell+1)}{r_0^2} f_0 \right) = (\eta_+ + \eta_-) r_0 K
\end{equation}
with $K = \eta_+ (\Delta P)_+ = \eta_- (\Delta P)_-$.
We finally obtain the finite difference approximation of $\eta \Delta P$:
\begin{equation}
(\eta \Delta P)_0 = \f{2\eta_+ \eta_-}{\eta_+ + \eta_-} \left( \f{ r_+ P_+ + r_- P_- - 2r_0 P_0}{r_0 dr^2} - \f{\ell(\ell+1)}{r_0^2} \right)
	\label{eq:dP_jump}
\end{equation}
Meaning that, for the finite difference formulation, $\eta$ must simply be replaced by its harmonic mean at the interface. Or equivalently $\sigma$ be replaced by its mean.

\subsubsection{Toroidal part}

Here, we take $f = rT$ in the finite difference formulation of equations \ref{eq:f_pm}.
In this case, $f_0$ is uniquely defined (by continuity) and $\eta_+ (\partial_r f)_{0+} = \eta_- (\partial_r f)_{0-}$.
Multiplying equation \ref{eq:fd_jump} by $\eta_\pm$ gives:
\begin{equation}
2\eta_\pm \left( \f{ f_\pm - f_0 }{dr^2} - \f{\ell(\ell+1)}{2r_0^2} f_0 \right)= \pm \eta_\pm \f{2}{dr} (\partial_r f)_{0\pm} + \eta_\pm (r\Delta T)_{0\pm}
\end{equation}
By taking the sum of these equations, we eliminate $\eta_\pm (\partial_r f)_{0\pm}$:
\begin{equation}
2\f{\eta_+ f_+ + \eta_- f_- - (\eta_+ + \eta_-)f_0 }{dr^2} - (\eta_+ + \eta_-) \f{\ell(\ell+1)}{r_0^2} f_0 = 2 r_0 K
\end{equation}
with $K = \eta_+ (\Delta T)_+ = \eta_- (\Delta T)_-$.
We finally obtain the finite difference approximation of $\eta \Delta T$:
\begin{equation}
(\eta \Delta T)_0 = \f{\eta_+ r_+ T_+ + \eta_- r_- T_- - (\eta_+ + \eta_-) r_0 T_0 }{r_0 dr^2}
	- \f{\eta_+ + \eta_-}{2} \f{\ell(\ell+1)}{r_0^2} T_0
	\label{eq:dT_jump}
\end{equation}
Note that we do not have to take into account the derivative of $\eta$ as in \ref{eq:dT_eta}, because $\eta$ is constant before and after the jump.


% substract equations 25:
%
% eta+*(2*(f+-f0)/dr2 - l*(l+1)/r2*f0)  - eta-*(2*(f--f0)/dr2 - l*(l+1)/r2*f0) = 4/dr*eta*f'
%
% 2/dr2 * (eta+*f+ - eta-*f-)   + f0*(eta- - eta+) * [ 2/dr2 + l*(l+1)/r2 ] = 4/dr*eta*f'
%
% 2/dr * (eta+*f+ - eta-*f-)   + f0*(eta- - eta+) * [ 2/dr + l*(l+1)*dr/r2 ] = 4*eta*f'



\paragraph*{alternate method:}
Interestingly, starting from the formulation for variable conductivity \ref{eq:dT_eta_alt} we can set $F=\eta \partial_r (rT)$ which is continuous and can be approximated at the points $r_0 \pm dr/2$ by first order finite difference:
\begin{equation}
F(r_0 \pm dr/2) = \pm \eta_\pm \f{r_\pm T_\pm - r_0 T_0}{dr}
\label{eq:F1}
\end{equation}
We can write the order one Taylor expansions for $F_\pm = F(r_0 \pm dr/2)$:
\begin{equation}
F_\pm = F_0 \pm \f{dr}{2} (\partial_r F)_{0\pm} + ...
\end{equation}
We multiply by $\pm2/dr$ and subtract $\eta_\pm \ell(\ell+1) T_0/r_0 $:
\begin{equation}
\pm 2 \f{F_\pm - F_0}{dr} - \eta_\pm \f{\ell(\ell+1)}{r_0} T_0= r_0 K
\label{eq:K_T_alt}
\end{equation}
where $K = (1/r_0)(\partial_r F)_{0\pm} - \eta_\pm \ell(\ell+1) T_0/r_0^2$ is the diffusion operator of equation \ref{eq:dT_eta_alt} (which must be continuous because $T$ is).
By adding equations \ref{eq:K_T_alt}, one obtains
\begin{equation}
2 \f{F_+ - F_-}{dr} - (\eta_+ + \eta_-) \f{\ell(\ell+1)}{r_0} T_0 = 2 r_0 K
\end{equation}
and by substituting $F_\pm$ by its first order approximation \ref{eq:F1}, we find for $K$:
\begin{equation}
K = \f{\eta_+ r_+ T_+ + \eta_- r_- T_- - (\eta_+ + \eta_-) r_0 T_0 }{r_0 dr^2}
	- \f{\eta_+ + \eta_-}{2} \f{\ell(\ell+1)}{r_0^2} T_0
\end{equation}
which is not so surprisingly the same expression as in equation \ref{eq:dT_jump}.
This alternate method of derivation appears less rigorous as it involves two order one Taylor expansion, instead of one order two expansion. It is however interesting as they show the coherency between continuous variation and discontinuity.



\section{Conductivity depends on $r,\theta,\phi$}

For the more general case of a conductivity $\eta(r,\theta,\phi)$ constant in time, we want to take advantage of the previous formalism
which solves implicitely and acurately the magnetic diffusion. In order to do so, we split $\eta$ in two parts:
\begin{equation}
\eta(r,\theta,\phi) = \bar{\eta}(r) + \eta'(r,\theta,\phi)
\end{equation}
where $\bar{\eta}(r)$ is the minimum value of $\eta$ at fixed $r$, implying that $\bar{\eta}$ and $\eta'$ are positive.

The induction equation now reads :
\begin{equation}
\partial_t \v{b} = \vnabla \times ( \v{u} \times \v{b} - \eta' \v{j} ) + \bar{\eta}(r) \Delta \v{b} +( \vnabla \times \v{b}) \times \vnabla \bar{\eta}(r)
\end{equation}

Hence, the problem is solved simply by replacing the non-linear, explicit term $\v{u}\times\v{b}$ by
$$ \v{u} \times \v{b} - \eta' \v{j} $$
while keeping as much as possible of the dissipation in the implicit terms.

\emph{This approach to $\theta$ and $\phi$ dependence of $\eta$ does not seem to be usable, as the required time-step drops dramatically.}

\end{document}

