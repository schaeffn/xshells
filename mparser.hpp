/** \file mparser.hpp
 * simple math parser class
 * based on "The desk calculator", pp 107-119, sec 6.1,
 * by Bjarne Stroustrup http://www.stroustrup.com/
 * No guarantees offered. Constructive comments to bs@research.att.com
 * 
 * added "group" to support ^ (exponent operator).
 * added custum function support sqrt, ...
 */

/*
    program:
	END			   // END is end-of-input
	expr_list END

    expr_list:
	expression PRINT	   // PRINT is semicolon
	expression PRINT expr_list

    expression:
	expression + term
	expression - term
	term

    term:
	term / primary
	term * primary
	primary

    primary:
	NUMBER
	NAME
	NAME = expression
	- primary
	( expression )
*/

#include <string>
#include <cctype>
#include <iostream>
#include <map>
#include <sstream>	// string streams

#include <math.h>

//#define MPARSER_DEBUG

using namespace std;

enum Token_value {
	NAME,		NUMBER,		END,		FUNC,
	PLUS='+',	MINUS='-',	MUL='*',	DIV='/',
	PRINT=';',	ASSIGN='=',	LP='(',		RP=')',
	POW='^', 	CMT='#'
};

typedef	double (*fptr)(double);

/// simple math parser class.
/// After the parsing has been done (using the parse() method), the value associated to
/// all variables can be retrieved from the var["var_name"] map.
/// e.g.:  double x = mp.var["x"];
class mparser
{
  private:
	Token_value curr_tok;
	double number_value;
	string string_value;
	istream* input;			// pointer to input stream
	int no_of_errors;

	Token_value get_token();
	double expr(bool);	// cannot do without
	double group(bool);
	double prim(bool get);		// handle primaries
	double term(bool get);		// multiply and divide

  public:
	map<string,fptr> func;		///< predefined functions of one variable.
	map<string,double> var;		///< predefined and parsed variables.

	mparser()
	{
		curr_tok = PRINT;
		// predefined variables:
		var["pi"] = M_PI;
		var["e"] = 2.7182818284590452354;
		// functions:
		func["sqrt"] = &sqrt;	func["exp"] = &exp;		func["log"] = &log;
		func["sin"] = &sin;		func["cos"] = &cos;		func["tan"] = &tan;
		func["atan"] = &atan; 
	}

	~mparser() { }

	/// Parse expression list in str.
	/// return number of errors. the result of last expression is returned in "result".
	int parse(const char *str, double *result=NULL);
};

int mparser::parse(const char* str, double *result)
{
	double res = 0.;
	no_of_errors = 0;
	input = new istringstream(str);
	curr_tok = NAME;		// start with something else than a comment
	while (*input) {		// allow multiple expressions.
		get_token();
		if (no_of_errors > 0) break;
		if (curr_tok == END) break;
		if (curr_tok == PRINT) continue;
		res = expr(false);
	}
	delete input;
	if (no_of_errors > 0) res = 0;
	if (result) *result = res;		// update result
	return no_of_errors;			// return number of errors.
}

Token_value mparser::get_token()
{
	char ch;

	do {	// skip whitespace and comments except '\n'
		if(!input->get(ch)) return curr_tok = END;
	} while (ch!='\n' && ((curr_tok==CMT) || isspace(ch)));
	#ifdef MPARSER_DEBUG
	cout << ch;
	#endif

	switch (ch) {
	case '#':
		return curr_tok=CMT;
	case ';':
	case '\n':
		return curr_tok=PRINT;
	case '*':
	case '/':
	case '+':
	case '-':
	case '(':
	case ')':
	case '=':
	case '^':
		return curr_tok=Token_value(ch);
	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9':
	case '.':
		input->putback(ch);
		*input >> number_value;
		return curr_tok=NUMBER;
	default:			// NAME, NAME=, or error
		if (isalpha(ch) || (ch=='_')) {
			string_value = ch;
			while (input->get(ch) && (isalnum(ch) || (ch=='_')))
				string_value += ch;	// string_value.push_back(ch);
							// to work around library bug
			input->putback(ch);
			if (func.count(string_value)) return curr_tok=FUNC;
			return curr_tok=NAME;
		}
		#ifdef MPARSER_DEBUG
			cout << "bad token\n";
		#endif
		no_of_errors ++;		//	error("bad token");
		return curr_tok=PRINT;
	}
}

double mparser::prim(bool get)		// handle primaries
{
	if (get) get_token();

	switch (curr_tok) {
	case NUMBER:		// floating-point constant
	{	double v = number_value;
		get_token();
		return v;
	}
	case NAME:
	{	string name = string_value;			// save variable name.
		if (get_token() == ASSIGN) {
			double e = expr(true);
			if (no_of_errors==0) var[name] = e;		// assign variable if no error.
		}
		if (!var.count(name)) {
			#ifdef MPARSER_DEBUG
				cout << "undefined variable\n";
			#endif
			no_of_errors++;		// undefined variable
			return 0;
		}
		return var[name];		// return variable value.
	}
	case MINUS:		// unary minus
		return -group(true);
	case FUNC:
	{	fptr f = func[string_value];
		if (get_token() == LP) return f(prim(false));
		#ifdef MPARSER_DEBUG
			cout << "expected parenthesis after function name\n";
		#endif
		no_of_errors ++;
		return 0;
	}
	case LP:
	{	double e = expr(true);
		if (curr_tok != RP) {
			#ifdef MPARSER_DEBUG
				cout << "missing right parenthesis\n";
			#endif
			no_of_errors ++;
			return 1;		// return error(") expected");
		}
		get_token();		// eat ')'
		return e;
	}
	default:
		#ifdef MPARSER_DEBUG
			cout << "primary expected\n";
		#endif
		no_of_errors ++;
		return 1;		// return error("primary expected");
	}
}

double mparser::group(bool get)
{
	double left = prim(get);

	for (;;)
		switch (curr_tok) {
		case POW:
			left = pow(left,prim(true));		// this should have precedence over mul/div
			break;
		default:
			return left;
		}
}

double mparser::term(bool get)		// multiply and divide
{
	double left = group(get);

	for (;;)
		switch (curr_tok) {
		case MUL:
			left *= group(true);
			break;
		case DIV:
			left /= group(true);
			break;
		default:
			return left;
		}
}

double mparser::expr(bool get)		// add and subtract
{
	double left = term(get);

	for (;;)				// ``forever''
		switch (curr_tok) {
		case PLUS:
			left += term(true);
			break;
		case MINUS:
			left -= term(true);
			break;
		case END:
		case PRINT:
		case RP:
		case CMT:
			return left;		// end of expression.
		default:
			#ifdef MPARSER_DEBUG
				cout << "bad expression\n";
			#endif
			no_of_errors++;
			return left;
		}
}


/*
// example program:
int main(int argc, char* argv[])
{

	mparser mp;
	double result;
	int no_of_errors;

	if (argc != 2) {
		cout << "1 argument needed !\n";
		return 1;
	}

	no_of_errors = mp.parse(argv[1], &result);
	cout << "result = " << result << "\n";
	if (no_of_errors > 0)
		cout << "error!\n";
	
	if (mp.var.count("a"))
		cout << "a=" << mp.var["a"] << "\n";

	return no_of_errors;
}
*/
