/*
 * Copyright (c) 2010-2023 Centre National de la Recherche Scientifique.
 * written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France).
 * 
 * nathanael.schaeffer@univ-grenoble-alpes.fr
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * 
 */

/// \file xshells_physics.cpp main program for
/// XSHELLS (eXtendable Spherical Harmonic Earth-Like Liquid Simulator).

#include "xshells_linop.cpp"

/*! \name Matrices */
//@{
typedef LinOp5l LOp5l;
typedef LU5l LOp5l_LU;

LinOp5l MUp;
#ifndef XS_HYPER_DIFF
typedef LinOp3ld LOp3l;
typedef LinOp3ld LOp3l_LU;
#else
typedef LinOp3l LOp3l;
typedef LU3l LOp3l_LU;
#endif
#ifndef XS_O4
LOp3l MBp, MBt, MUt, MT, MC;
typedef LinOp3ld LOpLap5;
LinOp3ld MU_Lr;
#else
typedef LinOp5l LOpLap5;
LinOp5l MU_Lr;
LinOp5l MBp, MBt, MUt, MT, MC;
#endif

double *__restrict Mcor;		///< Special Coriolis Matrix.
double *__restrict Mm_l2;		///< Special Coriolis Matrix.
//@}

#ifdef XS_ETA_VAR
double* etar = NULL;	/// conductivity as a function of r
void calc_eta(double eta0);
#endif

double diffusivity[MAXFIELD];

double get_diffusion_time() {
	double diff_max = 0;
	for (int f=0; f<MAXFIELD; f++) {
		if ((evol_ubt & EVOL(f)) && (diffusivity[f] > diff_max)) diff_max = diffusivity[f];
	}
	return r[NR-1]*r[NR-1]/diff_max;		// shortest diffusion time
}


struct spat_vect {
	double *r, *t, *p;

	void* alloc(size_t nspat_alloc, void* mem);
};

void* spat_vect::alloc(size_t nspat_alloc, void* mem)
{
	r = (double*) mem;
	t = r + nspat_alloc;		p = r + 2*nspat_alloc;
	return (r + 3*nspat_alloc);
}

/*
struct dual_vect : spat_vect {
	cplx *Q, *S, *T;

	void* alloc(size_t nspat_alloc, size_t nspec, void* mem, int nc=3);
};

void* dual_vect::alloc(size_t nspat_alloc, size_t nspec, void* mem, int nc)
{
	mem = spat_vect::alloc(nspat_alloc, mem);
	Q = (cplx*) mem;
	S = Q + nspec;		T = Q + 2*nspec;
	return (Q + nc*nspec);
}
*/

/// compute cross product: (xr,xt,xp) = (vr,vt,vp) X (wr,wt,wp)
/*
void cross_prod(spat_vect& x, const spat_vect& v, const spat_vect& w, size_t nspat0, size_t nspat1)
{
	const double* vr = v.r;		const double* vt = v.t;		const double* vp = v.p;
	const double* wr = w.r;		const double* wt = w.t;		const double* wp = w.p;
	double* xr = x.r;		double* xt = x.t;		double* xp = x.p;
	for (size_t k=nspat0/VSIZE; k<nspat1/VSIZE; k++) {
		rnd vvt = vread(vt,k);		rnd vwt = vread(wt,k);
		rnd vvp = vread(vp,k);		rnd vwp = vread(wp,k);
		rnd rr = vvt*vwp - vvp*vwt;	// AxB
		rnd vvr = vread(vr,k);		rnd vwr = vread(wr,k);
		rnd rt = vvp*vwr - vvr*vwp;
		rnd rp = vvr*vwt - vvt*vwr;
		vstore(xr,k, rr);	vstore(xt,k, rt);	vstore(xp,k, rp);
	}
}
*/
void cross_prod(double* xr, const double* vr, const double* wr, size_t nloop, size_t nspat)
{
	const double* vt = vr + nspat;		const double* vp = vr + 2*nspat;
	const double* wt = wr + nspat;		const double* wp = wr + 2*nspat;
	double* xt = xr + nspat;		double* xp = xr + 2*nspat;
	for (size_t k=0; k<nloop/VSIZE; k++) {
		rnd vvt = vread(vt,k);		rnd vwt = vread(wt,k);
		rnd vvp = vread(vp,k);		rnd vwp = vread(wp,k);
		rnd vvr = vread(vr,k);		rnd vwr = vread(wr,k);
		rnd rr = vvt*vwp - vvp*vwt;	// AxB
		rnd rt = vvp*vwr - vvr*vwp;
		rnd rp = vvr*vwt - vvt*vwr;
		vstore(xr,k, rr);	vstore(xt,k, rt);	vstore(xp,k, rp);
	}
}

/// compute cross product and add: (xr,xt,xp) += (vr,vt,vp) X (wr,wt,wp)
/*
void cross_prod_add(spat_vect& x, spat_vect& v, spat_vect& w, size_t nspat0, size_t nspat1)
{
	const double* vr = v.r;		const double* vt = v.t;		const double* vp = v.p;
	const double* wr = w.r;		const double* wt = w.t;		const double* wp = w.p;
	double* xr = x.r;			double* xt = x.t;			double* xp = x.p;
	for (size_t k=nspat0/VSIZE; k<nspat1/VSIZE; k++) {
		rnd vvt = vread(vt,k);		rnd vwt = vread(wt,k);
		rnd vvp = vread(vp,k);		rnd vwp = vread(wp,k);
		rnd rr = vvt*vwp - vvp*vwt;	// AxB
		rnd vvr = vread(vr,k);		rnd vwr = vread(wr,k);
		rnd rt = vvp*vwr - vvr*vwp;
		rnd rp = vvr*vwt - vvt*vwr;
		vmemadd(xr,k, rr);		vmemadd(xt,k, rt);		vmemadd(xp,k, rp);
	}
}
*/
/// compute cross product and add: (xr,xt,xp) += (vr,vt,vp) X (wr,wt,wp)
void cross_prod_add(double* xr, const double* vr, const double* wr, size_t nloop, size_t nspat)
{
	const double* vt = vr + nspat;		const double* vp = vr + 2*nspat;
	const double* wt = wr + nspat;		const double* wp = wr + 2*nspat;
	double* xt = xr + nspat;			double* xp = xr + 2*nspat;
	for (size_t k=0; k<nloop/VSIZE; k++) {
		rnd vvt = vread(vt,k);		rnd vwt = vread(wt,k);
		rnd vvp = vread(vp,k);		rnd vwp = vread(wp,k);
		rnd rr = vvt*vwp - vvp*vwt;	// AxB
		rnd vvr = vread(vr,k);		rnd vwr = vread(wr,k);
		rnd rt = vvp*vwr - vvr*vwp;
		rnd rp = vvr*vwt - vvt*vwr;
		vmemadd(xr,k, rr);		vmemadd(xt,k, rt);		vmemadd(xp,k, rp);
	}
}

void cross_prod_condadd(spat_vect& x, spat_vect& v, spat_vect& w, size_t nspat0, size_t nspat1, const int add=0)
{
	const double* vr = v.r;		const double* vt = v.t;		const double* vp = v.p;
	const double* wr = w.r;		const double* wt = w.t;		const double* wp = w.p;
	double* xr = x.r;			double* xt = x.t;			double* xp = x.p;
	for (size_t k=nspat0/VSIZE; k<nspat1/VSIZE; k++) {
		rnd vvt = vread(vt,k);		rnd vwt = vread(wt,k);
		rnd vvp = vread(vp,k);		rnd vwp = vread(wp,k);
		rnd rr = vvt*vwp - vvp*vwt;	// AxB
		rnd vvr = vread(vr,k);		rnd vwr = vread(wr,k);
		rnd rt = vvp*vwr - vvr*vwp;
		rnd rp = vvr*vwt - vvt*vwr;
		if (add) {
			vmemadd(xr,k, rr);		vmemadd(xt,k, rt);		vmemadd(xp,k, rp);
		} else {
			vstore(xr,k, rr);	vstore(xt,k, rt);	vstore(xp,k, rp);
		}
	}
}

void scal_vect(spat_vect& x, double* a, spat_vect& g,  size_t nspat0, size_t nspat1)
{
	const double* gr = g.r;		const double* gt = g.t;		const double* gp = g.p;
	double* xr = x.r;			double* xt = x.t;			double* xp = x.p;
	for (size_t k=nspat0/VSIZE; k<nspat1/VSIZE; k++) {
		rnd va = vread(a,k);		rnd grr = vread(gr,k);
		rnd gtt = vread(gt,k);		rnd gpp = vread(gp,k);
		grr *= va;		gtt *= va;		gpp *= va;
		vstore(xr,k, grr);	vstore(xt,k, gtt);	vstore(xp,k, gpp);
	}
}
void scal_vect(double* xr, const double* a, spat_vect& g,  size_t nloop, size_t nspat)
{
	double* xt = xr + nspat;		double* xp = xr + 2*nspat;
	const double* grr = g.r;			const double* gtt = g.t;			const double* gpp = g.p;
	for (size_t k=0; k<nloop/VSIZE; k++) {
		rnd va = vread(a,k);		rnd gr = vread(grr,k);
		rnd gt = vread(gtt,k);		rnd gp = vread(gpp,k);
		gr *= va;		gt *= va;		gp *= va;
		vstore(xr,k, gr);	vstore(xt,k, gt);	vstore(xp,k, gp);
	}
}

void scal_vect_add(spat_vect& x, double* a, spat_vect& g,  size_t nspat0, size_t nspat1)
{
	const double* gr = g.r;		const double* gt = g.t;		const double* gp = g.p;
	double* xr = x.r;			double* xt = x.t;			double* xp = x.p;
	for (size_t k=nspat0/VSIZE; k<nspat1/VSIZE; k++) {
		rnd va = vread(a,k);		rnd grr = vread(gr,k);
		rnd gtt = vread(gt,k);		rnd gpp = vread(gp,k);
		grr *= va;		gtt *= va;		gpp *= va;
		vmemadd(xr,k, grr);	vmemadd(xt,k, gtt);	vmemadd(xp,k, gpp);
	}
}
void scal_vect_add(double* xr, const double* a, spat_vect& g,  size_t nloop, size_t nspat)
{
	double* xt = xr + nspat;		double* xp = xr + 2*nspat;
	const double* grr = g.r;			const double* gtt = g.t;			const double* gpp = g.p;
	for (size_t k=0; k<nloop/VSIZE; k++) {
		rnd va = vread(a,k);		rnd gr = vread(grr,k);
		rnd gt = vread(gtt,k);		rnd gp = vread(gpp,k);
		gr *= va;		gt *= va;		gp *= va;
		vmemadd(xr,k, gr);		vmemadd(xt,k, gt);		vmemadd(xp,k, gp);
	}
}

#ifdef XS_MEAN_FIELD
/// alpha is an axisymmetric scalar field
void alpha_effect_add(spat_vect& x, const double* ar, const spat_vect& b, const int add=0)
{
	double* xr = x.r;	double* xt = x.t;	double* xp = x.p;
	const double* br = b.r;		const double* bt = b.t;		const double* bp = b.p;
	const int vnlat = NLAT/VSIZE;
	const int vnlat_pad = NLAT_PADDED/VSIZE;
	for (int ip=0; ip<NPHI; ip++) {
		#pragma omp simd
		for (int it=0; it<vnlat; it++) {
			rnd ark = vread(ar, it);
			int k = ip*vnlat_pad + it;
			rnd brr = vread(br, k);		rnd btt = vread(bt, k);		rnd bpp = vread(bp, k);
			if (add) {
				vmemadd(xr,k, ark*brr);		vmemadd(xt,k, ark*btt);		vmemadd(xp,k, ark*bpp);
			} else {
				vstore(xr,k, ark*brr);		vstore(xt,k, ark*btt);		vstore(xp,k, ark*bpp);
			}
		}
	}
}

/// alpha is an axisymmetric scalar field
void alpha_effect_add(double* xr, const double* ar, const double* br, size_t spat_dist)
{
	double* xt = xr + spat_dist;	double* xp = xr + 2*spat_dist;
	const double* bt = br + spat_dist;		const double* bp = br + 2*spat_dist;

	int it = 0;
	const int vnlat = NLAT / VSIZE;
	const int vnlat_pad = NLAT_PADDED/VSIZE;
	for (int ip=0; ip<NPHI; ip++) {
		#pragma omp simd
		for (int it=0; it<vnlat; it++) {
			rnd ark = vread(ar, it);
			int k = ip*vnlat_pad + it;
			rnd brr = vread(br, k);		rnd btt = vread(bt, k);		rnd bpp = vread(bp, k);
			vmemadd(xr,k, ark*brr);		vmemadd(xt,k, ark*btt);		vmemadd(xp,k, ark*bpp);
		}
	}
}
#endif

	/* MATRIX INITIALIZATION */

#ifdef XS_HYPER_DIFF
/// hyper_diff_l0 controls the start of hyper-diff in l-spectrum.
int calc_hyper_diff(double* diff_el, double factor, double hyper_diff_l0)
{
	int l0 = LMAX+1;
	for (int l=0; l<=LMAX; l++) diff_el[l] = 1.0;
	if ((hyper_diff_l0 > 0.0) && (factor > 1.0)) {
		l0 = LMAX - hyper_diff_l0;
		if (hyper_diff_l0 < 1)  l0 = hyper_diff_l0 * (LMAX+1);
		double q = pow(factor, 1.0/(LMAX-l0));
		for (int l=l0+1; l<=LMAX; l++) diff_el[l] = diff_el[l-1] * q;
	}
	return l0;
}
#endif

/// Initialize semi-implicit part of Induction equation.
///      matrix   MB = (1/dt + 0.5*eta*Lap)
/// and  matrix MB{p,t}_1 = (1/dt - 0.5*eta*Lap)^(-1)
/// Same matrix MB may be used for Poloidal and Toroidal components. Reverse is MBp_1 and MBt_1 (Polidal up to NR-1, Toroidal up to NR-2)
void init_Bmatrix(PolTor &Blm, double eta, double hyper_diff_l0 = 0, double h_eta = 1.0, const double cx = 1.0)
{
	if (eta <= 0.) runerr("diffusivity should be strictly positive.");

#ifdef XS_ETA_VAR
	const int irs = Blm.ir_bci;		// magnetic field limits.
	const int ire = Blm.ir_bco;

	#ifdef XS_HYPER_DIFF
	if ((h_eta != 0.0) && (h_eta != 1.0)) runerr("variable magnetic diffusivity is not compatible with magnetic hyper-diffusivity.");
	#endif
	if (etar == NULL) {
		etar = (double*) VMALLOC(NR * sizeof(double));
		if (etar==0) runerr("[init_Bmatrix] allocation error");
		for (int i=0; i<NR; i++) etar[i] = eta;		// constant conductivity as default value.
		#ifdef XS_ETA_PROFILE
			calc_eta(eta);		// profile defined in xshells.hpp
			double eta_min = eta;	double eta_max = eta;
			for (int i=irs; i<=ire; i++) {
				if (etar[i] < eta_min) eta_min = etar[i];
				if (etar[i] > eta_max) eta_max = etar[i];
			}
			PRINTF0("=> Variable conductivity profile eta(r) : %lg <= eta <= %lg\n", eta_min, eta_max);
		#endif
		if (i_mpi==0) write_vect("etar", etar, NR, "eta(r) : magnetic diffusivity as a function of r");
	}
	for (int i=NG; i<=NM; i+=(NM-NG)) {		// look for discontinuities at the interfaces.
		if ((i-2 >= irs) && (i+2 <= ire)) {
			if ( (etar[i-1] != etar[i+1]) && (etar[i-2] == etar[i-1]) && (etar[i+1] == etar[i+2]) ) {
				#ifdef XS_DEBUG
					PRINTF0(" + discontinuity found at r=%f (ir=%d)\n", r[i], i);
				#endif
				etar[i] = 2.*etar[i-1]*etar[i+1]/(etar[i-1] + etar[i+1]);	// ETA is the harmonic mean (for poloidal)
			}
		}
	}
	#define ETA etar[i]
#else
	#define ETA eta
#endif

/* POLOIDAL MATRIX */
/// Boundary conditions
///	r[ire] : T=0, dP/dr= -(l+1)/r P  (outer insulator) => only P is computed.
///	r[irs]==0 : T=0, P=0  => not computed at i=0.
/// r[irs]!=0 : T=0, dP/dr= l/r P  (inner insulator)
	int i0 = Blm.ir_bci;
	int i1 = Blm.ir_bco;
	if ((r[i0] == 0.0) || (Blm.bci != BC_MAGNETIC)) i0 += 1;
	if (Blm.bco != BC_MAGNETIC) i1 -= 1;
	MBp.alloc(i0, i1, LMAX);

	for(int i=i0; i<=i1; i++) {
		if ((i==Blm.ir_bci) || (i==Blm.ir_bco)) {
			double r_2 = 1.0/(r[i]*r[i]);
			if (i==Blm.ir_bci) {	// poloidal BC : dP/dr = l/r P  - (2l+1)/r Pin => allows to aproximate d2P/dr2 with only 2 points. (Pin is imposed from inside)
				MBp.set_Laplace_bc(i, 0.0, -r[i], cx*ETA);		// ghost point : imposed field Pin * (2l+1).
				double f = (cx*ETA)*2*(r_2 - 1.0/(r[i]*(r[i+1]-r[i])));		// only diagonal depends on l
				for (int l=0; l<=LMAX; l++)	MBp.add_di(i,l, l*f );
			} else {	// poloidal BC : dP/dr = -(l+1)/r P  + (2l+1)/r Pout => allows to aproximate d2P/dr2 with only 2 points (Pout is imposed from outside)
				MBp.set_Laplace_bc(i, 0.0, r[i], cx*ETA);		// ghost point = imposed field Pout * (2l+1).
				double f = (cx*ETA)*2*(r_2 - 1.0/(r[i]*(r[i-1]-r[i])));		// only diagonal depends on l
				for (int l=0; l<=LMAX; l++)	MBp.add_di(i,l, -(l+1)*f );
			}
		} else
			MBp.set_Laplace(i, cx*ETA);
	}

/* TOROIDAL MATRIX */
	MBt.alloc(Blm.ir_bci+1, Blm.ir_bco-1, LMAX);
	for(int i=Blm.ir_bci+1; i<Blm.ir_bco; i++) {
		MBt.set_Laplace(i, cx*ETA);
	}

#ifdef XS_ETA_VAR
	for(int i=irs+1; i<ire; i++) {
		OpCurlLapl& W = CURL_LAPL(i);		OpGrad& G = GRAD(i);
		double deta = G.Gl*etar[i-1] + G.Gd*etar[i] + G.Gu*etar[i+1];
									MBt.add_lo(i,   cx*deta*W.Wl );
		for (int l=0; l<=LMAX; l++) MBt.add_di(i,l, cx*deta*W.Wd );
									MBt.add_up(i,   cx*deta*W.Wu );
	}
	for (int i=NG; i<=NM; i+=(NM-NG)) {		// look for discontinuities at the interfaces.
		if ((i-2 >= irs) && (i+2 <= ire)) {
			if ( (etar[i-1] != etar[i+1]) && (etar[i-2] == etar[i-1]) && (etar[i+1] == etar[i+2]) ) {
				double eta_m = 0.5*(etar[i-1] + etar[i+1]);		// mean
				double dx_2 = r[i+1]-r[i-1];		dx_2 = 4.0/(dx_2 * dx_2);
				double r_1 = 1.0/r[i];		double r_2 = 1.0/(r[i]*r[i]);
											MBt.set_lo(i,    cx*etar[i-1]*(r[i-1]*r_1)*dx_2 );
				for (int l=0; l<=LMAX; l++)	MBt.set_di(i,l, -cx*eta_m*(dx_2+dx_2 + r_2*(l*(l+1))) );
											MBt.set_up(i,    cx*etar[i+1]*(r[i+1]*r_1)*dx_2 );

				MBt.copy_r(i+1, MBp);		// deta is zero before and after the interface
				MBt.copy_r(i-1, MBp);
			}
		}
	}
#endif
#undef ETA

	#ifdef XS_HYPER_DIFF
		/// magnetic diff as a function of l (for hyperdiffusivity).
		double* eta_el = (double*) malloc((LMAX+1) * sizeof(double));
		int l0 = calc_hyper_diff(eta_el, h_eta, hyper_diff_l0);
		if (l0 < LMAX) {
			PRINTF0("=> Hyper-magnetic-diff: starting at l=%d, max diff factor=%lg\n", l0, eta_el[LMAX]);
			#ifdef XS_DEBUG
				if (i_mpi==0) write_vect("eta_el", eta_el, LMAX+1, "eta(l) : magnetic diff factor as a function of l");
			#endif
			for(int i=i0; i<=Blm.ir_bco; i++)				MBp.scale(i, eta_el);
			for(int i=Blm.ir_bci+1; i<=Blm.ir_bco-1; i++)	MBt.scale(i, eta_el);
		}
		free(eta_el);
	#endif
}

void init_Coriolis_matrix()
{
	// SH matrices required for Coriolis force.
	Mcor = (double*) malloc(sizeof(double)*5*NLM);
	if (Mcor==0) runerr("[init_Coriolis_matrix] allocation error 1");
	Mm_l2 = Mcor + 4*NLM;
	mul_ct_matrix(shtns, Mcor + 2*NLM);
	if (Mcor[4*NLM-1] != 0.0) runerr("ERROR: mul_ct_matrix returns wrong result. Consider using shtns 2.8.1+");
	for (long i=0; i<NLM; i++) {
		long l = shtns->li[i];
		Mcor[4*i+2] = l_2[l]*Mcor[2*(i+NLM)]   * (1-l*l);		// for dP/dr
		Mcor[4*i+3] = l_2[l]*Mcor[2*(i+NLM)+1] * (-l*(l+2));
		Mcor[4*i]   = Mcor[4*i+2] * (1-l);		// for P/r
		Mcor[4*i+1] = Mcor[4*i+3] * (l+2);
	}
	for (long m=0, i=0; m<=MMAX*MRES; m+=MRES) {
		for (long l=m; l<=LMAX; l++, i++) {
			Mm_l2[i] = l_2[l] * m;		// m/(l*(l+1))
		}
	}
}


/// Initialize semi-implicit part of Velocity equation.
///      matrix   MUt = (1/dt + 0.5*nu*Lap)	  	MUp = -(1/dt + 0.5*nu*Lap)*Lap
/// and  matrix MUt_1 = (1/dt - 0.5*nu*Lap)^(-1)	MUp_1 = -((1/dt - 0.5*nu*Lap)*Lap)^(-1)
/// BC : boundary condition, can be BC_NO_SLIP or BC_FREE_SLIP
///	\param  I_ic : momoent of inertia of inner core, relative to solid sphere of radius r[NG]; 0 = fixed/imposed rotation of inner core (default).
///	\param  I_m  : momoent of inertia of mantle, relative to solid sphere of radius r[NM]; 0 = fixed/imposed rotation of mantle (default).
/// \todo FIXME : bc for r[NG] = 0 (no inner-core) !!!
void init_Umatrix(PolTor &Ulm, double nu, double hyper_diff_l0 = 0, double h_nu = 1.0, const double I_ic = 0, const double I_m = 0)
{
	const double cx = 1.0;

	init_Coriolis_matrix();

	if (nu <= 0.) runerr("viscosity should be strictly positive.");

/// r=0 constraint :
///	T=0, P=0  => not computed at i=0.
///	for l=1 : d2/dr2 P = 0
///	for l>1 : dP/dr = 0
/// Boundary conditions (NO SLIP)
///	P=0, T=0, dP/dr = 0  => not computed at boundary
///	with DeltaOmega (couette), at inner core : T(l=1,m=0) = r.DeltaOmega*Y10_ct
/// Boundary conditions (FREE SLIP)
///	P=0, d2/dr2 P = 0, d(T/r)/dr = 0  => only T computed at boundary

/// POLOIDAL
/// (d/dt.Lap - NU.Lap.Lap) Up = Toroidal(curl_NL)

	MU_Lr.alloc(NG, NM, LMAX);		// allocate the full range of r to avoid array overflows with free-slip.
	MUp.alloc(NG+1, NM-1, LMAX);
#ifdef XS_O4
	for (int i=NG+1; i<=NM-1; i++) {
		double d1r[5], d2r[5], d3r[5], d4r[5];
		if (i==NG+1) {
			fd_deriv_o4_bc(r, i, i-1, (Ulm.bci > BC_NO_SLIP) ? 2 : 1, d1r, d2r, d3r, d4r);
		} else
		if (i==NM-1) {
			fd_deriv_o4_bc(r, i, i+1, (Ulm.bco > BC_NO_SLIP) ? 2 : 1, d1r, d2r, d3r, d4r);
		} else {
			fd_deriv_o4(r, i, d1r, d2r, d3r, d4r);
		}
		MU_Lr.set_Laplace4(i, d1r, d2r);
		MUp.set_BiLaplace4(i, d1r, d2r, d3r, d4r, 0.0, cx*nu);
	}
#else
	for (int i=NG+1; i<=NM-1; i++) {
		const double* L0 = &CURL_LAPL(i-1).Ll;
		const double* L1 = &CURL_LAPL(i).Ll;
		const double* L2 = &CURL_LAPL(i+1).Ll;
		double Lbc[4];
		if (r[i-1] > 0.0  && (i==NG+1  ||  i==NM-1)) {		// boundary condition, express Laplace operator Lbc at boundary:
			const int ib = (i==NG+1) ? -1 : 1;
			double dx = r[i]-r[i+ib];
			double ri_1 = 1.0/r[i+ib];
			double dx_1 = 1.0/dx;
			const int bc = (ib == -1) ? Ulm.bci : Ulm.bco;
			if (bc == BC_ZERO) {	// zero velocity at boundary
				for (int k=0; k<4; k++) Lbc[k] = 0.0;
				Lbc[1-ib] = 2.*dx_1*dx_1;
			} else if (bc <= BC_NO_SLIP) {		// no-slip
				Lbc[1] = -2.*dx_1*dx_1;
				Lbc[1-ib] = -Lbc[1];
				Lbc[1+ib] = 2.0*ri_1 - 2.0*dx_1;		//	dP/dr stored in ghost.
				Lbc[3] = ri_1*ri_1;					// factor multiplied by -l(l+1), on the diagonal
			} else {	// stress-free
				Lbc[1-ib] = 2.*ri_1*dx_1;
				Lbc[1] = 2.*ri_1*(-dx_1 + ri_1*( 1. - dx*ri_1 ));
				Lbc[1+ib] = 0.0;
				Lbc[3] = ri_1*ri_1*( 2. - dx*ri_1 );		// factor multiplied by -l(l+1), on the diagonal
			}
			if (ib == -1)		L0 = Lbc;
			else if (ib == 1) 	L2 = Lbc;
		}
		MU_Lr.set_Laplace(i);
		MUp.set_BiLaplace2(i, L0, L1, L2, cx*nu);
	}
#endif

/// TOROIDAL
/// (d/dt - NU.Lap) Ut = Poloidal(curl_NL)
	int irs=NG+1;
	int ire=NM-1;
	if ( (Ulm.bci > BC_NO_SLIP  ||  I_ic!=0) && (r[NG] != 0.0) ) irs--;
	if (Ulm.bco > BC_NO_SLIP || I_m!=0) ire++;
	MUt.alloc(irs, ire, LMAX);

/*#ifdef XS_O4
	for(int i=irs; i<=ire; i++) {
		double d1r[5], d2r[5], d3r[5], d4r[5];
		if ((i==NG) || (i==NM)) {
			MUt.set_Laplace_bc(i,  -1.0/r[i], 1.0,  cx*nu);		// BC toroidal (free-slip) : d(T/r)/dr = 0 =>  dT/dr = T/r
		} else {
			fd_deriv_o4(r, i, d1r, d2r, d3r, d4r);
			MUt.set_Laplace4(i, d1r, d2r, cx*nu);
		}
	}
#else*/
	for(int i=irs; i<=ire; i++) {
		if ((i==NG) || (i==NM)) {
			MUt.set_Laplace_bc(i,  -1.0/r[i], 1.0,  cx*nu);		// BC toroidal (free-slip) : d(T/r)/dr = 0 =>  dT/dr = T/r
		} else {
			MUt.set_Laplace(i, cx*nu);
		}
	}
//#endif

	if ((I_ic !=0. && r[NG] > 0.) ||  I_m != 0.) {		// there is at least one solid shell dynamically evolving
		const int li_lm10 = 0;	// l=1, m=0 mapped to l=0
		MUt.copy_index(0, 1);	// copy matrix for l=1 to l=0
		unsigned short* li_map = (unsigned short*) malloc((NLM+1)*sizeof(unsigned short));
		memcpy(li_map, li, NLM * sizeof(unsigned short));
		li_map[NLM] = li_map[NLM-1];	// because we allow some overflow
		li_map[1] = 0;		// l=1,m=0 is mapped to l=0 (which is otherwise unused for toroidal)
		MUt.set_custom_mapping(li_map);

		// equation is :  I.dOmega/dt = Gamma_nu + Gamma_B
		//				  I/r . dT10/dt =  Gamma_nu + Gamma_B
		//				dT10/dt = r/I . (Gamma_nu + Gamma_B)
		
		// Gamma_nu =   nu * (r*dT/dr - T) * r2 * 8*pi/3 / Y10_ct
		// r.Gamma_nu/I = nu * (dT/dr - T/r) * r^(-1) * 5 / Y10_ct

		if (I_ic != 0.  &&  r[NG] > 0.) {		// Coupling with freely-rotating inner-core: replace Laplace(T) at ICB with equation of motion (l=1 only)
			MUt.zero(NG);	// clear previous boundary equation
			const double factor = cx*nu * 5.0 / (r[NG] * I_ic);
			const double dr = r[NG+1]-r[NG];
			MUt.set_di(NG,li_lm10, factor * (-1.0/dr - 1.0/r[NG]));
			MUt.set_up(NG, factor * 1.0/dr);
			MUt.flags |= LINOP_BCI_L0_ONLY;
			PRINTF0("=> freely rotating inner-core with moment of inertia %g * 8*pi/15 * ri^5 (with ri=%g)\n", I_ic, r[NG]);
		}
		if (I_m != 0) {		// Coupling with freely-rotating mantle: replace Laplace(T) at CMB with equation of motion (l=1 only)
			MUt.zero(NM);	// clear previous boundary equation
			const double factor = - cx*nu * 5.0 / (r[NM] * I_m);
			const double dr = r[NM]-r[NM-1];
			MUt.set_di(NM,li_lm10, factor * (1.0/dr - 1.0/r[NM]));
			MUt.set_lo(NM, factor * -1.0/dr);
			MUt.flags |= LINOP_BCO_L0_ONLY;
			PRINTF0("=> freely rotating mantle with moment of inertia %g * 8*pi/15 * ro^5 (with ro=%g)\n", I_m, r[NM]);
		}
	}

	#ifdef XS_HYPER_DIFF
		double* nu_el = (double*) malloc((LMAX+1) * sizeof(double));
		int l0 = calc_hyper_diff(nu_el, h_nu, hyper_diff_l0);
		nu_el[0] = nu_el[1];		// ensure the trick to put the l=1,m=0 into l=0 matrix is safe with respect to viscosity
		if (l0 < LMAX) {
			PRINTF0("=> Hyper-viscosity: starting at l=%d, max viscosity factor=%lg\n", l0, nu_el[LMAX]);
			if (i_mpi==0) write_vect("nu_el", nu_el, LMAX+1, "nu(l) : viscosity factor as a function of l");
			for (int i=irs; i<=ire; i++)	MUt.scale(i, nu_el);
			for (int i=NG+1; i<=NM-1; i++)	MUp.scale(i, nu_el);
		}
		free(nu_el);
	#endif
}

void init_Tmatrix(ScalarSH &Tlm, LOp3l &M, double kappa, double hyper_diff_l0 = 0, double h_kappa = 1.0,
					double biot_bci = 0.0, double biot_bco = 0.0)
{
/// Boundary conditions
///	r[ire] : FIXED_TEMP: T=0, FIXED_FLUX: dT/dr = 0.
///	r[irs]==0 : T=0 (for l>0) => not computed at i=0.
/// r[irs]!=0 : FIXED_TEMP: T=0, FIXED_FLUX: dT/dr = 0.
		// central condition : T=0 (l>0); dT/dr = 0 (even for l=1)
		// should be T=0 (don't compute) but for l=0... => requires 2 separate matrices ?
		// at r=0, the temperature equation reduces to dT/dt = S + k.Lap(T)
		// if you use a developement around the conductive profile, we have dT/dt = 0

	int irs = Tlm.ir_bci+1;		// temperature field limits.
	int ire = Tlm.ir_bco-1;
	if ((r[Tlm.ir_bci] == 0.0) || (Tlm.bci != BC_FIXED_TEMPERATURE)) irs = Tlm.ir_bci;
	if (Tlm.bco != BC_FIXED_TEMPERATURE) ire = Tlm.ir_bco;
	if (kappa <= 0.) runerr("diffusivity should be strictly positive.");

	M.alloc(irs, ire, LMAX);

	// Biot number links flux to temperature (non-dimensional exchange coefficient). Zero means fixed flux.
	if (Tlm.bci == BC_ROBIN) {
		PRINTF0("=> 'Robin' inner boundry condition, Biot number = %g\n", biot_bci);
	} else biot_bci = 0;
	if (Tlm.bco == BC_ROBIN) {
		PRINTF0("=> 'Robin' outer boundry condition, Biot number = %g\n", biot_bco);
	} else biot_bco = 0;

	for(int i=irs; i<=ire; i++) {
		if (i==Tlm.ir_bci) {
			M.set_Laplace_bc(i,  biot_bci, 1.0,  kappa);		// ghost point = imposed flux dT/dr (may be zero).
		} else
		if (i==Tlm.ir_bco) {
			M.set_Laplace_bc(i,  biot_bco, 1.0,  kappa);		// ghost point = imposed flux dT/dr (may be zero).
		} else {
			M.set_Laplace(i, kappa);
		}
	}

	#ifdef XS_HYPER_DIFF
		/// thermal diff as a function of l (for hyperdiffusivity).
		double* kappa_el = (double*) malloc((LMAX+1) * sizeof(double));
		int l0 = calc_hyper_diff(kappa_el, h_kappa, hyper_diff_l0);
		if (l0 < LMAX) {
			PRINTF0("=> Hyper-thermal-diff: starting at l=%d, max diff factor=%lg\n", l0, kappa_el[LMAX]);
			#ifdef XS_DEBUG
				if (i_mpi==0) write_vect("kappa_el", kappa_el, LMAX+1, "kappa(l) : thermal diff factor as a function of l");
			#endif
			for(int i=irs; i<=ire; i++)		M.scale(i, kappa_el);
		}
		free(kappa_el);
	#endif
}

	/* Special Terms */
/*
void axial_rot_advection(int ir, double Omega_z, cplx* Alm, cplx* NLlm)
{
	int lm=0; 	do {
		NLlm[lm] = 0.0;
	} while(++lm <= LMAX);
	for (int im=1; im<=MMAX; im++) {
		double mOm = -Omega_z * im*MRES;
		for (int l=im*MRES; l<=LMAX; l++) {
			NLlm[lm] = cplx(0,mOm)* Alm[lm];
			lm++;
		}
	}
}
*/
/// As the induction of an axial rotation does not require SH transforms,
/// we compute it lately.
void Axial_rot_induction(PolTor &NL, SolidBody& sb, const PolTor &Blm, StatSpecVect *B0lm = NULL)
{
	double Omega_z;
	v2d *QST;
	unsigned short *mis;
	unsigned *lms;
	int nj = 0;
	int irs = sb.irs;
	int ire = sb.ire;
	if (irs > ire) return;		// no solid body (boundary only).

	Omega_z = sb.Omega_z;
	if ((Omega_z == 0.0) || (sb.Omega_x != 0.0) || (sb.Omega_y != 0.0)) return;	// zero or already computed.	

	if ((B0lm) && (B0lm->mmax > 0)) {		// we must advect the base field.
		nj = B0lm->nlm;
		QST = (v2d*) B0lm->QST;
		mis = B0lm->mi;
		lms = B0lm->lm;
	}

	if (irs < NL.irs) irs = NL.irs;
	if (ire > NL.ire) ire = NL.ire;

	#if XS_OMP != 2
	#pragma omp for schedule(static) nowait
	for (int ir=irs; ir<=ire; ir++) {
		v2d* Bpr = (v2d*) Blm.Pol[ir];		v2d* NLpr = (v2d*) NL.Pol[ir];
		v2d* Btr = (v2d*) Blm.Tor[ir];		v2d* NLtr = (v2d*) NL.Tor[ir];
		int lm=0; 	do {
			NLpr[lm] = vdup(0.0);		NLtr[lm] = vdup(0.0);		// m=0
		} while(++lm <= LMAX);
		for (int im=1; im<=MMAX; im++) {
			double mOm = -Omega_z * im*MRES;
			for (int l=im*MRES; l<=LMAX; l++) {
				NLpr[lm] = IxKxZ(mOm, Bpr[lm]);
				NLtr[lm] = IxKxZ(mOm, Btr[lm]);
				lm++;
			}
		}
		for (int j=0; j<nj; j++) {
			double mOm = -Omega_z * mis[j];
			int lm = lms[j];
			NLpr[lm] += IxKxZ(mOm, QST[(ir*nj +j)*3] * r[ir]*l_2[lm] );
			NLtr[lm] += IxKxZ(mOm, QST[(ir*nj +j)*3 + 2] );
		}
	}
	#else
	 // for xsbig_hyb2, XS_OMP == 2
	int lm0 = 0;		int lm1 = NLM-1;
	thread_interval_lm(lm0, lm1);
	for (int ir=irs; ir<=ire; ir++) {
		v2d* Bpr = (v2d*) Blm.Pol[ir];		v2d* NLpr = (v2d*) NL.Pol[ir];
		v2d* Btr = (v2d*) Blm.Tor[ir];		v2d* NLtr = (v2d*) NL.Tor[ir];
		for (int lm = lm0; lm<=lm1; lm++) {
			double mOm = -Omega_z * em[lm];
			NLpr[lm] = IxKxZ(mOm, Bpr[lm]);
			NLtr[lm] = IxKxZ(mOm, Btr[lm]);
		}
		for (int j=0; j<nj; j++) {
			const int lm = lms[j];
			if IN_RANGE_INCLUSIVE(lm, lm0, lm1) {
				double mOm = -Omega_z * mis[j];
				NLpr[lm] += IxKxZ(mOm, QST[(ir*nj +j)*3] * r[ir]*l_2[lm] );
				NLtr[lm] += IxKxZ(mOm, QST[(ir*nj +j)*3 + 2] );
			}
		}
	}
	#endif
}

/// As the advection by an axial rotation does not require SH transforms,
/// we compute it lately.
void Axial_rot_advection(ScalarSH& NL, SolidBody& sb, const ScalarSH& Tlm, StatSpecScal *S0lm = NULL)
{
	double Omega_z;
	v2d *S;
	unsigned short *mis;
	unsigned *lms;
	int nj = 0;
	int irs = sb.irs;
	int ire = sb.ire;
	if (irs > ire) return;		// no solid body (boundary only).

	Omega_z = sb.Omega_z;
	if ((Omega_z == 0.0) || (sb.Omega_x != 0.0) || (sb.Omega_y != 0.0)) return;	// zero or already computed.

	if ((S0lm) && (S0lm->mmax > 0)) {		// we must advect the base field.
		nj = S0lm->nlm;
		S = (v2d*) S0lm->TdT;
		mis = S0lm->mi;
		lms = S0lm->lm;
	}

	if (irs < NL.irs) irs = NL.irs;
	if (ire > NL.ire) ire = NL.ire;

	#if XS_OMP != 2
	#pragma omp for schedule(static) nowait
	for (int ir=irs; ir<=ire; ir++) {
		v2d* Tr = (v2d*) Tlm[ir];		v2d* NLr = (v2d*) NL[ir];
		int lm=0; 	do {
			NLr[lm] = vdup(0.0);		// m=0
		} while(++lm <= LMAX);
		for (int im=1; im<=MMAX; im++) {
			double mOm = -Omega_z * im*MRES;
			for (int l=im*MRES; l<=LMAX; l++) {
				NLr[lm] = IxKxZ(mOm, Tr[lm]);
				lm++;
			}
		}
		for (int j=0; j<nj; j++) {
			double mOm = -Omega_z * mis[j];
			int lm = lms[j];
			NLr[lm] += IxKxZ(mOm, S[(ir*nj +j)*2]);
		}
	}
	#else
	 // for xsbig_hyb2, XS_OMP == 2
	int lm0 = 0;		int lm1 = NLM;
	thread_interval_lm(lm0, lm1);
	for (int ir=irs; ir<=ire; ir++) {
		v2d* Tr = (v2d*) Tlm[ir];		v2d* NLr = (v2d*) NL[ir];
		for (int lm = lm0; lm<=lm1; lm++) {
			double mOm = -Omega_z * em[lm];
			NLr[lm] = IxKxZ(mOm, Tr[lm]);
		}
		for (int j=0; j<nj; j++) {
			const int lm = lms[j];
			if IN_RANGE_INCLUSIVE(lm, lm0, lm1) {
				double mOm = -Omega_z * mis[j];
				NLr[lm] += IxKxZ(mOm, S[(ir*nj +j)*2]);
			}
		}
	}
	#endif
}


/// As the buoyancy from a radial gravity field does not require SH transforms,
/// we compute it lately.
void Radial_gravity_buoyancy(PolTor &NL, const ScalarSH &Tlm, const double* Grav0_r, const double factor = 1.0)
{
	int irs = Tlm.irs;
	int ire = Tlm.ire;
	int lm0 = 0;
	int lm1 = NLM-1;

	thread_interval_lm(lm0, lm1);
	if (lm0 == 0) lm0=1;					// no l=0 contribution.

	if (irs < NL.irs) irs = NL.irs;
	if (ire > NL.ire) ire = NL.ire;
	thread_interval_rad(irs, ire);

	for (int ir=irs; ir<=ire; ir++) {
		v2d* Tr = (v2d*) Tlm[ir];		v2d* NLp = (v2d*) NL.Tor[ir];		// pol & tor are exchanged !
		s2d galpha = vdup(factor * Grav0_r[ir]);		// here we assume only l=0 is present.
		LM_LOOP2( lm0, lm1,  NLp[lm] += galpha * Tr[lm];  )			// no l=0 contribution.
		// TODO: add T0lm
	}
	#pragma omp barrier
}

/// As the buoyancy from a radial gravity field does not require SH transforms,
/// we compute it lately.
void Radial_gravity_buoyancy(PolTor &NL, const ScalarSH &Tlm, const ScalarSH &Clm, const double* Grav0_r)
{
	int irs = Tlm.irs;
	int ire = Tlm.ire;
	int lm0 = 0;
	int lm1 = NLM-1;

	thread_interval_lm(lm0, lm1);
	if (lm0 == 0) lm0=1;					// no l=0 contribution.

	if (irs < NL.irs) irs = NL.irs;
	if (ire > NL.ire) ire = NL.ire;
	thread_interval_rad(irs, ire);

	if (evol_ubt & EVOL_C) {
		for (int ir=irs; ir<=ire; ir++) {
			v2d* Tr = (v2d*) Tlm[ir];		v2d* NLp = (v2d*) NL.Tor[ir];		// pol & tor are exchanged !
			s2d galpha = vdup(Grav0_r[ir]);		// here we assume only l=0 is present.
			v2d* Cr = (v2d*) Clm[ir];
			LM_LOOP2( lm0, lm1,  NLp[lm] += galpha * (Tr[lm] + Cr[lm]);  )			// no l=0 contribution.
			// TODO: add T0lm, C0lm
		}
	} else {
		for (int ir=irs; ir<=ire; ir++) {
			v2d* Tr = (v2d*) Tlm[ir];		v2d* NLp = (v2d*) NL.Tor[ir];		// pol & tor are exchanged !
			s2d galpha = vdup(Grav0_r[ir]);		// here we assume only l=0 is present.
			LM_LOOP2( lm0, lm1,  NLp[lm] += galpha * Tr[lm];  )			// no l=0 contribution.
			// TODO: add T0lm
		}
	}
	#pragma omp barrier
}

double angular_momentum_local_sum(PolTor &Ulm, int irs, int ire, double* Lxyz)
{
	const int lm10 = 1;		// LiM(shtns,1,0);
	const int lm11 = LMAX + 1;		//LiM(shtns, 1,1);
	const bool m1ok = ((MMAX>0)&&(MRES==1));
	cplx Wxy = 0;
	double Wz = 0;
	double n = 0;
	/*if (irs == Ulm.ir_bci  &&  InnerCore.freely_rotate) {	// angular momentum of inner-core
		double inertia_moment = InnerCore.IM * (3./(8.*M_PI));
		Wz += inertia_moment * real(Ulm.Tor[irs][lm10]) / r[irs];
		n += inertia_moment;
	}*/
	for (int ir=irs; ir<=ire; ir++) {
		double dV = r[ir]*r[ir]*r[ir] * Ulm.delta_r(ir);
		n += r[ir] * dV;
		Wz += dV * real(Ulm.Tor[ir][lm10]);
		if (m1ok)	Wxy += dV * Ulm.Tor[ir][lm11];
	}
	/*if (ire == Ulm.ir_bco  &&  Mantle.freely_rotate) {		// angular momentum of mantle
		double inertia_moment = Mantle.IM * (3./(8.*M_PI));
		Wz += inertia_moment * real(Ulm.Tor[ire][lm10]) / r[ire];
		n += inertia_moment;
	}*/
	Lxyz[0] = real(Wxy);
	Lxyz[1] = imag(Wxy);
	Lxyz[2] = Wz;
	return n;
}

/// If we have free-slip boundaries everywhere, we must ensure angular momentum conservation !
/// So we simply remove the angular momentum.
/// With MPI, the MPI_Allreduce call is an implicit synchronization point, so that MPI_Barrier is not needed afterwards.
void conserve_momentum(PolTor& Ulm, int kill_sbr)
{
	static int subtract = 0;
	static cplx Wxy0 = 0;
	static double Wz0 = 0;
	static double n;
	const int lm10 = 1;

	cplx Wxy;		// -Wx + I Wy
	double Wz;
	int irs, ire;

	irs = Ulm.irs;		ire = Ulm.ire;
	double Wxyz[3];
	angular_momentum_local_sum(Ulm, Ulm.irs, Ulm.ire, Wxyz);
	// contribution of solid shells:
	if (own(Ulm.ir_bci)  &&  InnerCore.freely_rotate) 	Wxyz[2] += InnerCore.IM * (3./(8.*M_PI)) * real(Ulm.Tor[Ulm.ir_bci][lm10]) / r[Ulm.ir_bci];
	if (own(Ulm.ir_bco)  &&  Mantle.freely_rotate) 	    Wxyz[2] += Mantle.IM    * (3./(8.*M_PI)) * real(Ulm.Tor[Ulm.ir_bco][lm10]) / r[Ulm.ir_bco];
	#ifdef XS_MPI
		double tmp[3];
		MPI_Allreduce(Wxyz, tmp, 3, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
		Wxyz[0] = tmp[0];	Wxyz[1] = tmp[1];	Wxyz[2] = tmp[2];
		if (Ulm.share_dn) irs--;	// also update halo.
		if (Ulm.share_up) ire++;
	#endif
	Wxy = cplx(Wxyz[0], Wxyz[1]);
	Wz = Wxyz[2];

	if (subtract == 0) {
		double ri = r[Ulm.ir_bci];		double ro = r[Ulm.ir_bco];
		n = ro*ro*ro*ro*ro - ri*ri*ri*ri*ri;		// normalization
		if (InnerCore.freely_rotate) n += InnerCore.IM * (15./(8*M_PI));
		if (Mantle.freely_rotate)    n += Mantle.IM    * (15./(8*M_PI));
		n = 5.0/n;
		if (kill_sbr > 1) {
			Wxy0 = Wxy;			Wz0 = Wz;		// keep initial angular momentum (otherwise it is set to zero).
		}
		double c = (8.*M_PI)/3.;
		subtract = 1;
		PRINTF0("   + conserve angular momentum of (%g,%g,%g)\n", real(Wxy0)*c/Y11_st, -imag(Wxy0)*c/Y11_st, Wz0*c/Y10_ct);
	} else {
		Wxy = (Wxy-Wxy0)*n;		Wz = (Wz-Wz0)*n;
		// subtract solid body rotation from field.
		for (int ir=irs; ir<=ire; ir++) {
			Ulm.Tor[ir][LiM(shtns,1,0)] -= Wz*r[ir];
			if ((MRES == 1)&&(MMAX > 0))
				Ulm.Tor[ir][LiM(shtns,1,1)] -= Wxy*r[ir];
		}
	}
}

/// in case of flux-boundary conditions, a drift in temperature can occur. This arbitrarily fixes the mean temperature of the outer boundary to 0.
void fix_mean_temperature(ScalarSH& Tlm)
{
	double Tout;
	int irs = Tlm.irs;
	int ire = Tlm.ire;
	if (own(Tlm.ir_bco)) Tout = Tlm[Tlm.ir_bco][0].real();
	#ifdef XS_MPI
		if (Tlm.share_dn) irs--;	// also update halo.
		if (Tlm.share_up) ire++;
		if (n_mpi > 1) MPI_Bcast((void*) &Tout, 1, MPI_DOUBLE, r_owner[Tlm.ir_bco], MPI_COMM_WORLD);	// Broadcast temperature of outer shell to all
	#endif

	// subtract Tout from field, so that Tout is set to zero:
	for (int ir=irs; ir<=ire; ir++)		Tlm[ir][0] -= Tout;
}


/// Magnetic torque computed using surface expression: integral over the sphere of B_r*B_phi*sin(theta)
double calc_TorqueMag_surf_spat(const PolTor &Blm, const StatSpecVect *B0lm, const int ir)
{
	MemBlock mem(sizeof(cplx)*3*NLM + sizeof(double)*3*shtns->nspat, 6);
	cplx *Q = mem.alloc_cplx(NLM);
	cplx *S = mem.alloc_cplx(NLM);
	cplx *T = mem.alloc_cplx(NLM);
	Shell2d_mbk br(shtns,mem), bt(shtns,mem), bp(shtns,mem);
	double torque;

	Blm.RadSph(ir, Q, S);
	LM_LOOP(  T[lm] = Blm.Tor[ir][lm];  )		// we need a copy here.
	if (B0lm) {
		int nj = B0lm->nlm;
		unsigned* lma = B0lm->lm;
		cplx* QST = B0lm->QST + ir*nj*3;	//QST[ir*nlm*3 + 3*j]
		for (int j=0; j<nj; j++) {		// Add Background Spectral
			int lm = lma[j];
			Q[lm] += QST[j*3];
			S[lm] += QST[j*3+1];
			T[lm] += QST[j*3+2];
		}
	}
	SHqst_to_spat(shtns, Q, S, T, br, bt, bp);

	for (int ip=0; ip<NPHI; ip++) {
		for (int it=0; it<NLAT; it++) {
			bt(it,ip) = bp(it,ip)*br(it,ip)*st[it];
		}
	}

	spat_to_SH_l(shtns, bt, Q, 0);
	torque = Q[0].real() * 4*M_PI/Y00_1  * r[ir]*r[ir]*r[ir];
	return(torque);
}

/// Magnetic torque computed using surface expression: inner-product in spectral space of B_r and B_phi*sin(theta)
double calc_TorqueMag_surf(const PolTor &Blm, const StatSpecVect *B0lm, const int ir)
{
	MemBlock mem(sizeof(cplx)*4*NLM, 4);
	cplx *Q = mem.alloc_cplx(NLM);
	cplx *S = mem.alloc_cplx(NLM);
	cplx *Q2 = mem.alloc_cplx(NLM);
	double torque;

	Blm.RadSph(ir, Q, S);
	cplx* T = Blm.Tor[ir];
	if (B0lm) {
		T = mem.alloc_cplx(NLM);
		LM_LOOP(  T[lm] = Blm.Tor[ir][lm];  )		// we need a copy here.
		int nj = B0lm->nlm;
		unsigned* lma = B0lm->lm;
		cplx* QST = B0lm->QST + ir*nj*3;	//QST[ir*nlm*3 + 3*j]
		for (int j=0; j<nj; j++) {		// Add Background Spectral
			int lm = lma[j];
			Q[lm] += QST[j*3];
			S[lm] += QST[j*3+1];
			T[lm] += QST[j*3+2];
		}
	}

	SH_mul_mx(shtns, st_dt_mx, T, Q2);		// sin(theta)*d/dtheta(T)
	for (int im=1; im<=MMAX; im++) {
		cplx Im = cplx(0., im*MRES);
		for (long l=im*MRES; l<=LMAX; l++) {
			long lm = LiM(shtns,l,im);
			Q2[lm] -= Im * S[lm];		// -B_phi*sin(theta) in spectral space
		}
	}
	torque = -2 * coenergy(Q, Q2) * r[ir]*r[ir]*r[ir];
	return(torque);
}

// TODO: check this function !
double calc_TorqueVisc(PolTor & Ulm, int outer=0)
{
	double torque = 0.0;
	double T0, T1;
	double dx_1, r2;
	const int lm = LM(shtns,1,0);
	int ir,ir1;

	if (outer) {
		if (Ulm.bco == BC_FREE_SLIP) return 0.0;
		ir = Ulm.ir_bco;
		ir1 = ir-1;
	} else {
		if (Ulm.bci == BC_FREE_SLIP) return 0.0;
		ir = Ulm.ir_bci;
		ir1 = ir+1;
	}
	// Gamma_z = mu * 8*pi/3 * r^3 * (dT/dr-T/r) / Y10_ct
	//         = mu * 8*pi/3 * r^2 * (r*dT/dr - T) / Y10_ct
	T0 = Ulm.Tor[ir][lm].real();
	T1 = Ulm.Tor[ir1][lm].real();
	dx_1 = r[ir]/(r[ir1]-r[ir]);
	r2 = r[ir]*r[ir];
	torque = ((T1-T0)*dx_1 - T0)*r2 * (8*M_PI/3)/Y10_ct;		// first order approx.

	return torque;
}


/// compute the non-linear term associated with coriolis force without SH transform, and add it to NL.
/// [tested for m=0 and m=1]
void Coriolis_force_add(PolTor &NL, const PolTor &Ulm, double Omega0, void* buf=0)
{
	s2d Wz0;
	int irs, ire;
	xs_array2d<v2d> Up, Ut, NLp, NLt;
	v2d *GradTP;

	int lm0 = 0;
	int lm1 = NLM-1;
	thread_interval_lm(lm0, lm1);

	Up = Ulm.Pol;	Ut = Ulm.Tor;	NLp = NL.Pol;	NLt = NL.Tor;

	if (buf) {
		GradTP = (v2d*) buf;
	} else {
		GradTP = (v2d*) VMALLOC( (2*NLM+2)*sizeof(v2d) );
		#ifdef XS_DEBUG
		if UNLIKELY(GradTP==0) runerr("[Coriolis_force_add] allocation error 0");
		#endif
	}

	#ifdef XS_ELLIPTIC
		#error "Linear Coriolis not supported with Elliptic BC"
		runerr("Linear Coriolis not supported with Elliptic BC");
	#endif
	Wz0 = vdup(Omega0 + Omega0);
	if (lm0 == 0) {
		lm0 = 1;		// l=0 is zero
		GradTP[0] = vdup(0.);		GradTP[1] = vdup(0.);		// allow overflow.
	}
	if (lm1 == NLM-1) {
		GradTP[2*NLM] = vdup(0.);		GradTP[2*NLM+1] = vdup(0.);		// allow overflow.
	}
	irs = Ulm.ir_bci+1;		ire = Ulm.ir_bco-1;
	if (Ulm.bci > BC_NO_SLIP) irs--;
	if (Ulm.bco > BC_NO_SLIP) ire++;
	mpi_interval(irs, ire);

	thread_interval_rad(irs, ire);
	for (int ir=irs; ir<=ire; ir++) {
		OpCurlLapl& L = CURL_LAPL(ir);
		s2d vr_1 = vdup(L.r_1);	s2d vr_2 = vdup(L.r_2);
		s2d dl = vdup(L.Ll);		s2d dd = vdup(L.Ld);		s2d du = vdup(L.Lu);
		if ((ir==Ulm.ir_bci) || (ir==Ulm.ir_bco)) {
			int BC = Ulm.bci;
			int ii = ir+1;
			int ig = ir-1;
			if (ir==Ulm.ir_bco) {
				BC = Ulm.bco;		ii = ir-1;		ig = ir+1;
			}
			s2d dr_1 = vdup(1.0/(r[ir]-r[ii]));
			s2d dr_2 = dr_1*dr_1;
			if (BC <= BC_NO_SLIP) {		// NO-SLIP
				LM_LOOP2 ( lm0, lm1,
					GradTP[2*lm]   = dr_1 * (Ut[ir][lm] - Ut[ii][lm]);		// grad(Ut) [order 1 approx]
					GradTP[2*lm+1] = Up[ig][lm];		// grad(Up) prescribed by ghost shell.
				)
				dr_2 += dr_2;
				dd = -dr_2;		dl = dr_2;		du = dr_2;
				if (ir==Ulm.ir_bci)  dl = vdup(2.)*(dr_1 + vr_1);		// [dP/dr stored at ir-1]
				else 				du = vdup(2.)*(dr_1 + vr_1);		// [dP/dr stored at ir+1]
			} else {					// FREE-SLIP
				LM_LOOP2 ( lm0, lm1,
					GradTP[2*lm]   = vr_1 * Ut[ir][lm];					// grad(Ut) : dT/dr = Tor/r
					GradTP[2*lm+1] = dr_1 * (Up[ir][lm] - Up[ii][lm]);		// grad(Up) [order 1 approx]
				)
				dr_2 = (vr_1+vr_1)*dr_1;
				dl = vdup(0.);		dd = vdup(0.);		du = vdup(0.);
				if (ir==Ulm.ir_bci)  du = -dr_2;
				else 				dl = -dr_2;
			}
		} else {
			OpGrad& G = GRAD(ir);
			s2d gl = vdup(G.Gl);		s2d gd = vdup(G.Gd);		s2d gu = vdup(G.Gu);
			LM_LOOP2 ( lm0, lm1,		// precompute gradient
				GradTP[2*lm]   = gl*Ut[ir-1][lm] + gd*Ut[ir][lm] + gu*Ut[ir+1][lm];		// grad(Ut)
				GradTP[2*lm+1] = gl*Up[ir-1][lm] + gd*Up[ir][lm] + gu*Up[ir+1][lm];		// grad(Up)
			)
		}
		#if XS_OMP == 2
		#pragma omp barrier
		#warning "Linear Coriolis may not work with xsbig_hyb2"
		#endif
		for (int lm=lm0; lm<=lm1; lm++) {
			s2d Mc00 = vdup(Mcor[4*lm]);	s2d Mc01 = vdup(Mcor[4*lm+1]);		// for P/r
			s2d Mc10 = vdup(Mcor[4*lm+2]);	s2d Mc11 = vdup(Mcor[4*lm+3]);		// for dP/dr
			v2d NLtt = - vr_1*( Mc00 * Ut[ir][lm-1] + Mc01 * Ut[ir][lm+1] );
			v2d NLpp = - vr_1*( Mc00 * Up[ir][lm-1] + Mc01 * Up[ir][lm+1] );
			NLtt -= (Mc10 * GradTP[2*(lm-1)]   + Mc11 * GradTP[2*(lm+1)]);
			NLpp -= (Mc10 * GradTP[2*(lm-1)+1] + Mc11 * GradTP[2*(lm+1)+1]);
			if (lm > LMAX) {		// m>0
				NLpp += IxKxZ(Mm_l2[lm], Ut[ir][lm]);
				NLtt -= IxKxZ(Mm_l2[lm], (dl*Up[ir-1][lm] + (dd - vdup(l2[lm])*vr_2)*Up[ir][lm] + du*Up[ir+1][lm]) );
			}
			NLp[ir][lm] += Wz0 * NLpp;
			NLt[ir][lm] += Wz0 * NLtt;
		}
	}
	if (buf==0)	VFREE(GradTP);
}

void solid_body_rotation_add(PolTor& Ulm, double Wz)
{
	int irs_sb = Ulm.irs-1;		// include ghost shells.
	int ire_sb = Ulm.ire+1;
	Wz *= Y10_ct;
	#ifdef XS_MPI
	if (n_mpi_shared > 1) {
		if (irs_sb >= irs_shared) irs_sb = Ulm.irs;		// there is no ghost shell here!
		if (ire_sb <= ire_shared) ire_sb = Ulm.ire;
		MPI_Barrier(comm_shared);
	}
	#endif
	for (int ir=irs_sb; ir<=ire_sb; ir++) {
		Ulm.Tor[ir][1] += Wz * Ulm.radius(ir);
	}
	#ifdef XS_MPI
	if (n_mpi_shared > 1) MPI_Barrier(comm_shared);
	#endif
}

#ifdef XS_LEGACY
/// Compute the electric potential field from U, B and eta.
void calc_Electric_Potential(ScalarSH& Vlm, const PolTor& Ulm, const PolTor& Blm)
{
	#ifdef XS_LINEAR
		if ((spat_need & NEED_J) == 0)
			J.alloc(B.irs, B.ire);			// in some cases, allocation is needed here.
	#endif
	Blm.cte_to_curl_spat(J0lm, &J, B.irs, B.ire);		// J+J0 on the whole domain
	Blm.cte_to_spat(B0lm, &B);			// B+B0
	if (evol_ubt & EVOL_U) Ulm.cte_to_spat(U0lm, &U);			// U+U0
	#ifndef XS_LINEAR
		B.NL_Magnetic(&U, &InnerCore, &Mantle, &B);		// B destroyed.
	#else
		B.NL_vect(U0, &B);		// U0 x B
		B.NL_vect_add(&U, B0);	// U x B0
	#endif
	#pragma omp for schedule(static)
	for (int ir=B.irs; ir<=B.ire; ir++) {
		#ifdef XS_ETA_VAR
			double eta = etar[ir];		// variable eta
		#endif
		for (int lm=0; lm < shtns->nspat; lm++) {
			B.vr[ir][lm] -= eta * J.vr[ir][lm];
			B.vt[ir][lm] -= eta * J.vt[ir][lm];
			B.vp[ir][lm] -= eta * J.vp[ir][lm];
		}
	}
	Vlm.from_grad_spat(B);				// this is the electric potential.
}
#else
#ifndef XS_LINEAR
/// Compute Electric potential at a given radius r.
void Electric_Potential_shell(const int ir, cplx* Vlm)
{
	double* u;
	double* b;
	cplx *Q, *S, *T;
	const size_t nspat = shtns->nspat;
  #ifdef XS_ETA_VAR
	const double eta = etar[ir];		// variable eta
  #else
	const double eta = diffusivity[::B];
  #endif
	int uzero = 0;		// flag

	u = (double*) VMALLOC(sizeof(double)*(6*NLM + 6*nspat));		// alloc aligned memory
	b = u + 3*nspat;
	Q = (cplx*) (u + 6*nspat);
	S = Q + NLM;		T = Q + 2*NLM;

	if ((ir >= Ulm.ir_bci) && (ir <= Ulm.ir_bco)) {		// fluid shell
		Ulm.RadSph(ir, Q, S);
		SHV3_SPAT(Q, S, Ulm.Tor[ir], u, u + nspat, u + 2*nspat);
	} else {	// solid shell
		SolidBody& solid = Mantle;
		if (ir < Ulm.ir_bci) solid = InnerCore;		// solid inner shell
		if ((solid.Omega_x == 0) && (solid.Omega_y == 0) && (solid.Omega_z == 0)) {		// u = 0
			uzero = 1;		// zero velocity field
		} else {
			solid.calc_spatial();
			for (size_t k=0; k<shtns->nspat; k++) {		// cross product (theta and phi components only)
				u[k] = 0.0;
				u[k + nspat]   = solid.vt_r[k] * r[ir];
				u[k + 2*nspat] = solid.vp_r[k] * r[ir];
			}
		}
	}
	if (uzero) {
		Blm.curl_QST(ir, Q, S, T);		// J, but only S is needed here
		Vlm[0] = 0.0;
		double mreta = -eta*r[ir];
		for (int lm=1; lm<NLM; lm++)	// l > 0
			Vlm[lm] = mreta*S[lm];
	} else {
		Blm.RadSph(ir, Q, S);
		SHV3_SPAT(Q, S, Blm.Tor[ir], b, b + nspat, b + 2*nspat);
		Blm.curl_QST(ir, Q, S, T);		// J, but only S is needed here
		cross_prod(u, u, b, 0, nspat);		// u cross b
		SPAT_SHV(u + nspat, u + 2*nspat, Q, T);		// keep only Q here
		spat_to_SH_l( shtns, u, Vlm, 0 );		// we only need l=0 here, for further integration in radius.
		for (int lm=1; lm<NLM; lm++)	// l > 0
			Vlm[lm] = (Q[lm] - eta*S[lm]) * r[ir];
	}
	VFREE(u);
}


/// Compute Electric potential everywhere. Perform radial integration of Electric_potential_shell()
void calc_Electric_Potential(ScalarSH& Vlm, const StateVector& Xlm)
{
	#pragma omp for
	for (int ir=Vlm.irs; ir<Vlm.ire; ir++) {
		Electric_Potential_shell(ir, Vlm[ir]);
	}

	// radial integration of l=0 component.
	#pragma omp barrier
	#pragma omp master
	{
		double sum[2] = {0, 0};		// sum and value at previous point. (start with 0)
		int ir = Vlm.irs;
	    #ifdef XS_MPI
		if (Vlm.share_dn) {
			MPI_Recv(&sum, 2, MPI_DOUBLE, i_mpi-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		} else
		#endif
		{
			sum[1] = real(Vlm[ir][0]);		// keep value for next step.
			Vlm[ir][0] = sum[0];
			ir++;
		}
		for (; ir<=Vlm.ire; ir++) {
			double p1 = real(Vlm[ir][0]);
			sum[0] += (sum[1] + p1)*0.5*(r[ir]-r[ir-1]);	// Trapezoidal rule
			Vlm[ir][0] = sum[0];
			sum[1] = p1;
		}
		#ifdef XS_MPI
		if (Vlm.share_up) {
			MPI_Send(&sum, 2, MPI_DOUBLE, i_mpi+1, 0, MPI_COMM_WORLD);
		}
		#endif
	}
	#pragma omp barrier
}
#endif
#endif

/// Contribution of local shells (MPI) to the gradient at a boundary. Useful for custom diagnostics.
/// Result should be summed across all processes to obtain the actual gradient.
cplx boundary_Grad_contrib(xs_array2d<cplx> S, const int ir_bc, const int dir, const int lm)
{
	cplx gr = 0.0;

	// compute gradient, with order 1 accuracy:
/*	double dr_1 = 1.0 / (r[ir_bc+dir]-r[ir_bc]);
	if (own(ir_bc))	gr -= dr_1 * S[ir_bc][lm];
	if (own(ir_bc+dir))	gr += dr_1 * S[ir_bc+dir][lm];
	return gr;
*/
	// compute gradient, with order 2 accuracy, uncentered scheme:
	double dr1 = r[ir_bc+dir]  -r[ir_bc];
	double dr2 = r[ir_bc+2*dir]-r[ir_bc];
	double dr1_2 = dr1*dr1;
	double dr2_2 = dr2*dr2;
	double norm = 1.0 / (dr1*dr2_2 - dr2*dr1_2);
	if (own(ir_bc))			gr += S[ir_bc][lm] * (dr1_2 - dr2_2);
	if (own(ir_bc+dir))		gr += S[ir_bc+dir][lm] * dr2_2;
	if (own(ir_bc+2*dir))	gr -= S[ir_bc+2*dir][lm] * dr1_2;
	return gr * norm;
}

/// Contribution of local shells (MPI) to the gradient at a boundary. Useful for custom diagnostics.
/// Result should be summed across all processes to obtain the actual gradient.
cplx boundary_Grad_contrib_o1(xs_array2d<cplx> S, const int ir_bc, const int dir, const int lm)
{
	cplx gr = 0.0;

	// compute gradient, with order 1 accuracy:
	double dr_1 = 1.0 / (r[ir_bc+dir]-r[ir_bc]);
	if (own(ir_bc))	gr -= dr_1 * S[ir_bc][lm];
	if (own(ir_bc+dir))	gr += dr_1 * S[ir_bc+dir][lm];
	return gr;
}

double explicit_term_T10_free_solid(const SolidBody& solid, int ir0, int fluid_dir, const xs_array2d<cplx>& UTor, const PolTor& Blm, double nu_IM)
{
	// correction for second order viscous torque.  TODO: make second order directly into Matrix for implicit treatment
	cplx dU = boundary_Grad_contrib(UTor, ir0, fluid_dir, 1)  -  boundary_Grad_contrib_o1(UTor, ir0, fluid_dir, 1);
	#ifdef XS_MPI
	if (r_owner[ir0] != r_owner[ir0+2*fluid_dir]) runerr("!!!!argh!!!!");
	#endif
	double nlu_t10 = real(dU) * nu_IM * 5.0 / r[ir0];	// nu_IM = viscosity / inertia_moment
	if ((evol_ubt & EVOL_B) && Blm.ir_bci < ir0) {
		double Tb = calc_TorqueMag_surf(Blm, 0, ir0);
		nlu_t10 += r[ir0] * Tb * Y10_ct / solid.IM;
	}
	return nlu_t10;
}

/* REFERENCE FRAME */

/// The reference frame, including global rotation, precession, etc...
class RefFrame {
	double Omega0_z;	///< keep track of rotation rate.
	cplx Omega0_xy;
	double eps_xy, eps_z, freq;
	double eps_poincare;
	int flags;		// bit 0 : 0=axial, 1=non-axial;  bit 1 : 0=oscillation, 1=rotation;
	int lm_forcing;
	double strain_ampl;		///< strain rate (l=2,m=2 potential flow)
	double strain_puls;		///< strain freqency

  public:
	double Omega_z;
	cplx Omega_xy;
	cplx poincare;
	cplx strain_22;		///< strain rate (l=2,m=2 potential flow)

    void update(const double t);
	void add_Poincare_force(PolTor& NLu);

	void set_rotation(double Omega0, double angle = 0.0);
	void set_libration(double Omega0, double amplitude, double frequency, char axis = 'z');
	void set_precession(double Omega0, double Omega_P, double angle = M_PI_2);
	void set_nutation(double Omega0, double amplitude, double frequency);
	void set_strain(double amplitude, double frequency = -1.0);
	int check_non_axial();
	int check_inertial();
	int check_time_dep() { return (lm_forcing > 0); }
};

int RefFrame::check_inertial()
{
	if (check_non_axial()) return 0;		// not inertial
	if ((Omega0_z == 0) && (eps_z == 0)) return 1;		// inertial
	return 0;	// not inertial
}

int RefFrame::check_non_axial()
{
	if (flags & 1) {		// non-axial rotation ?
		if ((MRES != 1)||(MMAX <= 0))
			runerr("non-axial rotation requires Mres=1 and Mmax>0");
	}
	return (flags & 1);
}

void RefFrame::update(const double t)
{
	if (lm_forcing) {
		double sin_wt = sin(freq*t);
		double cos_wt = cos(freq*t);
		if (flags & 2) {		// rotating
			poincare = eps_poincare * cplx( sin_wt, -cos_wt );
			Omega_xy = Omega0_xy + eps_xy * cplx( cos_wt, sin_wt );
		} else {		// oscillating
			poincare = eps_poincare * sin_wt;
			Omega_xy = Omega0_xy + eps_xy * cos_wt;
		}
		Omega_z = Omega0_z + eps_z * cos_wt;
	}
	#ifdef XS_STRAIN
	double wstrain_t = strain_puls * t;
	strain_22 = strain_ampl * cplx(cos(wstrain_t), -sin(wstrain_t));
	#endif
}

void RefFrame::set_rotation(double Omega0, double angle)
{
	eps_z = 0.0;	eps_xy = 0.0;	freq = 0;
	flags = 0;		// axial
	PRINTF0("[RefFrame] Global rotation rate %g, angle %.3f deg\n", Omega0, angle*180./M_PI);
	if (angle == 0.0) {
		Omega0_z = Omega0;
		Omega0_xy = 0.0;
	} else {
		Omega0_z = Omega0 * cos(angle);
		Omega0_xy = cplx( Omega0 * sin(angle) , 0.0 );
		flags = 1;		// mark as non-axial.
	}
	lm_forcing = 0;		// no inertial forcing (time independent).
	Omega_z = Omega0_z;
	Omega_xy = Omega0_xy;
	check_non_axial();
}

void RefFrame::set_libration(double Omega0, double amplitude, double frequency, char axis)
{
	set_rotation(Omega0);
	if ((frequency != 0.0) || (amplitude != 0.0)) {
		eps_poincare = amplitude*frequency;
		freq = frequency;
		if (axis == 'z') {
			PRINTF0("[RefFrame] Longitudinal libration with frequency %g, and amplitude %g\n", frequency, amplitude);
			eps_z = amplitude;
			flags = 0;		// axial, oscillating
			lm_forcing = LiM(shtns,1,0);		// time-dependent
			eps_poincare *= Y10_ct;		// sh-normalization
		} else {
			PRINTF0("[RefFrame] Latitudinal libration with frequency %g, and amplitude %g\n", frequency, amplitude);
			eps_xy = amplitude;
			flags = 1;		// non-axial, oscillating
			lm_forcing = LiM(shtns,1,1);
			eps_poincare *= Y11_st;		// sh-normalization
		}
		check_non_axial();
	}
}

void RefFrame::set_precession(double Omega0, double Omega_P, double angle)
{
	double cos_angle = cos(angle);
	if (fabs(angle-M_PI_2) < 1e-15) cos_angle = 0.0;
	set_rotation(Omega0 + Omega_P*cos_angle);
	PRINTF0("[RefFrame] Precessing at rate %g, angle %.3f deg\n", Omega_P, angle*180./M_PI);
	eps_xy = Omega_P*sin(angle);
	eps_poincare = Omega0*eps_xy;
	freq = Omega0;
	flags = 3;			// mark as non-axial, precessing.
	lm_forcing = LiM(shtns,1,1);
	eps_poincare *= Y11_st;		// sh-normalization
	check_non_axial();
}

void RefFrame::set_nutation(double Omega0, double amplitude, double frequency)
{
	set_rotation(Omega0);
	PRINTF0("[RefFrame] Nutation at rate %g, amplitude %g\n", frequency, amplitude);
	eps_xy = amplitude;
	freq = frequency;
	eps_poincare = frequency*eps_xy;
	flags = 3;			// mark as non-axial, precessing.
	lm_forcing = LiM(shtns,1,1);
	eps_poincare *= Y11_st;		// sh-normalization
	check_non_axial();
}

// initialize strain at given amplitude and frequency
void RefFrame::set_strain(double amplitude, double frequency)
{
	if (amplitude==0) return;
	if ((MRES>2) || (MMAX*MRES<2)) runerr("m=2 needed to represent strain");
	strain_puls = 2.*frequency;		// m=2
	strain_ampl = amplitude * sqrt(2.*M_PI/15.);	// for normalization of SH l=2,m=2
	PRINTF0("[RefFrame] Strain rate %g, rotating at angular velocity %g\n", amplitude, frequency);
}

void RefFrame::add_Poincare_force(PolTor& NLu)
{
	int lm = lm_forcing;
	if (lm) {
		cplx a = poincare;
		#pragma omp for schedule(static) nowait
		for (int ir=NLu.irs; ir<=NLu.ire; ir++) {
			NLu.Pol[ir][lm] += a*r[ir];
		}
		#pragma omp barrier
	}
}


class spat3D {
  private:
	double* spat;
	size_t spat_cdist;
	size_t spat_rdist;	// distance between shells (in units of double, not cplx)
	void* mem;			// keep memory reference to be able to free the memory.

  public:
	spat3D() { spat=0; mem=0; }
	void alloc(int irs, int ire, size_t nelem_spat, int nc_spat) {
		spat = 0;		mem = 0;
		const int nr = ire-irs+1;
		if (nr <= 0) return;
		const int chunk = ((VSIZE < 8) ? 8 : VSIZE);		// at least 64 byte alignement.
		nelem_spat = ((nelem_spat+chunk-1)/chunk)*chunk;		// round to VSIZE
		spat_rdist = nelem_spat*nc_spat;
		mem = VMALLOC( sizeof(double)*spat_rdist*nr );		// buffer
		spat_cdist = nelem_spat;
		spat = ((double*) mem);
		spat -= irs*spat_rdist;		// shift pointers
	}
	~spat3D() {
		if (mem) VFREE(mem);
	}
	double* operator() (int ir, int c) const {			// get pointer to the c spatial component
		return spat + spat_rdist*ir + spat_cdist*c;
	}
	size_t nspat() { return spat_cdist; }		// number of spatial elements
};


