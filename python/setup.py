#!/usr/bin/python

try:
    from setuptools import setup
except:
    from distutils.core import setup

setup(name='pyxshells',
      description='python tools for handling output of xshells code',
      url='https://nschaeff.bitbucket.io/xshells',
      author='Nathanael Schaeffer',
      py_modules=['pyxshells','xsplot'],
      requires=['numpy','matplotlib','shtns'],
)
