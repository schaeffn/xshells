XSHELLS CHANGE LOG:
-------------------

* v2.11  (11 Jan 2025)
	- New support for freely rotating inner-core and/or mantle, dynamically responding
	  to viscous and magnetic torques. Passes geodynamo benchmark with rotating
	  inner-core.
	- Fix major bug: purely linearized computations (`XS_LINEAR`) for double-diffusive
	  convection were wrong.
	- Minor bug fixes.

* v2.10.1  (6 Dec 2024)
	- Major bug fix, present since v2.2, and affecting *non-axisymmetric* magnetic runs
	  *using MPI* and with *moving, conducting* solid shells. Induction in the solids
	  was accidentaly disabled.

* v2.10  (26 Aug 2024)
	- Can record accurate time derivative of fields with `time_derivative=1`
	- Fix "viscous dissipation" (actually work of viscous forces) with
	  non-zero velocity at boundaries.
	- Optimized magnetic torque computation.
	- Improved tests.

* v2.9  (17 Apr 2024)
	- Major bugfix in `XS_LINEAR` mode, present since v2.5.
	- Change CFL safety factors for CNAB2 and SBDF2 for Libration to work safely.
	- Fix time-averaging of fields with variable time-step.
	- Add new surface dipole diagnostics, as our `f_dip` diagnostic is not "standard".
	- Remove OpenMP task-based parallelism, and other code clean up.
	- Many improvements to post-processing tools.
	- Bug fixes.

* v2.8  (21 Mar 2023)
	- New interactive easy installation program: simply run `./setup`
	- New diagnostics for geodynamo problem to compute other "fdip" variants.
	- Adjustments in time-step constraints (CFL factors) for better accuracy
	  and stability.
	- Bugfix: when using 1 or 2 shells per processes without shared memory (rare), 
	  crashes or bad results could occur with v2.7.
	- Bugfix in python interface pyxshells.py

* v2.7  (12 Jul 2022)
	- Runtime parameters are now stored in the energy file to keep their history.
	- Allow initial fields to be set with a sequence of fields, useful for setting
	  boundary conditions on top of another field.
	- New `xs_zavg.py` script to compute z-averages of fields.
	- Improvements in default linear solver (lower latency).
	- Bug fixes

* v2.6  (9 Dec 2021)
	- Support for Robin bondary conditions for temperature and composition fields.
	- Support for imposed heterogeneous heat flux (see `problems/heteroflux`).
	- Support for latitudinal/longitudinal libration of both upper and inner boundaries.
	- Bug fixes.

* v2.5.4  (3 Mar 2021)
	- Bug fixes: compilation errors for some diagnostics; compilation with older MPI.
	- Fix crash during initilization with `-shared_mem` option for small cases.
	- xspp: fix magnetic field prolongation.

* v2.5.3  (27 Jan 2021)
	- Fix crashes and deadlocks sometimes arising during initialization with 
	  the `-shared_mem` option (since v2.5).
	- Fix wrong initial fields when defined in physical space (since v2.5).
	- Improved xsplot.py and xspeek.py that can now also use gnuplot, including ascii plots.

* v2.5.2  (25 Dec 2020)
	- Fix major bug introduced in v2.5.
	- Fix bug arising on PowerPC (where char is unsigned by default).

* v2.5.1  (13 Oct 2020)  [**BUGGY, don't use!**]
	- Fix compilation issues
	- ./configure now honors the MPICXX environement variable
	- xsplot can now handle line profiles from xspp.

* v2.5  (1 Oct 2020)  [**BUGGY, don't use!**]
	- Optimization of memory accesses. 10 to 30 % faster with SHTns v3.4 or later.
	- Experimental MPI-3 shared memory mode (not fully standard compliant);
	  add `-shared_mem=N` to command line to allow processes to share memory across N cores.
	- Experimental bidirectional solve to speed-up MPI solves; add `-bidir_solve=1`.
	- pyxshells.py supports reading field files with fp48 compressed data.
	- small bug fixes.

* v2.4  (28 Nov 2019)
	- New available time steppers: PC2 (default), CNAB2, SBDF2, SBDF3 and BPR353.
	- Many fixes and improvements.

* v2.3.1  (25 Sep 2019)
	- Fix configuration and compilation issues.
	- Improved testing script (run `test.py`).
	- New control variables `sconv_stop` and `omp_tasks`, see manual.
	- Other minor fixes and cleanups.

* v2.3  (27 Aug 2019)
	- New testing framework (run `test.py` or `make test`).
	- New FP48 compression of floating point data (optional).
	- New default CFL safety factors for magnetic field, allowing smooth
	  transient in the geodynamo benchmark.
	- New support for mean-field dynamo with axisymmetric alpha effect (see
	  `problems/kinematic_dynamos`, thanks to C. Hardy).
	- Color in console output to highlight warnings and errors.
	- Computing in a full sphere now only allowed if enabled at compile time.
	- Code cleanup and internal improvements.
	- Some minor bugfixes.

* v2.2.3  (11 Jun 2019)
	- Fix computation of electric current at outer insulating boundary.
	- Setting hyperviscosity now only allowed if enabled at compile time.
	- Documentation update.

* v2.2.2  (20 Mar 2019)
	- Fix configure script to include optimization options correctly. 

* v2.2.1  (8 Mar 2019)
	- Easier configuration and install, including SHTns library configuration 
          and compilation, better intel MKL detection, and better intel KNL support.

* v2.2  (31 Oct 2018)
	- several optimizations & fixes.
	- diagnostics are now code snippets found in the diagnostics directory, for
          better code reuse and maintenance; all xshells.hpp files have been updated.
	- all parameters of xshells.par can be overridden by command line arguments.
	- OpenMP 4 tasks are now used when it results in better load balancing.
	- Smarter distribution of shells between MPI processes with solid shells.
	- xsplot.py can now handle energy files with diagnostics added between restarts.
	- pyxshells.py can read Parody restart files (and convert them to xshells).

* v2.1  (16 May 2018)
	- several minor bug fixed.
	- improved python scripts; added xspeek.py to pretty display the energy files.
	- hyper-diffusivity can also be applied on the magnetic field.

* v2.0  (4 Dec 2017)
	- *warning*: previous xshells.hpp may not compile as-is.
	- new: time-stepping scheme switched to Predictor-Corrector type allowing
          3 to 4 times larger time-steps (change xshells.par accordingly!).
	- new: support for double-diffusive convection.
	- fixed and improved linear mode, which now allows full computation of
	  perturbations around base-fields (optionally including non-linear terms).
	- support for AVX-512 vectorization (e.g. intel KNL).
	- new `xsimp` handles Coriolis force implicitely (beta).
	- internal changes in banded matrix handling.
	- `xspp` writes numpy files instead of text files for most slices.
	- improved plotting system: use xsplot to display almost all outputs from xspp.

* v1.4  (30 Sep 2016)
	- spectral convergence for each field stored in energy file.
	- drop the `time_scheme` option and the obsolete and poor corrector mode.
	- fix broken linear mode (without magnetic field).
	- improvments to pyxshells module (by E. Kaplan).
	- fix bug in z-averaging (thanks to E. Kaplan).
	- other minor fixes and improvements

* v1.3  (8 Mar 2016)
	- better spectral convergence (Sconv) check (max of all shells).
	- spectral convergence checks adjusted from xshells.par with
	  variables `sconv_lmin` and `sconv_mmin`.
	- variable l-truncation controlled from xshells.par using `rsat_ltr`.
	- fix sign of rotation vector along y.
	- fix bugs appearing when using MPI and few shells per process.
	- other bugfixes and improvements, especially to plotting scripts.

* v1.2  (6 Oct 2015)
	- new parallelization mode using OpenMP in the angular directions:
	  compile and run `xsbig_hyb2`.
	- arbitrary gravity fields allowed.
	- safer default CFL safety factors.
	- real-time spectral convergence check.
	- full linear mode (now supporting non-axisymmetric base fields).
	- user defined bulk forcings (example in problems/tidal).
	- fixes, improvements, optimizations.

* v1.1  (15 Jul 2015)
	- global rotation no longer required to be along z-axis.
	- real-time diagnostic plotting (using gnuplot).
	- at least 1 radial shell per process required (instead of 2 before).
	- new real-time test for spectral convergence.
	- many fixes, improvements and optimizations.

* v1.0  (3 Jan 2015)
	- first public release.
	- hybrid OpenMP/MPI parallelization.
	- extensible diagnostic system.

