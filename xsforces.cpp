/*
 * Copyright (c) 2010-2020 Centre National de la Recherche Scientifique.
 * written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France).
 * 
 * nathanael.schaeffer@univ-grenoble-alpes.fr
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * 
 */

/// \file xsforces.cpp main program for force computation.

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string>
#include <string.h>
#include <math.h>
#include <vector> 		// c++ standard vectors.


#include <shtns.h>

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

double dt_log;                          ///< time between logs

// radial limits for specific matrix multiplications
int Ut_i0, Ut_i1, Bp_i0, Bp_i1, T_i0, T_i1;

#define NEED_U 1
#define NEED_W 2
#define NEED_B 4
#define NEED_J 8
#define NEED_T 16
#define NEED_GT 32
int spat_need = 0;		///< flags for required computations of spatial fields.

enum field_id { U, B, T, C, MAXFIELD };				///< field enumeration.
const char symbol[MAXFIELD] = { 'U', 'B', 'T', 'C' };	///< symbols for these fields.
#define EVOL(f) (1U<<(f))

#define EVOL_U EVOL(::U)
#define EVOL_B EVOL(::B)
#define EVOL_T EVOL(::T)
#define EVOL_C EVOL(::C)
int evol_ubt = 0;		///< flags for evolution of fields.

int no_j0xb0 = 0;		///< if (no_j0xb0) the lorentz force associated with the imposed field is removed.

const double pi = M_PI;			///< pi = 3.14...

#include "xshells.hpp"
#include "grid.cpp"
#ifdef XS_MPI
	#error "MPI not supported, use xshells_big.cpp"
#endif

#ifdef _OPENMP
	#include <omp.h>
	#define DEB printf("thread %d :: %s:%u pass\n",omp_get_thread_num(), __FILE__, __LINE__)
#else
	#define DEB printf("%s:%u pass\n", __FILE__, __LINE__)
#endif

#ifndef XS_DEBUG
	#undef DEB
	#define DEB (0)
#endif

#define XS_LINEAR
#include "xshells_spectral.cpp"
#include "xshells_spatial.cpp"
#include "xshells_io.cpp"
#include "xshells_render.cpp"

SolidBody InnerCore, Mantle;		// Solid boundaries.
VectField Bx, Ux, W, J, GT;
PolTor Blm, Ulm, NLb1, NLb2, NLu1, NLu2;
PolTor Ulin, Blin;	// temporary fields, for matrix inversion.
ScalarSH Tlm, Clm, NLt1, NLt2, Tlin, Tavg;
ScalField Tx;
StatSpecVect *B0lm = NULL;		///< Imposed background magnetic field.
StatSpecVect *J0lm = NULL;		///< Imposed background current.
StatSpecVect *U0lm = NULL;		///< Imposed background velocity.
StatSpecVect *W0lm = NULL;		///< Imposed background vorticity.
StatSpecScal *T0lm = NULL;		///< Imposed background temperature.
StatSpecScal *C0lm = NULL;		///< Imposed background composition.
PolTor *NLubase = NULL;		///< bulk force or frozen flow term
VectField *Grav = NULL;		///< spatial gravity field (=grad(Phi) if l>0 involved)
double* Grav0_r = NULL;		///< central gravity divided by r (l=0 only).
PolTor Uavg, Bavg;			///< time-averaged fields.
#ifdef XS_LINEAR
	VectField *B0 = NULL;
	VectField *J0 = NULL;
	VectField *U0 = NULL;
	VectField *W0 = NULL;
	VectField *NL = NULL;
	VectField *GT0 = NULL;
	#define NL_ORDER 1
#else
	#define NL_ORDER 2
#endif
#ifdef XS_WRITE_SV
	cplx *Bp1, *Bp2;		// for time derivative of surface field
	cplx *Up1, *Ut1;
#endif

#include "xshells_physics.cpp"

#include "xshells_init.cpp"
#include "xshells.hpp"
#ifndef U_FORCING
	#define FORCING_M 0
#endif


// **************************
// ***** TIME STEPPING ******
// **************************

//#define LM_LOOP( action ) for (lm=0; lm<NLM; lm++) { action }
#ifdef VAR_LTR
  #define LTR ltr[ir]
#endif

void calc_dw_dt(PolTor &NLu, PolTor& Ulm, PolTor& Blm, ScalarSH& Tlm, double Omega0)
{
		Ulm.to_spat(&Ux);			// U+U0
		Ulm.to_curl_spat(&W, NG, NM, Omega0+Omega0);		// W = rot(U+U0) + 2.Omega0
		W.NL_Fluid(&Ux, &W);
		if (evol_ubt & EVOL_B) {
			Blm.to_curl_spat(&J, NG, NM);		// J
			Blm.to_spat(&Bx, NG, NM);			// B
			W.NL_vect_add(&J, &Bx);
		}
		NLu.curl_from_spat(&W);
		if (evol_ubt & EVOL_T) {
			Radial_gravity_buoyancy(NLu, Tlm, Grav0_r);		// add buoyancy due to central gravity.
		}
}



/// Update the imposed potential field associated to a solid body.
/// Update the imposed potential field associated to a solid body.
void update_BC_Mag(SolidBody *SB, cplx *P)
{
	cplx* P0 = SB->PB0lm;
	if (P0 != NULL) {
		if ((SB->Omega_x != 0.0)||(SB->Omega_y != 0.0)) runerr("Equatorial rotation not allowed with imposed potential field.");
		//for (int l=0; l <= SB->lmax; l++)	P[l] = P0[l];	// m=0 does not change.
		P0 += SB->lmax +1;
		for (int im=1; im <= SB->mmax; im++) {
			cplx a = cplx( cos(im*MRES * SB->phi0) , -sin(im*MRES * SB->phi0) );
			for (int l=im*MRES; l<=SB->lmax; l++)
				P[LiM(shtns,l,im)] = (*P0++) * a;
				// the externally imposed field has been multiplied by 2*(l+1) in load_init().
		}
	}
}

/// Update the velocity field at the boundary of a solid body.
inline void update_BC_U(SolidBody *SB, cplx *T, double rr)
{
	if ((MRES==1)&&(MMAX>0)) T[LiM(shtns,1,1)] = Y11_st * rr * cplx(SB->Omega_x , - SB->Omega_y);
	T[LiM(shtns,1,0)] = Y10_ct * rr * SB->Omega_z;
}


#ifdef VAR_LTR
  #undef LTR
#endif

/*
void zero_out_TC(Spatial &W, double stc = 0.35)
{
	stc *= r[NM];
	for (int ir=W.irs; ir<=W.ire; ir++) {
		for (int it=0; it<NLAT; it++) {
			if (r[ir]*st[it] <= stc) {
				for (int ic=0; ic<W.ncomp; ic++) {
					double *d = W.get_data(ic, ir);
					for (int ip=0; ip<NPHI; ip++) {
						d[ip*NLAT+it] = 0.0;
					}
				}
			}
		}
	}
}
*/


int main (int argc, char *argv[])
{
	double ftime = 0.0;			///< current fluid time.
	double rmin, rmax;
	double tmp, b0, u0, gmax;
	int i;
	char command[100];

	shtns_use_threads(0);
	printf("[XSHELLS-forces] compute forces\n");

	read_Par(argc, argv, 0);
	jpar.sht_type = sht_quick_init;
	#ifndef XS_HYPER_DIFF
		if (jpar.hyper_diff_l0 > 0) runerr("Hyper-diffusivity not compiled.");
	#endif

	FieldInfo fh;
	LMAX = 0;
	for (int f=0; f<MAXFIELD; f++) {
		if (jpar.ffile[f]) {
			evol_ubt |= EVOL(f);
			load_FieldInfo(jpar.ffile[f], &fh);
			if (fh.lmax > LMAX) LMAX = fh.lmax;
		}
	}
	if (jpar.rfile) {		// load external grid
		if (!( load_Field_grid(jpar.rfile) || load_grid(jpar.rfile) )) {
			sprintf(command, "cannot read grid from file '%s' !", jpar.rfile);		runerr(command);
		}
	}
	if (evol_ubt == 0)	runerr("Nothing to do ?! at least u, b or tp must be defined.");
	if ((jpar.ffile[U] == NULL) && (jpar.f0file[U] == NULL)) runerr("No velocity field defined: solve the diffusion analytically ;-)");

	init_sh(jpar.sht_type, jpar.polar_opt_max, NL_ORDER);
	#ifdef XS_VEC
		if (NLAT & 1) runerr("NLAT must be even !");
	#endif

	rmin = jpar.Rbs;		rmax = jpar.Rbe;
	if (rmin > jpar.Rus)	rmin = jpar.Rus;
	if (rmax < jpar.Rue)	rmax = jpar.Rue;
	if (rmin > jpar.Rts)	rmin = jpar.Rts;
	if (rmax < jpar.Rte)	rmax = jpar.Rte;
	if (rmin > jpar.Rcs)	rmin = jpar.Rcs;
	if (rmax < jpar.Rce)	rmax = jpar.Rce;
	init_rad_sph(rmin, jpar.Rus, jpar.Rue, rmax, jpar.Nin, jpar.Nout);

	srand( time(NULL) );		// we might need random numbers.
// set field limits :
	Blm.ir_bci = r_to_idx(jpar.Rbs);		Blm.ir_bco = r_to_idx(jpar.Rbe);
	Tlm.ir_bci = r_to_idx(jpar.Rts);		Tlm.ir_bco = r_to_idx(jpar.Rte);
	Clm.ir_bci = r_to_idx(jpar.Rcs);		Clm.ir_bco = r_to_idx(jpar.Rce);
// set solid body limits :
	InnerCore.set_limits(0, NG);
	Mantle.set_limits(NM,NR-1);

/* MEMORY ALLOCATION AND MATRIX INITIALIZATION */
	if (evol_ubt & EVOL_U) {
		Ulm.alloc(NG, NM, 2);
		Ulm.bci = (boundary) jpar.uBCin;	Ulm.bco = (boundary) jpar.uBCout;
		init_Umatrix(Ulm, jpar.nu);
	} else {
		no_j0xb0 = 0;		// no velocity field evolution, means no_j0xb0 is useless.
	}
	if (evol_ubt & EVOL_B) {
		Blm.alloc(Blm.ir_bci, Blm.ir_bco);
		Blm.bci = BC_MAGNETIC;	Blm.bco = BC_MAGNETIC;
		init_Bmatrix(Blm, jpar.eta);
	}
	if (evol_ubt & EVOL_T) {
		Tlm.alloc(Tlm.ir_bci, Tlm.ir_bco);
		Tlm.bci = (boundary) jpar.tBCin;	Tlm.bco = (boundary) jpar.tBCout;
		init_Tmatrix(Tlm, MT, jpar.kappa);
	}
	if (evol_ubt & EVOL_C) {
		Clm.alloc(Clm.ir_bci, Clm.ir_bco);
		Clm.bci = (boundary) jpar.cBCin;	Clm.bco = (boundary) jpar.cBCout;
		init_Tmatrix(Clm, MC, jpar.kappa_c);
	}

/* INITIALIZATION OF FIELDS */

	/* INIT MAGNETIC FIELDS */
	b0 = 0.0;
	if (evol_ubt & EVOL_B) {
		printf("=> Initial magnetic field b :\n");
		ftime = load_init(jpar.ffile[B], &Blm);
		Blm.bci = BC_MAGNETIC;		Blm.bco = BC_MAGNETIC;		// reset the right BC for magnetic field.
		InnerCore.set_external_B(Blm.Pol[Blm.irs-1]);		// copy boundary condition for future evolution.
		Mantle.set_external_B(   Blm.Pol[Blm.ire+1]);
		if (InnerCore.PB0lm != NULL) printf("   + Potential field imposed at inner boundary (lmax=%d, mmax=%d).\n", InnerCore.lmax, MRES*InnerCore.mmax);
		if (Mantle.PB0lm != NULL)    printf("   + Potential field imposed at outer boundary (lmax=%d, mmax=%d).\n", Mantle.lmax, MRES*Mantle.mmax);
	}

	/* INIT TEMPERATURE FIELDS */
	gmax = 0.0;
	if (evol_ubt & EVOL_T) {
		if (jpar.phi0file) {		/* INIT IMPOSED GRAVITY */
			StatSpecScal *Phi0 = NULL;
			ScalarSH *Phi = new ScalarSH(0, NR-1);
			printf("=> Imposed gravity potential Phi0 :\n");
			load_init(jpar.phi0file, Phi);
			i = Phi->make_static(&Phi0);
			if (i == 0) runerr("Imposed potential field has 0 modes !");		// probably something is wrong...
			printf("   + Imposed potential field has %d modes (l<=%d, m<=%d).\n",i,Phi0->lmax,Phi0->mmax);
			if (evol_ubt & EVOL_U) {
				if  (Phi0->lmax == 0) {
					Grav = NULL;		// no need for spatial gravity in the case l=0 (central gravity).
					Grav0_r = (double*) malloc(sizeof(double) * (NM-NG+1));
					Grav0_r -= NG;		// shift to access fluid shells.
					for (i=NG; i<=NM; i++) {
						Grav0_r[i] = Phi0->TdT[2*i+1].real()/(Y00_1*r[i]);		// dPhi/dr already computed.
						if (fabs(Grav0_r[i]*r[i]) > gmax) gmax = fabs(Grav0_r[i]*r[i]);		// record gmax
					}
					if (r[NG] == 0.0)  Grav0_r[0] = 0.0;		// should be zero gravity at r=0 (for l=0).
				} else {
					Grav0_r = NULL;		// no central gravity.
					Grav = new VectField(NG,NM);		// spatial gravity field needed for Navier-Stokes equation.
					Grav->from_gradSH(Phi);
					gmax = Grav->absmax();
				}
				if (gmax == 0.0) runerr("zero gravity, something wrong ?");
			}
			free_StatSpecScal(Phi0);
			delete Phi;
		} else {
			Grav = NULL;		Grav0_r = NULL;
			printf("=> No gravity field, temperature treated as a passive scalar.");
		}
		if (jpar.f0file[T]) {
			printf("=> Imposed Temperature field T0 :\n");
			load_init(jpar.f0file[T], &Tlm);
			i = Tlm.make_static(&T0lm);
			if (i == 0) runerr("Imposed temperature field has 0 modes !");		// probably something is wrong...
			printf("   + Imposed temperature field has %d modes (l<=%d, m<=%d).\n",i,T0lm->lmax,T0lm->mmax);
			Tlm.bci = (boundary) jpar.tBCin;	Tlm.bco = (boundary) jpar.tBCout;		// reset the right BC for Tlm
		}
		printf("=> Temperature field boundary conditions (%d, %d)\n",Tlm.bci, Tlm.bco);

		printf("=> Initial temperature field t :\n");
		ftime = load_init(jpar.ffile[T], &Tlm);
		Tlm.bci = (boundary) jpar.tBCin;		Tlm.bco = (boundary) jpar.tBCout;		// reset the right BC.
		if (Tlm.bci == BC_ZERO) Tlm.zero_out_shell(Tlm.irs);	// ensure zero boundary conditions.
		if (Tlm.bco == BC_ZERO) Tlm.zero_out_shell(Tlm.ire);
	}
	if (evol_ubt & EVOL_C) {
		if (jpar.f0file[C]) {
			printf("=> Imposed Composition field C0 :\n");
			load_init(jpar.f0file[C], &Clm);
			i = Clm.make_static(&C0lm);
			if (i == 0) runerr("Imposed temperature field has 0 modes !");		// probably something is wrong...
			printf("   + Imposed composition field has %d modes (l<=%d, m<=%d).\n",i,C0lm->lmax,C0lm->mmax);
			Clm.bci = (boundary) jpar.cBCin;	Clm.bco = (boundary) jpar.cBCout;		// reset the right BC for Tlm
		}
		printf("=> Composition field boundary conditions (%d, %d)\n",Clm.bci, Clm.bco);

		printf("=> Initial composition field c :\n");
		ftime = load_init(jpar.ffile[C], &Clm);
		Clm.bci = (boundary) jpar.cBCin;		Clm.bco = (boundary) jpar.cBCout;		// reset the right BC.
		if (Clm.bci == BC_ZERO) Clm.zero_out_shell(Clm.irs);	// ensure zero boundary conditions.
		if (Clm.bco == BC_ZERO) Clm.zero_out_shell(Clm.ire);
	}

	/* INIT VELOCITY FIELDS */
	if (evol_ubt & EVOL_U) {
		printf("=> Velocity field boundary conditions (%d, %d)\n",Ulm.bci, Ulm.bco);
		Ulm.zero_out();
		if (jpar.ffile[U]) {
			printf("=> Initial velocity field u :\n");
			ftime = load_init(jpar.ffile[U], &Ulm);
			Ulm.bci = (boundary) jpar.uBCin;		Ulm.bco = (boundary) jpar.uBCout;		// reset the right BC.
			if (Ulm.bci == BC_ZERO) Ulm.zero_out_shell(Ulm.irs);	// ensure zero boundary conditions.
			if (Ulm.bco == BC_ZERO) Ulm.zero_out_shell(Ulm.ire);
		}
	}

/* PRINT GRID */
	printf("[Grid] NR=%d => NG=%d, NM=%d, [rmin,rg,rm,rmax]=[%.4f, %.4f, %.4f, %.4f]\n       [dr(rmin), dr(rg), dr((NG+NM)/2), dr(rm), dr(rmax)]=[%.5e, %.5e, %.5e, %.5e, %.5e]\n", NR, NG,NM, r[0], r[NG], r[NM], r[NR-1], r[1]-r[0], r[NG+1]-r[NG], r[(NG+NM)/2+1]-r[(NG+NM)/2], r[NM]-r[NM-1], r[NR-1]-r[NR-2]);
	if ((r[0] != rmin)||(r[NR-1] != rmax)) printf("    !! Warning : rmin or rmax have changed !\n");

/* PRINT PARAMS */
	const double Omega0 = jpar.Omega0;
	const double dt = jpar.dt;		// time step.
	const char* job = jpar.job;
	printf("[Params] job name : ***  %s  ***\n",job);
	if (Omega0 != 0.0)
	printf("         Ek=%.2e, Ro=%.2e, Elsasser=%.2e, Lehnert=%.2e\n",jpar.nu/Omega0, u0/Omega0, b0*b0/(jpar.eta*Omega0), b0/Omega0);
	printf("         Pm=%.2e, Re=%.2e, S=%.2e, N=%.2e, M=%.2e\n",jpar.nu/jpar.eta, u0/jpar.nu, b0/jpar.eta, b0*b0/(u0*u0) , b0/sqrt(jpar.nu*jpar.eta));
	if ((Grav)||(Grav0_r))
	printf("         Pr=%.2e, gmax=%.2e, DeltaT=%.2e, Ra=%.2e, N^2=%.2e\n",jpar.nu/jpar.kappa, gmax, 0.0, gmax/(jpar.nu*jpar.kappa), 0.0);		// TODO: compute numbers.
	printf("         dt.Omega=%.2e, dt.nu.R^2=%.2e, dt.eta.R^2=%.2e\n",dt*Omega0, dt*jpar.nu*rmax*rmax, dt*jpar.eta*rmax*rmax);

/* COMPUTE FORCES */
	printf("NG=%d, NM=%d\n",NG, NM);
	NLu1.alloc(NG,NM);		NLu2.alloc(NG,NM);				NLt1.alloc(NG, NM);
	const int d_ir = 10;		// number of shells to exclude for visous force accounting.
	SpectralDiags sd(LMAX,MMAX);

	LinOp3ld Lap;
	Lap.alloc(NG+1,NM-1,LMAX);
	for (int i=NG+1; i<=NM-1; i++)	Lap.set_Laplace(i);

//	double scale = 0.01;
//	printf("WARNING: performing filter_scale(%g) !!!\n", scale);
//	Ulm.filter_scale(scale);		Blm.filter_scale(scale);		Tlm.filter_scale(scale);
	if (evol_ubt & EVOL_U) {
		Ux.alloc(NG, NM);

	  #pragma omp parallel
		Ulm.to_spat(&Ux);
	// Buoyancy power
	/*	if (evol_ubt & EVOL_T) {
			double *t;
			cplx *tlm;
			t = (double*) VMALLOC((shtns->nspat * sizeof(double));
			tlm = (cplx*) VMALLOC(NLM*sizeof(cplx));
			double Pw = 0.0;
			for (int ir=Ux.irs; ir<=Ux.ire; ir++) {
				double galpha = Grav0_r[ir]*r[ir];
				for (int lm=0; lm<NLM; lm++) {
					tlm[lm] = Tlm[ir][lm];
				}
				tlm[0] += T0lm->TdT[2*ir];
				SH_to_spat(shtns, tlm, t);
				for (int lm=0; lm<shtns->nspat; lm++) {
					t[lm] *= Ux.vr[ir][lm] * galpha;
				}
				spat_to_SH(shtns, t, tlm);
				Pw += Ulm.delta_r(ir)*real(tlm[0])*r[ir]*r[ir];
			}
			VFREE(tlm);
			VFREE(t);
			Pw *= 4.*M_PI/Y00_1 / shell_volume(r[Ux.irs],r[Ux.ire]);
			printf("  Buoyancy power = %g\n",Pw);
			return 0;
		}	*/
		W.alloc(NG, NM);
	// Coriolis force
		if (Omega0 != 0.0) {
		  #pragma omp parallel
			W.NL_Fluid_Coriolis(&Ux, Omega0);		// U ^ 2Omega0
			//W.write_hdf5("F_coriolis.h5", NG, NM);
			//zero_out_TC(W);
			tmp = W.energy(NG+d_ir, NM-d_ir);
			printf("Coriolis rms = %g\n",tmp);

			NLu2.zero_out();
//		  #pragma omp parallel
//			Coriolis_force_add(NLu2, Ulm, Omega0);
			NLu2.energy(sd, NG+d_ir, NM-d_ir);		// NLu2 = Coriolis curl
			printf("Coriolis curl = %g\n",sd.energy());
			NLu2.save_single("fieldFcori", LMAX, MMAX, ftime);
		}


	// Inertial force
	  #pragma omp parallel
	  {
		Ulm.to_curl_spat(&W, NG, NM, 0.0);	// omega
		W.NL_vect(&W, &Ux);		// omega ^ u
	  }
		tmp = W.energy(NG+d_ir, NM-d_ir);
		printf("Inertia (w^u) rms = %g\n",tmp);

	  #pragma omp parallel
	  {
		Ux.NL_dot(&Ux, &Ux);		// u^2
		NLt1.from_spat(&Ux);
		NLt1 *= 0.5;
		NLt1.to_grad_spat(&Ux);	// 0.5*grad(u^2)
		#pragma omp for
		for (int ir=Ux.irs; ir<=Ux.ire; ir++) {
			for (int lm=0; lm<shtns->nspat; lm++) {
				W.vr[ir][lm] += Ux.vr[ir][lm];
				W.vt[ir][lm] += Ux.vt[ir][lm];
				W.vp[ir][lm] += Ux.vp[ir][lm];
			}
		}
	  }
		tmp = W.energy(NG+d_ir, NM-d_ir);
		printf("Inertia (total) rms = %g\n",tmp);
	  #pragma omp parallel
		NLu1.curl_from_spat(&W);

		//W.write_hdf5("F_inertia.h5", NG, NM);
		//zero_out_TC(W);
		//W.energy(sd, NG+d_ir, NM-d_ir);
		//printf("Inertia (no tc) rms = %g\n",sd.energy());

		NLu1.energy(sd, NG+d_ir, NM-d_ir);
		printf("Inertia curl = %g\n",sd.energy());
		NLu1.save_single("fieldFadvc", LMAX, MMAX, ftime);
		NLu2 -= NLu1;								// inertia is rhs.
		NLu2.energy(sd, NG+d_ir, NM-d_ir);
		printf(" total curl = %g\n",sd.energy());

	// viscous force
		NLu1.zero_out();
		Lap.apply(Ulm.Tor, NLu1.Tor, 0, NLM-1);
		Lap.apply(Ulm.Pol, NLu1.Pol, 0, NLM-1);
		NLu1 *= jpar.nu;			// scale by viscosity
		NLu1.energy(sd, NG+d_ir,NM-d_ir);
		printf("Viscous rms = %g\n",sd.energy());
		//NLu1.to_spat(&W);
		//W.write_hdf5("F_visc.h5", NG, NM);
		//zero_out_TC(W);
		//tmp = W.energy(sd, NG+d_ir,NM-d_ir);
		//printf("Viscous rms (spat) = %g\n",tmp);
		NLu1.curl();
		NLu1.energy(sd, NG+d_ir,NM-d_ir);
		printf("Viscous curl = %g\n",sd.energy());
		NLu1.save_single("fieldFvisc", LMAX, MMAX, ftime);

	//	NLu2 += NLu1;
		NLu2.energy(sd, NG+d_ir, NM-d_ir);
		printf(" total curl = %g\n",sd.energy());
		Ux.free_field();		W.free_field();
	}

	// buoyancy force
	if (evol_ubt & EVOL_T) {
		Radial_gravity_buoyancy(NLu2, Tlm, Grav0_r);		// add buoyancy due to central gravity.
		NLu2.energy(sd, NG+d_ir, NM-d_ir);
		printf(" total with temperature buoyancy curl = %g\n",sd.energy());

		Tx.alloc(Tlm.irs, Tlm.ire);
		for (int ir=Tlm.irs; ir<=Tlm.ire; ir++) {
			double galpha = Grav0_r[ir]*r[ir];
			for (int lm=0; lm<NLM; lm++)  NLt1[ir][lm] = Tlm[ir][lm] * galpha;		// compute force
			NLt1[ir][0] = 0.0;		// remove l=0 component (equilibrated by pressure gradient)
		}
		NLt1.energy(sd, NG+d_ir, NM-d_ir);
		printf("Buoyancy rms (l>0) = %g\n",sd.energy());
	  #pragma omp parallel
		NLt1.to_spat(&Tx);
		//zero_out_TC(Tx);
		//tmp = Tx.energy(sd, NG+d_ir, NM-d_ir);
		//printf("Buoyancy rms (l>0, spat) = %g\n",tmp);
		//Tx.write_hdf5("F_buoy.h5",NG,NM);
		Tx.free_field();
		W.alloc(NG,NM);		W.zero_out();
	  #pragma omp parallel for
		for (int ir=Tlm.irs; ir<=Tlm.ire; ir++) {
			for (int lm=0; lm<NLM; lm++)  NLt1[ir][lm] = Tlm[ir][lm] * Grav0_r[ir];
			SHtor_to_spat(shtns, NLt1[ir], W.vt[ir], W.vp[ir]);
		}
		tmp = W.energy(NG+d_ir, NM-d_ir);
		printf("Buoyancy curl = %g\n",tmp);
		W.free_field();

		NLu1.zero_out();
		for (int ir=Tlm.irs; ir<=Tlm.ire; ir++) {
			for (int lm=0; lm<NLM; lm++)  NLu1.Tor[ir][lm] = Tlm[ir][lm] * Grav0_r[ir];
		}
		NLu1.energy(sd, NG+d_ir, NM-d_ir);
		printf("Buoyancy curl (2) = %g\n",sd.energy());
		NLu1.save_single("fieldFbuoy", LMAX, MMAX, ftime);
	}
	if (evol_ubt & EVOL_C) {
		Radial_gravity_buoyancy(NLu2, Clm, Grav0_r);		// add buoyancy due to central gravity.
		NLu2.energy(sd, NG+d_ir, NM-d_ir);
		printf(" total with composition buoyancy curl = %g\n",sd.energy());

		Tx.alloc(Clm.irs, Clm.ire);
		for (int ir=Clm.irs; ir<=Clm.ire; ir++) {
			double galpha = Grav0_r[ir]*r[ir];
			for (int lm=0; lm<NLM; lm++)  NLt1[ir][lm] = Clm[ir][lm] * galpha;		// compute force
			NLt1[ir][0] = 0.0;		// remove l=0 component (equilibrated by pressure gradient)
		}
		NLt1.energy(sd, NG+d_ir, NM-d_ir);
		printf("Composition Buoyancy rms (l>0) = %g\n",sd.energy());
	  #pragma omp parallel
		NLt1.to_spat(&Tx);
		//zero_out_TC(Tx);
		//tmp = Tx.energy(sd, NG+d_ir, NM-d_ir);
		//printf("Buoyancy rms (l>0, spat) = %g\n",tmp);
		//Tx.write_hdf5("F_buoy.h5",NG,NM);
		Tx.free_field();
		W.alloc(NG,NM);		W.zero_out();
	  #pragma omp parallel for
		for (int ir=Clm.irs; ir<=Clm.ire; ir++) {
			for (int lm=0; lm<NLM; lm++)  NLt1[ir][lm] = Clm[ir][lm] * Grav0_r[ir];
			SHtor_to_spat(shtns, NLt1[ir], W.vt[ir], W.vp[ir]);
		}
		tmp = W.energy(NG+d_ir, NM-d_ir);
		printf("Composition Buoyancy curl = %g\n",tmp);
		W.free_field();		NLt1.free_field();

		NLu1.zero_out();
		for (int ir=Clm.irs; ir<=Clm.ire; ir++) {
			for (int lm=0; lm<NLM; lm++)  NLu1.Tor[ir][lm] = Clm[ir][lm] * Grav0_r[ir];
		}
		NLu1.energy(sd, NG+d_ir, NM-d_ir);
		printf("Composition Buoyancy curl (2) = %g\n",sd.energy());
		NLu1.save_single("fieldFbuoy2", LMAX, MMAX, ftime);
	}
	NLt1.free_field();
		
	// Laplace force
	if (evol_ubt & EVOL_B) {
		Bx.alloc(NG, NM);		J.alloc(NG, NM);
	  #pragma omp parallel
	  {
		Blm.to_spat(&Bx, NG, NM);
		Blm.to_curl_spat(&J, NG, NM);
		J.NL_vect(&J, &Bx);
	  }
		//J.write_hdf5("F_laplace.h5", NG, NM);
		//zero_out_TC(J);
		tmp = J.energy(NG+d_ir, NM-d_ir);
		printf("Laplace rms = %g\n",tmp);
		NLu1.free_field();
		NLu1.alloc(NG,NM);
	  #pragma omp parallel
		NLu1.curl_from_spat(&J);
		NLu1.energy(sd, NG+d_ir, NM-d_ir);
		printf("Laplace curl = %g\n",sd.energy());
		NLu1.save_single("fieldFlapl", LMAX, MMAX, ftime);
		NLu2 += NLu1;
	}

	NLu2.energy(sd, NG+d_ir, NM-d_ir);
	printf("du/dt curl = %g\n",sd.energy());
	NLu2.save_single("fieldFdudt", LMAX, MMAX, ftime);

	NLu2.zero_out();
	Ux.alloc(NG,NM);		W.alloc(NG,NM);
  #pragma omp parallel
	calc_dw_dt(NLu2, Ulm, Blm, Tlm, Omega0);
	NLu2.energy(sd, NG+d_ir, NM-d_ir);
	printf("du/dt curl (2) = %g\n",sd.energy());
	NLu1.zero_out();
	Lap.apply(Ulm.Tor, NLu1.Tor, 0, NLM-1);
	Lap.apply(Ulm.Pol, NLu1.Pol, 0, NLM-1);
	NLu1 *= jpar.nu;			// scale by viscosity
	NLu1.curl();
	NLu2 += NLu1;
	NLu2.energy(sd, NG+d_ir, NM-d_ir);
	printf("du/dt curl (3) = %g\n",sd.energy());

	// induction term
	// B and U should be up to date (since calc_dw_dt):
	if (evol_ubt & EVOL_B) {
		NLu2.free_field();
		NLu2.alloc(NG,NM);
		NLu2.zero_out();
	  #pragma omp parallel
	  {
		Ux.NL_vect(&Ux, &Bx);		// U x B
		NLu2.curl_from_spat(&Ux);
	  }
		NLu2.energy(sd, NG+d_ir, NM-d_ir);
		printf("Induction term = %g\n",sd.energy());
		NLu2.save_single("fieldFindu", LMAX, MMAX, ftime);

		// Ohmic diffusion
		NLu1.free_field();
		NLu1.alloc(NG,NM);
		NLu1.zero_out();
		Lap.apply(Blm.Tor, NLu1.Tor, 0, NLM-1);
		Lap.apply(Blm.Pol, NLu1.Pol, 0, NLM-1);
		NLu1 *= jpar.eta;			// scale by diffusivity
		NLu1.energy(sd, NG+d_ir,NM-d_ir);
		printf("Ohmic diffusion rms = %g\n",sd.energy());

		// dB/dt
		NLu2 += NLu1;
		NLu2.energy(sd, NG+d_ir,NM-d_ir);
		printf("dB/dt rms = %g\n",sd.energy());
		//NLu2.save_single("fieldFdbdt", LMAX, MMAX, ftime);
	}

	return(0);
}
