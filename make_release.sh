#!/bin/bash

if [ "x$1" == "x" ]; then
	echo "usage: $0 <shtns_version>"
	exit
fi

shtns_ver=$1
shtns_tar=/tmp/shtns.tar.gz

#xs_id=`git rev-parse --short HEAD`
xs_id=`git describe --tag --always`

## 1) shtns archive
pushd shtns
rm -f $shtns_tar
git archive -o $shtns_tar --prefix=shtns/ v$shtns_ver
## display the archived tag:
echo "including shtns v$shtns_ver"
popd

## 2) xshells archive
xs_ver=`git describe --tags | sed "s/^v//"`
xs_tar=/tmp/xshells-${xs_ver}_shtns-${shtns_ver}.tar.gz
echo "xshells: $xs_ver > $xs_tar"
rm -f $xs_tar
git archive -o $xs_tar --prefix=xshells-$xs_ver/ HEAD

## 3) merge both, fix xshells id
pushd /tmp
tar xzf $xs_tar
rm $xs_tar
pushd xshells-$xs_ver
sed -i "s/^git_id=.*/git_id=$xs_id/" Makefile.in
tar xzf $shtns_tar
rm -rf shtns/shtns_jni
popd
tar czf $xs_tar xshells-$xs_ver/*

## done
popd


