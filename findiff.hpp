/// FINITE DIFFERENCE APPROXIMATION ///

void fd_deriv_o2(const double* x, const int i, double* Dx, double* D2x = 0);

/// boundary condition specified by:  a0*F + a1*F' + a2*F" = ghost
/// sign of dx specifies inner (dx > 0) or outer (dx < 0) boundary condition
void fd_deriv_o2_bc(const double dx, const double a0, const double a1, const double a2,
					double* Dx, double* D2x = 0);

void fd_deriv_o2_bc(const double dx, const int dk_zero, double* Dx, double* D2x = 0);


void fd_deriv_o4(const double* x, const int i, double* Dx, double* D2x = 0, double* D3x = 0, double* D4x = 0);

/// Boundary condition sepcified by F^(k)[i_bc] = ghost
/// where k = dk_zero is the order of the derivative specified in the ghost shell.
void fd_deriv_o4_bc(const double* x, const int i, const int i_bc, const int dk_zero, double* Dx, double* D2x = 0, double* D3x = 0, double* D4x = 0);

