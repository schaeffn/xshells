#!/usr/bin/python

## Usage:
## 1) run all tests:
##      ./test.py
## 2) run selected tests:
##      ./test.py geodynamo -stepper=PC2 -exe=./xsbig_hyb

## define test cases with xshells.hpp files, a list of xshells.par files that contain
## special TEST comments, and executables to build and test.
problems = [
        {'name': 'MAGNETIC DIPOLE DECAY RATE & KINEMATIC DYNAMO',
    'hpp': 'problems/kinematic_dynamos/xshells.hpp',
    'lst': ['problems/kinematic_dynamos/xshells.par', 'problems/kinematic_dynamos/xshells.par.dudley_james_s2t2', ],
    'exe': ['./xsbig', './xsbig_mpi', './xsbig_hyb']},
        {'name': 'GEODYNAMO BENCHMARK',
    'hpp': 'problems/geodynamo/xshells.hpp',
    'lst': ['problems/geodynamo/xshells.par', 'problems/geodynamo/xshells.par.bench2', ],
    'exe': ['./xsbig', './xsbig_mpi', './xsbig_hyb', './xsbig_hyb2']},
        {'name': 'FULL-SPHERE BENCHMARK',
    'hpp': 'problems/full_sphere/xshells.hpp',
    'lst': ['problems/full_sphere/xshells.par.fs3', 'problems/full_sphere/xshells.par.fs1', ],
    'exe': ['./xsbig', './xsbig_mpi', './xsbig_hyb', './xsbig_hyb2']},
        {'name': 'FULL-SPHERE PRECESSION',
     'hpp': 'problems/precession_sphere/xshells.hpp',
    'lst': ['problems/precession_sphere/xshells.par','problems/precession_sphere/xshells.par.rotframe', ],
    'exe': ['./xsbig', './xsbig_mpi', './xsbig_hyb', './xsbig_hyb2']},
        {'name': 'LIBRATION',
    'hpp': 'problems/libration/xshells.hpp',
    'lst': ['problems/libration/xshells.par.liblong_E1e-4', 'problems/libration/xshells.par.liblong_E1e-4_frame', 'problems/libration/xshells.par.liblat_E1e-6', ],
    'exe': ['./xsbig', './xsbig_mpi', './xsbig_hyb']},
        {'name': 'SPHEROID PRECESSION',
    'hpp': 'problems/precession_spheroid/xshells.hpp',
    'dat': ['problems/precession_spheroid/fieldU.init', ],
    'lst': ['problems/precession_spheroid/xshells.par', ],
    'exe': ['./xsbig',]},
        {'name': 'COUETTE',
    'hpp': 'problems/couette/xshells.hpp',
    'lst': ['problems/couette/xshells.par', 'problems/couette/xshells.par.dormy98', ],
    'exe': ['./xsbig', './xsbig_mpi', './xsbig_hyb']},
        {'name': 'STRESS FREE SOLID BODY ROTATION',
    'hpp': 'problems/sf_sbr/xshells.hpp',
    'lst': ['problems/sf_sbr/xshells.par'],
    'exe': ['./xsbig']},
       ]

# time steppers to test:
steppers = ['PC2', 'BPR353', 'CNAB2', 'SBDF2', 'SBDF3']

global_opt = ['-shared_mem=-1','-bidir_solve=1']   # with shared memory and bidirectional solves


import numpy as np
import re
from subprocess import call
from shutil import copyfile, copy
import os
import time
import sys
sys.path.insert(0, './python')   # allow to find the module xsplot, even if it is not installed:
import xsplot
import pyxshells
import signal

## some init code:
if hasattr(time,'perf_counter'):
    mytime = time.perf_counter
else:
    mytime = time.time


## Handle command line arguments:
exe=[]
for a in sys.argv[1:]:
    if a.startswith('-stepper'):   # select stepper
        steppers=[a[9:],]
    elif a.startswith('-exe'):     # select executable
        exe.append(a[5:])
    else:   # reduce the problems to the ones matching string given as argument (if given)
        pb2 = []
        for pb in problems:
            if a in pb['name'].lower():
                pb2.append(pb)
        problems = pb2
if len(exe) > 0:
    for pb in problems:
        pb['exe'] = exe


## functions

prev_diag_file = None   # we store a previous diagnostic for the same case with a different executable; these should give nearly exactly the same result

def compare_with_ref(diag_file, time, diag_dict, file_dict):
    """ compare result of run with a reference, for testing purposes """
    global prev_diag_file
    y=xsplot.load_diags(diag_file, print_header=False)
    w = np.where(y['t'] == time)[0]  # compare values at given time
    if len(w) == 0:
        return(1)   # time not available => error
    it = w[0]
    err_max = 0
    for d in diag_dict.keys():
        ref = diag_dict[d]  if prev_diag_file is None else  prev_diag_file[d][it]
        if d not in y:
            print(bcolors.FAIL + '!! diagnostic %s unavaiable !!' % d + bcolors.ENDC)
            return(2)   # diagnostic not available => error
        run = y[d][it]
        if np.isnan(run):
            return(Inf) # Nan ? => error
        err = run-ref
        if ref != 0.:
            err = err/ref
        err_max = max( err_max, abs(err) )
        print('  * ', d, ': ref=%g, run=%g  (err=%.2g)' % (ref,run,err))
    if prev_diag_file is None:  prev_diag_file = y

    if file_dict is not None:
        for f in file_dict.keys():
            run = pyxshells.load_field(f).energy()
            ref = file_dict[f]
            err = (run-ref)/ref
            err_max = max( err_max, abs(err) )
            print('  * ', f, ': ref=%g, run=%g  (err=%.2g)' % (ref,run,err))
    return(err_max)

def run_test(exe, job, cmd, parfile, stepper='PC2'):
    v = {'ref':None, 'time':None, 'opt':'', 'tol':5e-4, 'files':None}   # uninitialize
    exec(cmd,v)     # set variables
    nrj_file = 'energy.'+job
    if exe is not None:
        nthreads=4
        callpar = v['opt'].split()
        callpar = [exe, parfile] + callpar + global_opt
        if exe == './xsbig_mpi':
            callpar = ['mpirun', '-n', '4'] + callpar
        elif exe in ['./xsbig_hyb', './xsbig_hyb2']:
            callpar = ['mpirun', '--bind-to','none', '-n','2'] + callpar
            nthreads = 2
        callpar.append('-restart=0')  # we always want to start from scratch
        if v['files'] is None:
            callpar.append('-movie=0')  # we don't want to write so many files
        callpar.append('-stepper='+stepper)
        print(' '.join(callpar))
        if os.path.isfile(nrj_file):
            os.remove(nrj_file)
        with open('log.'+job, "w") as logfile:
            tex=mytime()
            ev=os.environ.copy()
            ev['OMP_NUM_THREADS'] = '%d' % nthreads
            call(callpar, stdout=logfile, env=ev)
            tex=mytime()-tex
    errmax = compare_with_ref(nrj_file, v['time'], v['ref'], v['files'])
    return(errmax,tex,v['tol'])

def make_exe(hpp, exe):
    copyfile(hpp, 'xshells.hpp')
    with open('xshells.hpp','a') as f:
        f.write('\n#define XS_DEBUG 1\n')   # append XS_DEBUG so that other runtime errors may be found
    os.system('make -j4 ' + ' '.join(exe))

## color in output:
class bcolors:
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'

jobreg = re.compile(r"job\s*=\s*([^#\s]*)")
passed, suspected, failed = 0,0,0

def print_summary(signum, frame):
    print(bcolors.BOLD + '**** SUMMARY ****' + bcolors.ENDC)
    if passed > 0:
        print(bcolors.OKGREEN + '   %d tests OK' % passed +bcolors.ENDC)
    if suspected > 0:
        print(bcolors.WARNING + '   %d tests suspect' % suspected +bcolors.ENDC)
    if failed > 0:
        print(bcolors.FAIL + '   %d tests FAILED' % failed +bcolors.ENDC)
        sys.exit(failed)
    sys.exit(signum)

signal.signal(signal.SIGINT, print_summary)

for pb in problems:
    exe = pb['exe']
    lst = pb['lst']
    print(bcolors.BOLD+bcolors.WARNING + '####', pb['name'], '####'+bcolors.ENDC)
    make_exe(pb['hpp'], exe)
    if 'dat' in pb.keys():
        for dat in pb['dat']:
            copy(dat,'./')
    for case in lst:
        tests = []
        f = open(case,'r')
        l = f.readline()
        while l != '':
            if l.startswith("#TEST "):
                tests.append(l.replace("#TEST ","").strip())
            if l[0] != '#':
                x = l.split('#')[0]  # remove everything after the #
                jobs = jobreg.findall(x)
                if len(jobs) > 0:
                    job = jobs[0]
            l=f.readline()

        print('  ***',case,'**',job,'***')
        for test in tests:
            for ts in steppers:
                prev_diag_file = None   # new test: do not compare to previous one
                for ex in exe:
                    exact = True  if prev_diag_file is not None else  False
                    err,tex,tol=run_test(ex, job, test, case, ts)
                    if exact:  tol = 1e-15  # very low tolerance when we run the exact same test with a different executable
                    print("\t max relative error = %.3g (tolerance=%.2g), runtime = %gs" % (err,tol,tex))
                    if err > 1e-2:
                        failed += 1
                        print(bcolors.FAIL+ "\t ERROR: more than 1% relative error with reference case. Something is wrong." +bcolors.ENDC)
                    elif err > tol:
                        suspected += 1
                        print(bcolors.WARNING+ "\t WARNING: differences with reference case are suspicious." +bcolors.ENDC)
                    else:
                        passed += 1
                        print(bcolors.OKGREEN + "\t PASS" +bcolors.ENDC)
                print('')

print_summary(0,0)
