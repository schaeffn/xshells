/*
 * Copyright (c) 2010-2020 Centre National de la Recherche Scientifique.
 * written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France).
 * 
 * nathanael.schaeffer@univ-grenoble-alpes.fr
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * 
 */

/// \file grid.cpp
/// Radial finite difference setup.

/** \file grid.cpp
  x-banded matrix inversion (x = 3,5).
	- Boundary condition are taken into acount (or not !).
	- For temporal semi-implicit evolution of fields.
	- Matrices have size NR*(LMAX+1), decomposition is done on NR.
*/

// Handles the various OpenMP possibilities (XS_OMP=1 by default).
#ifndef _OPENMP
	#undef XS_OMP
	#define XS_OMP 0
#else
	#ifndef XS_OMP
		#define XS_OMP 1
	#endif
#endif

#include <complex>
typedef std::complex<double> cplx;

#include <cstdint>		// for uintptr_t

/// finite difference approximations to derivatives
#include "findiff.hpp"

/// floating point not equal, with relative tolerance tol.
inline bool differ(double a, double b, const double tol=1e-12) {
	return (fabs(a-b) > tol*fabs(a+b));
}


/* SPHERICAL HARMONICS using SHTns v2*/
/// \name global variables for spherical harmonics.
//@{
shtns_cfg shtns = NULL;
int NLM, LMAX, MMAX;		// spherical harmonic sizes.
int MRES=1;
int NLAT=0;
int NPHI=0;
int NLAT_PADDED=0;
shtns_norm sht_norm;
const unsigned short *li;		// integer l array
double *__restrict el;
double *__restrict l2;
double *__restrict l_2;
double *__restrict em;		// m-array
const double *ct;
const double *st;
const double *wg;	// gauss weights for integration.
#ifdef VAR_LTR
unsigned short *__restrict ltr = NULL;		///< dynamic l truncation.
#endif
double *st_dt_mx;		// sparse matrix to compute sin(theta).d/dtheta in spectral space
//@}

/// \name global variables for spatial discretization.
//@{
int NR;	///< total number of radial shells
int NG;	///< radial shell for the solid inner core limit
int NM;	///< radial shell for the base of the mantle

#ifdef XS_MPI
  int i_mpi, n_mpi;				// mpi rank and number of processes.
  int irs_mpi, ire_mpi;			// limits (inclusive) for this mpi process (excluding ghost shells)
  int irs_shared, ire_shared;	// limits (inclusive) for shared memory access (excluding ghost shells)
  int i_mpi_shared = 0;			// mpi rank for the sharing memory communicator/group.
  int n_mpi_shared = 1;			// mpi number of ranks sharing memory.
  MPI_Comm comm_shared = MPI_COMM_SELF;		// MPI communicator for processes that can share memory.
  MPI_Comm comm_net = MPI_COMM_WORLD;		// MPI communicator for processes that cannot share memory.
  int i_mpi_net, n_mpi_net;		// mpi rank and number for inter-node communication (with same i_mpi_shared rank).
  int *__restrict r_owner = NULL;
  /// true if the shell is owned by me
  #define own(ir) (i_mpi == r_owner[ir])
#else
  #define irs_mpi 0
  #define ire_mpi (NR-1)
  #define i_mpi 0
  #define n_mpi 1
  // always true
  #define own(ir) 1
#endif
#define PRINTF0(...) if (i_mpi==0) { printf(__VA_ARGS__); fflush(stdout); }
double *__restrict r = NULL;	///< r[i] is the radius of grid points i<NR. NULL at init time.
double *__restrict r_1 = NULL;	///< 1/r at each grid point.
//@}

#undef LM_L_LOOP
#undef LM_LOOP2
#undef LM_L_LOOP2
///\def LM_L_LOOP : loop over all (l,im) and perform "action"  : l and lm are defined. (but NOT m and im)
// partial loop (useful for parallel operation)
#define LM_LOOP2( lms, lme, action ) { int lm=(lms); do { action } while(++lm <= (lme)); }

/*
int lms=blk0->lms;
for (blk_def* blk=blk0; blk<blk_end; ) {
	int l=blk->ls;
	//int m=blk->ms;
	int lme=(++blk)->lms;
	for (int ir=XX; ir<YY; ir++) {
		
		// read all from cache
		for (;  lm<=lme-VSIZE; lm+=VSIZE, l+=VSIZE) {
			// VECTORIZED
		}
		for (;  lm<lme; lm++, l++) {		// SCALAR
			
		}
	}
	lms=lme;
}


int lms=blk0->lms;
for (blk_def* blk=blk0; blk<blk_end; ) {
	int l=blk->ls;
	//int m=blk->ms;
	int lme=(++blk)->lms;

	for (;  lm<lme; lm++, l++) {		// SCALAR
		v2d a=
		v2d b=
		for (int ir=XX; ir<YY; ir++) {
			// read once, cache into registers.
			v2d c=
			a=b;
			b=c;
		}
	}
	lms=lme;
}
*/

#if XS_DEBUG > 1
// Each MPI process writes in a different file, with immediate effect. Can help to find deadlocks, or other mpi-related bugs
#define print_debug(...) { \
	char fn[32];	sprintf(fn,"debug_%03d.txt", i_mpi); \
	FILE* fp = fopen(fn,"a");	fprintf(fp, __VA_ARGS__ );	fclose(fp); }
#else
#define print_debug(...) {}
#endif

#define Y00_1 sh00_1(shtns)
#define Y10_ct sh10_ct(shtns)
#define Y11_st sh11_st(shtns)

#ifdef __VSX__
#define CACHE_LINE 128		// cache line size in bytes: 128 for IBM Power
#else
#define CACHE_LINE 64		// cache line size in bytes: 64 for x86 (default)
#endif

#if XS_VEC && __SSE2__
	// GCC vector support
	typedef double s2d __attribute__ ((vector_size (16)));		// vector that should behave like a real scalar for complex number multiplication.
	typedef double v2d __attribute__ ((vector_size (16)));		// vector that contains a complex number
	static const unsigned long long _abs_msk = 0x7FFFFFFFFFFFFFFFULL;		// a constant needed to take absolute value of vectors
	#define vxchg(a) ((v2d)_mm_shuffle_pd(a,a,1))					// swap the two elements of a vector of 2 doubles.
	#ifdef __AVX__
	#include <immintrin.h>
	typedef double v4d __attribute__ ((vector_size (32)));
	#define vall4(x) (v4d)_mm256_set1_pd(x)
	#define v2d_lo4(a) (v2d)_mm256_castpd256_pd128(a)
	#define vread4(mem, idx) (v4d)_mm256_loadu_pd( ((double*)(mem)) + (idx)*4 )
	#define vstore4(mem, idx, val) _mm256_storeu_pd(((double*)(mem)) + (idx)*4, val)
	inline static double reduce_add4(v4d a) {	// Latency=12c Skylake
		v2d t = (v2d)_mm256_castpd256_pd128(a) + (v2d)_mm256_extractf128_pd(a,1);
		return _mm_cvtsd_f64(t) + _mm_cvtsd_f64(_mm_unpackhi_pd(t,t));
	}
	inline static v4d vdup_x2(const double* a, const double* b) {   // intel=4c (+2c to read a and b pointers)
		v4d t1 = _mm256_set1_pd(*b);                    // rec. throughput 0.5, latency=3 (intel)
		v4d t0 = _mm256_castpd128_pd256(_mm_set1_pd(*a));       // rec. throughput 0.5, latency=2 (intel)
		return _mm256_blend_pd(t0,t1, 0xC);             // latency 1 (skl, rome) or 2 (knl)
	}
	inline static v4d vhadd4(v4d a, v4d b) {
		#ifndef __AVX512ER__
		return _mm256_hadd_pd(a,b);		// KNL: latency 15, rec.througput 8;  Skylake: lat 6, rec.th 2
		#else
		v4d t0 = _mm256_shuffle_pd(a, b, 0x5);	// a1,b0,a3,b2	// SKL: lat 1, rt 1;  KNL: lat 7, rt 2.
		v4d t1 = _mm256_blend_pd(a, b, 0xA);	// a0,b1,a2,b3	// SKL: lat 1, rt 0.33;  KNL: lat 2, rt 0.5.
		return t0+t1;	// SKL: lat 4, rt 0.5;  KNL: lat 6, rt 0.5
		#endif
	}
	inline static v2d vhadd2(v2d a, v2d b) {
		#ifndef __AVX512ER__
		return _mm_hadd_pd(a,b);		// KNL: latency 15, rec.througput 8;  Skylake: lat 6, rec.th 2
		#else
		v2d t0 = _mm_shuffle_pd(a, b, 0x1);	// a1,b0	// SKL: lat 1, rt 1;  KNL: lat 7, rt 2.
		v2d t1 = _mm_blend_pd(a, b, 0x2);	// a0,b1	// SKL: lat 1, rt 0.33;  KNL: lat 2, rt 0.5.
		return t0+t1;	// SKL: lat 4, rt 0.5;  KNL: lat 6, rt 0.5
		#endif
	}
	#endif
  #ifdef __AVX512F__
	#ifdef XS_DEBUG
		#warning "using AVX-512 vectorization"
	#endif
	#define VSIZE 8
	// number of rnd vectors in a 2D-array (NLAT must be multiple of 8)
	#define NV2D ((shtns->nspat)>>3)
	#define VNLAT (NLAT>>3)
	typedef double rnd __attribute__ ((vector_size (64)));
	#define vall(x) (rnd)_mm512_set1_pd(x)
	#define vread(mem, idx) (rnd)_mm512_loadu_pd( ((double*)(mem)) + (idx)*8 )
	#define vstore(mem, idx, val) _mm512_storeu_pd(((double*)(mem)) + (idx)*8, val)
	#define vmemadd(mem, idx, val) _mm512_storeu_pd(((double*)(mem)) + (idx)*8, val + vread(mem, idx))
	#define vabs_and_mask ((rnd)_mm512_castsi512_pd(_mm512_set1_epi64(_abs_msk)))
	#define vand(v, x) ((rnd)_mm512_castsi512_pd( _mm512_and_epi64(_mm512_castpd_si512(v), _mm512_castpd_si512(x))))
	#define vabs(a) (rnd)_mm512_abs_pd(a)
	#define vmax(a,b) (rnd)_mm512_max_pd(a,b)
	// mem=[0,1,2,3]  =>  a = [0,0,1,1,2,2,3,3]
	#define vread_dup(mem) (rnd)_mm512_permutexvar_pd( _mm512_set_epi64(3,3,2,2,1,1,0,0), _mm512_castpd256_pd512(_mm256_loadu_pd( (double*)(mem) )) )
	inline static v4d vread_dup4(const double* mem) {		// mem=[0,1]  =>  returns [0,0,1,1]
		#ifndef __AVX512ER__	// not KNL (total latency 4, rec.throughput 1)
		v4d t = _mm256_broadcast_pd( (__m128d*)(mem) );		// latency 3, rec.throughput 0.5
		return _mm256_shuffle_pd(t,t, 0xC);				// latency 1, rec.throughput 1
		#else   // on KNL, use other instructions with lower latency (total latency 7, rec. througput 1)
		v4d t0 = _mm256_broadcast_sd(mem);		// latency 5, rec. throughput 0.5
		v4d t1 = _mm256_broadcast_sd(mem+1);	// latency 5, rec. throughput 0.5
		return _mm256_blend_pd(t0,t1, 0xC);		// latency 2, rec. throughput 0.5
		#endif
	}
	#define vec_to_v2d(a) (v2d)_mm512_castpd512_pd128(a)
  #elif defined __AVX__
	#ifdef XS_DEBUG
		#warning "using AVX vectorization"
	#endif
	#define VSIZE 4
	// number of rnd vectors in a 2D-array (NLAT must be multiple of 4)
	#define NV2D ((shtns->nspat)>>2)
	#define VNLAT (NLAT>>2)
	typedef double rnd __attribute__ ((vector_size (32)));
	#define vall(x) (rnd)_mm256_set1_pd(x)
	#define vread(mem, idx) (rnd)_mm256_loadu_pd( ((double*)(mem)) + (idx)*4 )
	#define vstore(mem, idx, val) _mm256_storeu_pd(((double*)(mem)) + (idx)*4, val)
	#define vmemadd(mem, idx, val) _mm256_storeu_pd(((double*)(mem)) + (idx)*4, val + vread(mem, idx))
	#define vabs_and_mask ((rnd)_mm256_broadcast_sd((double*)&_abs_msk))
	//#define vabs_and_mask ((rnd)_mm256_castsi256_pd(_mm256_srli_epi64(_mm256_set1_epi64x(-1), 1)))
	#define vand(v, x) ((rnd)_mm256_and_pd(v, x))
	#define vabs(a) (rnd)_mm256_and_pd(a, _mm256_broadcast_sd((const double*)&_abs_msk))
	#define vmax(a,b) (rnd)_mm256_max_pd(a,b)
	inline static v4d vread_dup(const double* mem) {		// mem=[0,1]  =>  returns [0,0,1,1]
		v4d t = _mm256_broadcast_pd( (__m128d*)(mem) );		// latency 3, rec.throughput 0.5
		return _mm256_shuffle_pd(t,t, 0xC);				// latency 1, rec.throughput 1
	}
	#define vread_dup4 vread_dup
	#define vec_to_v2d(a) (v2d)_mm256_castpd256_pd128(a)
  #else
	#ifdef __SSE3__
		#include <pmmintrin.h>
		#ifdef XS_DEBUG
			#warning "using SSE3 vectorization"
		#endif
	#else
		#include <emmintrin.h>
		#ifdef XS_DEBUG
			#warning "using SSE2 vectorization"
		#endif
	#endif
	#define VSIZE 2
	// number of rnd vectors in a 2D-array (NLAT must be even)
	#define NV2D ((shtns->nspat)>>1)
	#define VNLAT (NLAT>>1)
	typedef double rnd __attribute__ ((vector_size (16)));
	#define vall(x) vdup(x)
	#define vread(mem, idx) ((s2d*)(mem))[idx]
	#define vstore(mem, idx, val) ((s2d*)(mem))[idx] = (val)
	#define vmemadd(mem, idx, val) ((s2d*)(mem))[idx] += (val)
	#define vabs_and_mask ((s2d)_mm_set1_pd(*(const double*)&_abs_msk))
	//#define vabs_and_mask ((s2d)_mm_castsi128_pd(_mm_srli_epi64(_mm_set1_epi64x(-1), 1)))
	#define vand(v, x) ((rnd)_mm_and_pd(v, x))
	#define vabs(a) (s2d)_mm_and_pd(a, _mm_set1_pd(*(const double*)&_abs_msk))
	#define vmax(a,b) _mm_max_pd(a,b)
	// mem=[0] => a=[0,0]
	#define vread_dup(mem) (rnd)_mm_set1_pd(*(mem))
	#define vec_to_v2d(a) (a)
  #endif
	#define vread2(mem, idx) (v2d)_mm_loadu_pd( ((double*)(mem)) + (idx)*2 )
	#define vstore2(mem, idx, val) _mm_storeu_pd(((double*)(mem)) + (idx)*2, val)
	inline static v2d IxKxZ(double k, v2d z) {		// I*k*z,  allowing to use FMA.
		return (v2d) _mm_setr_pd(-k,k) * vxchg(z);
	}
	// number of s2d vectors in a 2D-array (NLAT must be even)
	#define NS2D ((shtns->nspat)>>1)
	#define S2DNLAT (NLAT>>1)
	// vdup(x) takes a double and duplicate it to a vector of 2 doubles.
	#define vdup(x) (s2d)_mm_set1_pd(x)
	#ifdef __clang__
		// allow to compile with clang (llvm)
		#define VSUM(a) ( (a)[0]+(a)[1] )
	#else
		// gcc extensions
		#define VSUM(a) ( _mm_cvtsd_f64( (a) + (s2d)_mm_unpackhi_pd((a),(a))) )	//__builtin_ia32_vec_ext_v2df(a,0) + __builtin_ia32_vec_ext_v2df(a,1) )
	#endif
	#define VMALLOC(s)	_mm_malloc(s, (VSIZE < 8) ? 64 : VSIZE*8)
	#define VFREE(s)	_mm_free(s)
	#ifdef __AVX512F__
		#define v2d_lo(a) (v2d)_mm512_castpd512_pd128(a)
	#elif defined __AVX__
		#define v2d_lo(a) (v2d)_mm256_castpd256_pd128(a)
	#else
		#define v2d_lo(a) (a)
	#endif
#else
	#undef XS_VEC
	#define VSIZE 1
	typedef double rnd;
	typedef double s2d;
	typedef cplx v2d;
	#define vdup(x) (x)
	inline static cplx IxKxZ(double k, cplx z) {		// I*k*z
		return cplx(-k*imag(z), k*real(z));
	}
	#define VSUM(a) (a)
	// number of doubles in a 2D-array
	#define NV2D (shtns->nspat)
	#define NS2D (shtns->nspat)
	#define VNLAT NLAT
	#define S2DNLAT NLAT
	#define vall(x) (x)
	#define vread(mem, idx) ((double*)mem)[idx]
	#define vstore(mem, idx, val) ((double*)mem)[idx] = (val)
	#define vmemadd(mem, idx, val) ((double*)mem)[idx] += (val)
	#define vabs(a) fabs(a)
	inline double vmax(double a, double b) {  return (a > b) ? a : b;  }
    inline void* VMALLOC(size_t s) {
        void* p = 0;
        int align = CACHE_LINE;		// cache-line alignement
        //if (s >= 4096)	align = 4096;	// page alignement
        posix_memalign(&p, align, s);
        return p;
    }
    #define VFREE(s) free(s)
#endif

#define IN_RANGE_INCLUSIVE(i, low, high) ((unsigned)((i)-(low)) <= (unsigned)((high)-(low)))

#ifdef __GNUC__
#define LIKELY(x)    (__builtin_expect (!!(x), 1))
#define UNLIKELY(x)  (__builtin_expect (!!(x), 0))
#else
#define LIKELY(x)    (x)
#define UNLIKELY(x)  (x)
#endif

/// works for IEEE754 double (even when compiled with -ffinite-math or -ffast-math).
int isNaN(double d) {
	union { double d; unsigned long long i; } u = { d };
	return (u.i << 1) > 0xffe0000000000000ull;
}

#define COLOR_OK  "\033[92m"
#define COLOR_WRN "\033[93m"
#define COLOR_ERR "\033[91m"
#define COLOR_END "\033[0m"

[[noreturn]] void runerr(const char* error_text) {
#ifndef XS_MPI
    std::printf(COLOR_ERR "*** [xshells] Run-time error : %s " COLOR_END "\n", error_text);
#else
    std::printf(COLOR_ERR "*** [xshells mpi rank %d] Run-time error : %s " COLOR_END "\n", i_mpi, error_text);
#endif
    // If we do not flush, there is no guarantee that we will see the error message.
    std::fflush(stdout);
    std::fflush(stderr);
#if defined(XS_MPI)
    ::MPI_Abort(MPI_COMM_WORLD, 1);
    // Some MPI_Abort implementation may not kill the process so we exit()
    // anyway.
#endif
    std::exit(1);	// ruturn error code 1 = abort due to runtime error.
}

#define MEMBLOCK_ALIGN 64

/// A linear allocator in its simplest form.
/// Can borrow memory from another LinAlloc, which must not be used until the new one is destroyed.
/// All memory allocated is freed when the object is destroyed.
class LinAlloc {
  protected:
	char* mem_free = 0;		// pointer to the current free memory
	#ifdef XS_DEBUG
	char* mem_end = 0;		// pointer to the end of memory (for bounds check)
	uintptr_t* mem_orig = 0;	// pointer to the origin of the memory, if borrowed from another linear allocator.
	#endif
	LinAlloc() {}
  public:
	#ifdef XS_DEBUG
	LinAlloc(LinAlloc& mem) {
		mem_free = mem.mem_free;
		mem_end = mem.mem_end;
		mem_orig = (uintptr_t*) &mem.mem_free;
		if (*mem_orig & (MEMBLOCK_ALIGN-1)) {	// memory must be aligned.
			mem_free = 0;	mem_end = 0;	mem_orig = 0;		// invalid
			return;
		}
		*mem_orig |= 1;		// the origin allocator should not be used: mark as invalid
	}
	~LinAlloc() {	if (mem_orig) *mem_orig &= -2;  }		// mark origin LinAlloc to be usable again.
	#endif
	LinAlloc(const LinAlloc& mem) {
		mem_free = mem.mem_free;
		#ifdef XS_DEBUG
		mem_end = mem.mem_end;
		mem_orig = 0;
		#endif
	}
	void* alloc(size_t s) {
		char* p = mem_free;
		#ifdef XS_DEBUG
			if ((uintptr_t) p & 1) return 0;	// allocation forbidden by a child allocator.
			if (p+s > mem_end) return 0;		// not enough memory in block.
		#endif
		s = (s+MEMBLOCK_ALIGN-1) & -MEMBLOCK_ALIGN;		// align
		mem_free = p + s;
		return p;
	}
	double* alloc_double(size_t s) {
		return (double*) alloc(s*sizeof(double));
	}
	cplx* alloc_cplx(size_t s) {
		return (cplx*) alloc(s*sizeof(cplx));
	}
};

/// A memory block from which chunks can be allocated (linear allocator)
class MemBlock : public LinAlloc {
	char* mem_start = 0;	// pointer to the start of the memory
  public:
	void free_all() {
		uintptr_t addr = (((uintptr_t) mem_start) + (MEMBLOCK_ALIGN-1)) & -MEMBLOCK_ALIGN;		// align pointer
		mem_free = (char*) addr;
	}
	void reserve(size_t sze, unsigned nblocks=1) {
		sze += CACHE_LINE*nblocks;			// reserve padding for cache-line alignement for nblocks chunks.
		mem_start = (char*) shtns_malloc(sze);
		#ifdef XS_DEBUG
			mem_end = mem_start + sze;
		#endif
		free_all();
	}
	void discard() {  shtns_free(mem_start);	mem_start=0;	mem_free=0; }
	explicit MemBlock(size_t sze, unsigned nblocks=1) {  reserve(sze, nblocks);  }
	~MemBlock() { shtns_free(mem_start);  }
};


/// \name Finite difference operators.
//@{

/// A tri-diagonal matrix.
struct OpCurlLapl {
	double Wl,Wd,Wu,r_1;           // lower, diagonal, upper, 1/r
	double Ll,Ld,Lu,r_2;
};

struct OpGrad {
	double Gl,Gd,Gu,r_1;           // lower, diagonal, upper, 1/r
};

OpCurlLapl* CurlLapl_ = 0;
OpGrad* Grad_ = 0;

#define CURL_LAPL(i) CurlLapl_[i]
#define GRAD(i) Grad_[i]

void init_Deriv_sph(const double* r, int nr);	/// initialize derivative operators.

//@}

/*	Grid Generation with radial derivation operators.  */

#ifdef VAR_LTR
  #undef LTR

/// initializes variable l truncation. rsat=0 => no truncation.
void init_ltr(double rsat)
{
	if (ltr == NULL) {
		ltr = (unsigned short *) malloc( sizeof(unsigned short) * (NR+3) );		// allow some overflow.
		ltr += 1;	// ltr[-1] should be defined.
	}
	if (rsat<=0.0) {
		for (int ir=-1; ir<(NR+2); ir++) ltr[ir]=LMAX;
		return;
	}
	if (rsat > 1.) rsat=1.;
	rsat *= r[NR-1];
	PRINTF0("   variable l-truncation : ltr = lmax*sqrt(r/%.2f)  ",rsat);
	for (int ir=0; ir<NR; ir++) {
		int l = 1. + sqrt(r[ir]/rsat)*LMAX;
		if (l>LMAX) l=LMAX;
		ltr[ir] = l;
		#ifdef XS_DEBUG
			PRINTF0("%hu ",ltr[ir]);
		#endif
	}
	for (int ir=NR; ir<(NR+2); ir++)	ltr[ir] = ltr[NR-1];	// copy last value (ghost point and overflow)
	ltr[-1] = ltr[0];	// copy first value to -1 (ghost point).
	PRINTF0("\n");
}
#endif

/// grid with dr prop. to r^(1/3) : constant error as r->0.
void r0_Grid(double rmax, int n_end)
{
	double dr0, dr;

	dr0 = pow(rmax/n_end, 1.5);		// estimate
	r[0] = 0.0;
	dr = dr0;
	for (int i=1; i<=n_end; i++) {
		r[i] = r[i-1] + dr;
		dr = dr0*pow(r[i]/dr0, 1./3.);
	}

	dr = rmax/r[n_end];
	for (int i=1; i<n_end; i++) {		// rescale
		r[i] = r[i]*dr;
	}
	r[n_end] = rmax;
}

void Reg_Grid(double rmin, double rg, double rm, double rmax)		// Regular Grid
{
	double dx;
	long int i;

	if (rmin > rg) runerr("rmin > rg !");
	if (rg >= rm) runerr("rg >= rm !");
	if (rm > rmax) runerr("rm > rmax !");

	if (r == NULL) {	// if r not yet allocated, build a new radial grid.
		r = (double *) malloc(NR * sizeof(double));		// allocation of radial grid
	}

	NG = ceil( NR*(rg-rmin)/(rmax-rmin) );			// number of radial points in inner core.
	NM = NR-1 - ceil( NR*(rmax-rm)/(rmax-rmin) );	// number of radial points in conducting cmb.

	// inner core
	dx = (rg - rmin)/NG;
	for (i=0; i<NG; i++)
		r[i] = dx*i +rmin;
	r[NG] = rg;

	// outer core
	dx = (rm - rg)/(NM-NG);
	for (i=NG+1; i<NM; i++)
		r[i] = dx*(i-NG) +rg;
	r[NM] = rm;
		
	// mantle
	dx = (rmax - rm)/(NR-1-NM);
	for (i=NM+1; i<NR-1; i++)
		r[i] = dx*(i-NM) +rm;
	r[NR-1] = rmax;
}

/// The sum of the N first powers of the unknown x is y.
/// we assume x<1
double decre(int N,double y)
{
	double e,f,x;
	int i=1000;		// avoid infinite loop

	e=y/(y+1.);	f=N+1.;
	x=e;
	while(fabs(y+1-(1-pow(x,f))/(1-x))>0.0001) {
		x=e+pow(x,f)/(y+1);
		if (--i < 0) return 0;
	}
	return x;
}


/// Generate a grid with densification in Boundary Layers, based on code from D. Jault.
/// \param[in] rmin, rg, rmax are respectively the minimal, inner core and outer core radius.
/// \param[out] r[NR] is set with grid points (has to be allocated before function call)
/// \parmin[in] nin, nout are a number of points to be added to the inner and outer boundary layers.
///      NG is set with inner core index.
/// corresponds roughly to BL_Grid if nin >= nh*0.8 and nout <= nh*0.2
int BL_Grid(double rmin, double rg, double rm, double rmax, int nin, int nout)
{
	int nr1,nr2, nb,i,j;
	double e,q,hu,h;

	if (rmin > rg) runerr("rmin > rg !");
	if (rg >= rm) runerr("rg >= rm !");
	if (rm > rmax) runerr("rm > rmax !");
	if (nin+nout >= NR-1) runerr("nin+nout >= NR-1 !");

	#define non_fluid_points( n, size ) n*sqrt(size)

	nb = NR-1-(nin+nout);				// bulk pool
	e = log(nb);	h = 13.5*nb/(1.+e*e);		// points available for non-fluid layers.
	if (rg != rmin) {
		j = non_fluid_points( h, (rg-rmin)/(rmax-rmin) );
		NG = j + nin/4;		// inner core grid points
		nb -= j;		// substract from bulk pool
		nin -= nin/4;	// substract from bl pool
	} else NG = 0;
	if (rm != rmax) {
		j = non_fluid_points( h, (rmax-rm)/(rmax-rmin) );
		i =  j + nout/4;	// conducting mantle grid points
		NM = NR-1 -i;		// index of CMB
		nb -= j;			// substract from bulk pool
		nout -= nout/4;		// substract from bl pool
	} else NM = NR-1;
	nr1 = NG + (nb/4 + nin);		// 0.25 of bulk in inner BL
	nr2 = NM - (nb/4 + nout);		// 0.25 of bulk in outer BL
	if (rg == 0) nr1 = NG + (3*nb/20 + nin);		// don't concentrate without inner core.

	#undef non_fluid_points

//	printf("nr1=%d, nr2=%d\n",nr1,nr2);

	if ((nr1 >= nr2)||(nr1 <= NG)||(nr2 >= NM)) return 0;	// fail

	// each BL has thickness h = 0.15 of the gap
	// nr1 index of external shell of internal BL
	// nr2 index of internal shell of external BL
	h = (rm-rg)*0.15;
	hu=(rm-rg -2.*h)/(nr2-nr1);
	r[NG]=rg;		r[NM]=rm;
	r[nr1]=rg+h;	r[nr2]=rm-h;
	r[0]=rmin;		r[NR-1]=rmax;

	// Uniform grid in the bulk
	for(i=nr1+1; i<nr2; i++)
		r[i] = r[nr1] + (i-nr1)*hu;
//	printf("dr in the bulk = %.5e\n",hu);

	// Outer boundary layer
	q=decre(NM-nr2,h/hu);
	if (q==0) return 0;		// fail
	e=hu;
	for(i=nr2+1; i<NM; i++) {
		e*=q;
		r[i]=r[i-1]+e;
	}
//	printf("q outer bl = %f\n",1/q);

	// Inner boundary layer
	q=decre(nr1-NG,h/hu);
	if (q==0) return 0;		// fail
	e=hu;
	for(i=nr1-1; i>NG; i--) {
		e*=q;
		r[i]=r[i+1]-e;
	}
//	printf("q inner bl = %f\n",1/q);

	// Inner core
	if (NG > 0) {
		r[NG-1] = 2.*rg - r[NG+1];
		q = 1.09;	i=0;
		while (((e>(0.01/q))||(i==0))&&(q<1.3)) {		// try to limit step size at ir=0
			e = rg - r[NG-1];
			q += 0.01;
			for(i=NG-2; (i>0)&&(e<=(r[i+1]-rmin)/(i+1)); i--) {
				e*=q;
				r[i]=r[i+1]-e;
			}
		}
//		printf("q ic = %f\n",q);
		if (i==0) runerr("  !! Grid_BL : not enough points for good resolution in inner core\n");
		hu=(r[i+2]-rmin)/(i+2);
		for(j=0; j<=i+1; j++)
			r[j]=rmin + j*hu;
	}

	// Mantle
	if ( NM < (NR-1) ) {
		r[NM+1] = 2.*rm - r[NM-1];
		q = 1.02;	i=NR-1;
		while ((i==NR-1)&&(q<1.5)) {
			e = r[NM+1] - rm;
			q += 0.01;
			for (i=NM+2; (i<NR-1)&&(e <=(rmax-r[i-1])/(NR-i)); i++) {
				e*=q;
				r[i] = r[i-1] + e;
			}
		}
//		printf("q mantle = %f\n",q);
		if (i==NR-1) runerr("  !! Grid_BL : not enough points for good resolution in conducting mantle\n");
		hu=(rmax - r[i-2])/(NR-i+1);
		for(j=i-1; j<NR; j++)
			r[j]=rmax - (NR-1-j)*hu;
	}
	return 1;	// success.
}

/*
void Exp_Grid(double dx0, double alpha)		// Grille exponentielle.
{
	double dx;
	int i;

	r[0] = 0.0;
	dx = dx0;
	for (i=1;i<NR;i++)
	{
		dx = dx*alpha;
		r[i] = r[i-1] + dx;
	}

	printf("[GRID:Exp] NR=%d alpha=%f rmax=%f, dxmin=%f dxmax=%f\n",NR,alpha,r[NR-1],dx0,dx);
}

void Mixed_Grid(int Nreg, double rreg_max, double dxmax_mul)	// Génère une grille réguliere au centre, puis exponentielle.
{
	double dx, dxmax, alpha;
	int i, Nirr;

	if (Nreg > NR)	Nreg = NR;
// Grille : réguilière dans le coeur ...
	r[0] = 0.0;
	dx = rreg_max/(Nreg-1);
	for(i=1;i<Nreg;i++)
	{
		r[i] = r[0] + dx*i;
	}

// ... puis decroit exponentiellement, d'un facteur alpha.
	Nirr = (NR-1)-(Nreg+1);
	dxmax = dxmax_mul * log(Nirr);
	alpha = exp(log(dxmax/dx)/(double)Nirr);
	for (i=Nreg;i<NR;i++)
	{
		dx = dx*alpha;
		r[i] = r[i-1] + dx;
	}

	dxmax = dx;
	dx = r[1]-r[0];
	printf("[GRID:Mixed] NR=%d alpha=%f rmax=%f, dxmin=%f dxmax=%f\n",NR,alpha,r[NR-1],dx,dxmax);
}
*/

/// Generate Gradients, Laplaciens .... in spherical geometry.
void init_Deriv_sph(const double* r, int NR)
{
	// quick checks :
	if (r[0] < 0) runerr("Wrong grid: negative radius forbidden.");
	for (int i=1; i<NR; i++) {
		if (r[i] <= r[i-1]) runerr("Wrong grid: radii are not strictly increasing.");
	}

	#ifdef XS_DEBUG
		if (r_1) printf("  *** WARNING: init_Deriv_sph() was already called before.\n");
	#endif
	free(r_1);		// free previously allocated global data (NR may have changed !)

	// allocate global variables:
	r_1 = (double*) malloc(NR* sizeof(double));
	{	int i=0;
		if (r[0] == 0.) { r_1[0] = 0.;  i=1;    }
		for (; i<NR; i++) r_1[i] = 1.0/r[i];
	}

	VFREE(CurlLapl_);		// free any previously allocated derivatives.
	int nb = sizeof(OpCurlLapl) + sizeof(OpGrad);
	CurlLapl_ = (OpCurlLapl*) VMALLOC(NR * nb);
	memset(CurlLapl_,  0, NR * nb);		// all zeros
	Grad_ = (OpGrad*) (CurlLapl_ + NR);

	if (r[0] > 0.0) {
		Grad_[0].r_1 = r_1[0];
		CurlLapl_[0].r_1 = r_1[0];
		CurlLapl_[0].r_2 = 1./(r[0]*r[0]);
	}
	// Gradient and Laplace operators (second order)
	for (int i=1;i<NR-1;i++)
	{
		OpGrad& G = Grad_[i];
		OpCurlLapl& WL = CurlLapl_[i];
		fd_deriv_o2(r,i, &G.Gl, &WL.Ll);		// Gl,Gd,Gu is filled with the Gradient, Ll,Ld,Lu with the second order derivative operator.

		double ri_1 = r_1[i];
		G.r_1 = ri_1;
		WL.Wl = G.Gl;
		WL.Wd = G.Gd + ri_1;
		WL.Wu = G.Gu;
		WL.r_1 = ri_1;
		WL.Ll += 2.0*ri_1*G.Gl;	// Laplacien radial : d2/dr2 + 2/r.d/r
		WL.Ld += 2.0*ri_1*G.Gd;	// several definitions exist for regular grid.
		WL.Lu += 2.0*ri_1*G.Gu;	// this one seems a little better close to r=0.
		WL.r_2 = 1./(r[i]*r[i]);
	}
	int i = NR-1;
		CurlLapl_[i].r_1 = r_1[i];
		CurlLapl_[i].r_2 = 1./(r[i]*r[i]);
		Grad_[i].r_1 = r_1[i];
	// start and end operators must be determined by BC on a case by case basis.
}

/*
// Gradients, Laplaciens .... en cylindrique.
void init_Deriv_cyl()
{
	double t, rm, rp, grm, grp;
	int i;

	// creation des variables globales :
	r_1 = (double *) malloc(NR * sizeof(double));
	r_2 = (double *) malloc(NR * sizeof(double));
	dr = (double *) malloc(NR * sizeof(double));
	
	Gr = (struct TriDiag *) malloc(NR * sizeof(struct TriDiag));
	Wr = (struct TriDiag *) malloc(NR * sizeof(struct TriDiag));
	Lr = (struct TriDiag *) malloc(NR * sizeof(struct TriDiag));
	
	for(i=1;i<NR;i++)
	{
		r_1[i] = 1./r[i];
		r_2[i] = 1./(r[i]*r[i]);
	}
	i = 0;
		r_1[i] = 0.0;	r_2[i] = 0.0;

	// increments
	for(i=0;i<NR-1;i++)
		dr[i] = r[i+1]-r[i];
	dr[NR-1] = 0.0;

	// gradient et Laplacien
	for (i=1;i<NR-1;i++)
	{
		t = 1.0/((dr[i-1]+dr[i])*dr[i]*dr[i-1]);
		Gr[i].l = -dr[i]*dr[i]*t;
		Gr[i].d = (dr[i]*dr[i] - dr[i-1]*dr[i-1])*t;	// =0 en grille reguliere.
		Gr[i].u = dr[i-1]*dr[i-1]*t;

		Wr[i].l = r_1[i]* Gr[i].l * r[i-1];	// Wr = 1/r * d/dr(r .), pour la vorticite m=0 !
		Wr[i].d = Gr[i].d;
		Wr[i].u = r_1[i]* Gr[i].u * r[i+1];

		Lr[i].l =  2.0*dr[i]*t            + r_1[i]*Gr[i].l;
		Lr[i].d =  -2.0*(dr[i-1]+dr[i])*t + r_1[i]*Gr[i].d;
		Lr[i].u =  2.0*dr[i-1]*t          + r_1[i]*Gr[i].u;
	}
	// les extremites douvent etre déterminées par les CL au cas par cas.
	i = 0;
		Gr[i].l = 0.0; Gr[i].d = 0.0; Gr[i].u = 0.0;
		Wr[i].l = 0.0;	Wr[i].d = 0.0;	Wr[i].u = 0.0;
		Lr[i].l = 0.0;	Lr[i].d = 0.0;	Lr[i].u = 0.0;
	i = NR-1;
		Gr[i].l = 0.0; Gr[i].d = 0.0; Gr[i].u = 0.0;
		Wr[i].l = 0.0;	Wr[i].d = 0.0;	Wr[i].u = 0.0;
		Lr[i].l = 0.0;	Lr[i].d = 0.0;	Lr[i].u = 0.0;

}
*/

/// finds the index to the shell with closest radius to radius rr
long int r_to_idx(double rr)
{
	long int i;

	i = NR-2;
	while((r[i] > rr)&&(i>0)) i--;
	if ((rr-r[i]) > (r[i+1]-rr)) i++;
	return i;	// i is always between 0 and NR-1
}

/// finds the index of latitudinal grid point with closest colatitude to theta.
long int theta_to_idx(double theta)
{
	int i;
	double ct0 = cos(theta);
	i = NLAT-2;
	while((ct[i] < ct0)&&(i>0)) i--;
	if ((theta-acos(ct[i])) > (acos(ct[i+1])-theta)) i++;
	return i;
}

/// Build the radial grid, and matrices of spatial derivatives.
void init_rad_sph(double rmin, double Ric, double Rcmb, double rmax, int Nin, int Nout)
{
	if (r == NULL) {	// if r not yet allocated, build a new radial grid.
		r = (double *) malloc(NR * sizeof(double));		// allocation of radial grid
#ifdef XS_PENALIZE
		Reg_Grid(rmin, Ric, Rcmb, rmax);
#else
		int success = 0;
		if (NR>=48)		success = BL_Grid(rmin, Ric, Rcmb, rmax, Nin, Nout);
		if (!success)	{
			PRINTF0("[Grid] Cannot compute boundary layer grid, switching to regular grid.\n");
			Reg_Grid(rmin, Ric, Rcmb, rmax);
		}
#endif
	} else {
		NG = r_to_idx(Ric);
		NM = r_to_idx(Rcmb);
	}
	init_Deriv_sph(r, NR);	// build derivative matrices
}

#ifdef XS_MPI
#if MPI_VERSION >= 3
#include <vector>
std::vector<MPI_Win> win_list;
#endif
#endif

/// an OpenMP and/or an MPI-3 shared-memory barrier
inline void barrier_shared_mem()
{
	#ifdef XS_MPI
	if (n_mpi_shared > 1) {
		#pragma omp barrier
		#pragma omp master
		{
			MPI_Barrier(comm_shared);
			//for (int i=0; i<win_list.size(); i++)	MPI_Win_sync(win_list[i]);		// MPI_Win_sync sometimes crashes and/or degrades performance.
		}
	}
	#endif
	#pragma omp barrier
}

#if XS_MPI
/// returns the number of processes that will share memory (1 == no shared_memory)
int init_mpi_shared_mem(int shared_mem_max = 0)
{
	if (comm_shared != MPI_COMM_SELF)	MPI_Comm_free(&comm_shared);
	if (comm_net != MPI_COMM_WORLD)		MPI_Comm_free(&comm_net);
	comm_shared = MPI_COMM_SELF;
	comm_net = MPI_COMM_WORLD;
	n_mpi_shared = 1;		i_mpi_shared = 0;
	n_mpi_net = n_mpi;		i_mpi_net = i_mpi;
	irs_shared = irs_mpi;
	ire_shared = ire_mpi;

#if MPI_VERSION >= 3
	#if defined( __SSE2__ )
		// by default, on x86 machines (full cache coherency needed): enable shared memory up to half the number of total "cores".
		if (shared_mem_max == 0  &&  n_mpi >= 4) shared_mem_max = n_mpi*nthreads/2;
	#endif
	if (shared_mem_max == 0) shared_mem_max = 1;		// by default: disable shared memory.
	const int cl = CACHE_LINE/sizeof(cplx);
	if ((shared_mem_max < 0) || (shared_mem_max*cl > NLM+cl-1))	shared_mem_max = (NLM+cl-1)/cl;		// the number of cores sharing memory cannot exceed the number of cache lines for NLM.
	if (shared_mem_max >= 2*nthreads) {
		// Find out if some processes can share memory using MPI-3:
		MPI_Comm_split_type(MPI_COMM_WORLD, MPI_COMM_TYPE_SHARED, i_mpi, MPI_INFO_NULL, &comm_shared);
		MPI_Comm_rank(comm_shared, &i_mpi_shared);
		MPI_Comm_size(comm_shared, &n_mpi_shared);
		if ((shared_mem_max > 0) && (shared_mem_max < n_mpi_shared*nthreads)) {		// limit the number of processes sharing memory
			shared_mem_max /= nthreads;		// work in terms of processes, instead of cores.
			while (n_mpi_shared % shared_mem_max) shared_mem_max --;		// find a divisor
			if (shared_mem_max > 1) {		// we can split it to enforce the constraint !
				MPI_Comm comm_old = comm_shared;
				MPI_Comm_split(comm_old, i_mpi_shared/shared_mem_max, i_mpi, &comm_shared);
				MPI_Comm_free(&comm_old);
				MPI_Comm_rank(comm_shared, &i_mpi_shared);
				MPI_Comm_size(comm_shared, &n_mpi_shared);
				if (shared_mem_max < n_mpi_shared) runerr("STUPID ERROR!");
			}	// otherwise shared_mem_max = 1, which will disable shared mem in the following.
		}

		// For the shared solver to work, we must ensure n_mpi_shared is the same for everyone:
		int* n_all = (int*) malloc(sizeof(int) * n_mpi);
		MPI_Allgather(&n_mpi_shared, 1, MPI_INT, n_all, 1, MPI_INT, MPI_COMM_WORLD);
		for (int i=0; i<n_mpi; i++) {
			if (n_all[i] != n_mpi_shared) shared_mem_max = 1;	// shared memory won't work.
		}
		free(n_all);

		if (shared_mem_max == 1) {		// disable shared memory between processes.
			MPI_Comm_free(&comm_shared);
			comm_shared = MPI_COMM_SELF;
			n_mpi_shared = 1;		i_mpi_shared = 0;
		} else {
			// Find out which shells can be shared:
			MPI_Group grp_shared, grp_world;
			MPI_Comm_group(MPI_COMM_WORLD, &grp_world);
			MPI_Comm_group(comm_shared, &grp_shared);

			// r_shared: rank of owner in sharing group if it is sharable with me, otherwise MPI_UNDEFINED.
			int* r_shared = (int*) malloc((NR+2) * sizeof(int));	r_shared++;
			MPI_Group_translate_ranks(grp_world, NR+2, r_owner-1, grp_shared, r_shared-1);
			irs_shared = 0;		ire_shared = NR-1;
			while (r_shared[irs_shared] == MPI_UNDEFINED) irs_shared++;		// set shared limits.
			while (r_shared[ire_shared] == MPI_UNDEFINED) ire_shared--;

			free(r_shared-1);
			MPI_Group_free(&grp_shared);
			MPI_Group_free(&grp_world);

			MPI_Comm_split(MPI_COMM_WORLD, i_mpi_shared, i_mpi, &comm_net);		// communicator for inter-node communications with same i_mpi_shared rank.
			MPI_Comm_rank(comm_net, &i_mpi_net);
			MPI_Comm_size(comm_net, &n_mpi_net);
		}
	}
#endif
	return n_mpi_shared;
}


/// shared_mem: disable MPI-3 shared memory if set to 0 or 1; -1 will select all processes sharing a node;
/// other values define a maximum number of sharing cores.
void distribute_shells(int U_ir_bci, int U_ir_bco, int shared_mem=0)
{
	if (r_owner == 0) {
		r_owner = (int *) malloc((NR+2) * sizeof(int));
		r_owner++;		// shift pointer, to allow ghost shell to be [-1] and [NR].
	}
	r_owner[-1] = 0;			// ghost shell, owned by 0.
	r_owner[NR] = n_mpi-1;		// outer ghost shell, owned by last process.

	int nfluid = U_ir_bco - U_ir_bci +1;	// number of fluid shells (which require much more work)
	if ((nfluid == NR) || (NR == n_mpi)) {		// no worries
		for (int i=0; i<NR; i++)
			r_owner[i] = i*n_mpi/NR;	// set shell owner.
	} else {		// try some crude load-balancing:
		int nf_proc = (nfluid+n_mpi-1)/n_mpi;	// number of fluid shells per process (round up)
		PRINTF0("[distribute_shells] %d fluid shells per process.\n", nf_proc);
		int n_mpi_fluid = (nfluid+nf_proc-1)/nf_proc;
		int n_other = NR-nfluid;
		int n_mpi_left = (n_mpi - n_mpi_fluid) * ((U_ir_bci+n_other/2)/n_other);
		int n_mpi_right = n_mpi - n_mpi_fluid - n_mpi_left;
		for (int i=0; i<U_ir_bci; i++)
			r_owner[i] = i*n_mpi_left/U_ir_bci;				// assign processes to solid inner shells
		for (int i=0; i<nfluid; i++)
			r_owner[i+U_ir_bci] = i/nf_proc + n_mpi_left;	// assign processes to fluid shells
		int n_right = NR-1-U_ir_bco;
		for (int i=0; i<n_right; i++)
			r_owner[NR-1-i] = n_mpi-1 - i*n_mpi_right/n_right;		// assign processes to solid outer shells
	}
	// check if the partitioning is valid:
	int fail = 0;
	if ((r_owner[-1] != 0) || (r_owner[0] !=0)) fail++;
	for (int i=1; i<NR; i++) {
		if ((r_owner[i] < r_owner[i-1]) || (r_owner[i] > r_owner[i-1]+1)) fail ++;
	}
	if ((r_owner[NR] != n_mpi-1) || (r_owner[NR-1] != n_mpi-1)) fail++;
	if (fail) runerr("[distribute_shells] Shell partitioning to MPI processes failed.\n");

	irs_mpi = 0;		ire_mpi = NR-1;
	while (r_owner[irs_mpi] < i_mpi) irs_mpi++;		// set local limits
	while (r_owner[ire_mpi] > i_mpi) ire_mpi--;
	if (ire_mpi - irs_mpi < 0) runerr("[distribute_shells] at least 1 shell per process required.");

	// try to enable MPI-3 shared memory
	init_mpi_shared_mem(shared_mem);

	if ((i_mpi == 0) && (n_mpi_shared > 1)) printf(COLOR_WRN "[distribute_shells] using MPI-3 shared memory within groups of %d process (%d cores)." COLOR_END "\n", n_mpi_shared, n_mpi_shared*nthreads);
	#ifdef XS_DEBUG
		MPI_Barrier(MPI_COMM_WORLD);
		printf("mpi process %d handles %d shells (%d to %d) and shares %d shells (%d to %d) within a group of %d\n", i_mpi, ire_mpi-irs_mpi+1, irs_mpi, ire_mpi, ire_shared-irs_shared+1, irs_shared, ire_shared, n_mpi_shared);
		MPI_Barrier(MPI_COMM_WORLD);
	#endif
}
#endif

inline double phi_deg(int ip) {
	return (360./(NPHI*MRES))*ip;
}

inline double phi_rad(int ip) {
	return (M_PI/(NPHI*MRES))*(2*ip);
}

/// initializes SH without grid (default norm is orthonormal.
void init_sh_vars(int norm=sht_orthonormal)
{
	if (i_mpi == 0) shtns_verbose(1);		// only root process speaks.
	sht_norm = (shtns_norm) norm;		// set and save norm
	NLM = nlm_calc(LMAX, MMAX, MRES);
	shtns = shtns_create(LMAX, MMAX, MRES, sht_norm);
	li = shtns->li;

	const long cl = CACHE_LINE/sizeof(double);
	const long nlm_aligned = ((NLM+cl-1)/cl)*cl;		// round to cache line.
	l2 = (double*) VMALLOC(sizeof(double) * 6*nlm_aligned);
	if (l2==0) runerr("[init_sh_vars] allocation error");
	l_2 = l2 + nlm_aligned;		el = l2 + 2*nlm_aligned;	em = l2 + 3*nlm_aligned;
	st_dt_mx = l2 + 4*nlm_aligned;	// 2*NLM doubles
	for (int lm=0; lm<NLM; lm++) {
		unsigned l = li[lm];
		el[lm] = l;
		l2[lm] = l*(l+1);
		l_2[lm] = (l==0) ? 0.0 : 1.0/l2[lm];		// avoid division by 0 !
		em[lm] = shtns->mi[lm];
	}
	for (int lm=NLM; lm<nlm_aligned; lm++) {
		el[lm] = 0;		l2[lm] = 0;		l_2[lm] = 0;	em[lm] = 0;
	}
	st_dt_matrix(shtns, st_dt_mx);
}

/// initializes SH
void init_sh(int sht_type, double polar_opt_max, int nl_order)
{
	shtns_use_threads( (XS_OMP==2) ? 0 : 1 );		// use multi-threaded SHTns ?
	if (shtns == NULL) init_sh_vars();

	sht_type |= SHT_LOAD_SAVE_CFG;		// load and save SHTns + fftw config
	#if defined( SHT_ALLOW_PADDING ) && !defined( XS_LINEAR )
	sht_type |= SHT_ALLOW_PADDING;		// this SHTns v3.4 option leads to sensible performance improvements.
	#endif
	#ifdef XS_MPI
	if (i_mpi > 0) MPI_Barrier(MPI_COMM_WORLD);    // all processes except 0 wait here until process 0 has initialized shtns.
	#endif
	shtns_set_grid_auto(shtns, (shtns_type) sht_type, polar_opt_max, nl_order, &NLAT, &NPHI);    // this will save the config to a file on process 0 and load from file on other processes.
	#ifdef XS_MPI
	if (i_mpi == 0) MPI_Barrier(MPI_COMM_WORLD);   // other processes now resume and load the config from file.
	#endif

	#ifdef SHT_ALLOW_PADDING
	NLAT_PADDED = shtns->nlat_padded;		// support padded spatial layout
	#else
	NLAT_PADDED = NLAT;				// also support SHTns prior to v3.4
	#endif
	if (NLAT % VSIZE) runerr("Nlat must be a multiple of vector size.");
	if (i_mpi==0) shtns_print_cfg(shtns);
	ct = shtns->ct;		st = shtns->st;
	double* wg2 = (double*) malloc(NLAT*sizeof(double));
	shtns_gauss_wts(shtns, wg2);		// get gauss weights.
	for (int it=0; it<NLAT/2; it++)  wg2[NLAT-1-it] = wg2[it];		// gauss weights are symmetric.
	wg = wg2;		// set the read-only global pointer to gauss weigths.
}
